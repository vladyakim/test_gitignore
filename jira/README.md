# Module overview #

Soul purpose of **psoeng-jira** module is to ensure successful migration of JIRA configuration and data from one instance to another with least disruption of end-user experience as possible.

Below the general process and most common migration scenario is described in details.

## JIRA migration steps in details ##

*  **Get snapshots** Configuration Manager for JIRA (CMJ for short) snapshots are used not only for configuration and data move between instances but are also mandatory input for AMS generation and further migration discussions with customer.
    *  Once setup is done (`Env.java` and `migration.properties` and/or `local.migration.properties` updated accordingly),  snapshots from SOURCE and TARGET instances must be taken as those are the input for AMS generation and transformations discussions with Customer.
    *  Snapshot creation executes as JUnit tests that are available in:
`com.botronsoft.pso.migrations.jira.snapshot.create.SnapshotCreator`  
**NB!!!** snapshot creation in tests is designed to include all projects, filters, boards and dashboards. For reduced scope (exclusion of SOURCE projects, filters, agile boards and/or dashboards), changes must be applied accordingly.

    *  Snapshot creation can be also done via CMJ UI. CMJ documentation is available **[here]("https://botronsoft.atlassian.net/wiki/spaces/CMJ/overview?homepageId=4423686")**.


*  **Generate AMS**
AMS document layout and content might defer from engagement to engagement due to instance and/or Customer specifics. Still, any previous JIRA AMS can be used as a starting point for preparing current engagement AMS and modify it according to the needs. Common addendum to the AMS is the Appendix section that list in details all conflicting and non-conflicting item, how conflict are handled and in general provide overview what the impact on TARGET instance will be.
    * `com.botronsoft.pso.migrations.jira.ams.AMSGenerator` creates AMS appendix details with all new and conflicting items. Refer class java doc for further details
    
    
*  **User management**
Important step going further with actual migration execution is to agree how missing and/or conflicting users will be handled. Following must be done prior migration:
    * Export all SOURCE users to get list of those owning data, activity or setting within JIRA `com.botronsoft.pso.migrations.jira.ams.UserManagement.generateSourceUserReport()`
    * Export all TARGET users to get list of those owning data, activity or setting within JIRA `com.botronsoft.pso.migrations.jira.ams.UserManagement.generateTargetUserReport()`
    * Go through matching usernames whether those are same people or not (display name, email from export might be of a help doing so)
    * In case of different SOURCE/TARGET usernames but actually same people mapping must be confirmed
    * Agree on group membership whether source membership is preserved and should be applied on target
  
Goal is to come up with mapping of SOURCE to TARGET user and whether SOURCE users are to be created active or inactive. 


*  **Apply transformations** 
Having AMS approved and user management approach agreed we proceed with applying SOURCE snapshot transformations to align SOURCE data and configuration with approved AMS.
	`com.botronsoft.pso.migrations.jira.snapshot.transform` package holds classes used to apply snapshot transformations to comply with AMS described and agreed transformations. Transformation samples are available in [Transformation API Demo repository](https://bitbucket.org/botronsoft/cmj-api-demo/src/master/).


*  **Migrate SOURCE**
    * **Create users**  
All SOURCE users who own data, activity or reference must be created on TARGET prior deploy. Use `com.botronsoft.pso.migrations.jira.ams.UserManagement.createUsers()`. Go through class/method javadoc for further details.
    * **JIRA SOURCE configuration and data**	  
Once transformed snapshot is available it is to be deployed in TARGET JIRA. For Configuration Manager for JIRA instructions deploying a snapshot please refer **[CMJ documentation on Deployment](https://botronsoft.atlassian.net/wiki/spaces/CMJ/pages/4784143/Snapshot+Deployment)**.
Important steps to take into consideration:
        - Prior deploy!!! CMJ **MUST** be set to store a deployment mapping file. Go to **Configuration Manager** -> **Settings** -> check **Persist deployed snapshot to current system native ID mappings**
        - As general rule, doing migration on actual deployment common set of settings we are going with is: **Do not merge Object Descriptions**, **Do not merge changes to Avatars**, **Do not perform re-indexing during the deployment** from CMJ deployment options

    * **JIRA configuration not handled by CMJ**
                + _Filter and Dashboard Favorites_ see `com.botronsoft.pso.migrations.jira.migration.jira.config.FilterDashboardFavorites`
         + _User recently used items_ see `yet to be developed`
    * **Third party add-on configuration and data** 
    		see package `com.botronsoft.pso.migrations.jira.migration.apps`
    * **Update URLs in data** 
    		Once data is migrated over any URLs pointing to SOURCE JIRA data must be updated to point to migrated data on TARGET. Mapping files must be generated. See `com.botronsoft.pso.migrations.jira.migration.links.LinkUpdaterMappingGenerator`
    		**Mandatory** step is having CMJ Deployment mapping file `deploymentSnapshotToTargetNativeIdMap_....json` from data snapshot deployment available in the folder used with the java class above. 
            For the actual link update, there is REST endpoint `jmt/1.0/linksupdater/update`

	 
*  **Run tests** 
**NB!!!** CMJ Deployment mapping along with user, project mapping must be available in proper resource files prior test execution.
Upon successful deploy of configuration and data **psoeng-tests** module under package `com.botronsoft.pso.migrations.tests.jira` holds test to verify successful migration.
    * _SanityTestProjects.java_ compares project details such as number of issues, comments, attachments, issue links and worklogs whether matching on SOURCE and TARGET
    * _AgileBoards.java_ test Agile board details
    * _IssueAttributes.java_ test set of issue attributes on SOURCE and TARGET


*  **Provide Customer with migration reports** 
   Set of reports that are provided to customer to help end-users navigate through and validate migrated data  easier
    * _FilterAgileBoardDashboardMappingReport.java_ compares project details such as number of issues, comments, attachments, issue links and worklogs whether matching on SOURCE and TARGET	
    

*  **Useful migration scripts** 
	`com.botronsoft.pso.migrations.jira.scripts.remote` holds set of scripts that can be helpful during migration process. E.g. `InstanceIntegrityChecker.groovy` that handles most common JIRA integrity errors preventing user of creating CMJ snapshot.
 
