package com.botronsoft.pso.migrations.jira.ams;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.botronsoft.jira.rollout.model.jiraconfiguration.JiraBaseObject;
import com.botronsoft.jira.rollout.model.jiraconfiguration.JiraConfigurationRoot;
import com.botronsoft.jira.rollout.model.jiraconfiguration.NamedNode;
import com.botronsoft.jira.rollout.model.jiraconfiguration.field.JiraCustomField;
import com.botronsoft.jira.rollout.model.jiraconfiguration.field.JiraFieldLayout;
import com.botronsoft.jira.rollout.model.jiraconfiguration.field.JiraFieldLayoutScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssueLinkType;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssuePriority;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssuePriorityScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssueResolution;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssueStatus;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssueType;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssueTypeScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.project.JiraProject;
import com.botronsoft.jira.rollout.model.jiraconfiguration.project.JiraProjectCategory;
import com.botronsoft.jira.rollout.model.jiraconfiguration.scheme.JiraEvent;
import com.botronsoft.jira.rollout.model.jiraconfiguration.scheme.JiraIssueSecurityScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.scheme.JiraNotificationScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.scheme.JiraPermissionScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.screen.JiraFieldScreen;
import com.botronsoft.jira.rollout.model.jiraconfiguration.screen.JiraFieldScreenScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.screen.JiraIssueTypeScreenScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.user.JiraGroup;
import com.botronsoft.jira.rollout.model.jiraconfiguration.user.JiraRole;
import com.botronsoft.jira.rollout.model.jiraconfiguration.workflow.JiraWorkflow;
import com.botronsoft.jira.rollout.model.jiraconfiguration.workflow.JiraWorkflowScheme;
import com.botronsoft.pso.cmj.modelprocessor.api.snapshot.CMJSnapshot;
import com.botronsoft.pso.cmj.modelprocessor.api.snapshot.SnapshotStorageService;
import com.botronsoft.pso.cmj.modelprocessor.di.ModelProcessorModule;
import com.botronsoft.pso.cmj.modelprocessor.impl.DefaultCMJSnapshot;
import com.botronsoft.pso.migration.framework.snapshot.diff.ComparisonMatch;
import com.botronsoft.pso.migration.framework.snapshot.diff.SnapshotComparison;
import com.botronsoft.pso.migration.framework.snapshot.diff.impl.DefaultSnapshotComparator;
import com.botronsoft.pso.migrations.Env;
import com.google.common.collect.Lists;
import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * AMS appendix section generator.
 * <p>
 * Set class constants as appropriate:
 * <li>{@link #SNAPSHOTS_FOLDER} - folder where snapshots are available. Depends on <b>migration.properties</b> setup.
 * <li>{@link #SOURCE_SNAPSHOT} - Name of SOURCE snapshot. Must be available in {@link #SNAPSHOTS_FOLDER}
 * <li>{@link #TARGET_SNAPSHOT} - Name of TARGET snapshot. Must be available in {@link #SNAPSHOTS_FOLDER}
 */

// TODO: Add item sorting by name
public class AMSGenerator extends JiraAbstractAMSGenerator {

	private static final File SNAPSHOTS_FOLDER = Env.LOCAL_ENV.workfile("Snapshots");
	private static final String SOURCE_SNAPSHOT = "Source_projects_config.zip";
	private static final String TARGET_SNAPSHOT = "Target_system.zip";

	public AMSGenerator(SnapshotComparison snapshotComparison) {
		super(snapshotComparison);
	}

	@Override
	protected String prefix(String name) {
		String commonPefix = Env.SOURCE_JIRA_ENV.getRenamePrefix();
		return commonPefix + name;
	}

	void generateProjects() {
		MARKUP_BUILDER.sectionTitleLevel1("Projects");
		//
		generateAdditionsTable(JiraProject.class, columns("Project Key", "Project Name"), (p) -> {
			return row(p.getKey(), p.getName());
		});
		//
		MARKUP_BUILDER.sectionTitleLevel2("Conflicting Project keys");
		// TODO: Handle all cases
		generateMatchesTable(JiraProject.class,
				columns("Source Project Name", "Source Project Key", "Conflicting Reason", "Conflict Resolution"), (match) -> {
					JiraProject p = match.getLeft();
					// FIXME:
					return row(p.getName(), p.getKey(), "KEY", "N/A");
				});

		//
		MARKUP_BUILDER.sectionTitleLevel2("Conflicting Project names");
		ComparisonMatch<JiraConfigurationRoot> root = diff.getRoot();
		Map<String, List<JiraProject>> grouped = root.getRight().getProjects().stream().collect(groupingBy(p -> p.getName().toLowerCase()));
		List<List<String>> cells = root.getLeft().getProjects().stream().filter(p -> grouped.containsKey(p.getName().toLowerCase()))
				.map(p -> {
					JiraProject targetProject = grouped.get(p.getName().toLowerCase()).get(0);
					return row(p.getName(), p.getKey(), "Target Project With Key " + targetProject.getKey(), "N/A");
				}).collect(toList());
		if (cells.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
		} else {
			MARKUP_BUILDER.tableWithColumnSpecs(
					columns("Source Project Name", "Source Project Key", "Conflicting Reason", "Conflict Resolution"), cells);
		}

	}

	void generateFields() {
		MARKUP_BUILDER.sectionTitleLevel1("Custom Fields");
		//
		MARKUP_BUILDER.sectionTitleLevel2("New Fields");
		generateAdditionsTable(JiraCustomField.class, columns("ID", "Name", "Type"), (c) -> {
			return row(c.getNativeId(), c.getName(), c.getTypeId());
		});
		MARKUP_BUILDER.sectionTitleLevel2("Conflicting Fields");
		generateMatchesTable(JiraCustomField.class, columns("Source ID", "Target ID", "Name", "Type"), (c) -> {
			return row(c.getLeft().getNativeId(), c.getRight().getNativeId(), c.getLeft().getName(), c.getLeft().getTypeId());
		});
	}

	void generateCategories() {
		MARKUP_BUILDER.sectionTitleLevel1("Project Categories");
		statusTable(columns("Source Project Category", "Status"), JiraProjectCategory.class);
	}

	void generateGroups() {
		MARKUP_BUILDER.sectionTitleLevel1("Groups");
		statusTable(columns("Source Group", "Status"), JiraGroup.class);
	}

	void generateRoles() {
		MARKUP_BUILDER.sectionTitleLevel1("Project Roles");
		statusTable(columns("Source Project Role", "Status"), JiraRole.class);
	}

	void generateIssueTypes() {
		MARKUP_BUILDER.sectionTitleLevel1("Issue Types");
		statusTable(columns("Source Issue Type", "Status"), JiraIssueType.class);
	}

	void generateStatuses() {
		MARKUP_BUILDER.sectionTitleLevel1("Statuses");
		statusTable(columns("Source Status", "Status"), JiraIssueStatus.class);
	}

	void generateResolutions() {
		MARKUP_BUILDER.sectionTitleLevel1("Resolutions");
		statusTable(columns("Source Resolution", "Status"), JiraIssueResolution.class);
	}

	void generatePriorities() {
		MARKUP_BUILDER.sectionTitleLevel1("Priorities");
		statusTable(columns("Source Priority", "Status"), JiraIssuePriority.class);
	}

	void generateIssueLinks() {
		MARKUP_BUILDER.sectionTitleLevel1("Issue Links");
		statusTable(columns("Source Issue Link", "Status"), JiraIssueLinkType.class);
	}

	void generateWorkflows() {
		MARKUP_BUILDER.sectionTitleLevel1("Workflows");
		MARKUP_BUILDER.paragraph("In case SOURCE workflow name conflicts with TARGET workflow name,\n" + "\n"
				+ "new prefixed name is available in Target Workflow Name column.");
		//
		generateCombinedTable(JiraWorkflow.class, columns("Source Workflow Name", "Target Workflow Name"), (m) -> {
			return row(m.getLeft().getName(), prefix(m.getLeft().getName()));
		}, (c) -> {
			return row(c.getName(), " - ");
		});
	}

	void generateScreens() {
		MARKUP_BUILDER.sectionTitleLevel1("Screens");
		MARKUP_BUILDER.paragraph("In case SOURCE screen name conflicts with TARGET screen name,\n" + "\n"
				+ "new prefixed name is available in Target Screen Name column.");
		//
		generateCombinedTable(JiraFieldScreen.class, columns("Source Screen Name", "Target Screen Name"), (m) -> {
			return row(m.getLeft().getName(), prefix(m.getLeft().getName()));
		}, (c) -> {
			return row(c.getName(), " - ");
		});
	}

	void generateFieldConfigurationsAndSchemes() {
		MARKUP_BUILDER.sectionTitleLevel1("Field Configurations and Schemes");
		MARKUP_BUILDER.paragraph("In case SOURCE field configuration or scheme name conflicts with TARGET name,\n" + "\n"
				+ "new prefixed name is available in Target Name column.");
		List<Class<? extends JiraBaseObject>> list = Lists.newArrayList(JiraIssueTypeScheme.class, JiraWorkflowScheme.class,
				JiraIssuePriorityScheme.class, JiraNotificationScheme.class, JiraPermissionScheme.class, JiraIssueTypeScreenScheme.class,
				JiraFieldScreenScheme.class, JiraIssueSecurityScheme.class, JiraFieldLayoutScheme.class, JiraFieldLayout.class);
		//
		generateCombinedMultiTable(list, columns("Source Name", "Target Name", "Type"), (m) -> {
			NamedNode left = (NamedNode) m.getLeft();
			return row(left.getName(), prefix(left.getName()), getTypeDisplay(left.eClass()));
		}, (c) -> {
			NamedNode object = (NamedNode) c;
			return row(object.getName(), " - ", getTypeDisplay(object.eClass()));
		});
	}

	void generateEvents() {
		MARKUP_BUILDER.sectionTitleLevel1("Events");
		statusTable(columns("Source Event", "Status"), JiraEvent.class);
	}

	public void generate() {
		macroToc();
		generateProjects();
		generateCategories();
		generateRoles();
		generateFields();
		generateIssueTypes();
		generateStatuses();
		generateResolutions();
		generatePriorities();
		generateIssueLinks();
		//
		generateWorkflows();
		generateScreens();
		//
		generateFieldConfigurationsAndSchemes();
		generateEvents();
		generateGroups();

		System.out.println(MARKUP_BUILDER.toString());
	}

	/**
	 * Sample helper method for converting a snapshot with issues to snapshot without issues.
	 *
	 * @param snapshot
	 */
	public static void dropDataFromSnapshot(File snapshot) {
		Injector injector = Guice.createInjector(new ModelProcessorModule());
		SnapshotStorageService storageService = injector.getInstance(SnapshotStorageService.class);

		CMJSnapshot sourceSnapshot = storageService.loadSnapshot(snapshot);
		((DefaultCMJSnapshot) sourceSnapshot).dropDataRoot();
		storageService.storeSnapshotTransformed(sourceSnapshot);
	}

	public static void main(String[] args) {
		File basedir = SNAPSHOTS_FOLDER;
		File source = new File(basedir, SOURCE_SNAPSHOT);

		// used in case project snapshot with issues is used for AMS generation
		dropDataFromSnapshot(source);

		File target = new File(basedir, TARGET_SNAPSHOT);
		DefaultSnapshotComparator comparator = new DefaultSnapshotComparator();
		SnapshotComparison snapshotComparison = comparator.compare(source, target);
		new AMSGenerator(snapshotComparison).generate();

	}

}
