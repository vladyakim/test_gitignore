package com.botronsoft.pso.migrations.jira.ams;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.io.PrintStream;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import com.botronsoft.pso.jmt.reporting.model.user.UserInfo;
import com.google.common.collect.Lists;

class UserAnalyzerReportGenerator {
	private List<Column> columns;
	private String separator;
	private PrintStream out = System.out;

	private UserAnalyzerReportGenerator(String separator, List<Column> columns) {
		this.separator = separator;
		this.columns = columns;
	}

	static interface Column extends Function<UserInfo, String> {//
	}

	/**
	 * Functional interface allowing flexible way of condfiguring report columns
	 */
	interface Columns {
		Column INSTANCE = (f) -> String.valueOf(f.getInstance());
		Column USERNAME = (f) -> String.valueOf(f.getUsername());
		Column EMAIL = (f) -> String.valueOf(f.getEmail());
		Column DISPLAY_NAME = (f) -> String.valueOf(f.getDisplayName());
		Column HAS_REFERENCE = (f) -> (f.getRefs().isEmpty()) ? "No References Found" : String.valueOf(f.getRefs());
		Column ACTIVE = (f) -> String.valueOf(f.isActive());
		Column EXISTS = (f) -> String.valueOf(f.exists());
		Column GROUPS = (f) -> String.valueOf(f.getGroupMembership());

		/**
		 * The header value for each column
		 */
		Map<Column, String> HEADER = Stream.of(new Object[][] {
			//@formatter:off
			{ Columns.INSTANCE, "Instance" },
			{ Columns.USERNAME, "Username" },
			{ Columns.EMAIL, "Email" },
			{ Columns.DISPLAY_NAME, "Display Name" },
			{ Columns.HAS_REFERENCE, "User Has Data Reference" },
			{ Columns.ACTIVE, "Active On Instance" },
			{ Columns.EXISTS, "Exists On Instance" },
			{ Columns.GROUPS, "Group Membership" },
			//@formatter:on
		}).collect(toMap(data -> (Column) data[0], data -> (String) data[1]));
	}

	static class Builder {
		private String separator = ";";
		Set<Column> columns = new LinkedHashSet<>();

		Builder separator(String separatorChar) {
			separator = separatorChar;
			return this;
		}

		Builder columns(Column... cols) {
			columns.addAll(Lists.newArrayList(cols));
			return this;
		}

		UserAnalyzerReportGenerator build() {
			return new UserAnalyzerReportGenerator(separator, Lists.newArrayList(columns));
		}

	}

	public void csvAllFields(Collection<UserInfo> fields) {
		println(columns.stream().map(c -> Columns.HEADER.get(c)).collect(toList()));
		fields.forEach(field -> {
			println(columns.stream().map(c -> c.apply(field)).collect(toList()));
		});

		// FIXME: Make this configurable from builder
		// println("Total: " + fields.size());
	}

	private void println(Collection<String> columnValues) {
		println(columnValues.stream().collect(joining(separator)));
	}

	/**
	 * Outputs the line in the report
	 */
	private void println(String str) {
		out.println(str);
	}

}