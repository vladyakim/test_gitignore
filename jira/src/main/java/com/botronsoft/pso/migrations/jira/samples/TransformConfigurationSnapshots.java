package com.botronsoft.pso.migrations.jira.samples;

import org.junit.runner.RunWith;

import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migration.framework.transform.run.AbstractSnapshotProcessorRunnerSpecBuilder;
import com.botronsoft.pso.migration.framework.transform.run.DefaultSnapshotProcessorRunner;
import com.botronsoft.pso.migration.framework.transform.run.JUnitSnapshotProcessorRunner;
import com.botronsoft.pso.migration.framework.transform.spec.RunSpec;

/**
 * Historic runner of snapshot transformations described in parameter class in {@link #main(String[])}.
 * 
 * @deprecated use new sample class {@link com.botronsoft.pso.migrations.jira.snapshot.transform.SnapshotTransformations}
 */
@Deprecated
@RunWith(JUnitSnapshotProcessorRunner.class)
public class TransformConfigurationSnapshots extends AbstractSnapshotProcessorRunnerSpecBuilder {

	@RunSpec
	public void configure() {
		// Specify custom working directory - not recommended as it is dependent on machine
		// workdir(Environments.local().workfile("Snapshots")).snapshots("Source_projects_full_qa.zip").transfomer(AlarmTransformation.class);
		workdir(Environments.local().workfile("Snapshots")).snapshots("Source_projects_config12.zip")
				.transfomer(SnapshotTransformations.class);
	}

	/**
	 * You can run the same transformation as a normal Java program using the {@link DefaultSnapshotProcessorRunner#execute()}
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		DefaultSnapshotProcessorRunner.execute(TransformConfigurationSnapshots.class);
	}

}
