package com.botronsoft.pso.migrations.jira.migration.jira.config;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.jira.rollout.model.jiraconfiguration.JiraBaseObject;
import com.botronsoft.jira.rollout.model.jiraconfiguration.dashboard.JiraPortalPage;
import com.botronsoft.jira.rollout.model.jiraconfiguration.field.JiraCustomField;
import com.botronsoft.jira.rollout.model.jiraconfiguration.filter.JiraFilter;
import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.jmt.reporting.model.Favorite;
import com.botronsoft.pso.jmt.rest.client.favorites.FavoritesClient;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migration.framework.environment.JiraEnvironment;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
import com.google.common.collect.Lists;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

/**
 * Export, transform, import filters & dashboards favorites
 *
 */

public class FilterDashboardFavorites {
	private static final Logger log = LoggerFactory.getLogger(FilterDashboardFavorites.class);
	private static File baseDir;
	private static final JiraEnvironment TARGET_ENV = Environments.jira(Env.TARGET);
	private static final JiraEnvironment SOURCE_ENV = Environments.jira(Env.SOURCE);

	@BeforeClass
	public static void init() {
		baseDir = new File(Environments.local().workdir(), "favorites");
		baseDir.mkdir();
	}

	static File getFile() {
		return new File(baseDir, Env.SOURCE + "_favorites" + ".json");
	}

	static File getTransformedFile() {
		return new File(baseDir, Env.SOURCE + "_favorites" + ".transformed.json");
	}

	@Ignore
	@Test
	public void export() throws Exception {

		try {
			log.info("Exporting favs for {}", SOURCE_ENV.getName());
			List<Favorite> list = SOURCE_ENV.getJmtFavClient().export();
			File out = getFile();
			Files.write(out.toPath(), new GsonBuilder().create().toJson(list).getBytes());
			log.info("Exported favs for {} into {}", SOURCE_ENV.getName(), out.getAbsolutePath());
		} catch (Throwable e) {
			log.error("Error exporting data for {}", SOURCE_ENV.getName());
		}

	}

	@Ignore
	@Test
	public void exportAndTransform() throws Exception {

		File input = getFile();
		try (Reader r = Files.newBufferedReader(input.toPath())) {

			List<Favorite> favs = new GsonBuilder().create().fromJson(r,
					TypeToken.getParameterized(ArrayList.class, Favorite.class).getType());

			Map<String, String> filterMapping = getStaticMappingInternal(JiraFilter.class);
			Map<String, String> dashboardMapping = getStaticMappingInternal(JiraPortalPage.class);
			Map<String, String> usernameMap = new HashMap<String, String>();
			MappingFactory mappingFactoryReader = new FileSystemMappingFactory(Env.SOURCE_JIRA_ENV.getKey());

			MapperReader mappingReader = mappingFactoryReader.createReader();
			// getting all source and target users , as if one source user maps to more than one target user
			// it is given as key=SourceUser , value=TargetUser1,TargetUser2 etc
			Map<String, List<String>> helpMap = mappingReader.map(ObjectTypeKeys.USER_NAME);

			helpMap.forEach((k, v) -> usernameMap.put(k,
					mappingReader.onetoManyMap(ObjectTypeKeys.USER_NAME, k).stream().collect(Collectors.joining(","))));

			List<Favorite> transformed = favs.stream().map(f -> {
				Optional<String> newId = Optional.empty();
				switch (f.getType()) {
				case FILTER:
					newId = Optional.ofNullable(filterMapping.get(String.valueOf(f.getId())));
					break;
				case DASHBOARD:
					newId = Optional.ofNullable(dashboardMapping.get(String.valueOf(f.getId())));
					break;
				}
				Optional<String> newusername = Optional.ofNullable(usernameMap.get(f.getUsername().toLowerCase()));
				f.setId(newId.map(v -> Long.parseLong(v)).orElse(Long.valueOf(-1)));
				f.setUsername(newusername.orElse(f.getUsername()));
				return f;
			}).filter(f -> (f.getUsername() != null) && (f.getId() > 0)).collect(toList());

			log.info("Transformed {} of {} records for {}", transformed.size(), favs.size(), SOURCE_ENV.getName());
			File out = getTransformedFile();
			Files.write(out.toPath(), new GsonBuilder().create().toJson(transformed).getBytes());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Ignore
	@Test
	public void importData() throws Exception {
		FavoritesClient client = TARGET_ENV.getJmtFavClient();
		File input = getTransformedFile();
		try (Reader r = Files.newBufferedReader(input.toPath())) {
			List<Favorite> favs = new GsonBuilder().create().fromJson(r,
					TypeToken.getParameterized(ArrayList.class, Favorite.class).getType());
			log.info("Importing {} favs for {}", favs.size(), TARGET_ENV.getName());
			client.importData(favs);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Map<String, String> getStaticMappingInternal(Class<? extends JiraBaseObject> clazz) {
		Map<String, String> objectMappingFromCMJJson = getObjectMappingFromCMJJson(clazz.getSimpleName());
		if (clazz == JiraCustomField.class) {
			return objectMappingFromCMJJson.entrySet().stream()
					.collect(toMap(e -> e.getKey().replace("customfield_", ""), e -> e.getValue().replace("customfield_", "")));
		}
		return objectMappingFromCMJJson;
	}

	private static Map<String, String> getObjectMappingFromCMJJson(String objectName) {
		File[] jsonFiles = Environments.local().getFile("cmjmap").listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith("deploymentSnapshotToTargetNativeIdMap_") && name.endsWith(".json");
			}
		});
		Map<String, String> objectMap = new HashMap<String, String>();
		Lists.newArrayList(jsonFiles).stream().forEach(f -> {
			JsonElement root;
			try {
				root = new JsonParser().parse(com.google.common.io.Files.toString(f, Charset.defaultCharset()));
				JsonElement object = root.getAsJsonObject().get(objectName);
				if (object != null) {
					Set<Entry<String, JsonElement>> entrySet = object.getAsJsonObject().entrySet();
					Map<String, String> map = entrySet.stream().collect(toMap(e -> e.getKey(), e -> e.getValue().getAsString()));
					objectMap.putAll(map);
				}
			} catch (JsonSyntaxException | IOException e) {
				throw new RuntimeException(e);
			}
		});
		return objectMap;
	}
}
