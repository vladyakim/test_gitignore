package com.botronsoft.pso.migrations.jira.samples;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.jira.rollout.model.diff.impl.matching.identity.AgileBoardObjectIdProvider;
import com.botronsoft.jira.rollout.model.diff.impl.matching.identity.FilterObjectIdProvider;
import com.botronsoft.jira.rollout.model.diff.impl.matching.identity.JiraPortalPageObjectIdProvider;
import com.botronsoft.jira.rollout.model.diff.matching.ObjectIdProvider;
import com.botronsoft.jira.rollout.model.jiraconfiguration.JiraBaseObject;
import com.botronsoft.jira.rollout.model.jiraconfiguration.JiraPlugin;
import com.botronsoft.jira.rollout.model.jiraconfiguration.NamedNode;
import com.botronsoft.jira.rollout.model.jiraconfiguration.agile.AgileBoard;
import com.botronsoft.jira.rollout.model.jiraconfiguration.dashboard.JiraPortalPage;
import com.botronsoft.jira.rollout.model.jiraconfiguration.field.JiraCustomField;
import com.botronsoft.jira.rollout.model.jiraconfiguration.field.JiraFieldLayout;
import com.botronsoft.jira.rollout.model.jiraconfiguration.filter.JiraFilter;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssuePriority;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssuePriorityScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssueStatus;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssueTypeScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.project.JiraProject;
import com.botronsoft.jira.rollout.model.jiraconfiguration.scheme.JiraNotificationScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.scheme.JiraPermissionScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.screen.JiraFieldScreen;
import com.botronsoft.jira.rollout.model.jiraconfiguration.screen.JiraFieldScreenScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.user.JiraUser;
import com.botronsoft.pso.cmj.modelprocessor.api.lookup.Lookup;
import com.botronsoft.pso.cmj.modelprocessor.api.lookup.LookupFactory;
import com.botronsoft.pso.cmj.modelprocessor.api.lookup.LookupList;
import com.botronsoft.pso.cmj.modelprocessor.api.transform.MappingProvider;
import com.botronsoft.pso.commons.csv.MappingFileReader;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migration.framework.transform.spec.AbstractTransformationSpec;
import com.botronsoft.pso.migration.framework.transform.spec.PostTransform;
import com.botronsoft.pso.migration.framework.transform.spec.PreTransform;
import com.botronsoft.pso.migration.framework.transform.spec.Transform;
import com.botronsoft.pso.migrations.Env;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

/**
 * All classes under {@code package com.botronsoft.pso.migrations.jira.samples} are for reference only Will be removed once any useful code
 * within is moved where appropriate
 */
@Deprecated
public class SampleModelTransformation extends AbstractTransformationSpec {
	private static final Logger log = LoggerFactory.getLogger(SampleModelTransformation.class);

	static final String PREFIX = "Digital - ";

	@PreTransform
	public void fixUserReferences() {
		snapshot.lookup().byName(JiraUser.class, "poojasawkar@doshaheen.com").ifPresent(user -> {
			user.setName("psawkar@its.jnj.com");
		});
		snapshot.lookup().byName(JiraUser.class, "lcarval6@its.jnj.com").ifPresent(user -> {
			user.setName("luiscarvalho@ciandt.com");
		});
	}

	@PreTransform
	public void loadMappings() {
		// Generic Mapping Provider
		snapshot.transformer().registerMappingProvider(new MappingProvider() {
			@Override
			public <T> Optional<String> mapByNativeId(Class<T> type, String sourceNativeId) {
				return Optional.ofNullable(getStaticMappingInternal((Class<? extends JiraBaseObject>) type).get(sourceNativeId));
			}

			@Override
			public Optional<String> mapByName(Class<? extends JiraBaseObject> type, String sourceName) {
				return Optional.ofNullable(getStaticMappingInternal(type).get(sourceName));
			}
		});

	}

	public static Map<String, String> getStaticMappingInternal(Class<? extends JiraBaseObject> clazz) {
		Map<String, String> objectMappingFromCMJJson = getObjectMappingFromCMJJson(clazz.getSimpleName());
		if (clazz == JiraCustomField.class) {
			return objectMappingFromCMJJson.entrySet().stream()
					.collect(toMap(e -> e.getKey().replace("customfield_", ""), e -> e.getValue().replace("customfield_", "")));
		}
		return objectMappingFromCMJJson;
	}

	private static Map<String, String> getObjectMappingFromCMJJson(String objectName) {
		File[] jsonFiles = new File(Environments.getProperties().get("local.cmjmap").toString()).listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith("deploymentSnapshotToTargetNativeIdMap_") && name.endsWith(".json");
			}
		});
		Map<String, String> sprintMap = new HashMap<String, String>();
		Lists.newArrayList(jsonFiles).stream().forEach(f -> {
			JsonElement root;
			try {
				root = new JsonParser().parse(Files.toString(f, Charset.defaultCharset()));
				JsonElement sprints = root.getAsJsonObject().get(objectName);
				if (sprints != null) {
					Set<Entry<String, JsonElement>> entrySet = sprints.getAsJsonObject().entrySet();
					Map<String, String> map = entrySet.stream().collect(toMap(e -> e.getKey(), e -> e.getValue().getAsString()));
					sprintMap.putAll(map);
				}
			} catch (JsonSyntaxException | IOException e) {
				throw new RuntimeException(e);
			}
		});
		return sprintMap;
	}

	private void fixNames(Class<? extends NamedNode> clazz, ObjectIdProvider objectIdProvider) {
		snapshot.lookup().allOf(clazz).stream().collect(groupingBy(b -> objectIdProvider.getPrimaryObjectId(b))).entrySet().stream()
				.filter(e -> e.getValue().size() > 1).forEach(e -> {
					AtomicInteger index = new AtomicInteger();
					e.getValue().subList(1, e.getValue().size()).forEach(b -> {
						String newName = b.getName() + " (" + Env.SOURCE + " #" + index.incrementAndGet() + ")";
						log.warn("Renaming dublicate {} to {}", clazz.getSimpleName(), newName);
						b.setName(newName);
					});
				});
	}

	// Comment drop data on initial run
	// TODO: Uncomment durring JQL transform
	// @PreTransform
	// public void dropData() {
	// ((DefaultCMJSnapshot) snapshot).dropDataRoot();
	// }

	@PostTransform
	public void POST_FIX_dublicateBoardsFiltersDashboard() {

		// When we merge users this may lead to dublicate objects. FIXME: This should do into the transformer library
		fixNames(AgileBoard.class, new AgileBoardObjectIdProvider());
		fixNames(JiraFilter.class, new FilterObjectIdProvider());
		fixNames(JiraPortalPage.class, new JiraPortalPageObjectIdProvider());
	}

	// Remove Portfolio fields
	@Transform
	public void removePortfolio() {
		// Obsolete code
		// LookupFactory<JiraPlugin> plugin = snapshot.lookup(JiraPlugin.class);
		// HashSet<String> portfolioKeys = Sets.newHashSet("com.atlassian.teams", "com.atlassian.jpo");
		// transform(plugin.manyBy(p -> portfolioKeys.contains(p.getKey()))).delete();
		// //
		// LookupFactory<JiraCustomField> cf = snapshot.lookup(JiraCustomField.class);
		// transform(cf.manyBy(c -> portfolioKeys.stream().anyMatch(k -> c.getTypeId().startsWith(k)))).delete();
	}
	// Add context and searcher for Approvers
	// Make Story Points context
	// Make epic fields context global
	// Rename default object
	// Make renamed defaults none default
	// apply user rename
	// add groups from Confluence
	// do we need cleanup of missing items?

	@Transform
	public void prefixAllDefaultSchemes() {
		// Apply mapping file renames eg:
		// "Default Issue Type Scheme" to "Digital - Default Issue Type Scheme" make not default

		// Identify and collect all default items to be updated and renamed
		LookupFactory<JiraIssuePriorityScheme> prioritySchemes = snapshot.lookup(JiraIssuePriorityScheme.class);
		Lookup<JiraIssuePriorityScheme> defPriorityScheme = prioritySchemes.byName("Default priority scheme");
		transform(defPriorityScheme).renameTo(PREFIX + defPriorityScheme.get().getName());
		transform(defPriorityScheme).modify(sch -> {
			sch.setDescription("This was the Default priority scheme used in Digital JIRA");
			sch.setDefault(false);
		});

		LookupFactory<JiraIssueTypeScheme> issueTypeSchemes = snapshot.lookup(JiraIssueTypeScheme.class);
		Lookup<JiraIssueTypeScheme> defIssueTypeScheme = issueTypeSchemes.byName("Default Issue Type Scheme");
		transform(defIssueTypeScheme).renameTo(PREFIX + defIssueTypeScheme.get().getName());
		transform(defIssueTypeScheme).modify(sch -> {
			sch.setDescription("This was the Default issue type scheme used in Digital JIRA");
			sch.setDefault(false);
		});

		LookupFactory<JiraNotificationScheme> notificationSchemes = snapshot.lookup(JiraNotificationScheme.class);
		Lookup<JiraNotificationScheme> defNotificationScheme = notificationSchemes.byName("Default Notification Scheme");
		transform(defNotificationScheme).renameTo(PREFIX + defNotificationScheme.get().getName());
		transform(defNotificationScheme).modify(sch -> {
			sch.setDescription("This was the Default notification scheme used in Digital JIRA");
			sch.setDefault(false);
		});

		LookupFactory<JiraPermissionScheme> permissionSchemes = snapshot.lookup(JiraPermissionScheme.class);
		Lookup<JiraPermissionScheme> defPermissionScheme = permissionSchemes.byName("Default Permission Scheme");
		transform(defPermissionScheme).renameTo(PREFIX + defPermissionScheme.get().getName());
		transform(defPermissionScheme).modify(sch -> {
			sch.setDescription("This was the Default permission scheme used in Digital JIRA");
			sch.setDefault(false);
		});

		LookupFactory<JiraFieldLayout> fieldLayouts = snapshot.lookup(JiraFieldLayout.class);
		Lookup<JiraFieldLayout> defFieldLayout = fieldLayouts.byName("Default Field Configuration");
		transform(defFieldLayout).renameTo(PREFIX + defFieldLayout.get().getName());
		transform(defPermissionScheme).modify(sch -> {
			sch.setDescription("This was the Default field configuration used in Digital JIRA");
			sch.setDefault(false);
		});

		LookupFactory<JiraFieldScreenScheme> fieldScreenSchemes = snapshot.lookup(JiraFieldScreenScheme.class);
		Lookup<JiraFieldScreenScheme> defFieldScreenScheme = fieldScreenSchemes.byName("Default Screen Scheme");
		transform(defFieldScreenScheme).renameTo(PREFIX + defFieldScreenScheme.get().getName());
		transform(defFieldScreenScheme).modify(sch -> {
			sch.setDescription("This was the Default screen scheme used in Digital JIRA");
		});
	}

	@Transform
	public void removeObsoleteAddonsObjects() {

		LookupFactory<JiraPlugin> plugins = snapshot.lookup(JiraPlugin.class);
		LookupList<Lookup<JiraPlugin>> pluginsToDelete = plugins.byNames("Portfolio plans", "Portfolio Team Management");
		LookupFactory<JiraCustomField> cf = snapshot.lookup(JiraCustomField.class);
		LookupList<Lookup<JiraCustomField>> cfToDelete = cf.byIds(12318, 12319, 12320, 12321, 12322);
		chain(chain -> {
			cfToDelete.forEach(f -> {
				chain.transform(f).delete();
			});
			pluginsToDelete.forEach(p -> {
				chain.transform(p).delete();
			});
		});

	}

	@Transform
	public void alignStatuses() {

		chain(chain -> {
			chain.transform(lookup(JiraIssueStatus.class).byName("Under Review").optional()).modify((c) -> {
				c.setStatusCategoryKey("new");
			});

			chain.transform(lookup(JiraIssueStatus.class).byName("Under Review").optional()).renameTo("Under review");
		});

		transform(lookup(JiraIssueStatus.class).byName("Waiting for Support").optional()).renameTo("Waiting for support");
		transform(lookup(JiraIssueStatus.class).byName("Waiting for Customer").optional()).renameTo("Waiting for customer");

		transform(lookup(JiraIssueStatus.class).byName("Approved").optional()).modify((c) -> {
			c.setStatusCategoryKey("indeterminate");
			c.setIconUrl("/images/icons/statuses/generic.png");
		});
		transform(lookup(JiraIssueStatus.class).byName("Pending").optional()).modify((c) -> {
			c.setStatusCategoryKey("indeterminate");
			c.setIconUrl("/images/icons/status_generic.gif");
		});

		// Set status icon to be the same for all statuses to avoid unrelated diffs, as status icon is not used anymore
		transform(lookup(JiraIssueStatus.class).byNames("In Review", "To Do", "Backlog", "Done")).modify((c) -> {
			c.setIconUrl("/");
		});

		transform(lookup(JiraIssueStatus.class).byNames("New")).modify((c) -> {
			c.setIconUrl("/images/icons/statuses/generic.png");
		});

		transform(lookup(JiraIssueStatus.class).byNames("In QA")).modify((c) -> {
			c.setIconUrl("/images/icons/statuses/visible.png");
		});

		transform(lookup(JiraIssueStatus.class).byNames("Open")).modify((c) -> {
			c.setIconUrl("/images/icons/statuses/open.png");
		});

	}

	@Transform
	public void prefixConflictingScreens() {

		transform(lookup(JiraFieldScreen.class)
				.byNames("Default Screen", "JIRA Service Desk Resolve Issue Screen - 9", "Workflow Screen - 10").optional()).modify((c) -> {
					c.setName(PREFIX + c.getName());
				});

	}

	@Transform
	public void alignPriorityAttributes() {
		transform(lookup(JiraIssuePriority.class).byNames("High")).modify((c) -> {
			c.setIconUrl("/images/icons/priorities/major.svg");
			c.setColor("#F29328");
		});

		transform(lookup(JiraIssuePriority.class).byNames("Medium")).modify((c) -> {
			c.setIconUrl("/images/icons/priorities/medium.svg");
			c.setColor("#F7E402");
		});

		transform(lookup(JiraIssuePriority.class).byNames("Low")).modify((c) -> {
			c.setIconUrl("/images/icons/priorities/low.svg");
			c.setColor("#279904");
		});

		transform(lookup(JiraIssuePriority.class).byNames("Minor")).modify((c) -> {
			c.setColor("#999999");
		});
	}

	@Transform
	public void customFieldTransformations() {
		transform(lookup(JiraCustomField.class).byName("E-Signature")).modify((c) -> {
			c.getContexts().forEach(context -> {
				context.setName("Default Configuration Scheme for E-Signature");
			});
		});

		transform(lookup(JiraCustomField.class).byName("Approvers")).modify((c) -> {
			c.setSearcher("com.atlassian.jira.plugin.system.customfieldtypes:userpickergroupsearcher");
		});
	}

	@Transform
	public void changeSystemSnapshot() {
		LookupFactory<JiraPortalPage> dashboards = snapshot.lookup(JiraPortalPage.class);
		transform(dashboards.byId(10000).optional()).modify((d) -> {
			d.setName(PREFIX + d.getName());
			d.setDescription("Digital System Dashboard");
			d.setOwner(snapshot.lookup(JiraUser.class).byName("bmcdevit@XXX.com").get());
			d.setDefault(false);
		});

	}

	@Transform
	public void applyUserMapping() {

		Map<String, String> usersMap = new HashMap<String, String>();

		MappingFileReader reader = new MappingFileReader("users");
		Map<String, Set<String>> instanceMap = reader.loadMapOfLines("userMap.csv").withDelimiter(";").load();
		instanceMap.forEach((k, v) -> {
			usersMap.put(k, String.join(",", v));
		});

		LookupFactory<JiraUser> users = snapshot.lookup(JiraUser.class);

		usersMap.forEach((k, v) -> {
			if (users.byName(k).resolve().isPresent()) {
				transform(users.byName(k)).mergeUsersInto(v);
			}
		});

	}

	@Transform
	public void updateKBapplink() {

		String appLink = "aed628f7-9a7e-3883-b19f-4e03e760c912";

		List<JiraProject> projects = snapshot.lookup().allOf(JiraProject.class);
		projects.stream().filter(p -> p.getType().equalsIgnoreCase("service_desk") && (p.getServiceDesk().getKnowledgeBaseLink() != null))
				.forEach(p -> {
					transform(Lookup.of(p)).modify(currP -> {
						currP.getServiceDesk().getKnowledgeBaseLink().setApplicationId(appLink);
						currP.getServiceDesk().getKnowledgeBaseLink().setApplicationName("Confluence");
					});
				});
	}

}
