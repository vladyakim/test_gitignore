package com.botronsoft.pso.migrations.jira.ams;

import static com.botronsoft.pso.migrations.jira.ams.UserAnalyzerReportGenerator.Columns.ACTIVE;
import static com.botronsoft.pso.migrations.jira.ams.UserAnalyzerReportGenerator.Columns.DISPLAY_NAME;
import static com.botronsoft.pso.migrations.jira.ams.UserAnalyzerReportGenerator.Columns.EMAIL;
import static com.botronsoft.pso.migrations.jira.ams.UserAnalyzerReportGenerator.Columns.EXISTS;
import static com.botronsoft.pso.migrations.jira.ams.UserAnalyzerReportGenerator.Columns.GROUPS;
import static com.botronsoft.pso.migrations.jira.ams.UserAnalyzerReportGenerator.Columns.HAS_REFERENCE;
import static com.botronsoft.pso.migrations.jira.ams.UserAnalyzerReportGenerator.Columns.INSTANCE;
import static com.botronsoft.pso.migrations.jira.ams.UserAnalyzerReportGenerator.Columns.USERNAME;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.commons.csv.MappingFileReader;
import com.botronsoft.pso.jmt.reporting.model.user.UserInfo;
import com.botronsoft.pso.jmt.reporting.model.user.UserMappedEntry;
import com.botronsoft.pso.jmt.rest.client.UserClient;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class UserManagement {

	// Used to build email address for obsolete users. Update value accordingly, e.g. @company.com
	private static final String COMPANYDOMAIN = "@....";

	private static final File USER_MANAGMENT_DIR = Env.LOCAL_ENV.workfile("UserManagement");
	private static UserClient userClient;

	private static final Logger log = LoggerFactory.getLogger(UserManagement.class);

	/**
	 * Dumps all SOURCE usernames along with user references, group membership and active status
	 *
	 * @throws IOException
	 */
	// @Ignore
	@Test
	public void dumpSourceUsers() {
		List<UserInfo> users = new ArrayList<>();

		users.addAll(Env.SOURCE_JIRA_ENV.getJmtReportingClient().collectUsers());

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		File file = new File(USER_MANAGMENT_DIR, "users_" + Env.SOURCE + ".json");

		// creates USER_MANAGMENT_DIR if not available
		file.getParentFile().mkdir();

		try {
			file.createNewFile();
			Files.write(file.toPath(), gson.toJson(users).getBytes());
		} catch (IOException e) {
			log.error("Unable to create dumpfile!");
			e.printStackTrace();
		}
	}

	/**
	 * Dumps all TARGET usernames along with user references, group membership and active status
	 *
	 */
	// @Ignore
	@Test
	public void dumpTargetUsers() {
		List<UserInfo> users = new ArrayList<>();

		users.addAll(Env.TARGET_JIRA_ENV.getJmtReportingClient().collectUsers());

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		File file = new File(USER_MANAGMENT_DIR, "users_" + Env.TARGET + ".json");

		// creates USER_MANAGMENT_DIR if not available
		file.getParentFile().mkdir();

		try {
			file.createNewFile();
			Files.write(file.toPath(), gson.toJson(users).getBytes());
		} catch (IOException e) {
			log.error("Unable to create dumpfile!");
			e.printStackTrace();
		}
	}

	/**
	 * Method checking for users sharing same emails on SOURCE and TARGET or same usernames on SOURCE and TARGET in case no valid email on
	 * SOURCE
	 */
	// @Ignore
	@Test
	public void generateUsersMapping() {
		List<UserInfo> sourceUsers = Env.SOURCE_JIRA_ENV.getJmtReportingClient().collectUsers();
		List<UserInfo> targetUsers = Env.TARGET_JIRA_ENV.getJmtReportingClient().collectUsers();

		for (UserInfo user : sourceUsers) {
			if ((user.getEmail() != null) && !user.getEmail().equals("?")) {
				List<UserInfo> foundMatch = targetUsers.stream().filter(us -> us.getEmail().equals(user.getEmail()))
						.collect(Collectors.toList());
				if (foundMatch.size() == 1) {
					System.out.println("SOURCE user: " + user.getUsername() + " matched on email to " + foundMatch.get(0).getUsername());
				} else if (foundMatch.size() == 0) {
					System.out.println("No matching user sharing same email found for SOURCE user: " + user.getUsername());
				} else {
					System.out.println(
							"More than one email match found for SOURCE user: " + user.getUsername() + ", email: " + user.getEmail());
				}
			} else {
				List<UserInfo> foundMatch = targetUsers.stream().filter(us -> us.getUsername().equals(user.getUsername()))
						.collect(Collectors.toList());
				if (foundMatch.size() == 1) {
					System.out.println("Match inactive and /or with no valid email SOURCE user: " + user.getUsername() + " to "
							+ foundMatch.get(0).getUsername());
				} else if (foundMatch.size() == 0) {
					System.out.println("Match not found for inactive SOURCE user: " + user.getUsername());
				} else {
					System.out.println("More than one match for inactive and/or with no valid email SOURCE user found for: "
							+ user.getUsername() + ", email: " + user.getEmail());
				}
			}
		}

	}

	/**
	 * Method creates dump of all SOURCE users with their proper TARGET attributes:
	 * <li>applies username mapping available in <b>user_mapping.csv</b>
	 * <li>applies group names mapping available in <b>groupMap.csv</b>
	 * <li>removes any group membership for groups marked as not to be available on TARGET <b>groupsNotToCreate.csv</b>
	 */
	// @Ignore
	@Test
	public void generateMappedUserJson() {
		List<UserInfo> sourceUsers = Env.SOURCE_JIRA_ENV.getJmtReportingClient().collectUsers();
		List<UserInfo> targetUsers = Env.TARGET_JIRA_ENV.getJmtReportingClient().collectUsers();
		List<UserMappedEntry> sourceUsersMapped = new ArrayList<>();

		// Previously:
		// List<String> snapshotUsers = Files.readAllLines(new File(USER_MANAGMENT_DIR + "/snapshot_users.csv").toPath()).stream()
		// .map(String::toLowerCase).collect(Collectors.toList());

		// Whereas it's actually all users that have references is what we need. Use 'sourceUsers' instead. Those where refs is not empty
		List<String> snapshotUsers = sourceUsers.stream().filter(user -> !user.getRefs().isEmpty()).map(u -> u.getUsername().toLowerCase())
				.collect(toList());

		MappingFileReader reader = new MappingFileReader("user_management");
		Map<String, String> usersMap = reader.loadMapOfLines("user_mapping.csv").load().entrySet().stream()
				.collect(toMap(entry -> entry.getKey(), entry -> String.join("", entry.getValue())));

		Map<String, String> groupsMap = reader.loadMapOfLines("groupMap.csv").load().entrySet().stream()
				.collect(toMap(entry -> entry.getKey(), entry -> String.join("", entry.getValue())));
		Set<String> deletedGroups = reader.loadSet("groupsNotToCreate.csv").load();

		sourceUsers.stream().filter(su -> snapshotUsers.contains(su.getUsername())).forEach(user -> {
			UserMappedEntry currentUser = new UserMappedEntry();

			currentUser.setInstance(user.getInstance());
			currentUser.setUsername(user.getUsername());
			currentUser.setUserKey(user.getUserKey());
			currentUser.setActive(user.isActive());
			currentUser.setUserExisting(user.exists());
			currentUser.setRefs(user.getRefs());

			// can be obsolete
			if (!user.exists() && (usersMap.get(user.getUsername()) == null)) {
				String commonDisplayName = "Obsolete User obsolete_";
				currentUser.setDisplayName(commonDisplayName + user.getUserKey());
				currentUser.setEmail(
						("obsolete_" + (user.getUserKey().contains("@") ? user.getUserKey().substring(0, user.getUserKey().indexOf("@"))
								: user.getUserKey()) + COMPANYDOMAIN).replaceAll("\\s", ""));
				currentUser.setGroupMembership(Arrays.asList("mig_obsolete"));
				currentUser.setDstUsername("obsolete_" + user.getUsername());
				currentUser.setActiveOnTarget(false);

			} else {

				currentUser.setDisplayName(user.getDisplayName());
				currentUser.setEmail(user.getEmail());
				currentUser.setDstUsername(user.getUsername());

				// apply user mapping
				if (usersMap.get(user.getUsername()) != null) {
					currentUser.setDstUsername(usersMap.get(user.getUsername()));
				}

				Set<String> groups = user.getGroupMembership().stream()
						.map(group -> groupsMap.get(group) != null ? groupsMap.get(group) : group).collect(toSet());
				groups.removeAll(deletedGroups);

				if (!targetUsers.stream().filter(us -> us.getUsername().equalsIgnoreCase(currentUser.getUsername())).findAny()
						.isPresent()) {
					groups.add("mig_autocreate");
				}

				currentUser.setGroupMembership(groups);

				// in case all newly created users are to be inactive just set it to false,
				// otherwise current SOURCE active status will be set
				currentUser.setActiveOnTarget(user.isActive());

			}
			sourceUsersMapped.add(currentUser);
		});

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		File dir = Environments.local().workfile("UserManagement");
		File file = new File(dir, "users_" + Env.SOURCE + "_mapped.json");
		try {
			file.createNewFile();
			Files.write(file.toPath(), gson.toJson(sourceUsersMapped).getBytes());
		} catch (IOException e) {
			log.error("Unable to create dumpfile!");
			e.printStackTrace();
		}
	}

	/**
	 * Method creating all necessary SOURCE groups on TARGET
	 *
	 * <p>
	 * <b>Similar to {@link #applyGroupMembership()}</b> that creates any missing SOURCE group on TARGET that are not marked for deletion
	 * and have members. Can be used instead!
	 *
	 * <p>
	 * <b>Resources used</b>
	 * <li><b><i>groupMap.csv</i></b> - any known group map in format <b>source;target</b> per line
	 * <li><b><i>groupsNotToCreate.csv</i></b> - list of any source groups not to be created on TARGET. Single record per line.
	 * <li><b><i>nonExistingGroupsToCreate.csv</i></b> - any list of groups, one per line, that do not exist on SOURCE but are to be created
	 * on TARGET
	 */
	// @Ignore
	@Test
	public void createGroups() {

		userClient = Env.TARGET_JIRA_ENV.getJmtUserClient();

		MappingFileReader reader = new MappingFileReader("user_management");
		Map<String, String> groupsMap = reader.loadMapOfLines("groupMap.csv").load().entrySet().stream()
				.collect(toMap(entry -> entry.getKey(), entry -> String.join("", entry.getValue())));
		Set<String> deletedGroups = reader.loadSet("groupsNotToCreate.csv").load();
		Set<String> additionalGroups = reader.loadSet("nonExistingGroupsToCreate.csv").load();

		Set<String> sourceGroups = new HashSet<String>(Env.SOURCE_JIRA_ENV.getJiraClient().getAllGroups());
		Set<String> targetGroups = new HashSet<String>(Env.TARGET_JIRA_ENV.getJiraClient().getAllGroups());

		Set<String> sourceGroupsToCreate = sourceGroups.stream().map(group -> groupsMap.get(group) != null ? groupsMap.get(group) : group)
				.collect(toSet());

		sourceGroupsToCreate.removeAll(deletedGroups);
		sourceGroupsToCreate.addAll(additionalGroups);
		sourceGroupsToCreate.removeAll(targetGroups);

		userClient.createGroups(sourceGroupsToCreate);
	}

	/**
	 * Method creates all SOURCE users in <b>users_source_mapped.json</b> that do not exist on TARGET
	 *
	 * File used is the output from {@link #generateMappedUserJson()}
	 */
	// @Ignore
	@Test
	public void createUsers() {

		userClient = Env.TARGET_JIRA_ENV.getJmtUserClient();

		String json = jsonStringFromFile(USER_MANAGMENT_DIR + "/users_" + Env.SOURCE + "_mapped.json");

		List<UserMappedEntry> users = fromJson(UserMappedEntry.class, json);
		userClient.createUsers(users);
	}

	/**
	 * Method process all SOURCE users in <b>users_source_mapped.json</b> adding any group membership in file currently not set on TARGET
	 *
	 * File used is the output from {@link #generateMappedUserJson()}
	 */
	// @Ignore
	@Test
	public void applyGroupMembership() {

		userClient = Env.TARGET_JIRA_ENV.getJmtUserClient();

		MappingFileReader reader = new MappingFileReader("user_management");

		String json = jsonStringFromFile(USER_MANAGMENT_DIR + "/users_" + Env.SOURCE + "_mapped.json");
		List<UserMappedEntry> users = fromJson(UserMappedEntry.class, json);

		Map<String, Set<String>> groupMembership = users.stream().collect(toMap(UserMappedEntry::getDstUsername, userEntry -> {
			return new HashSet<String>(userEntry.getGroupMembership());
		}));

		userClient.applyGroupMembership(groupMembership);
	}

	/**
	 * Generate csv-like console output (delimited with ';') of all SOURCE user attributes.
	 * <p>
	 * Output can be used along with output from {@link #generateTargetUserReport()} for user mapping, user management discussion
	 */
	@Test
	public void generateSourceUserReport() {

		UserAnalyzerReportGenerator reportGenerator = new UserAnalyzerReportGenerator.Builder()
				.columns(INSTANCE, USERNAME, EMAIL, DISPLAY_NAME, HAS_REFERENCE, GROUPS, ACTIVE, EXISTS).build();
		reportGenerator.csvAllFields(Env.SOURCE_JIRA_ENV.getJmtReportingClient().collectUsers());

	}

	/**
	 * Generate csv-like console output (delimited with ';') of all TARGET user attributes.
	 * <p>
	 * Output can be used along with output from {@link #generateSourceUserReport()} for user mapping, user management discussion
	 */
	@Test
	public void generateTargetUserReport() {

		UserAnalyzerReportGenerator reportGenerator = new UserAnalyzerReportGenerator.Builder()
				.columns(INSTANCE, USERNAME, EMAIL, DISPLAY_NAME, HAS_REFERENCE, GROUPS, ACTIVE, EXISTS).build();
		reportGenerator.csvAllFields(Env.TARGET_JIRA_ENV.getJmtReportingClient().collectUsers());
	}

	protected <T> List<T> fromJson(Class<T> clazz, String json) {
		Gson gson = new GsonBuilder().create();
		Type type = TypeToken.getParameterized(ArrayList.class, clazz).getType();
		return gson.fromJson(json, type);
	}

	private String jsonStringFromFile(String pathToFile) {
		StringBuilder contentBuilder = new StringBuilder();

		try {
			Files.lines(Paths.get(pathToFile), StandardCharsets.UTF_8).forEach(s -> contentBuilder.append(s).append("\n"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return contentBuilder.toString();
	}

}
