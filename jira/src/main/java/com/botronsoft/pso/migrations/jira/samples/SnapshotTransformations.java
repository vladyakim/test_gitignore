package com.botronsoft.pso.migrations.jira.samples;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.jira.rollout.model.jiraconfiguration.JiraBaseObject;
import com.botronsoft.jira.rollout.model.jiraconfiguration.dashboard.JiraPortalPage;
import com.botronsoft.jira.rollout.model.jiraconfiguration.field.JiraCustomField;
import com.botronsoft.jira.rollout.model.jiraconfiguration.field.JiraCustomFieldContext;
import com.botronsoft.jira.rollout.model.jiraconfiguration.field.JiraFieldFactory;
import com.botronsoft.jira.rollout.model.jiraconfiguration.field.JiraFieldLayout;
import com.botronsoft.jira.rollout.model.jiraconfiguration.field.JiraOption;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssueLinkType;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssuePriority;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssuePriorityScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssueResolution;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssueStatus;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssueType;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssueTypeScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.project.JiraDefaultAssigneeType;
import com.botronsoft.jira.rollout.model.jiraconfiguration.project.JiraProject;
import com.botronsoft.jira.rollout.model.jiraconfiguration.scheme.JiraNotificationScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.scheme.JiraPermissionScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.screen.JiraFieldScreen;
import com.botronsoft.jira.rollout.model.jiraconfiguration.screen.JiraFieldScreenScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.screen.JiraIssueTypeScreenScheme;
import com.botronsoft.jira.rollout.model.jiraconfiguration.user.JiraGroup;
import com.botronsoft.jira.rollout.model.jiraconfiguration.user.JiraUser;
import com.botronsoft.jira.rollout.model.jiraconfiguration.workflow.JiraWorkflow;
import com.botronsoft.jira.rollout.model.jiraconfiguration.workflow.JiraWorkflowScheme;
import com.botronsoft.pso.cmj.modelprocessor.api.lookup.Lookup;
import com.botronsoft.pso.cmj.modelprocessor.api.lookup.LookupFactory;
import com.botronsoft.pso.cmj.modelprocessor.api.transform.MappingProvider;
import com.botronsoft.pso.cmj.modelprocessor.impl.DefaultCMJSnapshot;
import com.botronsoft.pso.commons.csv.MappingFileReader;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migration.framework.transform.spec.AbstractTransformationSpec;
import com.botronsoft.pso.migration.framework.transform.spec.PostTransform;
import com.botronsoft.pso.migration.framework.transform.spec.PreTransform;
import com.botronsoft.pso.migration.framework.transform.spec.Transform;
import com.botronsoft.pso.migrations.Env;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

/**
 * Historic approach for describing snapshot transformations to be applied
 *
 * @deprecated use new sample class {@link com.botronsoft.pso.migrations.jira.snapshot.transform.SnapshotTransformations}
 *
 */
@Deprecated
public class SnapshotTransformations extends AbstractTransformationSpec {
	private static final Logger log = LoggerFactory.getLogger(SnapshotTransformations.class);

	static final String PREFIX = Env.SOURCE_JIRA_ENV.getRenamePrefix();
	static final String KEYPREFIX = "OE";
	static Map<String, String> pKeys = new HashMap<String, String>();

	// Comment drop data on initial run
	// TODO: Uncomment durring JQL transform
	@PreTransform
	public void dropData() {
		((DefaultCMJSnapshot) snapshot).dropDataRoot();
	}

	@PreTransform
	public void loadUsers() throws IOException {
		// Generic Mapping Provider
		List<String> snapshotUsers = ((DefaultCMJSnapshot) snapshot).getConfiguration().getUsers().stream().map(user -> user.getName())
				.collect(toList());

		FileWriter out = new FileWriter(new File(Environments.local().workdir(), "snapshot_users.csv"));
		for (String snapshotUser : snapshotUsers) {
			out.write(snapshotUser + "\n");
		}
		out.close();
	}

	@PreTransform
	public void loadMappings() {
		// Generic Mapping Provider
		snapshot.transformer().registerMappingProvider(new MappingProvider() {
			@Override
			public <T> Optional<String> mapByNativeId(Class<T> type, String sourceNativeId) {
				return Optional.ofNullable(getStaticMappingInternal((Class<? extends JiraBaseObject>) type).get(sourceNativeId));
			}

			@Override
			public Optional<String> mapByName(Class<? extends JiraBaseObject> type, String sourceName) {
				return Optional.ofNullable(getStaticMappingInternal(type).get(sourceName));
			}
		});

	}

	public static Map<String, String> getStaticMappingInternal(Class<? extends JiraBaseObject> clazz) {
		Map<String, String> objectMappingFromCMJJson = getObjectMappingFromCMJJson(clazz.getSimpleName());
		if (clazz == JiraCustomField.class) {
			return objectMappingFromCMJJson.entrySet().stream()
					.collect(toMap(e -> e.getKey().replace("customfield_", ""), e -> e.getValue().replace("customfield_", "")));
		}
		return objectMappingFromCMJJson;
	}

	private static Map<String, String> getObjectMappingFromCMJJson(String objectName) {
		File[] jsonFiles = new File(Environments.local().getFile("cmjmap").toString()).listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith("deploymentSnapshotToTargetNativeIdMap_") && name.endsWith(".json");
			}
		});
		Map<String, String> sprintMap = new HashMap<String, String>();
		if (jsonFiles != null) {
			Lists.newArrayList(jsonFiles).stream().forEach(f -> {
				JsonElement root;
				try {
					root = new JsonParser().parse(Files.toString(f, Charset.defaultCharset()));
					JsonElement sprints = root.getAsJsonObject().get(objectName);
					if (sprints != null) {
						Set<Entry<String, JsonElement>> entrySet = sprints.getAsJsonObject().entrySet();
						Map<String, String> map = entrySet.stream().collect(toMap(e -> e.getKey(), e -> e.getValue().getAsString()));
						sprintMap.putAll(map);
					}
				} catch (JsonSyntaxException | IOException e) {
					throw new RuntimeException(e);
				}
			});
		}
		return sprintMap;
	}

	@Transform
	public void projectKeyTransformations() {

		transform(lookup(JiraProject.class).byKeys("SRCKEY").optional()).changeKey("RENAMEDSRCKEY");

		pKeys.put("SRCKEY", "RENAMEDSRCKEY");

	}

	@Transform
	public void defaultAssineeTransformations() {
		transform(lookup(JiraProject.class).all()).modify((project) -> {
			if ((project.getDefaultAssignee() == null) || project.getDefaultAssignee().equals(JiraDefaultAssigneeType.UNASSIGNED)) {
				project.setDefaultAssignee(JiraDefaultAssigneeType.PROJECT_LEAD);
			}
		});
	}

	@Transform
	public void userTransformations() {
		if (!lookup(JiraUser.class).byNames("src_user").get().isEmpty()) {
			transform(lookup(JiraUser.class).byNames("src_user")).mergeUsersInto("target_user");
		}
	}

	@Transform
	public void groupTransformations() {
		Map<String, String> groupsMap = new HashMap<String, String>();

		MappingFileReader reader = new MappingFileReader("user_management");
		Map<String, Set<String>> load = reader.loadMapOfLines("groupMap.csv").withDelimiter(";").load();
		load.forEach((k, v) -> {
			groupsMap.put(k, String.join(",", v));
		});

		LookupFactory<JiraGroup> groups = snapshot.lookup(JiraGroup.class);

		groupsMap.forEach((k, v) -> {
			if (groups.byName(k).resolve().isPresent()) {
				transform(groups.byName(k)).modify(group -> {
					group.setName(v);
				});
			}
		});

	}

	@Transform
	public void prefixWorkflows() {

		transform(lookup(JiraWorkflow.class).byName("classic default workflow").optional()).modify((c) -> {
			c.setName(PREFIX + c.getName());
			c.setDefault(false);
		});

	}

	@Transform
	public void prefixConflictingScreens() {
		transform(lookup(JiraFieldScreen.class).byNames("Default Screen").optional()).renameTo(PREFIX + "Default Screen");
	}

	@Transform
	public void prefixAllDefaultSchemes() {

		LookupFactory<JiraWorkflowScheme> workflowSchemes = snapshot.lookup(JiraWorkflowScheme.class);
		Lookup<JiraWorkflowScheme> defWorkflowScheme = workflowSchemes.byName("classic");
		transform(defWorkflowScheme).renameTo(PREFIX + defWorkflowScheme.get().getName());
		transform(defWorkflowScheme).modify(sch -> {
			sch.setDescription("This was the Default workflow scheme used in SOURCE JIRA");
		});

		LookupFactory<JiraIssuePriorityScheme> prioritySchemes = snapshot.lookup(JiraIssuePriorityScheme.class);
		Lookup<JiraIssuePriorityScheme> defPriorityScheme = prioritySchemes.byName("Default priority scheme");
		transform(defPriorityScheme).renameTo(PREFIX + defPriorityScheme.get().getName());
		transform(defPriorityScheme).modify(sch -> {
			sch.setDescription("This was the Default priority scheme used in SOURCE JIRA");
			sch.setDefault(false);
		});

		LookupFactory<JiraIssueTypeScheme> issueTypeSchemes = snapshot.lookup(JiraIssueTypeScheme.class);
		Lookup<JiraIssueTypeScheme> defIssueTypeScheme = issueTypeSchemes.byName("Default Issue Types");
		transform(defIssueTypeScheme).renameTo(PREFIX + defIssueTypeScheme.get().getName());
		transform(defIssueTypeScheme).modify(sch -> {
			sch.setDescription("This was the Default issue type scheme used in SOURCE JIRA");
			sch.setDefault(false);
		});

		LookupFactory<JiraIssueTypeScreenScheme> issueTypeScreenSchemes = snapshot.lookup(JiraIssueTypeScreenScheme.class);
		Lookup<JiraIssueTypeScreenScheme> defIssueTypeScreenScheme = issueTypeScreenSchemes.byName("Default Issue Type Screen Scheme");
		transform(defIssueTypeScreenScheme).renameTo(PREFIX + defIssueTypeScreenScheme.get().getName());
		transform(defIssueTypeScreenScheme).modify(sch -> {
			sch.setDescription("This was the Default issue type scheme used in SOURCE JIRA");
			sch.setDefault(false);
		});

		LookupFactory<JiraNotificationScheme> notificationSchemes = snapshot.lookup(JiraNotificationScheme.class);
		Lookup<JiraNotificationScheme> defNotificationScheme = notificationSchemes.byName("Default Notification Scheme");
		transform(defNotificationScheme).renameTo(PREFIX + defNotificationScheme.get().getName());
		transform(defNotificationScheme).modify(sch -> {
			sch.setDescription("This was the Default notification scheme used in SOURCE JIRA");
			sch.setDefault(false);
		});

		LookupFactory<JiraPermissionScheme> permissionSchemes = snapshot.lookup(JiraPermissionScheme.class);
		Lookup<JiraPermissionScheme> defPermissionScheme = permissionSchemes.byName("Default Permission Scheme");
		transform(defPermissionScheme).renameTo(PREFIX + defPermissionScheme.get().getName());
		transform(defPermissionScheme).modify(sch -> {
			sch.setDescription("This was the Default permission scheme used in SOURCE JIRA");
			sch.setDefault(false);
		});

		LookupFactory<JiraPermissionScheme> permissionSoftwareSchemes = snapshot.lookup(JiraPermissionScheme.class);
		Lookup<JiraPermissionScheme> defPermissionSoftwareScheme = permissionSoftwareSchemes.byName("Default software scheme");
		transform(defPermissionSoftwareScheme).renameTo(PREFIX + defPermissionSoftwareScheme.get().getName());
		transform(defPermissionSoftwareScheme).modify(sch -> {
			sch.setDescription("This was the Default software scheme used in SOURCE JIRA");
			sch.setDefault(false);
		});

		LookupFactory<JiraFieldLayout> fieldLayouts = snapshot.lookup(JiraFieldLayout.class);
		Lookup<JiraFieldLayout> defFieldLayout = fieldLayouts.byName("Default Field Configuration");
		transform(defFieldLayout).renameTo(PREFIX + defFieldLayout.get().getName());
		transform(defPermissionScheme).modify(sch -> {
			sch.setDescription("This was the Default field configuration used in SOURCE JIRA");
			sch.setDefault(false);
		});

		LookupFactory<JiraFieldScreenScheme> fieldScreenSchemes = snapshot.lookup(JiraFieldScreenScheme.class);
		Lookup<JiraFieldScreenScheme> defFieldScreenScheme = fieldScreenSchemes.byName("Default Screen Scheme");
		transform(defFieldScreenScheme).renameTo(PREFIX + defFieldScreenScheme.get().getName());
		transform(defFieldScreenScheme).modify(sch -> {
			sch.setDescription("This was the Default screen scheme used in SOURCE JIRA");
		});
	}

	@Transform
	public void customFieldTransformations() {
		transform(lookup(JiraCustomField.class).byName("Severity")).globalToProjectContext()
				.projects(snapshot.lookup(JiraProject.class).all()).add();

		transform(lookup(JiraCustomField.class).byName("Story Points")).modify((c) -> {
			JiraCustomFieldContext context = c.getContexts().get(0);
			context.getProjects().addAll(lookup(JiraProject.class).all().get());
			context.setGlobal(false);
			context.setAllProjects(false);
		});

		transform(lookup(JiraCustomField.class).byName("Flagged")).modify((c) -> {
			c.getContexts().forEach(context -> {
				context.getOptions().forEach(option -> {
					if (option.getValue().equals("Impediment")) {
						option.setValue("Yes");
					}
				});
				JiraOption option = JiraFieldFactory.eINSTANCE.createJiraOption();
				option.setValue("No");
				option.setSequence(1);
				context.getOptions().add(option);
			});
		});

		transform(lookup(JiraCustomField.class).byName("Flagged")).renameTo("Modules to Deploy Check");

		transform(lookup(JiraCustomField.class).byName("Global Rank")).delete();

		transform(lookup(JiraCustomField.class).byName("Rank (Obsolete)")).delete();

		transform(lookup(JiraCustomField.class).byName("Project Silo")).modify(c -> c.getContexts().get(0).getProjectKeys().clear());
	}

	@PostTransform
	public void changeAssignee() {
		if (((DefaultCMJSnapshot) snapshot).getData() != null) {
			((DefaultCMJSnapshot) snapshot).getData().getIssueContainers().forEach(issueContainer -> {
				JiraUser projectLead = issueContainer.getProject().getLead();
				issueContainer.getIssues().forEach(issue -> {
					if (issue.getAssignee() == null) {
						issue.setAssignee(projectLead);
					}
				});
			});
		}
	}

	@PostTransform
	public void changePKInIssueLinks() {
		if ((((DefaultCMJSnapshot) snapshot).getData() != null) && (((DefaultCMJSnapshot) snapshot).getData().getIssueLinks().size() > 0)) {
			((DefaultCMJSnapshot) snapshot).getData().getIssueLinks().forEach(issueLink -> {
				String dstProjectKey = issueLink.getDestinationProjectKey();
				String scrProjectKey = issueLink.getSourceProjectKey();
				if (pKeys.containsKey(scrProjectKey)) {
					issueLink.setSourceProjectKey(pKeys.get(scrProjectKey));
				}
				if (pKeys.containsKey(dstProjectKey)) {
					issueLink.setDestinationProjectKey(pKeys.get(dstProjectKey));
				}
			});
		}
	}

	@Transform
	public void deleteIssueLinkType() {
		transform(lookup(JiraIssueLinkType.class).byName("risk")).delete();
	}

	@Transform
	public void renameIssueType() {
		transform(lookup(JiraIssueType.class).byName("Sub-task")).renameTo("Sub-Task");
	}

	@Transform
	public void renameResolution() {
		transform(lookup(JiraIssueResolution.class).byName("Won't Do")).renameTo("Won't do");
	}

	@Transform
	public void modifyStatuses() {
		transform(lookup(JiraIssueStatus.class).byName("Open")).modify(status -> {
			status.setIconUrl("/images/icons/statuses/generic.png");
		});
		transform(lookup(JiraIssueStatus.class).byName("Testing")).modify(status -> {
			status.setIconUrl("/images/icons/statuses/down.png");
		});
		transform(lookup(JiraIssueStatus.class).byName("In Review")).modify(status -> {
			status.setIconUrl("/images/icons/statuses/generic.png");
		});
		transform(lookup(JiraIssueStatus.class).byName("Approved")).modify(status -> {
			status.setIconUrl("/images/icons/statuses/generic.png");
		});
		transform(lookup(JiraIssueStatus.class).byName("Accepted")).modify(status -> {
			status.setIconUrl("/images/icons/statuses/information.png");
			status.setStatusCategoryKey("done");
		});
		transform(lookup(JiraIssueStatus.class).byName("Pending Approval")).modify(status -> {
			status.setStatusCategoryKey("new");
		});
		transform(lookup(JiraIssueStatus.class).byName("Backlog")).modify(status -> {
			status.setIconUrl("/images/icons/subtask.gif");
		});

		transform(lookup(JiraIssueStatus.class).byName("Rejected")).modify(status -> {
			status.setIconUrl("/images/icons/status_generic.gif");
		});

		transform(lookup(JiraIssueStatus.class).byName("Selected for Development")).modify(status -> {
			status.setIconUrl("/images/icons/subtask.gif");
		});
	}

	@Transform
	public void renameStatus() {
		transform(lookup(JiraIssueStatus.class).byName("Ready For Production")).renameTo("Ready for Production");
	}

	@Transform
	public void deleteGroup() {
		transform(lookup(JiraGroup.class).byName("GROUPNAME")).delete();
	}

	@Transform
	public void renameSystemDashboard() {
		transform(lookup(JiraPortalPage.class).byName("System Dashboard")).modify(dashboard -> {
			dashboard.setName("Source System Dashboard");
			dashboard.setDefault(false);
			dashboard.setOwner(snapshot.lookup(JiraUser.class).byName("some_username").get());
		});
	}

	@Transform
	public void mergeResolutions() {
		transform(lookup(JiraIssueResolution.class).byName("None")).renameTo("Invalid");
		transform(lookup(JiraIssueResolution.class).byNames("Completed", "Repair", "Replace"))
				.mergeResolutionInto(lookup(JiraIssueResolution.class).byName("Done"));
		transform(lookup(JiraIssueResolution.class).byName("Document Change")).renameTo("Task Complete");
		transform(lookup(JiraIssueResolution.class).byName("Fixed")).renameTo("Fixed (bug)");
		transform(lookup(JiraIssueResolution.class).byName("Not an Issue")).renameTo("Expected Behavior");
		transform(lookup(JiraIssueResolution.class).byName("Testing")).renameTo("Validated (QE - Test Cases)");
		transform(lookup(JiraIssueResolution.class).byName("Process Change")).renameTo("Fixed Indirectly");
		transform(lookup(JiraIssueResolution.class).byName("Corrective Action Initiated"))
				.mergeResolutionInto(lookup(JiraIssueResolution.class).byName("Task Complete"));
		transform(lookup(JiraIssueResolution.class).byName("Maintenance"))
				.mergeResolutionInto(lookup(JiraIssueResolution.class).byName("Task Complete"));
		transform(lookup(JiraIssueResolution.class).byName("No Action Taken"))
				.mergeResolutionInto(lookup(JiraIssueResolution.class).byName("Expected Behavior"));
		transform(lookup(JiraIssueResolution.class).byName("Other"))
				.mergeResolutionInto(lookup(JiraIssueResolution.class).byName("Invalid"));
		transform(lookup(JiraIssueResolution.class).byNames("Resource Change", "Training"))
				.mergeResolutionInto(lookup(JiraIssueResolution.class).byName("Fixed Indirectly"));
		transform(lookup(JiraIssueResolution.class).byName("Rework")).renameTo("Regressed");
		transform(lookup(JiraIssueResolution.class).byName("In Development")).renameTo("Verified");
		transform(lookup(JiraIssueResolution.class).byName("Added to backlog")).renameTo("Not Released");
	}

	@Transform
	public void mergePriorities() {
		transform(lookup(JiraIssuePriority.class).byName("Unassigned")).renameTo("PX: Unprioritized");
		transform(lookup(JiraIssuePriority.class).byName("Blocker")).renameTo("P1: Very High");
		transform(lookup(JiraIssuePriority.class).byName("Critical")).renameTo("P2: High");
		transform(lookup(JiraIssuePriority.class).byName("Major")).renameTo("P3: Medium");
		transform(lookup(JiraIssuePriority.class).byName("Minor")).renameTo("P4: Low");
	}

	@Transform
	public void modifyPriorities() {
		transform(lookup(JiraIssuePriority.class).byName("PX: Unprioritized")).modify(priority -> {
			priority.setIconUrl("/images/icons/priorities/trivial2.png");
			priority.setColor("#cccccc");
		});

		transform(lookup(JiraIssuePriority.class).byName("P1: Very High")).modify(priority -> {
			priority.setIconUrl("/images/icons/priorities/blocker2.png");
			priority.setColor("#660000");
		});

		transform(lookup(JiraIssuePriority.class).byName("P2: High")).modify(priority -> {
			priority.setIconUrl("/images/icons/priorities/critical2.png");
		});

		transform(lookup(JiraIssuePriority.class).byName("P3: Medium")).modify(priority -> {
			priority.setIconUrl("/images/icons/priorities/medium.svg");
			priority.setColor("#ffff00");
		});

		transform(lookup(JiraIssuePriority.class).byName("P4: Low")).modify(priority -> {
			priority.setIconUrl("/images/icons/priorities/low2.png");
			priority.setColor("#2E4FA5");
		});
	}
}
