package com.botronsoft.pso.migrations.jira.migration.apps;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

import java.io.File;
import java.util.ArrayDeque;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.UsageCrossReferencer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.jmt.impl.model.GenericModelService;
import com.botronsoft.pso.jmt.model.jira.JiraObjectsRoot;
import com.botronsoft.pso.jmt.model.jira.JiraUser;

public class AppModelTransformer<T extends EObject> {
	private Logger log = LoggerFactory.getLogger(AppModelTransformer.class);
	private GenericModelService<T> modelService;

	interface TransformationMapping {
		Map<String, String> getChangedProjectKeys();

		Map<String, String> getRenamedGroups();

		Map<String, Set<String>> getUsersToMerge();
	}

	public AppModelTransformer(EPackage rootPackage) {
		modelService = new GenericModelService<>(rootPackage);
	}

	protected void persist(File modelFile, T model) {
		File outfile = new File(modelFile.getParentFile(), modelFile.getName() + ".transformed.xml");
		modelService.storeModel(model, outfile);
	}

	protected T load(File modelFile) {
		log.info("Loading model from {}", modelFile.getAbsolutePath());
		return modelService.loadModel(modelFile);
	}

	public void transform(JiraObjectsRoot root, TransformationMapping mapping) {
		// Transform project keys
		Map<String, String> changedProjectKeys = mapping.getChangedProjectKeys();
		root.getProjects().forEach(p -> {
			if (changedProjectKeys.containsKey(p.getKey().toUpperCase())) {
				String newKey = changedProjectKeys.get(p.getKey().toUpperCase());
				log.info("Updating changed project key from {} to {}", p.getKey(), newKey);
				p.setKey(newKey);
			}
		});

		// Transform groups
		Map<String, String> renamedGroups = mapping.getRenamedGroups();
		root.getGroups().forEach(g -> {
			if (renamedGroups.containsKey(g.getName())) {
				String newName = renamedGroups.get(g.getName());
				log.info("Updating changed group name from {} to {}", g.getName(), newName);
				g.setName(newName);
			}
		});

		// Users
		Map<String, Set<String>> toMerge = mapping.getUsersToMerge();
		// Index all users from the model by username
		Map<String, JiraUser> userIndex = root.getUsers().stream().collect(toMap(u -> u.getName().toLowerCase(), Function.identity()));

		// Resolve to model users
		Map<String, Set<JiraUser>> usersToMerge = toMerge.entrySet().parallelStream().collect(toMap(e -> e.getKey().toLowerCase(), e -> {
			return e.getValue().stream().map(name -> {
				JiraUser jiraUser = userIndex.get(name.toLowerCase());
				if (jiraUser == null) {
					log.trace("Can't find user with name {}", name);
				}
				return jiraUser;
			}).filter(u -> u != null).collect(toSet());
		}));

		usersToMerge.forEach((username, sourceUsers) -> {
			if (sourceUsers.isEmpty()) {
				// There're no users found in the model to be merged, so do nothing
				return;
			}
			if (sourceUsers.size() == 1) {
				// This is just 1 source user found, so we're going to rename it instead of merging
				JiraUser srcUser = sourceUsers.iterator().next();
				log.info("Renaming user {} to {}", srcUser.getName(), username);
				srcUser.setName(username);
				return;
			}

			// More than one source users should be merged into TARGET
			// First try to locate the target user (if exists)
			ArrayDeque<JiraUser> src = new ArrayDeque<>(sourceUsers);
			final JiraUser targetUser = Optional.ofNullable(userIndex.get(username.toLowerCase())).orElseGet(() -> {
				// Rename the first user in the set
				JiraUser u = src.removeFirst();
				u.setName(username);
				return u;
			});

			// Redirect all references to the target user
			UsageCrossReferencer.findAll(src, root).forEach((user, refs) -> {
				// Replace all refs
				log.info("Replacing {} refs to user {} with user {}", refs.size(), ((JiraUser) user).getName(), targetUser.getName());
				refs.stream().forEach(ref -> {
					EcoreUtil.replace(ref, user, targetUser);
				});
			});
			src.forEach(mergedUser -> {
				EcoreUtil.delete(mergedUser);
			});
		});
	}

}
