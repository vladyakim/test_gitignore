package com.botronsoft.pso.migrations.jira.reports;

import static java.util.stream.Collectors.toList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.jmt.reporting.model.customfield.BoardInfo;
import com.botronsoft.pso.jmt.reporting.model.customfield.DashboardInfo;
import com.botronsoft.pso.jmt.reporting.model.customfield.SearchRequestInfo;
import com.botronsoft.pso.migration.framework.environment.Environment;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class FilterAgileBoardDashboardMappingReport {
	private static final Logger log = LoggerFactory.getLogger(FilterAgileBoardDashboardMappingReport.class);
	private static final String AGILEBOARD = "Agile Board";
	private static final String DASHBOARD = "Dashboard";
	private static final String FILTER = "Filter";

	public static void main(String[] args) throws Exception {

		// /*collectDetails();*/
		// Map<String, Map<Long, Long>> boardMapping = objectMappingPerInstance("AgileBoard");
		// Map<String, Map<Long, Long>> filterMapping = objectMappingPerInstance("JiraFilter");
		// Map<String, Map<Long, Long>> dashboardMapping = objectMappingPerInstance("JiraPortalPage");

		Map<Long, Long> boardMap = new HashMap<Long, Long>();
		Map<Long, Long> filterMap = new HashMap<Long, Long>();
		Map<Long, Long> dashboardMap = new HashMap<Long, Long>();

		MappingFactory mappingFactoryReader = new FileSystemMappingFactory(Env.SOURCE_JIRA_ENV.getKey());

		MapperReader mappingReader = mappingFactoryReader.createReader();

		mappingReader.map(ObjectTypeKeys.BOARDID)
				.forEach((k, v) -> boardMap.put(Long.parseLong(k), Long.parseLong(v.stream().collect(Collectors.joining(",")))));

		mappingReader.map(ObjectTypeKeys.DASHBOARDID)
				.forEach((k, v) -> dashboardMap.put(Long.parseLong(k), Long.parseLong(v.stream().collect(Collectors.joining(",")))));

		mappingReader.map(ObjectTypeKeys.FILTERID)
				.forEach((k, v) -> filterMap.put(Long.parseLong(k), Long.parseLong(v.stream().collect(Collectors.joining(",")))));

		List<SearchRequestInfo> sourceFilterDetails = Env.SOURCE_JIRA_ENV.getJmtReportingClient().collectFilters().parallelStream()
				.filter(item -> filterMap.keySet().contains(item.getId())).collect(toList());
		List<BoardInfo> sourceBoardDetails = Env.SOURCE_JIRA_ENV.getJmtReportingClient().collectAgileBoards().parallelStream()
				.filter(item -> boardMap.keySet().contains(item.getId())).collect(toList());
		List<DashboardInfo> sourceDashboardDetails = Env.SOURCE_JIRA_ENV.getJmtReportingClient().collectDashboards().parallelStream()
				.filter(item -> dashboardMap.keySet().contains(item.getId())).collect(toList());

		List<SearchRequestInfo> targetFilterDetails = Env.TARGET_JIRA_ENV.getJmtReportingClient().collectFilters().parallelStream()
				.filter(item -> filterMap.values().contains(item.getId())).collect(toList());
		List<BoardInfo> targetBoardDetails = Env.TARGET_JIRA_ENV.getJmtReportingClient().collectAgileBoards().parallelStream()
				.filter(item -> boardMap.values().contains(item.getId())).collect(toList());
		List<DashboardInfo> targetDashboardDetails = Env.TARGET_JIRA_ENV.getJmtReportingClient().collectDashboards().parallelStream()
				.filter(item -> dashboardMap.values().contains(item.getId())).collect(toList());

		List<ReportedItem> report = new ArrayList<FilterAgileBoardDashboardMappingReport.ReportedItem>();

		sourceFilterDetails.stream().filter(item -> filterMap.containsKey(item.getId())).forEach(filter -> {
			FilterAgileBoardDashboardMappingReport outClass = new FilterAgileBoardDashboardMappingReport();
			FilterAgileBoardDashboardMappingReport.ReportedItem reportedItem = outClass.new ReportedItem();

			SearchRequestInfo targetFilter = targetFilterDetails.stream()
					.filter(targetItem -> filterMap.get(filter.getId()) == targetItem.getId()).findFirst().get();

			reportedItem.setSrcInstance(filter.getBaseUrl());
			reportedItem.setObjectType(FILTER);
			reportedItem.setAuthor(filter.getOwnerUsername());
			reportedItem.setName(filter.getName());
			reportedItem.setNewName(filter.getName().equalsIgnoreCase(targetFilter.getName()) ? "N/A" : targetFilter.getName());
			reportedItem.setSrcUrl(filter.getBaseUrl() + getUrlType(FILTER) + filter.getId());
			reportedItem.setDstUrl(targetFilter.getBaseUrl() + getUrlType(FILTER) + targetFilter.getId());

			report.add(reportedItem);

		});

		sourceBoardDetails.stream().filter(item -> boardMap.containsKey(item.getId())).forEach(board -> {
			FilterAgileBoardDashboardMappingReport outClass = new FilterAgileBoardDashboardMappingReport();
			FilterAgileBoardDashboardMappingReport.ReportedItem reportedItem = outClass.new ReportedItem();

			BoardInfo targetBoard = targetBoardDetails.stream().filter(targetItem -> boardMap.get(board.getId()) == targetItem.getId())
					.findFirst().get();

			reportedItem.setSrcInstance(board.getBaseUrl());
			reportedItem.setObjectType(AGILEBOARD);
			reportedItem.setAuthor(board.getOwnerUsername());
			reportedItem.setName(board.getName());
			reportedItem.setNewName(board.getName().equalsIgnoreCase(targetBoard.getName()) ? "N/A" : targetBoard.getName());
			reportedItem.setSrcUrl(board.getBaseUrl() + getUrlType(AGILEBOARD) + board.getId());
			reportedItem.setDstUrl(targetBoard.getBaseUrl() + getUrlType(AGILEBOARD) + targetBoard.getId());

			report.add(reportedItem);

		});

		sourceDashboardDetails.stream().filter(item -> dashboardMap.containsKey(item.getId())).forEach(dashboard -> {
			FilterAgileBoardDashboardMappingReport outClass = new FilterAgileBoardDashboardMappingReport();
			FilterAgileBoardDashboardMappingReport.ReportedItem reportedItem = outClass.new ReportedItem();

			DashboardInfo targetBoard = targetDashboardDetails.stream()
					.filter(targetItem -> dashboardMap.get(dashboard.getId()) == targetItem.getId()).findFirst().get();

			reportedItem.setSrcInstance(dashboard.getBaseUrl());
			reportedItem.setObjectType(DASHBOARD);
			reportedItem.setAuthor(dashboard.getOwnerUsername());
			reportedItem.setName(dashboard.getName());
			reportedItem.setNewName(dashboard.getName().equalsIgnoreCase(targetBoard.getName()) ? "N/A" : targetBoard.getName());
			reportedItem.setSrcUrl(dashboard.getBaseUrl() + getUrlType(DASHBOARD) + dashboard.getId());
			reportedItem.setDstUrl(targetBoard.getBaseUrl() + getUrlType(DASHBOARD) + targetBoard.getId());

			report.add(reportedItem);

		});

		System.out.println("||Source instance||Type||Source owner||Name||New name (if renamed on TARGET)||Source URL||TARGET URL||");

		report.forEach(e -> {
			System.out.println(
					String.format("|%s|%s|%s|%s|%s|%s|%s|", e.srcInstance, e.objectType, e.author, e.name, e.newName, e.srcUrl, e.dstUrl));
		});

	}

	static Map<String, Map<Long, Long>> objectMappingPerInstance(String objectType) {
		File inputDir = Environment.LOCAL.getFile("work", "Reports");
		Map<String, Map<Long, Long>> result = new HashMap<String, Map<Long, Long>>();

		try (Stream<Path> paths = Files.walk(Paths.get(inputDir.getAbsolutePath()))) {
			List<File> list = paths.filter(Files::isRegularFile).map(Path::toFile).collect(Collectors.toList()).stream()
					.filter(e -> e.getName().startsWith("deploymentSnapshotToTargetNativeIdMap")).collect(Collectors.toList());

			list.forEach(file -> {

				String path = file.getAbsolutePath();
				BufferedReader bufferedReader;
				try {
					bufferedReader = new BufferedReader(new FileReader(path));
					Gson gson = new Gson();
					HashMap<String, Map<String, String>> json = gson.fromJson(bufferedReader, HashMap.class);
					Map<String, String> map = json.get(objectType);
					result.put(file.getName().split("_")[1], map.entrySet().stream()
							.collect(Collectors.toMap(e -> Long.parseLong(e.getKey()), e -> Long.parseLong(e.getValue()))));

				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			});

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	static String getUrlType(String type) {
		switch (type) {
		case AGILEBOARD:
			return "/secure/RapidBoard.jspa?rapidView=";
		case FILTER:
			return "/issues/?filter=";
		case DASHBOARD:
			return "/secure/Dashboard.jspa?selectPageId=";
		}
		throw new IllegalArgumentException("Unkonwn type!");
	}

	// Can be based on something from migration.properties
	static String getInstanceShortName(String baseUrl) {
		switch (baseUrl) {
		case "SOURCEjira.CONPANY.com":
			return "SOURCE";
		case "NEWjira.CONPANY.com":
			return "TARGET";
		}
		throw new IllegalArgumentException("Unkonwn INSTANCE!");
	}

	static void dumpIntoLocalJson(List<?> collection, File output) throws IOException {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Files.write(output.toPath(), gson.toJson(collection).getBytes());
	}

	class ReportedItem {
		private String srcInstance;
		private String objectType;
		private String name;
		private String newName;
		private String author;
		private String srcUrl;
		private String dstUrl;

		public void setAuthor(String author) {
			this.author = author;
		}

		public void setSrcInstance(String srcInstance) {
			this.srcInstance = srcInstance;
		}

		public void setObjectType(String objectType) {
			this.objectType = objectType;
		}

		public void setName(String name) {
			this.name = name;
		}

		public void setNewName(String newName) {
			this.newName = newName;
		}

		public void setSrcUrl(String srcUrl) {
			this.srcUrl = srcUrl;
		}

		public void setDstUrl(String dstUrl) {
			this.dstUrl = dstUrl;
		}
	}

}
