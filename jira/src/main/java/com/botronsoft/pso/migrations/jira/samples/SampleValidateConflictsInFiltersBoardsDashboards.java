package com.botronsoft.pso.migrations.jira.samples;

import org.junit.runner.RunWith;

import com.botronsoft.pso.migration.framework.transform.run.AbstractSnapshotProcessorRunnerSpecBuilder;
import com.botronsoft.pso.migration.framework.transform.run.JUnitSnapshotProcessorRunner;
import com.botronsoft.pso.migration.framework.transform.spec.RunSpec;
import com.botronsoft.pso.migrations.jira.snapshot.transform.AMSFiltersBoardsDashboards;

/**
 * All classes under {@code package com.botronsoft.pso.migrations.jira.samples} are for reference only Will be removed once any useful code
 * within is moved where appropriate
 */
@Deprecated
// This runner is required if you want to run as JUnit...
@RunWith(JUnitSnapshotProcessorRunner.class)
public class SampleValidateConflictsInFiltersBoardsDashboards extends AbstractSnapshotProcessorRunnerSpecBuilder {

	private static final String TARGET_SNAPSHOT = "lfs_system_prior_deploy.zip";
	private static final String SOURCE_SNAPSHOT = "Source_projects_full.transformed.zip";

	@RunSpec
	public void configure() {
		snapshots(SOURCE_SNAPSHOT, TARGET_SNAPSHOT).listener(AMSFiltersBoardsDashboards.class);
	}

}
