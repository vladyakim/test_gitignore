package com.botronsoft.pso.migrations.jira.reports;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.javatuples.Triplet;

import com.botronsoft.pso.jmt.reporting.model.customfield.UserPermissionInfo;
import com.botronsoft.pso.jmt.rest.client.reporting.ReportingClient;
import com.botronsoft.pso.migrations.Env;
import com.google.common.collect.MapDifference;
import com.google.common.collect.MapDifference.ValueDifference;
import com.google.common.collect.Maps;
import com.opencsv.CSVWriter;
import com.opencsv.ICSVWriter;

public class UserPermissionReport {

	public static void main(String[] args) throws Exception {

		ReportingClient sourceReportingClient = Env.SOURCE_JIRA_ENV.getJmtReportingClient();
		ReportingClient targetReportingClient = Env.TARGET_JIRA_ENV.getJmtReportingClient();

		List<UserPermissionInfo> sourceUsersInfo = sourceReportingClient.collectUserPermissions();
		List<UserPermissionInfo> targetUsersInfo = targetReportingClient.collectUserPermissions();

		/*
		 * Here we group first on Project Keys, after in the Map Value we do a second grouping by Permission , which gives as list of user
		 * names for give permissons so the end result is Map with keys Project Keys, and for each Project Key there is Map with keys the
		 * Permissons for that Project and list of users have each of that Permissons
		 */

		// TODO: After we agree on the mapping strategy to add method for resolving target PKEYS and UserNames on Target

		Map<String, Map<String, List<String>>> sorceObjects = sourceUsersInfo.stream()
				.collect(Collectors.groupingBy(UserPermissionInfo::getProjectKey, Collectors.groupingBy(UserPermissionInfo::getType,
						Collectors.mapping(UserPermissionInfo::getUsername, Collectors.toList()))));

		Map<String, Map<String, List<String>>> targetObjects = targetUsersInfo.stream()
				.collect(Collectors.groupingBy(UserPermissionInfo::getProjectKey, Collectors.groupingBy(UserPermissionInfo::getType,
						Collectors.mapping(UserPermissionInfo::getUsername, Collectors.toList()))));

		/*
		 * here we use the Guava Map Difference functionality to compare both maps above for common and differing entries , more informaton
		 * on that on that link https://www.baeldung.com/java-compare-hashmaps#map-difference-using-guava
		 */

		MapDifference<String, Map<String, List<String>>> diff = Maps.difference(sorceObjects, targetObjects);
		Map<String, Map<String, List<String>>> sourceAndTarget = diff.entriesInCommon();
		Map<String, Map<String, List<String>>> sourceOnly = diff.entriesOnlyOnLeft();
		Map<String, Map<String, List<String>>> targetOnly = diff.entriesOnlyOnRight();
		Map<String, ValueDifference<Map<String, List<String>>>> permissionsDifference = diff.entriesDiffering();

		// the File where the results will be saved, default is Source Project Dir on FileSystem
		// you can tune it by your preferences
		FileWriter csvOutputFile = new FileWriter("report.csv");

		CSVWriter writer = new CSVWriter(csvOutputFile, ICSVWriter.DEFAULT_SEPARATOR, ICSVWriter.NO_QUOTE_CHARACTER,
				ICSVWriter.NO_ESCAPE_CHARACTER, ICSVWriter.DEFAULT_LINE_END);

		/*
		 * Here we create our CSV headers: the idea is that for each project key and permission we check if users have rights for source
		 * target and e.g lets say user John has permisson DELETE_ISSUES for Project ISL , on source , but not target, then for this project
		 * key and that permisson user goes into "User Losing Permission on Target" column etc.
		 *
		 */
		String[] header = { "ProjectKey", "Permission", "SOURCE", "TARGET", "Users in common", "User Losing Permission on Target",
				"User Gaining Permission on Target" };

		writer.writeNext(header);

		sourceAndTarget.forEach((s, entry) -> entry.forEach((s1, innerEntry) -> {
			String[] headerSourceOnly = { s, s1, "YES", "YES", innerEntry.stream().collect(Collectors.joining(";")), "", "" };
			writer.writeNext(headerSourceOnly);
		}));

		sourceOnly.forEach((s, entry) -> entry.forEach((s1, innerEntry) -> {
			String[] headerSourceOnly = { s, s1, "YES", "NO", "", innerEntry.stream().collect(Collectors.joining(";")), "" };
			writer.writeNext(headerSourceOnly);
		}));

		targetOnly.forEach((s, entry) -> entry.forEach((s1, innerEntry) -> {
			String[] headerTargetOnly = { s, s1, "NO", "YES", "", "", innerEntry.stream().collect(Collectors.joining(";")) };
			writer.writeNext(headerTargetOnly);
		}));

		permissionsDifference.forEach((s, entry) -> {
			Maps.difference(entry.leftValue(), entry.rightValue()).entriesOnlyOnRight().forEach((k, entry1) -> {

				String[] headerTargetOnly = { s, k, "NO", "YES", "", "", entry1.stream().collect(Collectors.joining(";")) };
				writer.writeNext(headerTargetOnly);

			});

			Maps.difference(entry.leftValue(), entry.rightValue()).entriesOnlyOnLeft().forEach((k, entry1) -> {

				String[] headerTargetOnly = { s, k, "YES", "NO", "", entry1.stream().collect(Collectors.joining(";")), "" };
				writer.writeNext(headerTargetOnly);

			});

			Maps.difference(entry.leftValue(), entry.rightValue()).entriesDiffering().forEach((k, entry1) -> {

				Triplet<String, String, String> helperTriplet = getCommonAndDifferentEntries(entry1.leftValue(), entry1.rightValue());

				String[] headerTargetOnly = { s, k, "YES", "YES", helperTriplet.getValue1(), helperTriplet.getValue0(),
						helperTriplet.getValue2() };
				writer.writeNext(headerTargetOnly);

			});

			Maps.difference(entry.leftValue(), entry.rightValue()).entriesInCommon().forEach((k, entry1) -> {

				String[] headerTargetOnly = { s, k, "YES", "YES", entry1.stream().collect(Collectors.joining(";")), "", "" };
				writer.writeNext(headerTargetOnly);

			});

		});

		writer.close();
		csvOutputFile.close();
	}

	/*
	 * convenience Method to compare list of users using common project permission on source and target the idea is that it will always
	 * produce Triplet where you can take the users that have that permission for respective instance if there are no users for this
	 * permission on a given instance, it will simply return empty String , for example if the source users dont have that permission on
	 * left it will be empty string (Triplet.value0)
	 *
	 */
	public static Triplet<String, String, String> getCommonAndDifferentEntries(List<String> listSource, List<String> listTarget) {

		List<String> listMiddle = new ArrayList<>(listSource);

		listMiddle.retainAll(listTarget);

		listSource.removeAll(listTarget);
		listTarget.removeAll(listSource);

		Triplet<String, String, String> result = Triplet.with(listSource.stream().collect(Collectors.joining(";")),
				listMiddle.stream().collect(Collectors.joining(";")), listTarget.stream().collect(Collectors.joining(";")));

		return result;

	}
}
