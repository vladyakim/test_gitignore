package com.botronsoft.pso.migrations.jira.migration.links;

import static java.util.stream.Collectors.toMap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.javatuples.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MapperWriter;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.jmt.reporting.model.customfield.AttachmentInfo;
import com.botronsoft.pso.jmt.reporting.model.customfield.IssueCommentInfo;
import com.botronsoft.pso.jmt.reporting.model.customfield.IssueInfo;
import com.botronsoft.pso.jmt.reporting.model.project.ProjectInfo;
import com.botronsoft.pso.jmt.reporting.model.user.UserMappedEntry;
import com.botronsoft.pso.jmt.rest.client.reporting.ReportingClient;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migration.framework.environment.JiraEnvironment;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

/**
 * Generates JIRA mapping csv files used by URL Replacer
 */
public class LinkUpdaterMappingGenerator {

	private static Map<String, String> pairs = new HashMap<String, String>() {
		{
			put("AgileBoard", ObjectTypeKeys.BOARDID);
			put("JiraCustomField", ObjectTypeKeys.CUSTOMFIELD);
			put("JiraProjectComponent", ObjectTypeKeys.COMPONENT);
			put("JiraPortalPage", ObjectTypeKeys.DASHBOARDID);
			put("JiraFilter", ObjectTypeKeys.FILTERID);
			put("JiraSDServiceDesk", ObjectTypeKeys.SERVICEDESKPORTAL);
			put("AgileSprint", ObjectTypeKeys.SPRINT);
			put("JiraIssueType", ObjectTypeKeys.ISSUETYPE);
			put("JiraIssuePriority", ObjectTypeKeys.PRIORITY);
			put("JiraProject", ObjectTypeKeys.PROJECTID);
			put("JiraIssueResolution", ObjectTypeKeys.RESOLUTION);
			put("JiraIssueStatus", ObjectTypeKeys.STATUS);
			put("JiraProjectVersion", ObjectTypeKeys.VERSIONID);
		}
	};;

	private static Pair<ReportingClient, ReportingClient> sourceTarget = new Pair<>(Env.SOURCE_JIRA_ENV.getJmtReportingClient(),
			Env.TARGET_JIRA_ENV.getJmtReportingClient());

	static URL url = ClassLoader.class.getResource("/jira/mappings");
	// File parentDirectory = new File(url.getPath());
	static File linkUpdate = new File(url.getPath());
	private static final Logger log = LoggerFactory.getLogger(LinkUpdaterMappingGenerator.class);

	public static void main(String[] args) throws Exception {

		// to create folder prior mapping generation if not yet available
		linkUpdate.mkdirs();

		generateMappingCsvFiles();

	}

	/**
	 * Method previously used to dump data issue, attachment, comment details in local files and use the files to build mapping. Currently
	 * {@link #generateMappingCsvFiles()} use REST resources to load collection of beans instead. In case of performance issues with
	 * {@link #generateMappingCsvFiles()} this can be used instead.
	 */
	@Deprecated
	static void dumpInputIntoLocalFiles() {
		Collection<JiraEnvironment> environments = Environments.jira(Env.SOURCE_INSTANCES);
		environments.add(Env.TARGET_JIRA_ENV);

		URL url = ClassLoader.class.getResource("/jira/LinksUpdater/input");
		// File parentDirectory = new File(url.getPath());
		File linkUpdateFolder = new File(url.getPath());
		// File linkUpdateFolder = Env.LOCAL_ENV.workfile("LinksUpdater/input");
		linkUpdateFolder.mkdirs();
		environments.stream().forEach(env -> {
			try {

				log.info("Dumping attachments info from {}", env.getName());
				env.getJmtReportingClient().collectAttachmentInfo(new File(linkUpdateFolder, "/" + env.getName() + "_Attachments.csv.gz"));
				log.info("Dumping issues info from {}", env.getName());
				env.getJmtReportingClient().collectIssueInfo(new File(linkUpdateFolder, "/" + env.getName() + "_Issues.csv.gz"));
				log.info("Dumping issue comments info from {}", env.getName());
				env.getJmtReportingClient()
						.collectIssueCommentInfo(new File(linkUpdateFolder, "/" + env.getName() + "_IssueComments.csv.gz"));
			} catch (Throwable t) {
				log.error("Error in dump from " + env.getName(), t);
			}
		});

	}

	static void generateMappingCsvFiles() throws URISyntaxException {
		Map<String, String> mappingErrors = new HashMap<String, String>();

		// here we fetch the url to the /jira/mappings folder in commons mapping project
		URI url = ClassLoader.class.getResource("/jira/mappings").toURI();

		MappingFactory mappingFactoryReader = new FileSystemMappingFactory(replaceResourcePath(Paths.get(url).toString()));
		MapperReader mappingReader = mappingFactoryReader.createReader();

		MapperWriter mappingWriter = mappingFactoryReader.createWriter();
		// here we generate the majority if the mapping files for jira objects
		// based on in .json file in common mappins,you can look for the name of the objects
		// in the pairs map
		pairs.forEach((g, h) -> {
			Map<String, String> interminanetMap = generateMappingFromCMJJson(g);
			interminanetMap.forEach((k, v) -> mappingWriter.writeMap(h, k, v));
		});
		// map needed in the generation of issuemap
		Map<String, List<String>> projectMap = mappingReader.map(ObjectTypeKeys.PROJECTKEY);

		// load map of all filters with changed names as filters might be referenced in URLs by name
		Map<String, List<String>> filterNameMap = mappingReader.map(ObjectTypeKeys.FILTERID);

		// All instance mapping generation
		// load latest user mapping json
		File file = new File(ClassLoader.class.getResource("/jira/user_management").getPath(), "/users_" + Env.SOURCE + "_mapped.json");
		Map<Long, Long> issueMap = generateIssueMap(sourceTarget, projectMap);

		Collection<UserMappedEntry> userData = loadCollectionFromJson(file);
		Map<String, String> usersMap = userData.stream().collect(toMap(UserMappedEntry::getUsername, UserMappedEntry::getDstUsername));

		// Writing maps to mapping files

		issueMap.forEach((k, v) -> mappingWriter.writeMap(ObjectTypeKeys.ISSUEID, String.valueOf(k), String.valueOf(v)));
		generateAttachementsMap(sourceTarget, issueMap)
				.forEach((k, v) -> mappingWriter.writeMap(ObjectTypeKeys.JIRAATTACHMENTID, String.valueOf(k), String.valueOf(v)));

		generateCommentsMap(sourceTarget, issueMap)
				.forEach((k, v) -> mappingWriter.writeMap(ObjectTypeKeys.COMMENTID, String.valueOf(k), String.valueOf(v)));

		usersMap.forEach((k, v) -> mappingWriter.writeMap(ObjectTypeKeys.JIRAUSERNAME, k, v));
		generateProjectMap(sourceTarget).forEach((k, v) -> mappingWriter.writeMap(ObjectTypeKeys.PROJECTKEY, k, v));
		// writeMapToFile(filterNameMap, getOutFile("project_filter_names"));

		filterNameMap.forEach(
				(k, v) -> mappingWriter.writeMap(ObjectTypeKeys.PROJECTFILTERNAME, k, v.stream().collect(Collectors.joining(","))));

		mappingErrors.forEach((k, v) -> mappingWriter.writeMap(ObjectTypeKeys.MAPERROR, k, v));

		log.info("Мapping generation DONE!");
	}

	private static Collection<UserMappedEntry> loadCollectionFromJson(File file) {
		try (FileReader jsonReader = new FileReader(file)) {
			return new GsonBuilder().create().fromJson(jsonReader, new TypeToken<Collection<UserMappedEntry>>() {
			}.getType());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static void writeMapToFile(Map<?, ?> map, File output) {
		try (PrintWriter w = new PrintWriter(output)) {
			map.forEach((k, v) -> {
				w.println(k + ";" + v);
			});
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static Map<String, String> generateMappingFromCMJJson(String objectName) {

		URL url = ClassLoader.class.getResource("/jira/mappings");
		// File parentDirectory = new File(url.getPath());
		File linkUpdateFolder = new File(url.getPath());
		File[] jsonFiles = linkUpdateFolder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith("deploymentSnapshotToTargetNativeIdMap_") && name.endsWith(".json");
			}
		});
		Map<String, String> objectMap = new HashMap<String, String>();
		Lists.newArrayList(jsonFiles).stream().forEach(file -> {
			JsonElement root;
			try {
				root = new JsonParser().parse(Files.toString(file, Charset.defaultCharset()));
				JsonElement object = root.getAsJsonObject().get(objectName);
				if (object != null) {
					Set<Entry<String, JsonElement>> entrySet = object.getAsJsonObject().entrySet();
					Map<String, String> map = new HashMap<String, String>();
					if (true) {
						map = entrySet.stream().collect(toMap(e -> e.getKey().replace("customfield_", ""),
								e -> e.getValue().getAsString().replace("customfield_", "")));
					} else {
						map = entrySet.stream().collect(toMap(e -> e.getKey(), e -> e.getValue().getAsString()));
					}
					objectMap.putAll(map);
				}
			} catch (JsonSyntaxException | IOException e) {
				throw new RuntimeException(e);
			}
		});
		return objectMap;
	}

	// Generates the issueMap using the REST clients and the previously generated projectMap
	private static Map<Long, Long> generateIssueMap(Pair<ReportingClient, ReportingClient> sourceTarget,
			Map<String, List<String>> projectMap) {

		List<IssueInfo> srcIssues = sourceTarget.getValue0().collectIssueInfo();
		Map<Long, Long> issueMap = new HashMap<Long, Long>();

		Map<String, String> mappingErrors = new HashMap<String, String>();

		List<IssueInfo> targetIssues = sourceTarget.getValue1().collectIssueInfo();

		Map<String, Long> targetIssueMap = targetIssues.stream().collect(toMap(IssueInfo::getKey, IssueInfo::getIssueId));

		srcIssues.forEach(i -> {

			Long targetIssueId = targetIssueMap
					.get((projectMap.get(i.getProjectKey()) == null ? i.getProjectKey() : projectMap.get(i.getProjectKey())) + "-"
							+ i.getIssueNumber());
			if (targetIssueId == null) {
				String errorMsg = String.format("Mapping not found for source issue key: %s", i.getKey());
				mappingErrors.put(errorMsg, "issue");
			} else {
				issueMap.put(i.getIssueId(), targetIssueId);
			}
		});

		return issueMap;

	}

	// method to generate the comment ID's mapping between target and source using rest client and prevously generated issueMap
	private static Map<Long, Long> generateCommentsMap(Pair<ReportingClient, ReportingClient> sourceTarget, Map<Long, Long> issueMap) {

		Map<Long, Long> commentsMap = new HashMap<Long, Long>();
		Map<String, String> mappingErrors = new HashMap<String, String>();

		List<IssueCommentInfo> targetIssueComments = sourceTarget.getValue1().collectIssueCommentInfo();
		List<IssueInfo> srcIssues = sourceTarget.getValue0().collectIssueInfo();

		Map<String, Long> targetCommentsMap = targetIssueComments.stream()
				.collect(toMap(IssueCommentInfo::getKey, IssueCommentInfo::getCommentId));

		List<IssueCommentInfo> srcComments = sourceTarget.getValue0().collectIssueCommentInfo();

		srcComments.forEach(c -> {
			Long targetCommentInfo = null;
			if (issueMap.get(c.getIssueId()) == null) {
				// In case of partial migration login below can be disabled. As it would throw error for each not migrated issue
				String errorMsg = String.format("Unmapped comment entry for instance: %s and issue: %s", c.getInstance(), c.getIssueId());
				mappingErrors.put(errorMsg, "comment");
			} else {
				String scrCommentMappingKey = issueMap.get(c.getIssueId()) + "" + c.getCommentLength() + "" + c.getCreationDate();
				targetCommentInfo = targetCommentsMap.get(scrCommentMappingKey);
			}

			if (targetCommentInfo != null) {
				commentsMap.put(c.getCommentId(), targetCommentInfo);
			} else {
				Optional<IssueInfo> issueWithMissingComments = srcIssues.stream().filter(comment -> comment.getIssueId() == c.getIssueId())
						.collect(Collectors.toList()).stream().findFirst();

				issueWithMissingComments.ifPresent(issue -> {
					String errorMsg = String.format("Missing comment on TARGET for source issue: %s created by %s with internal id %s",
							issue.getKey(), c.getAuthorKey(), c.getCommentId());
					mappingErrors.put(errorMsg, "comment");
				});
			}

		});

		return commentsMap;

	}

	// generates the map of source and target Attachements ID's using the REST endpoint and the previously generated IssueMap

	private static Map<Long, Long> generateAttachementsMap(Pair<ReportingClient, ReportingClient> sourceTarget, Map<Long, Long> issueMap) {

		Map<Long, Long> attachmentsMap = new HashMap<Long, Long>();

		Map<String, String> mappingErrors = new HashMap<String, String>();
		List<IssueInfo> srcIssues = sourceTarget.getValue0().collectIssueInfo();
		List<AttachmentInfo> targetAttachments = sourceTarget.getValue1().collectAttachmentInfo();
		Map<String, Long> targetAttachmentsMap = targetAttachments.stream()
				.collect(toMap(AttachmentInfo::getKey, AttachmentInfo::getAttachmentId));

		List<AttachmentInfo> srcAttachments = sourceTarget.getValue0().collectAttachmentInfo();

		srcAttachments.forEach(a -> {

			Long targetAttachmentId = null;

			if (issueMap.get(a.getIssueId()) == null) {
				// In case of partial migration login below can be disabled. As it would throw error for each not migrated issue
				String errorMsg = String.format("Unmapped attachment entry for instance: %s and issue: %s", a.getInstance(),
						a.getIssueId());
				mappingErrors.put(errorMsg, "attachment");
			} else {
				String scrAttachmentMappingKey = issueMap.get(a.getIssueId()) + a.getCreationDate() + a.getFilename() + a.getFilesize();
				targetAttachmentId = targetAttachmentsMap.get(scrAttachmentMappingKey);
			}

			if (targetAttachmentId != null) {
				attachmentsMap.put(a.getAttachmentId(), targetAttachmentId);
			} else {
				Optional<IssueInfo> issueWithMissingAttachment = srcIssues.stream().filter(issue -> issue.getIssueId() == a.getIssueId())
						.collect(Collectors.toList()).stream().findFirst();

				issueWithMissingAttachment.ifPresent(issue -> {
					String errorMsg = String.format("Missing attachment on TARGET for source issue: %s with internal id %s", issue.getKey(),
							a.getAttachmentId());
					mappingErrors.put(errorMsg, "attachment");
				});

			}

		});

		return attachmentsMap;

	}

	// generates the map of source and target Projects using the REST endpoint
	private static Map<String, String> generateProjectMap(Pair<ReportingClient, ReportingClient> sourceTarget) {

		Map<Long, String> sourceProjects = sourceTarget.getValue0().getProjects().stream()
				.collect(toMap(ProjectInfo::getId, ProjectInfo::getKey));
		Map<Long, String> targetProjects = sourceTarget.getValue1().getProjects().stream()
				.collect(toMap(ProjectInfo::getId, ProjectInfo::getKey));

		Map<String, String> allImportedProjectMap = generateMappingFromCMJJson("JiraProject");
		allImportedProjectMap.entrySet().stream().collect(toMap(entry -> sourceProjects.get(Long.valueOf(entry.getKey())),
				entry -> targetProjects.get(Long.valueOf(entry.getValue()))));

		return allImportedProjectMap;

	}

	// Utility Method to navigate to correct resource folder based on OS
	private static String replaceResourcePath(String path) {
		String OS = System.getProperty("os.name").toLowerCase();
		if (OS.contains("windows")) {

			return path.replace("\\mapping\\target\\classes\\jira\\mappings", "\\mapping\\src\\main\\resources\\jira\\mappings");

		}

		return path.replace("/mapping/target/classes/jira/mappings", "/mapping/src/main/resources/jira/mappings");

	}
}
