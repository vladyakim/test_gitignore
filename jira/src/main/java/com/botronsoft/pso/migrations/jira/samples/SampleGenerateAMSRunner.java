package com.botronsoft.pso.migrations.jira.samples;

import org.junit.runner.RunWith;

import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migration.framework.transform.run.AbstractSnapshotProcessorRunnerSpecBuilder;
import com.botronsoft.pso.migration.framework.transform.run.JUnitSnapshotProcessorRunner;
import com.botronsoft.pso.migration.framework.transform.spec.RunSpec;
import com.botronsoft.pso.migrations.jira.snapshot.transform.AMSFiltersBoardsDashboards;

/**
 * All classes under {@code package com.botronsoft.pso.migrations.jira.samples} are for reference only Will be removed once any useful code
 * within is moved where appropriate
 */
@Deprecated
// This runner is required if you want to run as JUnit...
@RunWith(JUnitSnapshotProcessorRunner.class)
public class SampleGenerateAMSRunner extends AbstractSnapshotProcessorRunnerSpecBuilder {

	@RunSpec
	public void configure() {
		snapshots("Source_projects_config.zip", "Source_projects_config.zip").workdir(Environments.local().workfile("Snapshots"))
				.listener(AMSFiltersBoardsDashboards.class);
	}

}
