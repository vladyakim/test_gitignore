	package com.botronsoft.pso.migrations.jira.scripts.remote

import java.util.concurrent.atomic.AtomicLong

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.entity.Select
import com.atlassian.jira.entity.Select.SelectColumnsFromContext
import com.atlassian.jira.issue.AttachmentManager
import com.atlassian.jira.issue.attachment.Attachment
import com.atlassian.jira.issue.attachment.AttachmentConstants
import com.atlassian.jira.issue.attachment.AttachmentKeys
import com.atlassian.jira.issue.attachment.FileSystemAttachmentStore

/**
 * Class providing single method that checks all issue attachments and returns information
 * about any attachment missing in attachment storage location.
 * <p/>Executed via ScriptRunner
 * <p/><b/>Uncomment class init and method call
 *
 */

class CheckForMissingAttachments {

	static final Logger log = LoggerFactory.getLogger(CheckForMissingAttachments)
	AttachmentManager am = ComponentAccessor.getAttachmentManager();
	FileSystemAttachmentStore fileStore = ComponentAccessor.getComponent(FileSystemAttachmentStore)

	def checkAll() {
		log.info("START: Check for missing attachments.");
		String table = AttachmentConstants.ATTACHMENT_ENTITY_NAME;
		List<String> columns = Arrays.asList("id");

		SelectColumnsFromContext<Long> query = Select.id().from(table);
		List<Long> attachmentIds = query.runWith(ComponentAccessor.getOfBizDelegator()).asList()
		AtomicLong counter = new AtomicLong();

		attachmentIds.stream().parallel().forEach({id->
			log.trace("Checking attachment {} of {}",counter.incrementAndGet(),attachmentIds.size());
			Attachment attachment = am.getAttachment(id)
			File attachmentFile = fileStore.getAttachmentFile(AttachmentKeys.from(attachment));
			if(!attachmentFile.exists()) {
				log.warn("Attachment file with id:{}, filename:'{}' for issue '{}' does not exist. File ->{}",attachment.id,
						attachment.filename,attachment.getIssue()?.key,attachmentFile.getAbsolutePath())
			}
		});

		log.info("Check for missing attachments COMPLETE!");
	}
}

//new CheckForMissingAttachments().checkAll()
