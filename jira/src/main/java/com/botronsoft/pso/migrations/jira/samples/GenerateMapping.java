package com.botronsoft.pso.migrations.jira.samples;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import com.botronsoft.pso.commons.mapping.api.MapperWriter;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migration.framework.environment.JiraEnvironment;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;

/**
 * @author NikolayNikolaev
 *
 *         Class to generate mapping files to be used by other classes dependant on mappings
 */
public class GenerateMapping {

	MappingFactory factory;
	MapperWriter writer;

	public GenerateMapping(JiraEnvironment env) throws URISyntaxException {
		URI url = ClassLoader.class.getResource("/jira/mappings").toURI();
		factory = new FileSystemMappingFactory(Paths.get(url).toString() + "/" + env.getKey());
		writer = factory.createWriter();

	}

	// this will created fille with the name format "ObjectTypeKeys.PROJECTNAME" +".csv"
	// in respective path with key the source name and value the target name
	// same logic follows for the other Jira objects below
	private void writeProjecKeyMap() {
		writer.writeMap(ObjectTypeKeys.PROJECTNAME, "DEF", "DEFFTEST");
	}

	private void writeProjectNameMap() {
		writer.writeMap(ObjectTypeKeys.PROJECTKEY, "DEFAULT", "TESTDEFAULT");
	}

	private void writeUserNames() {
		writer.writeOnetoManyMap(ObjectTypeKeys.USER_NAME, "test1", "test4");
		writer.writeOnetoManyMap(ObjectTypeKeys.USER_NAME, "test1", "test5");
		writer.writeOnetoManyMap(ObjectTypeKeys.USER_NAME, "test1", "test6");
		writer.writeOnetoManyMap(ObjectTypeKeys.USER_NAME, "test2", "test7");

	}

	private void writePriorityMap() {
		writer.writeMap(ObjectTypeKeys.PRIORITY, "Minor", "P4: Low");
	}

	private void writeResolutionMap() {
		writer.writeMap(ObjectTypeKeys.RESOLUTION, "Completed", "Done");
	}

	private void writeBoardsMap() {
		writer.writeMap(ObjectTypeKeys.BOARDID, "1", "3");
	}

	private void writeDashboardsMap() {
		writer.writeMap(ObjectTypeKeys.DASHBOARDID, "10", "30");
	}

	private void writeStatusMap() {
		writer.writeOnetoManyMap(ObjectTypeKeys.STATUS, "Closed", "Tested");
		writer.writeOnetoManyMap(ObjectTypeKeys.STATUS, "Viewed", "Tested");
	}

	private void writeFiltersIdMap() {
		writer.writeMap(ObjectTypeKeys.FILTERID, "2", "4");
	}

	public static void main(String args[]) {
		Environments.jira(Env.SOURCE_INSTANCES).parallelStream().forEach(env -> {
			GenerateMapping mapping;
			try {
				mapping = new GenerateMapping(env);
				mapping.writePriorityMap();
				;
				mapping.writeBoardsMap();
				;
				mapping.writeResolutionMap();
				mapping.writeProjecKeyMap();
				mapping.writeStatusMap();
				mapping.writeDashboardsMap();
				mapping.writeFiltersIdMap();

				mapping.writeProjectNameMap();
				;
				mapping.writeUserNames();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		});
	}
}
