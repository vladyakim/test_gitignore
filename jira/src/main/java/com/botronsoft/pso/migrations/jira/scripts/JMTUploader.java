package com.botronsoft.pso.migrations.jira.scripts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;

/**
 * Upload the JMT plugin to an instance
 */
public class JMTUploader {
	private static final Logger log = LoggerFactory.getLogger(JMTUploader.class);

	public static void main(String[] args) {
		Environments.jira(Env.SOURCE_INSTANCES).stream().forEach(env -> {
			env.getJmtUMPClient().uploadAddon(Environments.local().getFile(Env.JMTJAR));
			log.info("Installed JMT into {} !", env.getName());
		});
	}
}
