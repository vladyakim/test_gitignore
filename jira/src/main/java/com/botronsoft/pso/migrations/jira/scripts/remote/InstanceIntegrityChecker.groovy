package com.botronsoft.pso.migrations.jira.scripts.remote


import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.onresolve.scriptrunner.runner.customisers.JiraAgileBean
import com.onresolve.scriptrunner.runner.customisers.WithPlugin

import com.atlassian.crowd.embedded.api.User
import com.atlassian.greenhopper.manager.cardlayout.CardLayoutManager
import com.atlassian.greenhopper.manager.color.CardColorManager
import com.atlassian.greenhopper.manager.detailview.DetailViewFieldManager
import com.atlassian.greenhopper.manager.estimatestatistic.EstimateStatisticManager
import com.atlassian.greenhopper.manager.rapidview.BoardAdminManager
import com.atlassian.greenhopper.manager.rapidview.RapidViewManager
import com.atlassian.greenhopper.model.rapid.CardColorStrategy
import com.atlassian.greenhopper.model.rapid.Column
import com.atlassian.greenhopper.model.rapid.RapidView
import com.atlassian.greenhopper.model.rapid.StatisticsFieldConfig
import com.atlassian.greenhopper.model.validation.ErrorCollection
import com.atlassian.greenhopper.service.rapid.view.ColumnService
import com.atlassian.greenhopper.service.rapid.view.QuickFilterService
import com.atlassian.greenhopper.service.rapid.view.RapidViewService
import com.atlassian.greenhopper.service.rapid.view.SwimlaneService
import com.atlassian.greenhopper.service.workflow.WorkflowService
import com.atlassian.greenhopper.web.rapid.view.RapidViewHelper
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.config.IssueTypeManager
import com.atlassian.jira.config.PriorityManager
import com.atlassian.jira.config.StatusManager
import com.atlassian.jira.issue.fields.FieldManager
import com.atlassian.jira.issue.search.SearchRequest
import com.atlassian.jira.issue.search.SearchRequestEntity
import com.atlassian.jira.issue.search.SearchRequestManager
import com.atlassian.jira.issue.status.Status
import com.atlassian.jira.portal.PortalPage
import com.atlassian.jira.portal.PortalPageManager
import com.atlassian.jira.portal.PortletConfiguration
import com.atlassian.jira.portal.PortletConfigurationManager
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.util.BaseUrl
import com.atlassian.jira.util.Consumer
import com.atlassian.jira.util.Visitor

import groovy.transform.Memoized
/**
 * Fix all integrity check errors on a JIRA Instance
 */
@WithPlugin("com.pyxis.greenhopper.jira")
class  JIRAIntegrityChecker {
	Logger log = LoggerFactory.getLogger(JIRAIntegrityChecker)

	@JiraAgileBean
	RapidViewService rapidViewService

	@JiraAgileBean
	RapidViewHelper rapidViewHelper

	@JiraAgileBean
	RapidViewManager rapidViewManager

	@JiraAgileBean
	BoardAdminManager boardAdminManager

	@JiraAgileBean
	CardColorManager cardColorManager

	@JiraAgileBean
	CardLayoutManager cardLayoutManager

	@JiraAgileBean
	DetailViewFieldManager detailViewFieldManager

	@JiraAgileBean
	EstimateStatisticManager estimateStatisticManager

	@JiraAgileBean
	ColumnService columnService

	@JiraAgileBean
	SwimlaneService swimlaneService

	@JiraAgileBean
	QuickFilterService quickFilterService

	@JiraAgileBean
	WorkflowService workflowService

	StatusManager statusManager = ComponentAccessor.getComponent(StatusManager)
	IssueTypeManager itm = ComponentAccessor.getComponent(IssueTypeManager)
	PriorityManager priorityManager = ComponentAccessor.getComponent(PriorityManager)
	//	CustomFieldManager customFieldManager = ComponentAccessor.getComponent(CustomFieldManager)
	FieldManager fieldManager = ComponentAccessor.getFieldManager()
	SearchRequestManager srm = ComponentAccessor.getComponent(SearchRequestManager)
	SearchRequest sr = ComponentAccessor.getComponent(SearchRequest.class)
	//	User user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
	User user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().getDirectoryUser()
	ApplicationUser applicationUser = ComponentAccessor.getJiraAuthenticationContext().getUser()
	PortalPageManager portalPageManager  = ComponentAccessor.getComponent(PortalPageManager)
	PortletConfigurationManager portletConfigurationManager = ComponentAccessor.getComponent(PortletConfigurationManager)
	com.atlassian.jira.bc.issue.search.SearchService searchService = ComponentAccessor.getComponent(com.atlassian.jira.bc.issue.search.SearchService)

	static class NoCheck implements RapidViewManager.RapidViewPermissionCheck
	{
		@Override
		public boolean check(RapidView view)
		{
			return true;
		}
	}

	@Memoized
	public static String getBaseUrl() {
		try {
			return ComponentAccessor.getComponent(BaseUrl).getBaseUrl()
		} catch (Throwable t) {
			return "";
		}
	}

	private String getBoardID(RapidView board) {
		StringBuilder idBuilder = new StringBuilder();
		idBuilder.append(board.getName());
		idBuilder.append("_");
		idBuilder.append(board.getOwner().toLowerCase());
		idBuilder.append("_");
		idBuilder.append(board.isSprintSupportEnabled() ? "scrum" : "kanban");
		return idBuilder.toString();
	}

	/**
	 * Deletes boards with missing filters
	 * @return list of removed boards
	 */
	public String removeBoardsWithMissingFilters(boolean fix) {
		def removed = []
		rapidViewManager.getAll(new NoCheck()).value.each {b->
			if(srm.getSearchRequestById(b.savedFilterId)==null) {
				if(fix) {
					def res = rapidViewManager.delete(b)
				}
				removed.add("${b.id},${b.name},${b.owner},/secure/RapidBoard.jspa?rapidView=${b.id}")
				log.info("Deleting board because of missing filter ${b.id}, ${b.name} ")
			}
		}
		def result  = removed.join("\n")
		/<textarea cols="400" rows="10">$result<\/textarea>/
	}



	/**
	 * Fixes Card Color, Detail View, Card Layout, Estimate Statistic
	 * @param boardIds
	 * @return
	 */
	public String fixCardDetailStats() {
		return fixCardDetailStats(null);
	}
	public String fixCardDetailStats(boardIds) {
		log.info("START: Fix Agile Boards: Card Color, Detail View, Card Layout, Estimate Statistic")

		def boards = getBoards(boardIds)
		def changedBoards = [].toSet()
		boards.each { RapidView b->
			//Estimate statistic
			def estimateFieldId = estimateStatisticManager.getEstimateStatistic(b).fieldId
			if(estimateFieldId!=null && fieldManager.getField(estimateFieldId)==null) {
				log.warn("(${b.id}) Estimate statistic field (${estimateFieldId}) does not exist for board ${b.id}")
				estimateStatisticManager.updateEstimateStatistic(b, StatisticsFieldConfig.buildNone())
				changedBoards.add(b)
			}

			//Card Layout Remove references to missing custom fields
			cardLayoutManager.getAll(b).getValue().each {
				if(fieldManager.getField(it.getFieldId()) == null) {
					log.debug("Removing card layout from board ${b.name} for non-found field '${it.getFieldId()}'")
					cardLayoutManager.delete(b, it.id);
					changedBoards.add(b)
				}
			}

			// Detail View
			detailViewFieldManager.getAll(b).value.each {
				if(fieldManager.getField(it.getFieldId()) == null) {
					log.debug("Removing detail view field from board ${b.name} for non-found field '${it.getFieldId()}'")
					detailViewFieldManager.delete(b, it.id)
					changedBoards.add(b)
				}
			}


			//Check card Colors
			cardColorManager.getAll(b).getValue().each {
				switch(it.strategy) {
					case CardColorStrategy.ISSUE_TYPE:
						if(itm.getIssueType(it.getValue()) == null) {
							//remove color
							log.debug("Removing card color from board ${b.name} for non-found issue type ${it.value}")
							cardColorManager.delete(b, it.id);
							changedBoards.add(b)
						}
						break;
					case CardColorStrategy.PRIORITY:
						if(priorityManager.getPriority(it.getValue()) == null) {
							//remove color
							log.debug("Removing card color from board ${b.name} for non-found priority ${it.value}")
							cardColorManager.delete(b, it.id);
							changedBoards.add(b)
						}
						break;
					default : break;
				}
			}
			def duplicates = cardColorManager.getAll(b).value.groupBy{
				it.getStrategy().toString()+":"+it.getValue()
			}.findAll{k,v->v.size()>1}

			if(!duplicates.isEmpty()) {
				log.debug("Dublicate CARD COLOR detected in board ${b.name} with id ${b.id}: $duplicates")
			}

			duplicates.each {k,v->
				v.eachWithIndex {val,idx->
					log.debug("$idx")
					if(idx>0) {
						log.debug("Removing dublicate card color from board ${b.name} with value ${val.value}")
						cardColorManager.delete(b, val.id)
						changedBoards.add(b)
					}
				}
			}
		}
		log.info("COMPLETE: Fix Agile Boards: Card Color, Detail View, Card Layout, Estimate Statistic")
		return changedBoards.collect{RapidView b-> "${b.id},${b.name},${b.owner},${getBaseUrl()}/secure/RapidBoard.jspa?rapidView=${b.id}" }.join("<br>")
	}



	/**
	 * Remove any duplicate quick filters and swimlanes
	 * @return
	 */
	public String fixDupQuickFiltersSwimlanes() {
		fixDupQuickFiltersSwimlanes(null)
	}

	def fixDuplicateColumns() {
		def boards = getBoards()
		def changes = [].toSet()
		boards.each{b->
			Collection<Column> columns = columnService.getAllColumns(b)
			def dups = columns.groupBy{it.name}.findAll{k,v->v.size()>1}
			def nameMap = dups.keySet().collectEntries{
				[(it):0]
			}
			if(!nameMap.keySet().empty) {
				def newCols = columns.collect{
					def name =  it.name
					if(nameMap.containsKey(name)) {
						if(nameMap.get(name)>0) {
							name = "$name ${nameMap.get(it.name)}"
							changes.add("${b.id},${b.name},${b.owner},${getBaseUrl()}/secure/RapidBoard.jspa?rapidView=${b.id},COLUMNS,${it.name},${name}")
						}
						nameMap.put(name, nameMap.get(it.name)+1)
					}
					Column.builder().max(it.max).min(it.min).statusIds(it.statusIds).name(name).build()
				}
				columnService.updateColumns(applicationUser, b, newCols)
				log.debug("Updated columns in board ${b.name}")
			}
		}
		return changes
	}
	public String fixDupQuickFiltersSwimlanes(boardIds) {
		log.info("START: Fix Agile Boards: Duplicate Quick Filter, Swimlane")

		def boards = getBoards(boardIds)
		def changes = [].toSet()
		log.info("Checking ${boards.size()} boards")
		boards.each { RapidView b->
			//Swimlanes
			def swimlanes = swimlaneService.loadSwimlanes(b)
			//			if(b.getSwimlaneStrategy() == SwimlaneStrategy.CUSTOM)
			def duplicatesSL = swimlanes.groupBy{it.name}.findAll{k,v->v.size()>1}
			if(!duplicatesSL.isEmpty()) {
				log.debug("Dublicate Swimlanes detected in board ${b.name} with id ${b.id}: $duplicatesSL")
				duplicatesSL.each {k,v->
					v.eachWithIndex {val,idx->
						log.debug("$idx")
						if(idx>0) {
							log.debug("Removing dublicate swimlane from board ${b.name} with name ${val.name}")
							def errors = new ErrorCollection();
							swimlaneService.delete(applicationUser, b, val.id, errors)
							changes.add("${b.id},${b.name},${b.owner},${getBaseUrl()}/secure/RapidBoard.jspa?rapidView=${b.id},SWIMLANE,${val.name},${val.query}")
							if(errors.hasErrors()) {
								log.warn("${errors.getErrors()}")
							}
						}
					}
				}
			}
			//Quick Filters
			def quickFilters = quickFilterService.loadQuickFilters(b)
			def duplicates = quickFilters.groupBy{it.name}.findAll{k,v->v.size()>1}
			if(!duplicates.isEmpty()) {
				log.debug("Dublicate Quick Filters detected in board ${b.name} with id ${b.id}: $duplicates")
				duplicates.each {k,v->
					v.eachWithIndex {val,idx->
						log.debug("$idx")
						if(idx>0) {
							log.debug("Removing dublicate quick filter from board ${b.name} with name ${val.name}")
							def errors = new ErrorCollection();
							quickFilterService.delete(applicationUser, b, val.id, errors)
							changes.add("${b.id},${b.name},${b.owner},${getBaseUrl()}/secure/RapidBoard.jspa?rapidView=${b.id},QUICKFILTER,${val.name},${val.query}")
							if(errors.hasErrors()) {
								log.warn("${errors.getErrors()}")
							}
						}
					}
				}
			}
		}
		log.info("COMPLETE: Fix Agile Boards: Duplicate Quick Filter, Swimlane")
		//
		return changes.join("<br>")
	}


	/**
	 * Rename duplicate named boards from the same owner
	 * @param boardIds
	 * @param fix
	 * @return
	 */
	public String fixDuplicateBoards(boolean fix) {
		return fixDuplicateBoards(null, fix);
	}
	public String fixDuplicateBoards(boardIds, boolean fix) {
		def boards = getBoards(boardIds)

		def boardsById = boards.groupBy({board->getBoardID(board)})
		def renamed = []
		boardsById.findAll{k,v->v.size()>1}.each {k,v->
			//Check for dublicate name
			log.info("Detected dublicates for key $k, ${v.size()}")
			v.eachWithIndex {RapidView b, index->
				if(index!=0) {
					def newName = "${b.name} ${index}"
					renamed.add("${b.name},$newName,${b.owner},${getBaseUrl()}/secure/RapidBoard.jspa?rapidView=${b.id}")
					if(fix) {
						def updateBoard  = RapidView.builder(b).name(newName).build()
						log.info("INDEX $index : Renaming board ${b.id} from ${b.name} to ${updateBoard.name}")
						def result = rapidViewManager.update(updateBoard);
						if(result.invalid) {
							log.warn("Failed to rename board ${b.name} ${result.getErrors()}")
						}
					}
				}
			}
		}

		log.info("COMPLETE: Cleanup of Agile Boards")
		return renamed.join("<br>");
	}


	private Collection<RapidView> getBoards(boardIds) {
		if(boardIds==null) {
			return rapidViewManager.getAll(new NoCheck()).value
		} else {
			return  boardIds.collect{
				rapidViewManager.get(Long.valueOf(it)).value
			}
		}

	}

	def getBoardsWithMissingStatusInColumn(boardIds, validStatuses, statusMapping) {
		def boards = getBoards(boardIds)
		boards.each { RapidView b->
			// Check column statuses
			def iStatuses = validStatuses.collect{name->statusManager.getStatuses().find{s->s.name.equals(name)}.id.toString()}
			def colsToUpdate =[]
			def usedReplacements = []
			columnService.getColumns(b).each {column->
				def newStatuses = column.statusIds.collect()
				column.statusIds.findAll{!(it in iStatuses)}.each {
					def status = statusManager.getStatus(it)
					//					workflowService.getAccessibleStatuses(user, null)
					def replacement = statusMapping.get(status.name)
					if(!replacement) {
						//						log.warn("Can't find replacement status for ${status.name}")
						//						log.debug("Found invalid status ${status.name} in column ${column.name} in board ${b.name}")
						log.info("Removing  ${status.name} from  column ${column.name} in board ${b.name}")
						newStatuses.remove(it)
					} else {
						newStatuses.remove(it)
						if(replacement in usedReplacements || isBoardStatusAlreadyUsed(b,getStatus(replacement))) {
							log.warn("Status $replacement was already assigned to column in board ${b.name}")
						} else {
							newStatuses.add(getStatus(replacement).id)
							log.debug("Replacing invalid status ${status.name} with $replacement in column ${column.name} in board ${b.name}")
							usedReplacements.add(replacement)
						}
					}
				}

				//				colsToUpdate.add(Column.builder(column).statusIds(newStatuses.unique()).build())
			}
			//			def owner = ComponentAccessor.getUserUtil().getUser(b.getOwner())

			//FIXME: Disable column fix
			//			def result = columnService.updateColumns(user, b, colsToUpdate)
			//			if(result.invalid) {
			//				log.error(result.errors.getErrors())
			//			}
		}
		log.info("COMPLETE: Cleanup of Agile Boards")
	}

	private boolean isBoardStatusAlreadyUsed(b,status) {
		return columnService.getMappedStatuses(b).contains(status)
	}

	private Status getStatus(String name) {
		statusManager.getStatuses().find{s->s.name.equals(name)}
	}

	//Dashboards


	def removeGadgetsWithMissingBoardIds(boolean remove) {
		def result = []
		portalPageManager.all.foreach(new Consumer<PortalPage>(){
					void consume(PortalPage pp){
						result.add(removeGadgetsWithMissingBoardIdsInDashboard(pp.id, remove))
					}
				})

		def resultString = result.findResults{it?:null}.flatten().join("\n").split(",")
		/<textarea cols="400" rows="10">$resultString <\/textarea>/

	}

	def removeGadgetsWithMissingBoardIdsInDashboard(id, boolean remove) {
		def changedDashboards = []
		//def dashboard = portalPageManager.getPortalPageById(id)
		portletConfigurationManager.getByPortalPage(id).findAll{PortletConfiguration portlet->
			//Find all JIRA Agile gadgets AND have rapidViewId property
			portlet.gadgetURI.toString().startsWith("rest/gadgets/1.0/g/com.pyxis.greenhopper.jira") &&
					portlet.getUserPrefs().containsKey("rapidViewId")
		}.findAll{PortletConfiguration portlet->
			//Find all rapidViewIds which point to non existing agile board
			def rapidViewId = portlet.getUserPrefs().get("rapidViewId")
			try {
				return rapidViewManager.get(Long.valueOf(rapidViewId)).isInvalid()
			} catch (NumberFormatException e) {
				return false
			}
		}.each{PortletConfiguration portlet->
			changedDashboards.add("${portlet.dashboardPageId} with portlet ID ${portlet.id} of type ${portlet.gadgetURI}")
			if(remove) {
				log.debug("Removing portlet from dashboard ${portlet.dashboardPageId}//dashboardPageId} with portlet ID ${portlet.id} of type ${portlet.gadgetURI}")
				portletConfigurationManager.delete(portlet)
			}
			//Remove all portlets
		}
		//def result  = changedDashboards.join("\n")
		return changedDashboards
	}

	/**
	 * Identifies filter with same name belonging to the same user.
	 * Depending on the input it either reports duplicates ONLY, or update name adding _index
	 * @param rename
	 * @return report of duplicated filters
	 */
	def fixDuplicatedFilters(boolean rename) {
		//TODO implement better report output what's duplicated and what's renamed
		log.info("Searching for duplicate filters...")
		def filters = []
		srm.visitAll(new Visitor<SearchRequestEntity>(){
					void visit(SearchRequestEntity e) {
						SearchRequest sr = srm.getSearchRequestById(e.id)
						filters.add(sr)
					}
				})
		def dups = filters.groupBy{SearchRequest sr-> "${sr.name.toLowerCase()}:${sr.owner.username}" }.findAll{it.value.size() >1}
		//		if(delete) {
		//			dups.each {k,Collection v->
		//				v.eachWithIndex {SearchRequest e,idx->
		//					if(idx>0) {
		//						//srm.delete(e.id)
		//						log.debug("Deleting duplicate filter ${e.id}")
		//					}
		//				}
		//			}
		//		}
		if(rename) {
			dups.each {k,Collection v->
				v.eachWithIndex {SearchRequest e,idx->
					if(idx>0) {
						def name = e.getName()
						e.setName(name+"_"+idx)
						srm.update(e)
						log.debug("Renaming duplicate filter ${e.id} ${name} to ${e.name}")
					}
				}
			}
			return log.info("Renaming filters completed!")
		}
		dups.collect{"${it.key}: ${it.value.size()}"}.join("<br>")
		log.info("DONE: Searching for duplicate filters. Dublicates found ${dups.size()}")
	}

	def validateFilters(File report) {
		//TODO implement better report output what's duplicated and what's renamed
		log.info("Checking filter if valid...")

		def filters = []

		if(report.exists()) {
			report.delete()
		}

		//ss.validateQuery(user., query, searchRequestId)

		def filtersReport = report
		filtersReport << "||Filter Name||Owner||Query||Is filter valid?||\n"

		def results = []
		filters.each{ filter ->
			SearchRequest sr = srm.getSearchRequestById(filter)

			def query = searchService.parseQuery(sr.owner,sr.query.queryString).query
			def validateResult = searchService.validateQuery(applicationUser, query)
			def msg = "Yes"
			if(validateResult.hasAnyErrors()) {
				//				log.error(sr.query.queryString)
				msg = validateResult.errorMessages.join(";")
			}
			log.info("|${filter}|${sr.name}|${sr.owner.username}|${sr.query.queryString}|$msg|")
			def line = "|${sr.name}|${sr.owner.username}|${sr.query.queryString}|$msg|\n"
			line = line.replaceAll("\\[", "&#91;").replaceAll("\\]", "&#93;")
			results.add(line)

			//filtersReport << line
		}

		results.sort().each{filterRecord -> filtersReport << filterRecord}

		log.info("DONE: Checking filter validity}")
	}

	//new JIRAIntegrityChecker().validateFilters(new File("/tmp/"))

	def flushBoardAdminCache() {
		boardAdminManager.flushCache()
	}

}
//new JIRAIntegrityChecker().fixCardDetailStats()
//new JIRAIntegrityChecker().fixDupQuickFiltersSwimlanes()
//new JIRAIntegrityChecker().fixDuplicateColumns()
//new JIRAIntegrityChecker().fixDuplicateBoards(true)
//new JIRAIntegrityChecker().removeBoardsWithMissingFilters(true)
//new JIRAIntegrityChecker().removeGadgetsWithMissingBoardIds(true)
//new JIRAIntegrityChecker().fixDuplicatedFilters(true)
//
//new JIRAIntegrityChecker().flushBoardAdminCache()
//new JIRAIntegrityChecker().validateFilters(new File("/tmp/filters.txt"))

