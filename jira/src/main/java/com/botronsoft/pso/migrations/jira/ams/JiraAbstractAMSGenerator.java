package com.botronsoft.pso.migrations.jira.ams;

import static java.util.stream.Collectors.toList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import org.eclipse.emf.ecore.EClass;

import com.botronsoft.jira.rollout.model.jiraconfiguration.JiraBaseObject;
import com.botronsoft.jira.rollout.model.jiraconfiguration.NamedNode;
import com.botronsoft.jira.rollout.model.jiraconfiguration.field.JiraFieldPackage;
import com.botronsoft.jira.rollout.model.jiraconfiguration.issue.JiraIssuePackage;
import com.botronsoft.jira.rollout.model.jiraconfiguration.scheme.JiraSchemePackage;
import com.botronsoft.jira.rollout.model.jiraconfiguration.screen.JiraScreenPackage;
import com.botronsoft.jira.rollout.model.jiraconfiguration.workflow.JiraWorkflowPackage;
import com.botronsoft.pso.migration.framework.snapshot.diff.ComparisonMatch;
import com.botronsoft.pso.migration.framework.snapshot.diff.SnapshotComparison;
import com.botronsoft.pso.migrations.AbstractAMSGenerator;
import com.google.common.collect.Lists;

import io.github.swagger2markup.markup.builder.MarkupTableColumn;

/**
 * Sample JIRA AMS generator - the code is a bit messy in the initial version
 *
 */
public abstract class JiraAbstractAMSGenerator extends AbstractAMSGenerator {
	protected SnapshotComparison diff;

	protected JiraAbstractAMSGenerator(SnapshotComparison diff) {
		this.diff = diff;

	}

	protected String getTypeDisplay(EClass eClass) {
		// FIXME: move to static map
		Map<EClass, String> m = new HashMap<>();
		m.put(JiraScreenPackage.Literals.JIRA_FIELD_SCREEN_SCHEME, "Screen Scheme");
		m.put(JiraScreenPackage.Literals.JIRA_ISSUE_TYPE_SCREEN_SCHEME, "Issue Type Screen Scheme");
		m.put(JiraFieldPackage.Literals.JIRA_FIELD_LAYOUT, "Field Configuration");
		m.put(JiraFieldPackage.Literals.JIRA_FIELD_LAYOUT_SCHEME, "Field Configuration Scheme");
		m.put(JiraSchemePackage.Literals.JIRA_ISSUE_SECURITY_SCHEME, "Issue Security Scheme");
		m.put(JiraSchemePackage.Literals.JIRA_NOTIFICATION_SCHEME, "Notification Scheme");
		m.put(JiraSchemePackage.Literals.JIRA_PERMISSION_SCHEME, "Permission Scheme");
		m.put(JiraIssuePackage.Literals.JIRA_ISSUE_PRIORITY_SCHEME, "Priority Scheme");
		m.put(JiraIssuePackage.Literals.JIRA_ISSUE_TYPE_SCHEME, "Issue Type Scheme");
		m.put(JiraWorkflowPackage.Literals.JIRA_WORKFLOW_SCHEME, "Workflow Scheme");
		//
		return Optional.ofNullable(m.get(eClass)).orElse(eClass.getName());
	}

	protected void statusTable(List<MarkupTableColumn> columns, Class<? extends NamedNode> clazz) {
		generateCombinedTable(clazz, columns, (m) -> {
			return row(m.getLeft().getName(), "Already exists on TARGET");
		}, (c) -> {
			return row(c.getName(), "New");
		});
	}

	protected <T extends JiraBaseObject> void generateCombinedTable(Class<T> clazz, List<MarkupTableColumn> tableHeaders,
			Function<ComparisonMatch<T>, List<String>> matchRowFunction, Function<T, List<String>> addRowFunction) {
		List<List<String>> cells = Lists.newArrayList();
		//
		List<ComparisonMatch<T>> matched = diff.getMatches(clazz);
		if (!matched.isEmpty()) {
			cells.addAll(matched.stream().map(matchRowFunction).collect(toList()));
		}
		List<T> added = diff.getAdditions(Lists.newArrayList(clazz));
		if (!added.isEmpty()) {
			cells.addAll(added.stream().map(addRowFunction).collect(toList()));
		}
		if (cells.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}

	protected <T extends JiraBaseObject> void generateCombinedMultiTable(List<Class<? extends JiraBaseObject>> types,
			List<MarkupTableColumn> tableHeaders, Function<ComparisonMatch<T>, List<String>> matchRowFunction,
			Function<T, List<String>> addRowFunction) {
		List<List<String>> cells = Lists.newArrayList();
		//
		List<ComparisonMatch<T>> matched = diff.getMatches(types);
		if (!matched.isEmpty()) {
			cells.addAll(matched.stream().map(matchRowFunction).collect(toList()));
		}
		List<T> added = diff.getAdditions(types);
		if (!added.isEmpty()) {
			cells.addAll(added.stream().map(addRowFunction).collect(toList()));
		}
		if (cells.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}

	//
	protected <T extends JiraBaseObject> void generateMatchesTable(Class<T> clazz, List<MarkupTableColumn> tableHeaders,
			Function<ComparisonMatch<T>, List<String>> rowFunction) {
		List<ComparisonMatch<T>> matched = diff.getMatches(clazz);
		if (matched.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}
		List<List<String>> cells = matched.stream().map(rowFunction).collect(toList());
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}

	protected <T extends JiraBaseObject> void generateAdditionsTable(Class<T> clazz, List<MarkupTableColumn> tableHeaders,
			Function<T, List<String>> rowFunction) {
		List<T> added = diff.getAdditions(Lists.newArrayList(clazz));
		if (added.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}
		List<List<String>> cells = added.stream().map(rowFunction).collect(toList());
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}

	protected <T extends JiraBaseObject> void generateTable(Class<T> clazz, List<Class<? extends JiraBaseObject>> types,
			List<MarkupTableColumn> tableHeaders, Function<T, List<String>> rowFunction) {
		List<T> added = diff.getAdditions(types);
		if (added.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}
		List<List<String>> cells = added.stream().map(rowFunction).collect(toList());
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}
}
