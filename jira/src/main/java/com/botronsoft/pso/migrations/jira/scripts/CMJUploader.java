package com.botronsoft.pso.migrations.jira.scripts;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.jmt.rest.client.upm.JiraUMPClient;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migration.framework.environment.JiraEnvironment;
import com.botronsoft.pso.migrations.Env;
import com.google.common.collect.Lists;

/**
 * Upload the CMJ plugin to an instance
 *
 */
public class CMJUploader {
	private static final Logger log = LoggerFactory.getLogger(CMJUploader.class);
	static final List<String> CMJ_COMPONENTS = Collections
			.unmodifiableList(Lists.newArrayList("com.botronsoft.jira.poweradmin", "com.botronsoft.jira.configurationmanager",
					"com.botronsoft.jira.rollout.integrity-check", "com.botronsoft.jira.rollout.configuration-manager-core"));

	public static void main(String[] args) {
		Collection<JiraEnvironment> instances = Lists.newArrayList();
		instances = Lists.newArrayList(Environments.jira(Env.TARGET));

		instances.stream().parallel().forEach(env -> {
			JiraUMPClient client = env.getJmtUMPClient();
			log.info("Un-installing CMJ from {}...", env.getName());
			client.uninstallAddons(CMJ_COMPONENTS);
			log.info("Un-install from {} COMPLETE!", env.getName());
		});

		instances.stream().forEach(env -> {
			// Put a limit on the upload rate as the VPN connection is breaking all the time
			JiraUMPClient client = env.getJmtUMPClient(FileUtils.ONE_MB);
			log.info("Installing CMJ into {}...", env.getName());
			client.uploadAddon(Environments.local().getFile(Env.CMJJAR));
			log.info("Installed CMJ into {} !", env.getName());
		});
	}
}
