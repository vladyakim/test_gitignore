package com.botronsoft.pso.migrations.jira.migration.apps;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.jfree.util.Log;
import org.junit.Ignore;
import org.junit.Test;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.jmt.apps.reminderforjira.model.ReminderModelPackage;
import com.botronsoft.pso.jmt.apps.reminderforjira.model.Root;
import com.botronsoft.pso.jmt.rest.client.FileManagerClient;
import com.botronsoft.pso.jmt.rest.client.apps.AppsClient;
import com.botronsoft.pso.jmt.rest.client.apps.AppsClient.JmtApp;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migration.framework.environment.JiraEnvironment;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
import com.botronsoft.pso.migrations.jira.migration.apps.AppModelTransformer.TransformationMapping;
import com.google.common.collect.Lists;

/**
 * Legacy code for transforming and migrating 3rd party add-ons. Left as reference for something yet to be refactored.
 * <p>
 * <b>FIXME:</b> This class depends on code from jmt-plugin which is not correct. The models should be moved into core-lib or jmt-common
 */
@Deprecated
public class TransformAppModels {
	private static final String REMINDERFORJIRA_XML = "reminderforjira.xml";
	DefaultTransformationMapping mapping = new DefaultTransformationMapping();

	File getModelFile(String name) {
		return downloadModelFile(name);
	}

	File downloadModelFile(String name) {
		FileManagerClient client = Environments.jira(Env.SOURCE).getJmtFileManagerClient();
		File basedir = Environments.local().getFile(Env.WORKDIR);
		File modelFile = new File(basedir, name);
		if (!modelFile.exists()) {
			client.downloadFile("export/" + name, modelFile);
		}
		return modelFile;
	}

	public static class DefaultTransformationMapping implements TransformationMapping {

		private Map<String, Set<String>> userMap = new HashMap<>();
		private Map<String, String> groupMap = new HashMap<>();
		private Map<String, String> projectMap = new HashMap<>();

		DefaultTransformationMapping() {

			MappingFactory mappingFactoryReader = new FileSystemMappingFactory(Env.SOURCE_JIRA_ENV.getKey());

			MapperReader mappingReader = mappingFactoryReader.createReader();
			// getting all source and target users , as if one source user maps to more than one target user
			// it is given as key=SourceUser , value=TargetUser1,TargetUser2 etc
			mappingReader.map(ObjectTypeKeys.USER_NAME).forEach((k, v) -> userMap.put(k, new HashSet<String>(v)));
			mappingReader.map(ObjectTypeKeys.PROJECTNAME).forEach((k, v) -> projectMap.put(k, v.stream().collect(Collectors.joining(","))));

			mappingReader.map(ObjectTypeKeys.GROUPNAME).forEach((k, v) -> groupMap.put(k, v.stream().collect(Collectors.joining(","))));

		}

		@Override
		public Map<String, String> getChangedProjectKeys() {
			return projectMap;
		}

		@Override
		public Map<String, Set<String>> getUsersToMerge() {
			return userMap;
		}

		@Override
		public Map<String, String> getRenamedGroups() {
			return groupMap;
		}

	}

	// @Ignore
	@Test
	public void exportAppData() {
		JiraEnvironment source = Environments.jira(Env.SOURCE);
		AppsClient appsClient = source.getJmtAppsClient();
		Lists.newArrayList(JmtApp.ReminderForJira).parallelStream().forEach(app -> {
			try {
				appsClient.exportData(app);
			} catch (Throwable t) {
				Log.error(t);
			}
		});

	}

	String getTransformed(String name) {
		return name + ".transformed.xml";
	}

	@Ignore
	@Test
	public void importData() {
		JiraEnvironment target = Environments.jira(Env.TARGET);
		AppsClient client = target.getJmtAppsClient();
		client.importData(JmtApp.ReminderForJira, getImportFile(REMINDERFORJIRA_XML));
	}

	String getImportFile(String name) {
		String jiraHomeFolder = ""; // provide path to JIRA Home
		return jiraHomeFolder + getTransformed(name);
	}

	@Ignore
	@Test
	public void uploadTransformedFiles() {
		JiraEnvironment target = Environments.jira(Env.TARGET);
		FileManagerClient fileManager = target.getJmtFileManagerClient();
		File basedir = Environments.local().getFile(Env.WORKDIR);
		String destinationPath = "import";
		fileManager.upload(destinationPath, new File(basedir, getTransformed(REMINDERFORJIRA_XML)));
	}

	@Test
	public void temp() {
		JiraEnvironment target = Environments.jira(Env.TARGET);
	}

	@Test
	public void transformReminderForJira() throws Exception {
		File modelFile = getModelFile(REMINDERFORJIRA_XML);

		// TODO: change to Reminder Root and instance
		AppModelTransformer<Root> transformer = new AppModelTransformer<com.botronsoft.pso.jmt.apps.reminderforjira.model.Root>(
				ReminderModelPackage.eINSTANCE);
		com.botronsoft.pso.jmt.apps.reminderforjira.model.Root root = transformer.load(modelFile);
		transformer.transform(root.getJiraRoot(), mapping);
		transformer.persist(modelFile, root);
	}

}
