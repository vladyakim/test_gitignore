package com.botronsoft.pso.migrations.jira.snapshot.create;

import static com.botronsoft.jira.cmj.rest.client.SnapshotOptions.Builder.create;
import static java.util.stream.Collectors.toList;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.botronsoft.jira.cmj.rest.client.CMJRestClient;
import com.botronsoft.jira.cmj.rest.client.SnapshotOptions;
import com.botronsoft.pso.jmt.rest.client.reporting.ReportingClient;
import com.botronsoft.pso.migrations.Env;

public class SnapshotCreator {
	private static final String SOURCE_PROJECTS_FULL = "Source_projects_full";
	private static final String SOURCE_PROJECTS_CONFIG_ONLY = "Source_projects_config";
	private static final String TARGET_SYSTEM = "Target_system";
	private static CMJRestClient cmjClient;
	private static ReportingClient reportingClient;

	@BeforeClass
	public static void initClient() {
		reportingClient = Env.SOURCE_JIRA_ENV.getJmtReportingClient();
		cmjClient = Env.SOURCE_JIRA_ENV.getCMJClient();
	}

	// @Ignore
	@Test
	public void sourceProjectSnapshotWithIssuesFull() throws Exception {
		List<String> projectKeys = reportingClient.getProjects().stream().map(p -> p.getKey()).distinct().collect(toList());
		List<Long> filterIds = reportingClient.collectFilters().stream().map(f -> f.getId()).distinct().collect(toList());
		List<Long> boardIds = reportingClient.collectAgileBoards().stream().map(b -> b.getId()).distinct().collect(toList());
		List<Long> dashboardIds = reportingClient.collectDashboards().stream().map(d -> d.getId()).distinct().collect(toList());
		// Adding System Dashboard
		dashboardIds.add(new Long(10000));
		deleteSnapshotIfExists(SOURCE_PROJECTS_FULL);
		SnapshotOptions options = create(SOURCE_PROJECTS_FULL).description("Source projects data full")
		//@formatter:off
			.scopeProjectWithIssues()
			.withCustomFieldsWithValue()
			.projects(projectKeys)
			.filters(filterIds)
			.boards(boardIds)
			.dashboards(dashboardIds)
			.build();
			//@formatter:on
		cmjClient.createSnapshot(options);
	}

	// @Ignore
	@Test
	public void sourceProjectSnapshotWithoutIssues() throws Exception {
		List<String> projectKeys = reportingClient.getProjects().stream().map(p -> p.getKey()).distinct().collect(toList());
		List<Long> filterIds = reportingClient.collectFilters().stream().map(f -> f.getId()).distinct().collect(toList());
		List<Long> boardIds = reportingClient.collectAgileBoards().stream().map(b -> b.getId()).distinct().collect(toList());
		List<Long> dashboardIds = reportingClient.collectDashboards().stream().map(d -> d.getId()).distinct().collect(toList());

		deleteSnapshotIfExists(SOURCE_PROJECTS_CONFIG_ONLY);
		SnapshotOptions options = create(SOURCE_PROJECTS_CONFIG_ONLY).description("Source projects config only")
		//@formatter:off
			.scopeProject()
			.withCustomFieldsWithValue()
			.projects(projectKeys)
			.filters(filterIds)
			.boards(boardIds)
			.dashboards(dashboardIds)
			.build();
		//@formatter:on
		cmjClient.createSnapshot(options);
	}

	private void deleteSnapshotIfExists(String name) {
		try {
			cmjClient.deleteSnapshotByName(name);
		} catch (Exception e) {
			//
		}
	}

	// @Ignore
	@Test
	public void targetSystemSnaphost() throws Exception {
		cmjClient = Env.TARGET_JIRA_ENV.getCMJClient();
		deleteSnapshotIfExists(TARGET_SYSTEM);
		SnapshotOptions options = create(TARGET_SYSTEM).description("")
		//@formatter:off
			.scopeSystem()
			.withAllAgileBoards()
			.withAllDashboards()
			.withAllFilters()
			.build();
			//@formatter:on
		cmjClient.createSnapshot(options);
	}

}
