package com.botronsoft.pso.migrations.jira.samples;

import org.junit.runner.RunWith;

import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migration.framework.transform.run.AbstractSnapshotProcessorRunnerSpecBuilder;
import com.botronsoft.pso.migration.framework.transform.run.DefaultSnapshotProcessorRunner;
import com.botronsoft.pso.migration.framework.transform.run.JUnitSnapshotProcessorRunner;
import com.botronsoft.pso.migration.framework.transform.spec.RunSpec;

/**
 * All classes under {@code package com.botronsoft.pso.migrations.jira.samples} are for reference only Will be removed once any useful code
 * within is moved where appropriate
 */
@Deprecated
@RunWith(JUnitSnapshotProcessorRunner.class)
public class TransformConfigurationSnapshotsJQLRun extends AbstractSnapshotProcessorRunnerSpecBuilder {

	@RunSpec
	public void configure() {
		// Specify custom working directory - not recommended as it is dependent on machine
		workdir(Environments.local().workfile("Snapshots")).snapshots("Source_projects_config.zip")
				.transfomer(SampleModelTransformation.class);

		// // This uses the default working directory from local env
		// snapshots("project.zip", "XXXXSystem.zip").transfomer(SampleTransformationScript.class);
		// //
		// snapshots("project.zip", "XXXXSystem.zip").listener(SampleTransformationScript.class);

		// 1. Run for a single snapshot file
		// 2. Run for a list of snapshot files
		// 2. Run for environment instance
		// 3. Run for list of environments
	}

	/**
	 * You can run the same transformation as a normal Java program using the {@link DefaultSnapshotProcessorRunner#execute()}
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		DefaultSnapshotProcessorRunner.execute(TransformConfigurationSnapshotsJQLRun.class);
	}

}
