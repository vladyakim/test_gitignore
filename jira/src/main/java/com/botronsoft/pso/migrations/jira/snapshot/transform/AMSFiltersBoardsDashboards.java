package com.botronsoft.pso.migrations.jira.snapshot.transform;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.jira.rollout.model.diff.impl.matching.identity.AgileBoardObjectIdProvider;
import com.botronsoft.jira.rollout.model.diff.impl.matching.identity.FilterObjectIdProvider;
import com.botronsoft.jira.rollout.model.diff.impl.matching.identity.JiraPortalPageObjectIdProvider;
import com.botronsoft.jira.rollout.model.jiraconfiguration.JiraBaseObject;
import com.botronsoft.jira.rollout.model.jiraconfiguration.agile.AgileBoard;
import com.botronsoft.jira.rollout.model.jiraconfiguration.dashboard.JiraPortalPage;
import com.botronsoft.jira.rollout.model.jiraconfiguration.filter.JiraFilter;
import com.botronsoft.pso.cmj.modelprocessor.api.lookup.ObjectLookup;
import com.botronsoft.pso.migration.framework.environment.Environment;
import com.botronsoft.pso.migration.framework.transform.events.SessionComplete;
import com.botronsoft.pso.migration.framework.transform.events.SnapshotTransformed;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;

/**
 * Legacy class for retrieving and renaming duplicated boards, filters and dashboards. Class also generate new unique names. Code left for
 * reference as yet to be refactored and incorporated in snapshot processor.
 *
 */
@Deprecated
public class AMSFiltersBoardsDashboards {
	private Logger log = LoggerFactory.getLogger(AMSFiltersBoardsDashboards.class);

	private List<ObjectBean> allBoards = new ArrayList<>();
	private List<ObjectBean> allDashBoards = new ArrayList<>();
	private List<ObjectBean> allFilters = new ArrayList<>();

	static class ObjectBean {
		Environment env;
		String objectId;
		String nativeId;
		String name;
		Class<? extends JiraBaseObject> clazz;

		public ObjectBean(Environment env, String objectId, String nativeId, String name, Class<? extends JiraBaseObject> clazz) {
			this.env = env;
			this.objectId = objectId;
			this.nativeId = nativeId;
			this.clazz = clazz;
			this.name = name;
		}

		public String getObjectId() {
			return objectId;
		}
	}

	@Subscribe
	public void collectNames(SnapshotTransformed e) {
		log.info("Collecting names from {}", e.getEnvironment().getName());
		// Collect all boards
		AgileBoardObjectIdProvider agileBoardObjectIdProvider = new AgileBoardObjectIdProvider();
		ObjectLookup lookup = e.getSnapshot().lookup();
		allBoards.addAll(lookup
				.allOf(AgileBoard.class).stream().map(b -> new ObjectBean(e.getEnvironment(),
						agileBoardObjectIdProvider.getPrimaryObjectId(b), b.getNativeId(), b.getName(), AgileBoard.class))
				.collect(toList()));

		// FilterId
		FilterObjectIdProvider filterObjectIdProvider = new FilterObjectIdProvider();
		allFilters
				.addAll(lookup
						.allOf(JiraFilter.class).stream().map(b -> new ObjectBean(e.getEnvironment(),
								filterObjectIdProvider.getPrimaryObjectId(b), b.getNativeId(), b.getName(), JiraFilter.class))
						.collect(toList()));

		// DashboardsId
		JiraPortalPageObjectIdProvider dashboardObjectIdProvider = new JiraPortalPageObjectIdProvider();
		allDashBoards.addAll(lookup
				.allOf(JiraPortalPage.class).stream().map(b -> new ObjectBean(e.getEnvironment(),
						dashboardObjectIdProvider.getPrimaryObjectId(b), b.getNativeId(), b.getName(), JiraPortalPage.class))
				.collect(toList()));
	}

	/**
	 * @param e
	 */
	@Subscribe
	public void sessionComplete(SessionComplete e) {

		List<String> renames = new ArrayList<>();

		// //Boards
		// if a board id frequency is more than 1 this means the object is available on more than one instance and has to be renamed
		generateRenames(renames, allBoards);
		generateRenames(renames, allFilters);
		generateRenames(renames, allDashBoards);

		//
		renames.forEach(r -> {
			System.out.println(r);
		});
		//
		log.info("Persisting boards, filters, dashboards rename file to ....");
	}

	private void generateRenames(List<String> renames, List<ObjectBean> objects) {
		List<String> boardIds = objects.stream().map(ObjectBean::getObjectId).collect(toList());
		objects.forEach(b -> {
			if (Collections.frequency(boardIds, b.getObjectId()) > 1) {
				renames.add(Lists.newArrayList(b.env.getName(), b.clazz.getName(), b.nativeId, b.env.getRenamePrefix() + " " + b.name)
						.stream().collect(joining(";")));
			}
		});
	}
}
