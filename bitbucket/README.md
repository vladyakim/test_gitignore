# Module overview #

Soul purpose of **psoend-bitbucket** module is to ensure successful migration of Bitbucket configuration and data from one instance to another with least disruption of end-user experience as possible.

Below the general process and most common migration scenario is described in details.

## Bitbucket migration steps in details ##

*  **Generate AMS**
AMS document layout and content might defer from engagement to engagement due to instance and/or Customer specifics. Still, any previous Bitbucket AMS can be used as a starting point for preparing current engagement AMS and modify it according to the needs. Common addendum to the AMS is the Appendix section that list in details all conflicting and non-conflicting items, how conflict are handled and in general provide overview what the impact on TARGET instance will be.
    * `com.botronsoft.pso.migrations.bitbucket.BitbucketAMSGenerator` creates AMS appendix details with all new and conflicting project (personal projects included).
    
*  **Generate Mapping**
Having project scope defined and any conflict in project name/keys resolved, next step is to have proper mapping to be used throughout migration.
    * `com.botronsoft.pso.migrations.bitbucket.GenerateMapping` generates mapping files according to the input provided. Use class to set any known mapping, e.g. project key and/or name, as well as migration scope - list of migrated projects. Use sample and adjust accordingly.
        

*  **Get snapshots** Configuration Manager for Bitbucket (CMB for short) snapshots are used for configuration and data move between instances.
    *  Once setup is done (`Env.java` and `migration.properties` and/or `local.migration.properties` updated accordingly),  snapshot from SOURCE can be taken.
    *  Snapshot creation executes as JUnit tests that are available in:
`com.botronsoft.pso.migrations.bitbucket.SnapshotCreator`. Refer to javadoc within class to pickup snapshot creation options according to the needs. Snapshots can be automatically downloaded for further proccesing, such as transformations, e.g. project key changes. 
**NB!!!** using the download method you must have the properties used set in you migration.properties or local.migration.properties file with proper values.
    
*  **User management**
To be updated! 


*  **Apply transformations** 
Having AMS approved we proceed with applying SOURCE snapshot transformations to align SOURCE data and configuration with approved AMS. 
	`com.botronsoft.pso.migrations.bitbucket.BitbucketSnapshotTransformer` use class to apply proper transformations according to approved AMS


*  **Migrate SOURCE**
	* **Upload snapshot on TARGET**	 
`com.botronsoft.pso.migrations.bitbucket.uploadSnapshot()` 
**NB!!!** using the upload method you must have the properties used set in you migration.properties or local.migration.properties file with proper values. 	
	* **Deploy on TARGET**	 
`com.botronsoft.pso.migrations.bitbucket.deployOnTarget()`. Go through class/method javadoc for further details on option selection.
    * **Reindex JIRA issues**	  
`com.botronsoft.pso.migrations.bitbucket.reindexJIRAIssuesOnTarget()` rebuilds links to JIRA issues from a linked JIRA instance
	* **Links update**
To do	

	 
*  **Run tests** 
To do

*  **Provide Customer with migration reports** 
To do
    

*  **Useful migration scripts** 
To do
 
