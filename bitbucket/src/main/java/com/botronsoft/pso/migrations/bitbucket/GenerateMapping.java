package com.botronsoft.pso.migrations.bitbucket;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import com.botronsoft.pso.commons.mapping.api.MapperWriter;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.migration.framework.environment.BitbucketEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;

public class GenerateMapping {

	MappingFactory factory;
	MapperWriter writer;

	public GenerateMapping(BitbucketEnvironment env) throws URISyntaxException {

		URI url = ClassLoader.class.getResource("/bitbucket/source").toURI();
		factory = new FileSystemMappingFactory(Paths.get(url).toString() + "/" + env.getKey());
		writer = factory.createWriter();
	}

	private void writeBitbucketProjectKeys() {
		writer.writeMap(ObjectTypeKeys.BB_PROJECTKEY, "PII", "SRCPII");
	}

	private void writeBitbucketProjectNames() {
		writer.writeMap(ObjectTypeKeys.BB_PROJECTNAME, "Project 2", "Source Project 2");
	}

	private void writeUserNames() {
		writer.writeMap(ObjectTypeKeys.BB_USER_NAME, "vlad", "vladmir");
	}

	private void writeMigratedBitbucketProjects() {
		writer.writeMap(ObjectTypeKeys.BB_MIGRATED_PROJECTKEYS, "PI", null);
		writer.writeMap(ObjectTypeKeys.BB_MIGRATED_PROJECTKEYS, "PII", null);
		writer.writeMap(ObjectTypeKeys.BB_MIGRATED_PROJECTKEYS, "PIII", null);
		writer.writeMap(ObjectTypeKeys.BB_MIGRATED_PROJECTKEYS, "PVI", null);
	}

	public static void main(String args[]) throws URISyntaxException, IOException {

		Environments.bitbucket(Env.SOURCE_INSTANCES).parallelStream().forEach(env -> {
			GenerateMapping mapping;
			try {
				mapping = new GenerateMapping(env);
				mapping.writeMigratedBitbucketProjects();
				mapping.writeBitbucketProjectKeys();
				mapping.writeBitbucketProjectNames();
				mapping.writeUserNames();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		});
	}
}
