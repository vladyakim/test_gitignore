package com.botronsoft.pso.migrations.bitbucket;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.json.JSONException;
import org.json.JSONObject;

import com.botronsoft.bitbucket.cmb.rest.client.CMBRestClient;
import com.botronsoft.bitbucket.cmb.rest.client.model.SimpleProject;
import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.migration.framework.environment.BitbucketEnvironment;
import com.botronsoft.pso.migration.framework.environment.DefaultBitbucketEnvironment;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
import com.botronsoft.pso.rest.client.BitBucketClient;
import com.botronsoft.pso.rest.client.UPMClient;
import com.botronsoft.pso.rest.client.model.AddonInfo;

public class BitbucketAMSGenarator extends BitbucketAbstractAMSGenerator {

	private static CMBRestClient sourceCMBClient = Env.SOURCE_BB_ENV.getCMBRestClient();
	private static CMBRestClient targetCMBClient = Env.TARGET_BB_ENV.getCMBRestClient();
	private static BitBucketClient sourceRestClient = Env.SOURCE_BB_ENV.getBitBucketClient();
	private static BitBucketClient targetRestClient = Env.TARGET_BB_ENV.getBitBucketClient();
	private static UPMClient sourceUPMClient = Env.SOURCE_BB_ENV.getUpmClient();
	private static UPMClient targetUPMClient = Env.TARGET_BB_ENV.getUpmClient();
	private static BitbucketEnvironment env = new DefaultBitbucketEnvironment(Env.SOURCE);
	private static MappingFactory mappingFactory = new FileSystemMappingFactory("src/main/resources/" + env.getKey());
	private static MapperReader mappingReader = mappingFactory.createReader();

	@Override
	protected String prefix(String name) {
		// TODO GET project and name prefix?
		return null;
	}

	void generateScope() {
		MARKUP_BUILDER.sectionTitleLevel1("Scope");

		List<List<String>> instances = new ArrayList<List<String>>();
		String sourceBaseUrl = Env.SOURCE_BB_ENV.getBaseUrl();
		String targetBaseUrl = Env.TARGET_BB_ENV.getBaseUrl();
		instances.add(Arrays.asList(buildHyperlink(sourceBaseUrl, sourceBaseUrl), buildHyperlink(targetBaseUrl, targetBaseUrl)));

		MARKUP_BUILDER.tableWithColumnSpecs(columns("Source URL", "Target URL"), instances);
	}

	void generateProjects() {

		Map<String, String> sourceProjects = sourceCMBClient.project().getAllProjects().stream()
				.collect(toMap(SimpleProject::getKey, SimpleProject::getName));
		Map<String, String> targetProjects = targetCMBClient.project().getAllProjects().stream()
				.collect(toMap(SimpleProject::getKey, SimpleProject::getName));

		Set<String> projectWithConflictingKeys = sourceProjects.keySet().stream().collect(toSet());
		projectWithConflictingKeys.retainAll(targetProjects.keySet());
		Set<String> projectWIthConflictingNames = sourceProjects.entrySet().stream().filter(entry -> {
			List<String> lowerProjectNames = targetProjects.values().stream().map(e -> e.toLowerCase()).collect(toList());
			return lowerProjectNames.contains(entry.getValue().toLowerCase());
		}).map(e -> e.getKey()).collect(toSet());

		Set<String> additionsProjectKeys = sourceProjects.entrySet().stream()
				.filter(project -> !projectWithConflictingKeys.contains(project.getKey())
						&& !projectWIthConflictingNames.contains(project.getKey()))
				.map(p -> p.getKey()).collect(toSet());

		MARKUP_BUILDER.sectionTitleLevel1("Projects");

		Map<String, String> newProjects = sourceProjects.entrySet().stream().filter(entry -> additionsProjectKeys.contains(entry.getKey()))
				.collect(toMap(Map.Entry::getKey, Map.Entry::getValue));

		generateAdditionsTable(newProjects, columns("Project Key", "Project Name"));

		MARKUP_BUILDER.sectionTitleLevel2("Conflicting Project keys");

		generateMatchesKeysTable(
				sourceProjects.entrySet().stream().filter(entry -> projectWithConflictingKeys.contains(entry.getKey()))
						.collect(toMap(Map.Entry::getKey, Map.Entry::getValue)),
				columns("Source Project Name", "Source Project Key", "Conflicting Reason", "Conflict Resolution"), "Project Key", "N/A");

		MARKUP_BUILDER.sectionTitleLevel2("Conflicting Project names");

		generateMatchesNamesTable(
				sourceProjects.entrySet().stream().filter(entry -> projectWIthConflictingNames.contains(entry.getKey()))
						.collect(toMap(Map.Entry::getKey, Map.Entry::getValue)),
				columns("Source Project Name", "Source Project Key", "Conflicting Reason", "Conflict Resolution"), targetProjects, "N/A");

	}

	void generatePersonalProjects() {

		Map<String, List<String>> sourcePersonalProjects = new HashMap<String, List<String>>();
		Map<String, List<String>> targetPersonalProjects = new HashMap<String, List<String>>();

		// gets all SOURCE personal projects with at least one repo within
		List<String> sourceUsers = sourceRestClient.getAllUsers();
		for (String user : sourceUsers) {
			List<JSONObject> sReposPage = sourceRestClient.getProjectRepositories("~" + user, 0, 25);
			if (sReposPage.size() > 0) {
				sourcePersonalProjects.put("~" + user, sReposPage.stream().map(json -> {
					String slugName = null;
					try {
						slugName = json.getString("slug");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return slugName;
				}).collect(toList()));
			}
		}

		// gets all TARGET personal projects with at least one repo within
		List<String> targetUsers = targetRestClient.getAllUsers();
		for (String user : targetUsers) {
			List<JSONObject> tReposPage = targetRestClient.getProjectRepositories("~" + user, 0, 25);
			if (tReposPage.size() > 0) {
				targetPersonalProjects.put("~" + user, tReposPage.stream().map(json -> {
					String slugName = null;
					try {
						slugName = json.getString("slug");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return slugName;
				}).collect(toList()));
			}
		}

		MARKUP_BUILDER.sectionTitleLevel2("Personal Projects");

		Map<String, String> additionalProjects = sourcePersonalProjects.keySet().stream().filter(key -> {
			String mappedUsername = mappingReader.map(ObjectTypeKeys.BB_USER_NAME).get(key) == null ? key
					: mappingReader.map(ObjectTypeKeys.BB_USER_NAME).get(key).stream().collect(toList()).get(0);
			return !targetPersonalProjects.keySet().contains(mappedUsername);
		}).collect(toMap(entry -> String.valueOf(entry), entry -> String.valueOf("")));

		generateAdditionsTable(additionalProjects, columns("Personal Project Key"));

		MARKUP_BUILDER.sectionTitleLevel3("Matching Personal Projects without matching repository names in between");

		Map<String, String> matchingProjectsWithoutConflicts = sourcePersonalProjects.entrySet().stream().filter(entry -> {
			String mappedUsername = mappingReader.map(ObjectTypeKeys.BB_USER_NAME).get(entry.getKey()) == null ? entry.getKey()
					: mappingReader.map(ObjectTypeKeys.BB_USER_NAME).get(entry.getKey()).stream().collect(toList()).get(0);
			return targetPersonalProjects.keySet().contains(mappedUsername)
					&& !targetPersonalProjects.get(mappedUsername).stream().anyMatch(repo -> entry.getValue().contains(repo));
		}).collect(toMap(entry -> String.valueOf(entry.getKey()), entry -> String.valueOf("")));

		generateAdditionsTable(matchingProjectsWithoutConflicts, columns("Non-conflicting Personal Project Key"));

		MARKUP_BUILDER.sectionTitleLevel3("Matching Personal Projects with conclicting repository names");

		Map<String, List<String>> matchingProjectsWithRepoConflicts = sourcePersonalProjects.entrySet().stream().filter(entry -> {
			String mappedUsername = mappingReader.map(ObjectTypeKeys.BB_USER_NAME).get(entry.getKey()) == null ? entry.getKey()
					: mappingReader.map(ObjectTypeKeys.BB_USER_NAME).get(entry.getKey()).stream().collect(toList()).get(0);
			return targetPersonalProjects.keySet().contains(mappedUsername)
					&& targetPersonalProjects.get(mappedUsername).stream().anyMatch(repo -> entry.getValue().contains(repo));
		}).collect(toMap(entry -> String.valueOf(entry.getKey()), entry -> {
			String mappedUsername = mappingReader.map(ObjectTypeKeys.BB_USER_NAME).get(entry.getKey()) == null ? entry.getKey()
					: mappingReader.map(ObjectTypeKeys.BB_USER_NAME).get(entry.getKey()).stream().collect(toList()).get(0);
			List<String> targetRepositories = targetPersonalProjects.get(mappedUsername);
			return entry.getValue().stream().distinct().filter(targetRepositories::contains).collect(toList());
		}));

		generatePersonalProjectsConflictsTable(matchingProjectsWithRepoConflicts,
				columns("Source Project Key", "Conflicting Repositories", "Conflict Resolution"), "N/A");

	}

	void generatePermissions() {

		MARKUP_BUILDER.sectionTitleLevel1("Permissions");

		MARKUP_BUILDER.newLine();
		MARKUP_BUILDER.paragraph("Project and repository permissions will retain as is.\n" + "\n" + "Except for the following cases:\n"
				+ "\n" + "* For all non-existing users on TARGET, the project or repository level permission will be dropped.\n"
				+ "* For all non-existing groups on TARGET, the project or repository level permission will be dropped.\n"
				+ "* In case group exists on both instances and has more users on TARGET, an unauthorized access will be granted to them.");

		MARKUP_BUILDER.sectionTitleLevel2("Project Permissions");
		MARKUP_BUILDER.newLine();
		MARKUP_BUILDER.paragraph("All project level permissions will be migrated.");

		MARKUP_BUILDER.sectionTitleLevel2("Repository Permissions");
		MARKUP_BUILDER.newLine();
		MARKUP_BUILDER.paragraph("All repository level permissions will be migrated.\n");

		MARKUP_BUILDER.sectionTitleLevel3("Matching groups");
		MARKUP_BUILDER.newLine();

		// All source groups used in project permissions
		List<String> sourceGroupsInPermissions = sourceCMBClient.project().getAllProjects().stream().map(p -> p.getKey()).collect(toList())
				.stream().map(p -> sourceRestClient.getProjectGroupPermissions(p, 0, 999).getList("values.group?.name"))
				.flatMap(Collection::stream).map(entry -> entry.toString()).collect(toList());

		// All source groups used in repo permissions
		sourceCMBClient.project().getAllProjects().stream().map(p -> p.getKey()).forEach(p -> {
			sourceRestClient.getProjectRepositories(p, 0, 999).forEach(repo -> {
				String slugName = null;
				try {
					slugName = repo.getString("slug");

				} catch (JSONException e) {
					// do nothing
				}
				if (slugName != null) {
					List<String> repoGroups = sourceRestClient.getRepoGroupPermissions(p, slugName, 0, 9999).getList("values.group?.name")
							.stream().map(entry -> entry.toString()).collect(toList());
					sourceGroupsInPermissions.addAll(repoGroups);
				}

			});
		});

		List<String> allTargetGroups = targetRestClient.getAllGroups();

		// leave only common groups
		sourceGroupsInPermissions.retainAll(allTargetGroups);

		Set<String> matchingGroups = new TreeSet<String>(sourceGroupsInPermissions);

		if (sourceGroupsInPermissions.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
		} else {
			MARKUP_BUILDER.tableWithColumnSpecs(columns("Group name"),
					matchingGroups.stream().map(g -> Arrays.asList(g)).collect(toList()));
		}
	}

	void generateUserInstalledAddons() {
		List<AddonInfo> sourceUserInstalledAddons = sourceUPMClient.getAddons().stream().filter(addon -> addon.isUserInstalled())
				.collect(toList());
		List<AddonInfo> targetUserInstalledAddons = targetUPMClient.getAddons().stream().filter(addon -> addon.isUserInstalled())
				.collect(toList());

		MARKUP_BUILDER.sectionTitleLevel1("User-installed add-ons");

		Map<String, List<String>> result = new TreeMap<String, List<String>>();

		sourceUserInstalledAddons.stream().forEach(sourceAddon -> {

			String name = sourceAddon.getName();
			ArrayList<String> values = new ArrayList<String>();

			if (sourceAddon.getMarketplaceListing().isPresent()) {
				name = buildHyperlink(sourceAddon.getName(), sourceAddon.getMarketplaceListing().get());
			}

			Optional<AddonInfo> matchingTargetAddon = targetUserInstalledAddons.stream()
					.filter(addon -> addon.getKey().equalsIgnoreCase(sourceAddon.getKey())).findFirst();

			values.add(sourceAddon.getVersion());
			values.add(matchingTargetAddon.isPresent() ? matchingTargetAddon.get().getVersion() : "N/A");
			values.add("TBD");
			values.add("TBD");

			result.put(name, values);
		});

		generateUserInstalledAddonsTable(result, columns("Add-on", "SOURCE version", "TARGET version (if available)",
				"Would it be available on TARGET", "Data/configuration migration required"));

	}

	void generateGroups() {
		MARKUP_BUILDER.sectionTitleLevel1("Groups");
	}

	void generateUsers() {
		MARKUP_BUILDER.sectionTitleLevel1("Users");
	}

	void generateAMSData() {
		macroToc();
		generateScope();
		generateProjects();
		generatePersonalProjects();
		generatePermissions();
		generateGroups();
		generateUsers();
		generateUserInstalledAddons();
		System.out.println(MARKUP_BUILDER.toString());
	}

	private String buildHyperlink(String lable, String url) {
		return "[" + lable + "|" + url + "]";
	}

	public static void main(String[] args) {
		new BitbucketAMSGenarator().generateAMSData();

	}

}
