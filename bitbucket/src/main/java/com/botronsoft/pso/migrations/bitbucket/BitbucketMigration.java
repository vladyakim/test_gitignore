package com.botronsoft.pso.migrations.bitbucket;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.io.File;
import java.util.Collection;
import java.util.Set;

import org.json.JSONException;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.bitbucket.cmb.rest.client.CMBRestClient;
import com.botronsoft.bitbucket.cmb.rest.client.model.DeployMode;
import com.botronsoft.bitbucket.cmb.rest.client.model.DeployOptions;
import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.migration.framework.environment.BitbucketEnvironment;
import com.botronsoft.pso.migration.framework.environment.DefaultBitbucketEnvironment;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
import com.botronsoft.pso.rest.client.BitBucketClient;
import com.botronsoft.pso.rest.client.model.AddonInfo;

public class BitbucketMigration {

	private static final Logger log = LoggerFactory.getLogger(BitbucketMigration.class);

	private static CMBRestClient sourceCMBClient = Env.SOURCE_BB_ENV.getCMBRestClient();
	private static CMBRestClient targetCMBClient = Env.TARGET_BB_ENV.getCMBRestClient();
	private static BitBucketClient targetBBRestClient = Env.TARGET_BB_ENV.getBitBucketClient();
	private static BitbucketEnvironment env = new DefaultBitbucketEnvironment(Env.SOURCE);
	private static MappingFactory mappingFactory = new FileSystemMappingFactory("src/main/resources/" + env.getKey());
	private static MapperReader mappingReader = mappingFactory.createReader();

	/**
	 * Snapshot upload method. Bitbucket snapshot id and necessary path variables must be defined in migration.properties.
	 */
	@Test
	public void uploadSnapshot() {

		// TODO Must delete snapshot.xml file and rename transformed one to snapshot.xml
		// automated upload
		String snapshotId = Env.LOCAL_ENV.getProperty("bitbucket.snapshotid");
		String sourcePath = Env.LOCAL_ENV.getProperty("bitbucket.workdir") + "/" + snapshotId;
		String destinationPath = "export/backups/" + snapshotId + "/";
		targetCMBClient.fileManager().upload(destinationPath, new File(sourcePath));

	}

	/**
	 * Deploys a snapshot on Target instance.
	 * <p>
	 * Deployment options:
	 * <ul>
	 * <li>create() - snapshot id to be deployed as input parameter
	 * <li>dataPath() - path to Git data and repository attachments if those are provided separately and not included within the snapshot
	 * <li>mode() - mode of deploy process:
	 * <ul>
	 * <li>DEFAULT - if repository exists - skip it (do not merge any repository config/data). If repository doesn't exist - ADD</li>
	 * <li>NO_PULL_REQUESTS - imports without pull request. If repository exists - skip it (do not merge any repository config/data). If
	 * repository doesn't exist - ADD without any pull requests</li>
	 * <li>PULL_REQUESTS_ONLY - imports only pull requests. There is a file which contains a list of all repositories that must be skipped.
	 * This file is generated on every deployment. If such a file is contained in the snapshot - it is read and all repositories listed
	 * inside are skipped. If repository exists and is listed in the file - skip. If repository exists but is not listed in the file -
	 * delete all pull requests and merge all from snapshot. If repository doesn't exist - fail and stop deployment</li>
	 * <li>SETTINGS_ONLY - only project and repository settings are imported</li>
	 * <li>AUDIT_ONLY - only project and repository audit data is imported</li>
	 * </ul>
	 * <li>build() - sets deployment options
	 */
	@Test
	public void deployOnTarget() {

		String snapshotId = Env.LOCAL_ENV.getProperty("bitbucket.snapshotid");
		String datPath = "/tmp/data/";

		targetCMBClient.logging().debug();

		DeployOptions options = DeployOptions.Builder.create(snapshotId).dataPath(datPath).mode(DeployMode.DEFAULT).build();

		targetCMBClient.deploy(options);
	}

	/**
	 * Deletes all SOURCE in-scope projects, <b>except for personal projects!!!</b>
	 */
	// @Ignore
	@Test
	public void deleteTargetProjects() {

		Set<String> projectsToDelete = mappingReader.map(ObjectTypeKeys.BB_MIGRATED_PROJECTKEYS).keySet().stream().map(key -> {
			String destinationKey = key;
			if (mappingReader.map(ObjectTypeKeys.BB_PROJECTKEY).containsKey(key)) {
				destinationKey = mappingReader.map(ObjectTypeKeys.BB_PROJECTKEY).get(key).stream().collect(toList()).get(0);
			}
			return destinationKey;
		}).collect(toSet());

		projectsToDelete.forEach(project -> {
			// delete all repos except for any personal repos from SOURCE
			targetBBRestClient.getProjectRepositories(project, 0, 9999).forEach(repoEntry -> {
				try {
					String repoName = repoEntry.getString("slug");
					targetBBRestClient.deleteRepository(project, repoName);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			});

			// delete actual project
			targetBBRestClient.deleteProject(project);

		});

	}

	/**
	 * Rebuilds links between Bitbucket and JIRA issues for provided Bitbucket projects.
	 */
	@Test
	public void reindexJIRAIssuesOnTarget() {

		Set<String> scopedProjects = mappingReader.map(ObjectTypeKeys.BB_MIGRATED_PROJECTKEYS).keySet().stream().map(key -> {
			String destinationKey = key;
			if (mappingReader.map(ObjectTypeKeys.BB_PROJECTKEY).containsKey(key)) {
				destinationKey = mappingReader.map(ObjectTypeKeys.BB_PROJECTKEY).get(key).stream().collect(toList()).get(0);
			}
			return destinationKey;
		}).collect(toSet());

		log.info("Reindexing projects {} started!", scopedProjects);

		targetCMBClient.jira().reindexJiraIssues(scopedProjects);

		log.info("Finished re-indexing projects!!!!");
	}

	@Test
	public void addOns() {
		Collection<AddonInfo> addons = Env.SOURCE_BB_ENV.getUpmClient().getAddons().stream().filter(addon -> addon.isUserInstalled())
				.collect(toList());
		System.out.println();

	}

}
