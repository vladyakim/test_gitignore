package com.botronsoft.pso.migrations.bitbucket;

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.bitbucket.cmb.rest.client.CMBRestClient;
import com.botronsoft.bitbucket.cmb.rest.client.model.CreateSnapshotOptions;
import com.botronsoft.bitbucket.cmb.rest.client.model.Snapshot;
import com.botronsoft.pso.migrations.Env;

/**
 * <b>CMB snapshot creator</b><br>
 * Proceeding with snapshot creation and option selection, there are few considerations to take into account.
 * <li><b>Scenario 1</b> Source instance stores insignificant amount of data, then repository and attachment data can be included within the
 * snapshot itself, either compressed or not</li>
 * <li><b>Scenario 2</b> We have Source instance with huge amount of repository and attachment data. Having GB or even TBs sized snapshot is
 * impractical, thus consider coping Source data folder to Target (e.g. rsync) and provide it as repository/attachment source path during
 * deploy</li>
 * <li><b>Scenario 3</b> Due security limitations we might not be allowed to copy (rsync) data from SOURCE file system, then regardless data
 * size, inclusion within snapshot might be the only option.</li>
 * <p>
 * <b>Snapshot creation options</b>
 * <li>builder() - snapshot name is provided as input parameter</li>
 * <li>description() - sets snapshot description</li>
 * <li>withData() - includes Git data and repository attachments</li>
 * <li>exploded() - stores exploded in a directory (instead of a zip file)</li>
 * <li>compressed() - compress archives (Git repository archives)</li>
 * <li>withForks() - export forked repositories</li>
 * <li>withPullRequests() - export pull requests</li>
 * <li>build() - sets snapshot options</li>
 */
public class SnapshotCreator {

	private static final Logger log = LoggerFactory.getLogger(SnapshotCreator.class);

	private static CMBRestClient sourceCMBClient;

	@BeforeClass
	public static void initClient() {
		sourceCMBClient = Env.SOURCE_BB_ENV.getCMBRestClient();
	}

	/**
	 * Method generating CMB snapshot of all projects on current SOURCE instance.
	 * <p>
	 * <ul>
	 * <li>Personal projects are included only if there's at least one repository within.</li></li>
	 * </ul>
	 * </ul>
	 * <ul>
	 * <li>Logging level can be set be using this sample construct</li>
	 * <ul>
	 * <li>Use debug for troubleshooting and detailed progress monitoring.
	 *
	 * <pre>
	 * sourceCMBClient.logging().debug();
	 * </pre>
	 *
	 * </li>
	 * </ul>
	 * </ul>
	 * <ul>
	 * <li>Snapshot option<br>
	 * More on option details refer to {@link SnapshotCreator}</li>
	 * <ul>
	 * <li>withForks() - repository forks are included
	 * <li>exploded() - no compression applied
	 * <li>withPullRequests() - pull requests are included in snapshot</li>
	 * </ul>
	 * </ul>
	 */
	@Test
	public void createProjectSnapshotWithAllProjectsPersonalIncluded() {

		CreateSnapshotOptions options = CreateSnapshotOptions.builder("All_projects").withForks().exploded().withPullRequests().withData()
				.build();

		// enable debug login (follow progress and troubleshoot if needed)
		sourceCMBClient.logging().debug();

		// sourceCMBClient.createSnapshot(options);

		// automated download sample
		Snapshot createSnapshot = sourceCMBClient.createSnapshot(options);
		// sourceCMBClient.fileManager().downloadDirectory("export/backups/" + createSnapshot.getId(),
		// new File(Env.LOCAL_ENV.getProperty("bitbucket.workdir")));

	}

	/**
	 * Method generating CMB snapshot of all projects on current SOURCE instance without personal projects.
	 *
	 * <pre>
	 * <b>Unless personal project repo is forked!</b>
	 * </pre>
	 * <ul>
	 * <li>Logging level can be set be using this sample construct</li>
	 * <ul>
	 * <li>Use debug for troubleshooting and detailed progress monitoring.
	 *
	 * <pre>
	 * sourceCMBClient.logging().debug();
	 * </pre>
	 *
	 * </li>
	 * </ul>
	 * </ul>
	 * <ul>
	 * <li>Snapshot option<br>
	 * More on option details refer to {@link SnapshotCreator}</li>
	 * <ul>
	 * <li>projects() - sets the project scope
	 * <li>withForks() - repository forks are included
	 * <li>exploded() - no compression applied
	 * <li>withPullRequests() - pull requests are included in snapshot</li>
	 * </ul>
	 * </ul>
	 */
	@Test
	public void createProjectSnapshotAllProjectsWithoutPersonal() {

		List<String> projects = sourceCMBClient.project().getAllProjects().stream().map(proj -> proj.getKey()).collect(toList());

		CreateSnapshotOptions options = CreateSnapshotOptions.builder("All_without_personal_projects").projects(projects).withForks()
				.exploded().withPullRequests().build();

		// enable debug login (follow progress and troubleshoot if needed)
		sourceCMBClient.logging().debug();

		sourceCMBClient.createSnapshot(options);

		// automated download sample
		// Snapshot createSnapshot = sourceCMBClient.createSnapshot(options);
		// sourceCMBClient.fileManager().downloadFile("export/backups/" + createSnapshot.getId() + "/snapshot.xml", new File("/tmp/"));

	}

	/**
	 * Method generating CMB snapshot of <b>projects subset</b> from current SOURCE instance.
	 * <p>
	 * <ul>
	 * <li>Projects are included only if explicitly provided (same applied for personal projects)</li>
	 * <ul>
	 * <li>manually add project keys in {@code List<String> projects}.</li>
	 * <li>personal projects are by prepending user name with ~</li>
	 *
	 * <pre>
	 * projects.add("~user");
	 * </pre>
	 *
	 * </ul>
	 * </ul>
	 * <ul>
	 * <li>Logging level can be set be using this sample construct</li>
	 * <ul>
	 * <li>Use debug for troubleshooting and detailed progress monitoring.
	 *
	 * <pre>
	 * sourceCMBClient.logging().debug();
	 * </pre>
	 *
	 * </li>
	 * </ul>
	 * </ul>
	 * <ul>
	 * <li>Snapshot option<br>
	 * More on option details refer to {@link SnapshotCreator}</li>
	 * <ul>
	 * <li>projects() - sets the project scope
	 * <li>withForks() - repository forks are included
	 * <li>exploded() - no compression applied
	 * <li>withPullRequests() - pull requests are included in snapshot</li>
	 * <li>withData() - includes only shared data of in scope projects' repositories</li>
	 * </ul>
	 * </ul>
	 */
	@Test
	public void createProjectSnapshotWithSpecificProjects() {

		List<String> projects = new ArrayList<String>();
		// add subset of projects, e.g.
		// projects.add("~localadmin");
		// projects.add("~ivan");
		// projects.add("PI");
		//
		CreateSnapshotOptions options = CreateSnapshotOptions.builder("Partial_scope").projects(projects).withForks().exploded()
				.withPullRequests().withData().build();

		// enable debug login (follow progress and troubleshoot if needed)
		sourceCMBClient.logging().debug();

		sourceCMBClient.createSnapshot(options);

	}

	/**
	 * Snapshot download method. Bitbucket snapshot id and necessary path variables must be defined in migration.properties.
	 */
	@Test
	public void downloadSnapshot() {

		// automated download
		sourceCMBClient.fileManager().downloadDirectory("export/backups/" + Env.LOCAL_ENV.getProperty("bitbucket.snapshotid"),
				new File(Env.LOCAL_ENV.getProperty("bitbucket.workdir")));

	}

}