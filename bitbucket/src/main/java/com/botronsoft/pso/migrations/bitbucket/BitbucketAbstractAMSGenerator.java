package com.botronsoft.pso.migrations.bitbucket;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.botronsoft.pso.migrations.AbstractAMSGenerator;

import io.github.swagger2markup.markup.builder.MarkupTableColumn;

/**
 * Sample Bitbucket AMS generator
 *
 */
public abstract class BitbucketAbstractAMSGenerator extends AbstractAMSGenerator {

	private ArrayList<String> arrayList;

	protected void generateAdditionsTable(Map<String, String> additions, List<MarkupTableColumn> tableHeaders) {
		if (additions.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}

		List<List<String>> cells = new TreeMap<>(additions).entrySet().stream().map(entry -> {
			List<String> result = new ArrayList<String>();
			if (!entry.getValue().equalsIgnoreCase("")) {
				return Arrays.asList(entry.getKey(), entry.getValue());
			}
			return Arrays.asList(entry.getKey());
		}).collect(toList());
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}

	protected void generateMatchesKeysTable(Map<String, String> conflictingKeys, List<MarkupTableColumn> tableHeaders, String reason,
			String resolution) {
		// List<ComparisonMatch<T>> matched = diff.getMatches(clazz);
		if (conflictingKeys.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}
		List<List<String>> cells = new TreeMap<>(conflictingKeys).entrySet().stream()
				.map(entry -> Arrays.asList(entry.getKey(), entry.getValue(), reason, resolution)).collect(toList());
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}

	protected void generateMatchesNamesTable(Map<String, String> conflictingKeys, List<MarkupTableColumn> tableHeaders,
			Map<String, String> targetProjects, String resolution) {
		if (conflictingKeys.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}
		List<List<String>> cells = new TreeMap<>(conflictingKeys).entrySet().stream().map(entry -> {
			return Arrays.asList(entry.getKey(), entry.getValue(),
					String.format("Target Project with key *%s* with same name *%s*",
							targetProjects.entrySet().stream().filter(project -> project.getValue().equalsIgnoreCase(entry.getValue()))
									.findFirst().get().getKey(),
							targetProjects.entrySet().stream().filter(project -> project.getValue().equalsIgnoreCase(entry.getValue()))
									.findFirst().get().getValue()),
					resolution);
		}).collect(toList());
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}

	protected void generatePersonalProjectsConflictsTable(Map<String, List<String>> conflictingKeys, List<MarkupTableColumn> tableHeaders,
			String resolution) {
		if (conflictingKeys.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}
		List<List<String>> cells = new TreeMap<>(conflictingKeys).entrySet().stream().map(entry -> {
			return Arrays.asList(entry.getKey(), String.join(" \\\\", entry.getValue()), resolution);
		}).collect(toList());
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}

	protected void generateUserInstalledAddonsTable(Map<String, List<String>> addons, List<MarkupTableColumn> tableHeaders) {
		if (addons.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}
		List<List<String>> cells = addons.entrySet().stream().map(entry -> {
			ArrayList<String> columns = new ArrayList<String>(Arrays.asList(entry.getKey()));
			columns.addAll(entry.getValue());
			return columns;
		}).collect(toList());
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}
}
