package com.botronsoft.pso.migrations.bitbucket;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.junit.runners.MethodSorters.NAME_ASCENDING;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.bitbucket.cmb.model.load.BitbucketModelService;
import com.botronsoft.bitbucket.cmb.model.load.Model;
import com.botronsoft.bitbucket.cmb.model.stashconfiguration.StashConfigurationRoot;
import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MapperWriter;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.migration.framework.environment.BitbucketEnvironment;
import com.botronsoft.pso.migration.framework.environment.DefaultBitbucketEnvironment;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;

@FixMethodOrder(NAME_ASCENDING)
public class BitbucketSnapshotTransformer {

	private static final Logger log = LoggerFactory.getLogger(BitbucketSnapshotTransformer.class);

	private static BitbucketEnvironment env = new DefaultBitbucketEnvironment(Env.SOURCE);
	private static MappingFactory mappingFactory = new FileSystemMappingFactory("src/main/resources/" + env.getKey());
	private static MapperReader mappingReader = mappingFactory.createReader();
	private static Model<StashConfigurationRoot> cmbModel;
	private static File snapshotFile;

	// TODO: Do we need writter?
	private MapperWriter mappingWriter;

	@BeforeClass
	public static void initialize() {

		snapshotFile = new File(
				Env.LOCAL_ENV.getFile("bitbucket.workdir") + "/" + Env.LOCAL_ENV.getProperty("bitbucket.snapshotid") + "/snapshot.xml");

		cmbModel = new BitbucketModelService().load(snapshotFile);
	}

	@Test
	public void changeProjectKeys() {
		Map<String, String> renamedProjectKeys = mappingReader.map(ObjectTypeKeys.BB_PROJECTKEY).entrySet().stream()
				.collect(toMap(entry -> entry.getKey(), entry -> entry.getValue().stream().collect(toList()).get(0)));
		cmbModel.getModel().getProjects().stream().filter(p -> renamedProjectKeys.containsKey(p.getKey())).forEach(project -> {
			project.setKey(renamedProjectKeys.get(project.getKey()));
		});
	}

	@Test
	public void renameProjects() {
		Map<String, String> renamedProjectNames = mappingReader.map(ObjectTypeKeys.BB_PROJECTNAME).entrySet().stream()
				.collect(toMap(entry -> entry.getKey(), entry -> entry.getValue().stream().collect(toList()).get(0)));
		cmbModel.getModel().getProjects().stream().filter(p -> renamedProjectNames.containsKey(p.getName())).forEach(project -> {
			project.setName(renamedProjectNames.get(project.getName()));
		});
	}

	@Test
	public void applyUserChanges() {

		// rename users
		Map<String, String> userMap = mappingReader.map(ObjectTypeKeys.BB_USER_NAME).entrySet().stream()
				.collect(toMap(entry -> entry.getKey(), entry -> entry.getValue().stream().collect(toList()).get(0)));
		cmbModel.getModel().getUsers().stream().filter(user -> userMap.containsKey(user.getName())).forEach(user -> {
			user.setName(userMap.get(user.getName()));
		});

		// TODO user merge?

	}

	@AfterClass
	public static void persistChanges() {
		save(snapshotFile);
	}

	private static void save(File modelFile) {
		File outfile = new File(modelFile.getParentFile(), modelFile.getName() + ".transformed.xml");
		try {
			Files.deleteIfExists(outfile.toPath());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		new BitbucketModelService().storeModel(cmbModel.getModel(), outfile);
		log.info("Saved transformed snapshot file to {}", outfile.getAbsolutePath());
	}
}
