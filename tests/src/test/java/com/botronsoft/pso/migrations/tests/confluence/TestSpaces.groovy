package com.botronsoft.pso.migrations.tests.confluence;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.*;
import static org.junit.Assume.*;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import com.botronsoft.pso.commons.mapping.api.MapperReader
import com.botronsoft.pso.commons.mapping.api.MappingFactory
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory
import com.botronsoft.pso.confluence.cmc.rest.client.CMCRestClient
import com.botronsoft.pso.migration.framework.environment.Environments
import com.botronsoft.pso.migrations.Env
import com.botronsoft.pso.migrations.ObjectTypeKeys
import com.botronsoft.pso.rest.client.ConfluenceClient;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import  com.google.common.cache.LoadingCache;
import com.googlecode.junittoolbox.ParallelParameterized;

import groovy.util.logging.Slf4j
import io.restassured.path.json.JsonPath

@RunWith(ParallelParameterized.class)
@Slf4j
public class TestSpaces {
	private static ConfluenceClient sourceClient;
	private static ConfluenceClient targetClient;
	private static CMCRestClient sourceCMCClient
	private static CMCRestClient targetCMCClient

	static MappingFactory factory = new FileSystemMappingFactory(Environments.confluence(Env.SOURCE).getKey());
	static MapperReader reader = factory.createReader();

	private String spaceKey;


	@Parameters(name = "space {0}")
	public static Collection<Object[]> data() throws Exception {
		return new ArrayList<String>(reader.map(ObjectTypeKeys.MIGRATED_SPACE_KEY).keySet());
	}

	static LoadingCache createCache(final ConfluenceClient client) {
		return  CacheBuilder.newBuilder()
				.maximumSize(36)
				.expireAfterWrite(5, TimeUnit.SECONDS)
				.build(
				new CacheLoader<String, JsonPath>() {
					public JsonPath load(String id) throws Exception {
						def res = client.getSpace(id)
						if(res==null) {
							throw new Exception("Space not found")
						}
						return res
					}
				});
	}

	@BeforeClass
	public static void prepare() throws Exception {

		sourceClient = Environments.confluence(Env.SOURCE).getConfluenceClient();
		targetClient = Environments.confluence(Env.TARGET).getConfluenceClient();
		sourceCMCClient = Environments.confluence(Env.SOURCE).getCMCClient();
		targetCMCClient = Environments.confluence(Env.TARGET).getCMCClient();

		String.metaClass.fix = {
			delegate.replace("\u2019", "'")?.replace('–','-').normalize().trim()
		}
	}

	def  LoadingCache sourceCache
	def  LoadingCache targetCache
	def static Collection<String> missingSpaceKeys = [].toSet()

	@AfterClass
	public static void printErrors() throws Exception {
		println missingSpaceKeys.sort()
	}



	public TestSpaces(String spaceKey) {
		this.spaceKey = spaceKey
		sourceCache = createCache(sourceClient)
		targetCache = createCache(targetClient)
	}

	JsonPath getSourceSpace() {
		try {
			sourceCache.get(spaceKey)
		} catch(ExecutionException e) {
			return null
		}
	}
	JsonPath getTargetSpace() {
		try {
			return targetCache.get(reader.map(spaceKey))
		} catch(ExecutionException e) {
			return null
		}
	}


	@Test
	public void spaceContents() {
		assumeTrue(targetSpace!=null)

		def sSpaceTitles = sourceClient.getSpaceContent(spaceKey, null).collect{it.getList("page.results.title")}.flatten()
		def tSpaceTitles = targetClient.getSpaceContent(reader.map(spaceKey), null).collect{it.getList("page.results.title")}.flatten()

		println sSpaceTitles
		println tSpaceTitles

		assertThat("Pages are different different",tSpaceTitles.collect{it?.fix()}.sort(),
		containsInAnyOrder(sSpaceTitles.collect{it?.fix()}.sort().toArray()));
	}
}
