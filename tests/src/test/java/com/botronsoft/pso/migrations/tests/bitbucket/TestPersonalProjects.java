package com.botronsoft.pso.migrations.tests.bitbucket;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.migration.framework.environment.BitbucketEnvironment;
import com.botronsoft.pso.migration.framework.environment.DefaultBitbucketEnvironment;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
import com.botronsoft.pso.rest.client.BitBucketClient;
import com.google.common.collect.Lists;
import com.googlecode.junittoolbox.ParallelParameterized;

@RunWith(ParallelParameterized.class)
public class TestPersonalProjects {

	private static final Logger log = LoggerFactory.getLogger(TestPersonalProjects.class);
	public static final int MAX_PAGE_LIMIT = 1048576;

	private static BitBucketClient sourceClient = Env.SOURCE_BB_ENV.getBitBucketClient();
	private static BitBucketClient targetClient = Env.TARGET_BB_ENV.getBitBucketClient();
	private String sourceUser;
	private String targetUser;
	private static BitbucketEnvironment env = new DefaultBitbucketEnvironment(Env.SOURCE);;
	private static MappingFactory mappingFactory = new FileSystemMappingFactory("src/main/resources/" + env.getKey());
	private static MapperReader mappingReader = mappingFactory.createReader();

	public TestPersonalProjects(String sourceUser, String targetUser) {
		this.sourceUser = sourceUser;
		this.targetUser = targetUser;
	}

	@Parameters(name = "user: {0} -> {1}")
	public static Collection<String[]> data() throws Exception {
		ArrayList<String[]> result = new ArrayList<>();

		List<String> users = Env.SOURCE_BB_ENV.getBitBucketClient().getAllUsers();

		// TODO: mapping currently is not loaded must copy mapping files from bitbucket resource folder
		Map<String, String> usersMapping = mappingReader.map(ObjectTypeKeys.BB_USER_NAME).entrySet().stream()
				.collect(toMap(entry -> entry.getKey(), entry -> entry.getValue().stream().collect(toList()).get(0)));

		for (String user : users) {
			String targetUser = usersMapping.containsKey(user) ? usersMapping.get(user) : user;
			result.add(new String[] { user, targetUser });
		}

		return result;
	}

	@Test
	public void reposMatch() throws Exception {
		List<JSONObject> sReposPage = sourceClient.getProjectRepositories("~" + sourceUser, 0, MAX_PAGE_LIMIT);
		List<JSONObject> tReposPage = targetClient.getProjectRepositories("~" + targetUser, 0, MAX_PAGE_LIMIT);

		assumeTrue("Project is missing on target", tReposPage != null);

		List<String> tRepos = collectRepos(tReposPage);
		List<String> sRepos = collectRepos(sReposPage);

		assertThat("Repositories mismatch", Arrays.asList(tRepos), containsInAnyOrder(sRepos));

		for (Pair<JSONObject, JSONObject> p : pair(sReposPage, tReposPage)) {
			JSONObject sRepo = p.getLeft();
			JSONObject tRepo = p.getRight();
			assertThat("Name is different of repo", (tRepo.get("name")), equalTo(sRepo.get("name")));
			assertThat("Public setting is different of repo", tRepo.get("public"), equalTo(sRepo.get("public")));

		}

	}

	public static <T> List<Pair<T, T>> pair(Iterable<T> left, Iterable<T> right) {
		Iterator<T> rightIterator = right.iterator();
		return Lists.newArrayList(left).stream().map(l -> Pair.of(l, rightIterator.next())).collect(Collectors.toList());
	}

	private static List<String> collectRepos(List<JSONObject> reposPage) {
		return reposPage.stream().map(s -> {
			try {
				return String.valueOf(s.get("slug"));
			} catch (JSONException e) {

				// not sure whether this is the correct way to cause test failure or if I should return empty list instead?!
				log.error("Unable to retrieve repo list missing json element. See exception for futher details\n{}", e.getMessage());
				return null;
			}
		}).collect(Collectors.toList());
	}
}
