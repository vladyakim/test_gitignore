package com.botronsoft.pso.migrations.tests.jira;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import com.botronsoft.pso.commons.csv.MappingFileReader;
import com.botronsoft.pso.jmt.rest.client.reporting.ReportingClient;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.rest.client.JiraClient;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.googlecode.junittoolbox.ParallelParameterized;

import groovy.util.logging.Slf4j;
import io.restassured.path.json.JsonPath;

@RunWith(ParallelParameterized.class)
@Slf4j
public class IssueAttributes {

	private static final JiraClient sourceClient = Environments.jira(Env.SOURCE).getJiraClient();
	private static final ReportingClient sourceJmtReportingClient = Environments.jira(Env.SOURCE).getJmtReportingClient();
	private static final boolean testSubset = false;
	private static final int issueSubsetCount = 5;
	private static JiraClient targetClient;
	private static Map<String, String> projectMap;
	private static Map<String, String> userMap;
	private static Map<String, String> issueMap;
	private static Map<String, String> priorityMap;
	private static Map<String, String> resolutionMap;

	private String sourceKey;
	private String targetKey;

	@Parameters(name = "issues [{0}]:[{1}]")
	public static Collection<Object[]> data() throws Exception {

		List<String> projectSubset = new ArrayList<String>();
		List<String> inScopeIssueKeys = new ArrayList<String>();

		MappingFileReader mappingReader = new MappingFileReader("jira/mappings");
		Set<String> load = mappingReader.loadSet("projectMap.csv").load();
		projectMap = new HashMap<>();
		issueMap = new HashMap<>();

		load.stream().forEach(l -> {
			List<String> list = Arrays.asList(l.split(";"));
			String source = list.get(0);
			String target = list.get(1);
			projectMap.put(source, target);
		});

		load = mappingReader.loadSet("inScopeProjectList.csv").load();
		projectSubset.addAll(load);

		if (testSubset) {
			// Must get all projects if no subset or only those that are in subset if list provided
			List<List<String>> allProjectIssues = new ArrayList<List<String>>();

			if (projectSubset.isEmpty()) {
				allProjectIssues = sourceClient.getAllProjects().keySet().stream()
						.map(pkey -> sourceJmtReportingClient.collectIssueKeys(new ArrayList<String>(Arrays.asList(pkey))))
						.collect(toList());
			} else {
				allProjectIssues = sourceClient.getAllProjects().keySet().stream().filter(key -> projectSubset.contains(key))
						.map(pkey -> sourceJmtReportingClient.collectIssueKeys(new ArrayList<String>(Arrays.asList(pkey))))
						.collect(toList());
			}

			allProjectIssues.stream().forEach(issues -> {
				List<String> randomSubset = issues;
				if (issues.size() > issueSubsetCount) {
					randomSubset = pickNRandom(issues, issueSubsetCount);
				}
				inScopeIssueKeys.addAll(randomSubset);
			});

		} else {
			inScopeIssueKeys = sourceJmtReportingClient.collectIssueKeys(projectSubset);
		}

		inScopeIssueKeys.stream().forEach(issueKey -> {
			// if (Long.valueOf(issueKey.split("-")[1]) <= 10) {
			issueMap.put(issueKey,
					projectMap.get(issueKey.split("-")[0]) != null ? projectMap.get(issueKey.split("-")[0]) + "-" + issueKey.split("-")[1]
							: issueKey);
			// }
		});

		return issueMap.entrySet().stream().map(e -> Arrays.asList(e.getKey(), e.getValue())).map(p -> p.toArray())
				.collect(Collectors.toList());
	}

	static com.google.common.cache.LoadingCache<String, JsonPath> createCache(final JiraClient client) {
		return CacheBuilder.newBuilder().maximumSize(36).expireAfterWrite(5, TimeUnit.SECONDS).build(new CacheLoader<String, JsonPath>() {
			@Override
			public JsonPath load(String key) throws Exception {
				JsonPath res = client.getIssue(key);
				if (res == null) {
					throw new Exception("Issue not found");
				}
				return res;
			}
		});
	}

	private static List<String> pickNRandom(List<String> list, int n) {
		List<String> copy = new ArrayList<String>(list);
		Collections.shuffle(copy);
		return copy.subList(0, n);
	}

	@BeforeClass
	public static void prepare() throws Exception {

		// sourceClient = Environments.jira(Env.SOURCE).getJiraClient();
		targetClient = Environments.jira(Env.TARGET).getJiraClient();

		MappingFileReader mappingReader = new MappingFileReader("jira/user_management");
		Set<String> load = mappingReader.loadSet("user_mapping.csv").load();

		// Load user mapping
		userMap = dataToMap(load, new HashMap<String, String>());

		// Load priority map
		mappingReader = new MappingFileReader("jira/mappings");
		load = mappingReader.loadSet("priorityMap.csv").load();
		priorityMap = dataToMap(load, new HashMap<String, String>());

		// Load priority map
		mappingReader = new MappingFileReader("jira/mappings");
		load = mappingReader.loadSet("resolutionMap.csv").load();
		resolutionMap = dataToMap(load, new HashMap<String, String>());

	}

	@Before
	public void checkTargetIssueExists() {
		assumeThat("Target issue does not exist!", getTargetIssue(), notNullValue());
	}

	private static Map<String, String> dataToMap(Set<String> load, Map<String, String> map) {
		load.stream().forEach(l -> {
			List<String> list = Arrays.asList(l.split(";"));
			String source = list.get(0);
			String target = list.get(1);
			map.put(source, target);
		});

		return map;
	}

	com.google.common.cache.LoadingCache<String, JsonPath> sourceCache;
	com.google.common.cache.LoadingCache<String, JsonPath> targetCache;

	public IssueAttributes(String sourceKey, String targetKey) {
		this.sourceKey = sourceKey;
		this.targetKey = targetKey;
		sourceCache = createCache(sourceClient);
		targetCache = createCache(targetClient);
	}

	JsonPath getTargetIssue() {
		try {

			return targetCache.get(targetKey);
			// return targetClient.getIssue(targetKey);
		} catch (Exception e) {
			return null;
		}
	}

	JsonPath getSourceIssue() {

		try {
			return sourceCache.get(sourceKey);
			// return targetClient.getIssue(targetKey);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Verify that Issue is reported by the same user (applying mapping) on Source and Target
	 */

	@Test
	public void reporter() {

		String sourceReporter = getSourceIssue().getString("fields.reporter.name");
		String sourceReporterMapped = userMap.get(sourceReporter) == null ? sourceReporter : userMap.get(sourceReporter);
		String targetReporter = getTargetIssue().getString("fields.reporter.name");

		assertEquals("Reporter for issue not matching", sourceReporterMapped, targetReporter);

	}

	/**
	 * Verifies that issue assignee match on both SOURCE and TARGET.
	 *
	 * Also cover the case that SOURCE allows unassigned issues whereas TARGET don't and we are setting project lead for unassigned issues
	 * on TARGET
	 */

	@Test
	public void assignee() {

		String sourceAssignee = getSourceIssue().getString("fields.assignee.name");
		String targetAssignee = getTargetIssue().getString("fields.assignee.name");

		// Checks that whether unassigned SOURCE issue is assigned on TARGET, assuming we are setting default assignee
		if ((sourceAssignee == null) && (targetAssignee != null)) {
			sourceAssignee = sourceClient.getProjectInfo(getSourceIssue().getString("fields.project.key")).get("lead.name");
		}

		String sourceAssigneeMapped = userMap.get(sourceAssignee) == null ? sourceAssignee : userMap.get(sourceAssignee);

		assertEquals("Reporter for issue not matching", sourceAssigneeMapped, targetAssignee);
	}

	@Test
	public void priorities() {

		String sourcePriority = getSourceIssue().getString("fields.priority.name");
		String sourcePriorityMapped = priorityMap.get(sourcePriority) == null ? sourcePriority : priorityMap.get(sourcePriority);
		String targetPriority = getTargetIssue().getString("fields.priority.name");

		assertEquals("Priority for issue not matching", sourcePriorityMapped, targetPriority);
	}

	@Test
	public void resolutions() {

		String sourceResolution = getSourceIssue().getString("fields.resolution.name");
		String sourceResolutionMapped = resolutionMap.get(sourceResolution) == null ? sourceResolution
				: resolutionMap.get(sourceResolution);
		String targetResolution = getTargetIssue().getString("fields.resolution.name");

		assertEquals("Resolution for issue not matching", sourceResolutionMapped, targetResolution);

	}

	@Test
	public void commentsCount() throws Exception {

		int sourceCommentsCount = getSourceIssue().getInt("fields.comment.total");
		int targetCommentsCount = getTargetIssue().getInt("fields.comment.total");

		assertEquals("Comment count mismatch", sourceCommentsCount, targetCommentsCount);

	}

	@Test
	public void issueLinksCount() throws Exception {

		List<Object> sourceIssueLinks = getSourceIssue().getList("fields.issuelinks");
		List<Object> targetIssueLinks = getTargetIssue().getList("fields.issuelinks");

		assertEquals("Issue link count mismatch", sourceIssueLinks.size(), targetIssueLinks.size());

	}

	@Ignore
	@Test
	public void issueLinks() throws Exception {

		List<Object> sourceIssueLinks = getSourceIssue().getList("fields.issuelinks");
		List<Object> targetIssueLinks = getTargetIssue().getList("fields.issuelinks");

		// if (sourceIssueLinks.size() == targetIssueLinks.size()) {
		// // TODO: check each outward link
		//
		// } else {
		// fail("Issue links count mismatch!");
		// }
	}

	@Test
	public void sprintCounts() throws Exception {
	}

}
