/**
 *
 */
package com.botronsoft.pso.migrations.tests.jira.tempo;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.botronsoft.pso.rest.client.api.RestClientImpl;
import com.botronsoft.pso.rest.client.tempo.TempoClient;

import io.restassured.path.json.JsonPath;

/**
 * @author AtanasAtanasov
 *
 */
public class TestJIRATempo {

	public static final String URL = "http://localhost:2990";
	public static final String USER_NAME = "admin";
	public static final String USER_PASS = "admin";

	public static final String URL_TARGET = "http://localhost:2990";
	public static final String USER_NAME_TARGET = "admin";
	public static final String USER_PASS_TARGET = "admin";

	private static TempoClient clientSource;
	private static TempoClient clientTarget;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		clientSource = new RestClientImpl(URL, USER_NAME, USER_PASS).getTempoClient();
		clientTarget = new RestClientImpl(URL_TARGET, USER_NAME_TARGET, USER_PASS_TARGET).getTempoClient();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCompareWANames() {

		JsonPath sourceJson = clientSource.getWorkAttributes();
		JsonPath targetJson = clientTarget.getWorkAttributes();

		List<String> sourceNames = sourceJson.get("name");
		List<String> targetNames = targetJson.get("name");

		assertTrue(sourceNames.equals(targetNames));
	}

}
