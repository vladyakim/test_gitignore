package com.botronsoft.pso.migrations.tests.framework

import java.util.concurrent.TimeUnit

import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.LoadingCache

import groovy.transform.TypeChecked
@TypeChecked
class CacheRule implements TestRule{
	Closure loadClosure

	LoadingCache cache

	CacheRule(Closure loadClosure) {
		this.loadClosure =  loadClosure
		cache = createCache()
	}

	CacheRule(int size, Closure loadClosure) {
		this.loadClosure =  loadClosure
		cache = createCache(size)
	}

	def LoadingCache createCache(int size = 36) {
		return  CacheBuilder.newBuilder()
				.maximumSize(size)
				.expireAfterWrite(5, TimeUnit.MINUTES)
				.build(
				new CacheLoader<String, Object>() {
					public Object load(String id) throws Exception {
						def res = loadClosure.call(id)
						if(res==null) {
							throw new Exception("Object not found")
						}
						return res
					}
				});
	}

	/**
	 * Gets the value from the cache, trying to load it first. If value can't be loaded returns <code>null</code>
	 * @param id
	 * @return
	 */
	def Object get(Object id) {
		try {
			return cache.get(id)
		} catch(Throwable e) {
			return null
		}
	}

	@Override
	public Statement apply(Statement base, Description description) {
		return base
	}
}
