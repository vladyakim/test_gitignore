package com.botronsoft.pso.migrations.tests.bitbucket;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.rest.client.BitBucketClient;
import com.google.common.collect.Lists;
import com.googlecode.junittoolbox.ParallelParameterized;

import io.restassured.path.json.JsonPath;

@RunWith(ParallelParameterized.class)
public class TestRepositories {

	private static final Logger log = LoggerFactory.getLogger(TestRepositories.class);
	public static final int MAX_PAGE_LIMIT = 10000;

	private static BitBucketClient sourceClient = Env.SOURCE_BB_ENV.getBitBucketClient();
	private static BitBucketClient targetClient = Env.TARGET_BB_ENV.getBitBucketClient();

	private String projectKey;
	private String repoSlug;

	public TestRepositories(String projectKey, String repoSlug) {
		this.projectKey = projectKey;
		this.repoSlug = repoSlug;
	}

	@Parameters(name = "repo: {1} in project: {0}")
	public static Collection<String[]> data() throws Exception {

		ArrayList<String[]> result = new ArrayList<>();
		List<String> projects = Env.SOURCE_BB_ENV.getBitBucketClient().getAllProjectsKeys();

		for (String key : projects) {

			List<JSONObject> sReposPage = sourceClient.getProjectRepositories(key, 0, MAX_PAGE_LIMIT);
			List<String> sRepos = collectRepos(sReposPage);
			for (String repo : sRepos) {
				result.add(new String[] { key, repo });
			}
		}
		return result;
	}

	@Test
	public void repoBasicDetailsMatch() throws Exception {

		JsonPath sRepo = sourceClient.getRepositoryDetails(projectKey, repoSlug);
		JsonPath tRepo = targetClient.getRepositoryDetails(projectKey, repoSlug);

		assumeTrue("Repository is missing on target", tRepo != null);

		assertThat("Name mismatch", tRepo.get("name"), equalTo(sRepo.get("name")));
		assertThat("Type mismatch", tRepo.get("type"), equalTo(sRepo.get("type")));
		assertThat("Repos slug mismatch", tRepo.get("slug"), equalTo(sRepo.get("slug")));
		assertThat("Repos isPublic setting mismatch", tRepo.get("public"), equalTo(sRepo.get("public")));
	}

	@Test
	public void repoDetailsMatch() throws Exception {

		JsonPath sRepo = sourceClient.getRepositoryDetails(projectKey, repoSlug);
		JsonPath tRepo = targetClient.getRepositoryDetails(projectKey, repoSlug);

		assumeTrue("Repository is missing on target", tRepo != null);

		assertThat("Repos SCM ID mismatch", tRepo.getString("scmId"), equalTo(sRepo.getString("scmId")));
		assertThat("Repos state mismatch", tRepo.getString("state"), equalTo(sRepo.getString("state")));
		assertThat("Repos statusMessage mismatch", tRepo.getString("statusMessage"), equalTo(sRepo.getString("statusMessage")));
		assertThat("Repos forkable mismatch", tRepo.getBoolean("forkable"), equalTo(sRepo.getBoolean("forkable")));
		assertThat("Repos public mismatch", tRepo.getBoolean("public"), equalTo(sRepo.getBoolean("public")));
	}

	@Test
	public void repoDefaultBranchMatch() throws Exception {

		JsonPath sBranch = sourceClient.getRepositoryDefaultBranch(projectKey, repoSlug);
		JsonPath tBranch = targetClient.getRepositoryDefaultBranch(projectKey, repoSlug);

		assumeTrue("Repository is missing on target", tBranch != null);
		assumeTrue("There is no default branch", sBranch != null);

		assertThat("Default Branch display ID mismatch", tBranch.get("displayId"), equalTo(sBranch.get("displayId")));
		assertThat("Default Branch display latest changest mismatch", tBranch.get("latestChangeset"),
				equalTo(sBranch.get("latestChangeset")));
	}

	@Test
	public void repoKeysMatch() throws Exception {

		List<String> sKeys = sourceClient.getRepositoryKeys(projectKey, repoSlug, 0, MAX_PAGE_LIMIT);
		List<String> tKeys = targetClient.getRepositoryKeys(projectKey, repoSlug, 0, MAX_PAGE_LIMIT);

		assertTrue("No corresponding TARGET keys!", !tKeys.isEmpty());

		assertThat("SSH key count not matching.", targetClient.getRepositoryKeysSize(projectKey, repoSlug),
				equalTo(sourceClient.getRepositoryKeysSize(projectKey, repoSlug)));

		for (int i = 0; i < tKeys.size(); i++) {
			String sLabel = sKeys.get(i);
			String tLabel = tKeys.get(i);

			assertThat("SSH Key label mismatch for target " + tLabel + " and source " + sLabel, tLabel, equalTo(sLabel));
		}
	}

	@Test
	public void branchesMatch() throws Exception {

		List<JSONObject> sBranches = sourceClient.getRepositoryBranches(projectKey, repoSlug, 0, MAX_PAGE_LIMIT);
		List<JSONObject> tBranches = targetClient.getRepositoryBranches(projectKey, repoSlug, 0, MAX_PAGE_LIMIT);

		assumeTrue("Repository is missing on target", tBranches != null);

		assertThat("Branch count not matching.", targetClient.getRepositoryBranchesSize(projectKey, repoSlug),
				equalTo(sourceClient.getRepositoryBranchesSize(projectKey, repoSlug)));

		for (int i = 0; i < tBranches.size(); i++) {
			JSONObject sBranch = sBranches.get(i);
			JSONObject tBranch = tBranches.get(i);

			assertThat("Branch name mismatch for target " + tBranch.get("displayId") + " and source " + sBranch.get("displayId"),
					tBranch.get("displayId"), equalTo(sBranch.get("displayId")));
			assertThat("Branch isDefault mismatch for target " + tBranch.get("displayId") + " and source " + sBranch.get("displayId"),
					tBranch.get("isDefault"), equalTo(sBranch.get("isDefault")));
			assertThat("Branch changeset mismatch for target " + tBranch.get("displayId") + " and source " + sBranch.get("displayId"),
					tBranch.get("latestChangeset"), equalTo(sBranch.get("latestChangeset")));
		}
	}

	@SuppressWarnings("unchecked")
	@Test
	public void repoUserPermissionsMatch() throws Exception {

		JsonPath sJson = sourceClient.getRepoUserPermissions(projectKey, repoSlug, 0, MAX_PAGE_LIMIT);
		JsonPath tJson = targetClient.getRepoUserPermissions(projectKey, repoSlug, 0, MAX_PAGE_LIMIT);

		assumeTrue(sJson != null);
		assumeTrue(tJson != null);

		List<Map<String, ?>> sValues = new ArrayList<>(sJson.get("values"));
		List<Map<String, ?>> tValues = new ArrayList<>(tJson.get("values"));

		assertThat("Repo user permissions count not matching. Expected: " + sValues + ", actual:" + tValues, tValues.size(),
				equalTo(sValues.size()));
		Collections.sort(sValues, USER_NAME_COMPARATOR);
		Collections.sort(tValues, USER_NAME_COMPARATOR);
		for (int i = 0; i < tValues.size(); i++) {
			Map<String, ?> sItem = sValues.get(i);
			Map<String, ?> tItem = tValues.get(i);
			String sName = (String) ((Map<String, ?>) sItem.get("user")).get("name");
			assertThat("Permission type mismatch for user " + sName, (String) (tItem.get("permission")),
					equalTo((String) (sItem.get("permission"))));
			assertThat("Permission user type mismatch for user " + sName, (String) ((Map<String, ?>) tItem.get("user")).get("type"),
					equalTo((String) ((Map<String, ?>) sItem.get("user")).get("type")));
		}
	}

	@Ignore
	@SuppressWarnings("unchecked")
	@Test
	public void repoGroupPermissionsMatch() throws Exception {

		JsonPath sJson = sourceClient.getRepoGroupPermissions(projectKey, repoSlug, 0, MAX_PAGE_LIMIT);
		JsonPath tJson = targetClient.getRepoGroupPermissions(projectKey, repoSlug, 0, MAX_PAGE_LIMIT);

		assumeTrue(sJson != null);
		assumeTrue(tJson != null);

		List<Map<String, ?>> sValues = new ArrayList<>(sJson.get("values"));
		List<Map<String, ?>> tValues = new ArrayList<>(tJson.get("values"));
		assertThat("Repo group permissions count not matching. Expected: " + sValues + ", actual:" + tValues, tValues.size(),
				equalTo(sValues.size()));
		Collections.sort(sValues, GROUP_NAME_COMPARATOR);
		Collections.sort(tValues, GROUP_NAME_COMPARATOR);
		for (int i = 0; i < tValues.size(); i++) {
			Map<String, ?> sItem = sValues.get(i);
			Map<String, ?> tItem = tValues.get(i);
			String sName = (String) ((Map<String, ?>) sItem.get("group")).get("name");
			String tName = (String) ((Map<String, ?>) tItem.get("group")).get("name");
			assertThat("Permission group name mismatch ", tName, equalTo(sName));
			assertThat("Permission type mismatch for group " + sName, (String) (tItem.get("permission")),
					equalTo((String) (sItem.get("permission"))));
		}
	}

	@SuppressWarnings("unchecked")
	@Test
	public void incomingPullRequestsMatch() throws Exception {

		JsonPath sJson = sourceClient.getIncomigPullRequests(projectKey, repoSlug, 0, MAX_PAGE_LIMIT);
		JsonPath tJson = targetClient.getIncomigPullRequests(projectKey, repoSlug, 0, MAX_PAGE_LIMIT);

		assumeTrue(sJson != null);
		assumeTrue(tJson != null);

		assertThat("Incoming pull requests total count not matching.", targetClient.getIncomigPullRequestsSize(projectKey, repoSlug),
				equalTo(sourceClient.getIncomigPullRequestsSize(projectKey, repoSlug)));
		List<Map<String, ?>> sPullRequests = new ArrayList<>(sJson.get("values"));
		List<Map<String, ?>> tPullRequests = new ArrayList<>(tJson.get("values"));
		assertThat("Incoming pull request count not matching. Expected: " + sPullRequests + ", actual: " + tPullRequests,
				tPullRequests.size(), equalTo(sPullRequests.size()));

		assumeTrue("No Pull Requests in source repo. Nothing to compare.", sPullRequests.size() > 0);

		Collections.sort(sPullRequests, PULL_REQUEST_COMPARATOR);
		Collections.sort(tPullRequests, PULL_REQUEST_COMPARATOR);
		for (int i = 0; i < tPullRequests.size(); i++) {
			Map<String, ?> sPullRequest = sPullRequests.get(i);
			Map<String, ?> tPullRequest = tPullRequests.get(i);
			String sTitle = (String) sPullRequest.get("title");
			String tTitle = (String) tPullRequest.get("title");
			assertThat("Incomig pull request title mismatch for target " + tTitle + " and source " + sTitle, tTitle, equalTo(sTitle));
			assertThat("Incomig pull request created date mismatch for target " + tTitle + " and source " + sTitle,
					(Long) tPullRequest.get("createdDate"), equalTo((Long) sPullRequest.get("createdDate")));
			assertThat("Incomig pull request updated date mismatch for target " + tTitle + " and source " + sTitle,
					(Long) tPullRequest.get("updatedDate"), equalTo((Long) sPullRequest.get("updatedDate")));
			assertThat("Incomig pull request open mismatch for target " + tTitle + " and source " + sTitle,
					(Boolean) tPullRequest.get("open"), equalTo((Boolean) sPullRequest.get("open")));
			assertThat("Incomig pull request closed mismatch for target " + tTitle + " and source " + sTitle,
					(Boolean) tPullRequest.get("closed"), equalTo((Boolean) sPullRequest.get("closed")));
			assertThat("Incomig pull request closed mismatch for target " + tTitle + " and source " + sTitle,
					(Boolean) tPullRequest.get("locked"), equalTo((Boolean) sPullRequest.get("locked")));
			assertThat("Incomig pull request state mismatch for target " + tTitle + " and source " + sTitle,
					(String) tPullRequest.get("state"), equalTo((String) sPullRequest.get("state")));
			assertThat("Incomig pull request from ref commit mismatch for target " + tTitle + " and source " + sTitle,
					(String) ((Map<String, ?>) tPullRequest.get("fromRef")).get("latestCommit"),
					equalTo((String) ((Map<String, ?>) sPullRequest.get("fromRef")).get("latestCommit")));
			assertThat("Incomig pull request from ref repo slug mismatch for target " + tTitle + " and source " + sTitle,
					(String) ((Map<String, ?>) ((Map<String, ?>) tPullRequest.get("fromRef")).get("repository")).get("slug"),
					equalTo((String) ((Map<String, ?>) ((Map<String, ?>) sPullRequest.get("fromRef")).get("repository")).get("slug")));
			assertThat("Incomig pull request from ref displayId mismatch for target " + tTitle + " and source " + sTitle,
					(String) ((Map<String, ?>) tPullRequest.get("fromRef")).get("displayId"),
					equalTo((String) ((Map<String, ?>) sPullRequest.get("fromRef")).get("displayId")));
			assertThat("Incomig pull request to ref commit mismatch for target " + tTitle + " and source " + sTitle,
					(String) ((Map<String, ?>) tPullRequest.get("toRef")).get("latestCommit"),
					equalTo((String) ((Map<String, ?>) sPullRequest.get("toRef")).get("latestCommit")));
			assertThat("Incomig pull request to ref repo slug mismatch for target " + tTitle + " and source " + sTitle,
					(String) ((Map<String, ?>) ((Map<String, ?>) tPullRequest.get("toRef")).get("repository")).get("slug"),
					equalTo((String) ((Map<String, ?>) ((Map<String, ?>) sPullRequest.get("toRef")).get("repository")).get("slug")));
			assertThat("Incomig pull request to ref displayId mismatch for target " + tTitle + " and source " + sTitle,
					(String) ((Map<String, ?>) tPullRequest.get("toRef")).get("displayId"),
					equalTo((String) ((Map<String, ?>) sPullRequest.get("toRef")).get("displayId")));
		}
	}

	@SuppressWarnings("unchecked")
	@Test
	public void outgoingPullRequestsMatch() throws Exception {

		JsonPath sJson = sourceClient.getOutgoingPullRequests(projectKey, repoSlug, 0, MAX_PAGE_LIMIT);
		JsonPath tJson = targetClient.getOutgoingPullRequests(projectKey, repoSlug, 0, MAX_PAGE_LIMIT);

		assumeTrue(sJson != null);
		assumeTrue(tJson != null);

		assertThat("Incoming pull requests total count not matching.", targetClient.getOutgoingPullRequestsSize(projectKey, repoSlug),
				equalTo(sourceClient.getOutgoingPullRequestsSize(projectKey, repoSlug)));
		List<Map<String, ?>> sPullRequests = new ArrayList<>(sJson.get("values"));
		List<Map<String, ?>> tPullRequests = new ArrayList<>(tJson.get("values"));
		assertThat("Outgoing pull request count not matching. Expected: " + sPullRequests + ",actual: " + tPullRequests,
				tPullRequests.size(), equalTo(sPullRequests.size()));

		assumeTrue("No Pull Requests in source repo. Nothing to compare.", sPullRequests.size() > 0);

		Collections.sort(sPullRequests, PULL_REQUEST_COMPARATOR);
		Collections.sort(tPullRequests, PULL_REQUEST_COMPARATOR);
		for (int i = 0; i < tPullRequests.size(); i++) {
			Map<String, ?> sPullRequest = sPullRequests.get(i);
			Map<String, ?> tPullRequest = tPullRequests.get(i);
			String sTitle = (String) sPullRequest.get("title");
			String tTitle = (String) tPullRequest.get("title");
			assertThat("Outgoing pull request title mismatch for target " + tTitle + " and source " + sTitle, tTitle, equalTo(sTitle));
			assertThat("Outgoing pull request created date mismatch for target " + tTitle + " and source " + sTitle,
					(Long) tPullRequest.get("createdDate"), equalTo((Long) sPullRequest.get("createdDate")));
			assertThat("Outgoing pull request updated date mismatch for target " + tTitle + " and source " + sTitle,
					(Long) tPullRequest.get("updatedDate"), equalTo((Long) sPullRequest.get("updatedDate")));
			assertThat("Outgoing pull request open mismatch for target " + tTitle + " and source " + sTitle,
					(Boolean) tPullRequest.get("open"), equalTo((Boolean) sPullRequest.get("open")));
			assertThat("Outgoing pull request closed mismatch for target " + tTitle + " and source " + sTitle,
					(Boolean) tPullRequest.get("closed"), equalTo((Boolean) sPullRequest.get("closed")));
			assertThat("Outgoing pull request closed mismatch for target " + tTitle + " and source " + sTitle,
					(Boolean) tPullRequest.get("locked"), equalTo((Boolean) sPullRequest.get("locked")));
			assertThat("Outgoing pull request state mismatch for target " + tTitle + " and source " + sTitle,
					(String) tPullRequest.get("state"), equalTo((String) sPullRequest.get("state")));
			assertThat("Outgoing pull request from ref commit mismatch for target " + tTitle + " and source " + sTitle,
					(String) ((Map<String, ?>) tPullRequest.get("fromRef")).get("latestCommit"),
					equalTo((String) ((Map<String, ?>) sPullRequest.get("fromRef")).get("latestCommit")));
			assertThat("Outgoing pull request from ref repo slug mismatch for target " + tTitle + " and source " + sTitle,
					(String) ((Map<String, ?>) ((Map<String, ?>) tPullRequest.get("fromRef")).get("repository")).get("slug"),
					equalTo((String) ((Map<String, ?>) ((Map<String, ?>) sPullRequest.get("fromRef")).get("repository")).get("slug")));
			assertThat("Outgoing pull request from ref displayId mismatch for target " + tTitle + " and source " + sTitle,
					(String) ((Map<String, ?>) tPullRequest.get("fromRef")).get("displayId"),
					equalTo((String) ((Map<String, ?>) sPullRequest.get("fromRef")).get("displayId")));
			assertThat("Outgoing pull request to ref commit mismatch for target " + tTitle + " and source " + sTitle,
					(String) ((Map<String, ?>) tPullRequest.get("toRef")).get("latestCommit"),
					equalTo((String) ((Map<String, ?>) sPullRequest.get("toRef")).get("latestCommit")));
			assertThat("Outgoing pull request to ref repo slug mismatch for target " + tTitle + " and source " + sTitle,
					(String) ((Map<String, ?>) ((Map<String, ?>) tPullRequest.get("toRef")).get("repository")).get("slug"),
					equalTo((String) ((Map<String, ?>) ((Map<String, ?>) sPullRequest.get("toRef")).get("repository")).get("slug")));
			assertThat("Outgoing pull request to ref displayId mismatch for target " + tTitle + " and source " + sTitle,
					(String) ((Map<String, ?>) tPullRequest.get("toRef")).get("displayId"),
					equalTo((String) ((Map<String, ?>) sPullRequest.get("toRef")).get("displayId")));
		}
	}

	@Test
	public void commitsMatch() throws Exception {

		List<Object> sCommits = sourceClient.getAllCommits(projectKey, repoSlug);
		List<Object> tCommits = targetClient.getAllCommits(projectKey, repoSlug);

		assumeTrue("Source repository is empty", sCommits.size() > 0);

		assertThat("Commits total count not matching.", tCommits.size(), equalTo(sCommits.size()));

		assertThat("Commits count not matching. Expected: " + sCommits + ", actual: " + tCommits, tCommits.size(),
				equalTo(sCommits.size()));
		Collections.sort(sCommits, COMMIT_COMPARATOR);
		Collections.sort(tCommits, COMMIT_COMPARATOR);
		for (int i = 0; i < tCommits.size(); i++) {
			Map sCommit = (Map) sCommits.get(i);
			Map tCommit = (Map) tCommits.get(i);
			String sDisplayId = (String) sCommit.get("displayId");
			String tDisplayId = (String) tCommit.get("displayId");
			assertThat("Commit displayId mismatch for target " + tDisplayId + " and source " + sDisplayId, tDisplayId, equalTo(sDisplayId));
			assertThat("Commit message mismatch for target " + tDisplayId + " and source " + sDisplayId, (String) tCommit.get("message"),
					equalTo((String) sCommit.get("message")));
			assertThat("Commit authorTimestamp mismatch for target " + tDisplayId + " and source " + sDisplayId,
					(Long) tCommit.get("authorTimestamp"), equalTo((Long) sCommit.get("authorTimestamp")));
		}
	}

	@Test
	public void hooksMatch() throws Exception {

		JsonPath sJson = sourceClient.getHooks(projectKey, repoSlug, 0, MAX_PAGE_LIMIT);
		JsonPath tJson = targetClient.getHooks(projectKey, repoSlug, 0, MAX_PAGE_LIMIT);

		assumeTrue("No hook config", sJson != null);

		assertTrue("Hooks total count not matching.",
				targetClient.getHooksSize(projectKey, repoSlug) >= (sourceClient.getHooksSize(projectKey, repoSlug)));

		List<Map<String, ?>> sValues = new ArrayList<>(sJson.get("values"));
		List<Map<String, ?>> tValues = new ArrayList<>(tJson.get("values"));

		Collections.sort(sValues, HOOK_COMPARATOR);
		Collections.sort(tValues, HOOK_COMPARATOR);

		for (Map<String, ?> sItem : sValues) {
			String sHook = (String) ((Map) sItem.get("details")).get("name");

			Map<String, ?> tItem = tValues.stream().filter(h -> ((String) ((Map) h.get("details")).get("name")).equals(sHook)).findAny()
					.orElse(null);

			String tHook = (String) ((Map) tItem.get("details")).get("name");

			assertThat("Hook '" + sHook + "' enabled not matching", tItem.get("enabled"), equalTo(sItem.get("enabled")));
		}

	}

	@Test
	public void tagsMatch() throws Exception {

		JsonPath sJson = sourceClient.getTags(projectKey, repoSlug, 0, MAX_PAGE_LIMIT);
		JsonPath tJson = targetClient.getTags(projectKey, repoSlug, 0, MAX_PAGE_LIMIT);

		assumeTrue(sJson != null);
		assumeTrue(tJson != null);

		assertThat("Tags count matching", tJson.get("size"), equalTo(sJson.get("size")));

		List<Map<String, ?>> sValues = new ArrayList<>(sJson.get("values"));
		List<Map<String, ?>> tValues = new ArrayList<>(tJson.get("values"));

		for (int i = 0; i < tValues.size(); i++) {
			Map<String, ?> sItem = sValues.get(i);
			Map<String, ?> tItem = tValues.get(i);

			assertThat("Tag id not matching", tItem.get("id"), equalTo(sItem.get("id")));
			assertThat("Tag display id not matching", tItem.get("displayId"), equalTo(sItem.get("displayId")));
			assertThat("Latest changeset not matching", tItem.get("latestChangeset"), equalTo(sItem.get("latestChangeset")));
			assertThat("Latest commit not matching", tItem.get("latestCommit"), equalTo(sItem.get("latestCommit")));
			assertThat("Hash not matching", tItem.get("hash"), equalTo(sItem.get("hash")));
		}

	}

	// TODO:
	// Verify pull request reviewed status - compare role, approve status
	/// rest/api/1.0/projects/{projectKey}/repos/{repositorySlug}/pull-requests/{pullRequestId}
	// Verify JIRA integration
	// Verify pull request comment dates
	// Verify files
	// Verify comments in commit and PR
	// Verify comment arrachments
	// Verify PR tasks, comments, activities,changes
	// Verify watchers
	// Verify participants
	// Jenkins settings

	private static final Comparator<Map<String, ?>> GROUP_NAME_COMPARATOR = new Comparator<Map<String, ?>>() {
		@SuppressWarnings("unchecked")
		@Override
		public int compare(Map<String, ?> o1, Map<String, ?> o2) {
			String key1 = (String) ((Map<String, ?>) o1.get("group")).get("name");
			String key2 = (String) ((Map<String, ?>) o2.get("group")).get("name");
			return key1.toLowerCase().compareTo(key2.toLowerCase());
		}
	};
	private static final Comparator<Map<String, ?>> USER_NAME_COMPARATOR = new Comparator<Map<String, ?>>() {
		@SuppressWarnings("unchecked")
		@Override
		public int compare(Map<String, ?> o1, Map<String, ?> o2) {
			String key1 = (String) ((Map<String, ?>) o1.get("user")).get("displayName");
			String key2 = (String) ((Map<String, ?>) o2.get("user")).get("displayName");
			return key1.toLowerCase().compareTo(key2.toLowerCase());
		}
	};
	private static final Comparator<Map<String, ?>> PULL_REQUEST_COMPARATOR = new Comparator<Map<String, ?>>() {
		@Override
		public int compare(Map<String, ?> o1, Map<String, ?> o2) {
			Long created1 = (Long) o1.get("createdDate");
			Long created2 = (Long) o2.get("createdDate");
			int compare1 = created1.compareTo(created2);
			if (compare1 != 0) {
				return compare1;
			}
			String title1 = (String) o1.get("title");
			String title2 = (String) o2.get("title");
			return title1.compareTo(title2);
		}
	};
	private static final Comparator<Object> COMMIT_COMPARATOR = new Comparator<Object>() {
		@Override
		public int compare(Object o1, Object o2) {
			Long created1 = (Long) ((Map) o1).get("authorTimestamp");
			Long created2 = (Long) ((Map) o2).get("authorTimestamp");
			return created1.compareTo(created2);
		}
	};

	private static final Comparator<Map<String, ?>> HOOK_COMPARATOR = new Comparator<Map<String, ?>>() {
		@Override
		public int compare(Map<String, ?> o1, Map<String, ?> o2) {
			String key1 = (String) ((Map) o1.get("details")).get("name");
			String key2 = (String) ((Map) o2.get("details")).get("name");
			return key1.compareTo(key2);
		}
	};

	public static <T> List<Pair<T, T>> pair(Iterable<T> left, Iterable<T> right) {
		Iterator<T> rightIterator = right.iterator();
		return Lists.newArrayList(left).stream().map(l -> Pair.of(l, rightIterator.next())).collect(Collectors.toList());
	}

	private static List<String> collectRepos(List<JSONObject> reposPage) {
		return reposPage.stream().map(s -> {
			try {
				return String.valueOf(s.get("slug"));
			} catch (JSONException e) {

				// not sure whether this is the correct way to cause test failure or if I should return empty list instead?!
				log.error("Unable to retrieve repo list missing json element. See exception for futher details\n{}", e.getMessage());
				return null;
			}
		}).collect(Collectors.toList());
	}

}
