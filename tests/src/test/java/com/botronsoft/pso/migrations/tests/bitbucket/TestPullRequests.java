package com.botronsoft.pso.migrations.tests.bitbucket;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.rest.client.BitBucketClient;
import com.google.gson.GsonBuilder;
import com.googlecode.junittoolbox.ParallelParameterized;

import io.restassured.path.json.JsonPath;

@RunWith(ParallelParameterized.class)
public class TestPullRequests {

	private static final Logger log = LoggerFactory.getLogger(TestPullRequests.class);
	public static final int MAX_PAGE_LIMIT = 1048576;

	private static BitBucketClient sourceClient = Env.SOURCE_BB_ENV.getBitBucketClient();
	private static BitBucketClient targetClient = Env.TARGET_BB_ENV.getBitBucketClient();

	private String projectKey;
	private String repoSlug;
	private String pullReqId;

	public TestPullRequests(String projectKey, String repoSlug, String pullReqId) {
		this.projectKey = projectKey;
		this.repoSlug = repoSlug;
		this.pullReqId = pullReqId;
	}

	@Parameters(name = "pull request ID: {2} for repo: {1} in project: {0}")
	public static Collection<String[]> data() throws Exception {

		ArrayList<String[]> result = new ArrayList<>();
		ArrayList<Integer> pullRequestIds = new ArrayList<>();

		List<String> projects = Env.SOURCE_BB_ENV.getBitBucketClient().getAllProjectsKeys();

		for (String key : projects) {

			List<JSONObject> sReposPage = sourceClient.getProjectRepositories(key, 0, MAX_PAGE_LIMIT);
			List<String> sRepos = collectRepos(sReposPage);
			for (String repo : sRepos) {

				int startPosition = 0;
				int resultLimit = 100;

				JsonPath sJson;
				do {
					sJson = sourceClient.getPullRequest(key, repo, startPosition, resultLimit);

					List<Integer> sValuesTemp = new ArrayList<>();
					sValuesTemp = sJson.get("values.id");

					pullRequestIds.addAll(sValuesTemp);

					startPosition += 100;
					sValuesTemp = new ArrayList<>();

				} while (!sJson.getBoolean("isLastPage"));

				for (Integer id : pullRequestIds) {
					result.add(new String[] { key, repo, id.toString() });

					// We have Json already?! No need to make another call within the test
				}

				pullRequestIds = new ArrayList<>();
			}

		}
		return result;

	}

	private static List<String> collectRepos(List<JSONObject> sReposPage) {
		return sReposPage.stream().map(s -> {
			try {
				return String.valueOf(s.get("slug"));
			} catch (JSONException e) {

				// not sure whether this is the correct way to cause test failure or if I should return empty list instead?!
				log.error("Unable to retrieve repo list missing json element. See exception for futher details\n{}", e.getMessage());
				return null;
			}
		}).collect(Collectors.toList());
	}

	@Test
	public void pullRequestReviewersMatch() throws Exception {

		JsonPath sJson = sourceClient.getPullRequestDetails(projectKey, repoSlug, pullReqId);
		JsonPath tJson = targetClient.getPullRequestDetails(projectKey, repoSlug, pullReqId);

		assertThat("No corresponding data retrieved from TARGET", tJson, is(notNullValue()));

		List<Object> tReviewers = tJson.getList("reviewers.collect{/${it.role};${it.user.name.toLowerCase()};${it.status}/}");
		List<Object> sReviewers = sJson.getList("reviewers.collect{/${it.role};${it.user.name.toLowerCase()};${it.status}/}");

		assertThat("Pull request reviewer details mismatch", Arrays.asList(tReviewers), containsInAnyOrder(sReviewers));
	}

	@Test
	public void pullRequestActivityMatch() throws Exception {

		List<JsonPath> sValues = new ArrayList<>();
		List<JsonPath> tValues = new ArrayList<>();

		int startPosition = 0;
		int resultLimit = 100;

		JsonPath sJson = null;
		JsonPath tJson = null;

		do {
			try {
				sJson = sourceClient.getPullRequestActivities(projectKey, repoSlug, pullReqId, startPosition, resultLimit);
			} catch (Exception e) {
				fail("Exception on Source: " + e.toString());
			}
			try {
				tJson = targetClient.getPullRequestActivities(projectKey, repoSlug, pullReqId, startPosition, resultLimit);
				assertThat("No corresponding data retrieved from TARGET", tJson, is(notNullValue()));
			} catch (Exception e) {
				fail("Exception on Target: " + e.toString());
			}

			List<JsonPath> sValuesTemp = new ArrayList<>(sJson.get("values"));
			List<JsonPath> tValuesTemp = new ArrayList<>(tJson.get("values"));

			for (Object sItem : sJson.getList("values")) {
				JsonPath sItemJson = JsonPath.from(new GsonBuilder().setPrettyPrinting().create().toJson(sItem));
				sValues.add(sItemJson);
			}
			for (Object tItem : tJson.getList("values")) {
				JsonPath tItemJson = JsonPath.from(new GsonBuilder().setPrettyPrinting().create().toJson(tItem));
				tValues.add(tItemJson);
			}

			startPosition += 100;
			sValuesTemp = null;
			tValuesTemp = null;

		} while (!sJson.getBoolean("isLastPage"));

		// TODO: not checked if ordered all pull request are retrieved by the REST sort
		// all
		// activities by createdDate?
		List<Object> tActivities = tJson.getList("values.collect{/${it.createdDate};${it.user.name};${it.action};"
				+ "${it?.fromHash};${it?.previousFromHash};${it?.previousToHash};${it?.toHash};"
				+ "${it.commit?.id};${it.commit?.author?.name};" + "${it.commit?.authorTimestamp};${it.commit?.message}/}");

		List<Object> sActivities = sJson.getList("values.collect{/${it.createdDate};${it.user.name};${it.action};"
				+ "${it?.fromHash};${it?.previousFromHash};${it?.previousToHash};${it?.toHash};"
				+ "${it.commit?.id};${it.commit?.author?.name};" + "${it.commit?.authorTimestamp};${it.commit?.message}/}");

		assertThat("Pull request activities mismatch", Arrays.asList(tActivities), containsInAnyOrder(sActivities));
		// check if needed to add rescoped and commit.parents, added.commits and so on

	}
}
