package com.botronsoft.pso.migrations.tests.jira;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import com.botronsoft.pso.commons.csv.MappingFileReader;
import com.botronsoft.pso.jmt.reporting.model.project.ProjectExtendedInfo;
import com.botronsoft.pso.jmt.reporting.model.project.ProjectInfo;
import com.botronsoft.pso.jmt.rest.client.reporting.ReportingClient;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.rest.client.JiraClient;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.googlecode.junittoolbox.ParallelParameterized;

import groovy.util.logging.Slf4j;

@RunWith(ParallelParameterized.class)
@Slf4j
public class SanityTestProjects {

	private static final JiraClient sourceClient = Environments.jira(Env.SOURCE).getJiraClient();
	private static JiraClient targetClient;
	private static ReportingClient sourceReportingClient;
	private static ReportingClient targetReportingClient;
	private static Map<String, String> projectMap;
	private static Map<String, String> projectNameMap;
	private static Set<String> projectsInScope;

	private String sourceKey;
	private String targetKey;

	@Parameters(name = "projects {0}:{1}")
	public static Collection<Object[]> data() throws Exception {

		MappingFileReader mappingReader = new MappingFileReader("mappings");
		Set<String> load = mappingReader.loadSet("projectMap.csv").load();
		projectMap = loadToMap(load, new HashMap<>());

		// inScopeProjectList.csv must be single project key per line
		projectsInScope = mappingReader.loadSet("inScopeProjectList.csv").load();

		// Check if no scope is provided in input file, then all instance projects are retrieved as parameters
		if (projectsInScope.isEmpty()) {
			projectsInScope = sourceClient.getAllProjects().keySet();
		}

		List<ProjectInfo> projectsInfo = Environments.jira(Env.SOURCE).getJmtReportingClient().getProjects().stream()
				.filter(project -> projectsInScope.contains(project.getKey())).collect(toList());

		projectsInfo.stream().filter(p -> !projectMap.containsKey(p.getKey()))
				.map(notRenamedProject -> projectMap.put(notRenamedProject.getKey(), notRenamedProject.getKey())).collect(toList());

		return projectMap.entrySet().stream().map(e -> Arrays.asList(e.getKey(), e.getValue())).map(p -> p.toArray())
				.collect(Collectors.toList());
	}

	static com.google.common.cache.LoadingCache<String, ProjectExtendedInfo> createCache(final ReportingClient client) {
		return CacheBuilder.newBuilder().maximumSize(36).expireAfterWrite(5, TimeUnit.SECONDS)
				.build(new CacheLoader<String, ProjectExtendedInfo>() {
					@Override
					public ProjectExtendedInfo load(String key) throws Exception {
						ProjectExtendedInfo res = client.getSingleProjectsExtendedInfo(key);
						if (res == null) {
							throw new Exception("Project not found");
						}
						return res;
					}
				});
	}

	@BeforeClass
	public static void prepare() throws Exception {

		// sourceClient already initialized - used in test parameters build
		// sourceClient = Environments.jira(Env.SOURCE).getJiraClient();
		targetClient = Environments.jira(Env.TARGET).getJiraClient();
		sourceReportingClient = Environments.jira(Env.SOURCE).getJmtReportingClient();
		targetReportingClient = Environments.jira(Env.TARGET).getJmtReportingClient();

		projectNameMap = new HashMap<>();

		MappingFileReader mappingReader = new MappingFileReader("mappings");
		Set<String> load = mappingReader.loadSet("projectNameMap.csv").load();
		projectNameMap = loadToMap(load, new HashMap<>());

	}

	private static HashMap<String, String> loadToMap(Set<String> input, HashMap<String, String> output) {
		input.stream().forEach(l -> {
			List<String> list = Arrays.asList(l.split(";"));
			String source = list.get(0);
			String target = list.get(1);
			output.put(source, target);
		});

		return output;
	}

	com.google.common.cache.LoadingCache<String, ProjectExtendedInfo> sourceCache;
	com.google.common.cache.LoadingCache<String, ProjectExtendedInfo> targetCache;

	public SanityTestProjects(String sourceKey, String targetKey) {
		this.sourceKey = sourceKey;
		this.targetKey = targetKey;
		sourceCache = createCache(sourceReportingClient);
		targetCache = createCache(targetReportingClient);
	}

	ProjectExtendedInfo getTargetProject() {
		try {
			return targetCache.get(targetKey);
		} catch (Exception e) {
			return null;
		}
	}

	ProjectExtendedInfo getSourceProject() {
		try {
			return sourceCache.get(sourceKey);
		} catch (Exception e) {
			return null;
		}
	}

	@Test
	public void targetProjectExists() {

		assertThat("Target project does not exist!", getTargetProject(), notNullValue());

	}

	@Test
	public void projectNames() {

		String srcProjectName = getSourceProject().getName();

		String srcProjectNameMapped = projectNameMap.get(srcProjectName) == null ? srcProjectName : projectNameMap.get(srcProjectName);

		assertThat("Project name mismatch!", srcProjectNameMapped, is(getTargetProject().getName()));

	}

	@Test
	public void issuesInProject() {

		assertThat(getTargetProject().getIssueCount(), is(getSourceProject().getIssueCount()));

	}

	@Test
	public void attachmentsInProject() {

		assertThat(getTargetProject().getAttachmentsCount(), is(getSourceProject().getAttachmentsCount()));

	}

	@Test
	public void issueLinksInProject() {

		// Get number of outward links for only in scope projects
		long sourceIssueLinks = getSourceProject().getIssueLinksCount().entrySet().stream()
				.filter(entry -> projectsInScope.contains(entry.getKey()))
				.collect(toMap(project -> project.getKey(), project -> project.getValue())).values().stream().mapToLong(Long::longValue)
				.sum();

		// do not filter to in scope projects for target. Must be only in scope ones migrated anyway.
		// If there is mismatch then either links are missing not as expected or imported issue somehow get existing link?!
		long targetIssueLinks = getTargetProject().getIssueLinksCount().values().stream().mapToLong(Long::longValue).sum();

		assertThat(targetIssueLinks, is(sourceIssueLinks));
	}

	@Test
	public void commentsInProject() {

		assertThat(getTargetProject().getCommentsCount(), is(getSourceProject().getCommentsCount()));

	}

	@Test
	public void worklogsInProject() {

		assertThat(getTargetProject().getWorklogsCount(), is(getSourceProject().getWorklogsCount()));

	}
}