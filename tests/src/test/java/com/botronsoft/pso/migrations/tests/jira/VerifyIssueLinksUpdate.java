package com.botronsoft.pso.migrations.tests.jira;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import com.botronsoft.pso.commons.csv.MappingFileReader;
import com.botronsoft.pso.jmt.rest.client.reporting.ReportingClient;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.rest.client.JiraClient;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.googlecode.junittoolbox.ParallelParameterized;

import groovy.util.logging.Slf4j;
import io.restassured.path.json.JsonPath;

@RunWith(ParallelParameterized.class)
@Slf4j
public class VerifyIssueLinksUpdate {

	private static final JiraClient sourceClient = Environments.jira(Env.SOURCE).getJiraClient();
	private static final ReportingClient sourceJmtReportingClient = Environments.jira(Env.SOURCE).getJmtReportingClient();
	private static final boolean testSubset = true;
	private static final int issueSubsetCount = 20;
	private static JiraClient targetClient;
	private static Map<String, String> projectMap;
	private static Map<String, String> userMap;
	private static Map<String, String> issueMap;
	private static Map<String, String> priorityMap;
	private static Map<String, String> resolutionMap;

	private String sourceKey;
	private String targetKey;

	@Parameters(name = "issues [{0}]:[{1}]")
	public static Collection<Object[]> data() throws Exception {

		List<String> projectKeysUserCanBrowse = sourceClient.getProjectsUserCanBrowse();
		List<String> inScopeIssueKeys = new ArrayList<String>();

		MappingFileReader mappingReader = new MappingFileReader("mappings");
		Set<String> load = mappingReader.loadSet("projectMap.csv").load();
		projectMap = new HashMap<>();
		issueMap = new HashMap<>();

		load.stream().forEach(l -> {
			List<String> list = Arrays.asList(l.split(";"));
			String source = list.get(0);
			String target = list.get(1);
			projectMap.put(source, target);
		});

		// if (testSubset) {
		//
		// List<List<String>> allProjectIssues = projectKeysUserCanBrowse.stream()
		// .map(pkey -> sourceJmtReportingClient.collectIssueKeys(new ArrayList<String>(Arrays.asList(pkey)))).collect(toList());
		//
		// allProjectIssues.stream().forEach(issues -> {
		// List<String> randomSubset = issues;
		// if (issues.size() > issueSubsetCount) {
		// randomSubset = pickNRandom(issues, issueSubsetCount);
		// }
		// inScopeIssueKeys.addAll(randomSubset);
		// });
		//
		// } else {
		// inScopeIssueKeys = sourceJmtReportingClient.collectIssueKeys(projectKeysUserCanBrowse);
		// }

		// inScopeIssueKeys.stream().forEach(issueKey -> {
		// // if (Long.valueOf(issueKey.split("-")[1]) <= 10) {
		// issueMap.put(issueKey,
		// projectMap.get(issueKey.split("-")[0]) != null ? projectMap.get(issueKey.split("-")[0]) + "-" + issueKey.split("-")[1]
		// : issueKey);
		// // }
		// });

		issueMap.put("DCAPP-94", "DCAPP-94");

		return issueMap.entrySet().stream().map(e -> Arrays.asList(e.getKey(), e.getValue())).map(p -> p.toArray())
				.collect(Collectors.toList());
	}

	static com.google.common.cache.LoadingCache<String, JsonPath> createCache(final JiraClient client) {
		return CacheBuilder.newBuilder().maximumSize(36).expireAfterWrite(5, TimeUnit.SECONDS).build(new CacheLoader<String, JsonPath>() {
			@Override
			public JsonPath load(String key) throws Exception {
				JsonPath res = client.getIssue(key);
				if (res == null) {
					throw new Exception("Issue not found");
				}
				return res;
			}
		});
	}

	private static List<String> pickNRandom(List<String> list, int n) {
		List<String> copy = new ArrayList<String>(list);
		Collections.shuffle(copy);
		return copy.subList(0, n);
	}

	@BeforeClass
	public static void prepare() throws Exception {

		// sourceClient = Environments.jira(Env.SOURCE).getJiraClient();
		targetClient = Environments.jira(Env.TARGET).getJiraClient();

		MappingFileReader mappingReader = new MappingFileReader("user_management");
		Set<String> load = mappingReader.loadSet("user_mapping.csv").load();

		// Load user mapping
		userMap = dataToMap(load, new HashMap<String, String>());

		// Load priority map
		mappingReader = new MappingFileReader("mappings");
		load = mappingReader.loadSet("priorityMap.csv").load();
		priorityMap = dataToMap(load, new HashMap<String, String>());

		// Load priority map
		mappingReader = new MappingFileReader("mappings");
		load = mappingReader.loadSet("resolutionMap.csv").load();
		resolutionMap = dataToMap(load, new HashMap<String, String>());

	}

	@Before
	public void checkTargetIssueExists() {
		// assumeThat("Target issue does not exist!", getTargetIssue(), notNullValue());
	}

	private static Map<String, String> dataToMap(Set<String> load, Map<String, String> map) {
		load.stream().forEach(l -> {
			List<String> list = Arrays.asList(l.split(";"));
			String source = list.get(0);
			String target = list.get(1);
			map.put(source, target);
		});

		return map;
	}

	com.google.common.cache.LoadingCache<String, JsonPath> sourceCache;
	com.google.common.cache.LoadingCache<String, JsonPath> targetCache;

	public VerifyIssueLinksUpdate(String sourceKey, String targetKey) {
		this.sourceKey = sourceKey;
		this.targetKey = targetKey;
		sourceCache = createCache(sourceClient);
		targetCache = createCache(targetClient);
	}

	JsonPath getTargetIssue() {
		try {

			return targetCache.get(targetKey);
			// return targetClient.getIssue(targetKey);
		} catch (Exception e) {
			return null;
		}
	}

	JsonPath getSourceIssue() {

		try {
			return sourceCache.get(sourceKey);
			// return targetClient.getIssue(targetKey);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 *
	 */

	@Test
	public void verifyIssueLinksUpdate() throws IOException, NoSuchAlgorithmException {
		// Gather all static and remote links
		// Could use info already dumped by link updater (still missing issue key - might be useful to include )
		// Get all for applications we are migrating (must be updated)
		// Load mapping
		// Run validation tests
		// Parameters must be the links but also having issue ref where link is coming from (rest call list of links)

		String sourceUsername = Environments.jira(Env.SOURCE).getProperty("username");
		String sourcePassword = Environments.jira(Env.SOURCE).getProperty("password");

		String targetUsername = Environments.jira(Env.TARGET).getProperty("username");
		String targetPassword = Environments.jira(Env.TARGET).getProperty("password");

		String sourceAuth = sourceUsername + ":" + sourcePassword;
		String sourceBasicAuth = "Basic " + new String(Base64.getEncoder().encode(sourceAuth.getBytes()));

		String targetAuth = targetUsername + ":" + targetPassword;
		String targetBasicAuth = "Basic " + new String(Base64.getEncoder().encode(targetAuth.getBytes()));

		URL sourceUrl = new URL("http://pso004prod.botronlabs.com/browse/CSI-6");
		HttpURLConnection sourceConnection = (HttpURLConnection) sourceUrl.openConnection();
		sourceConnection.setRequestMethod("GET");
		sourceConnection.setRequestProperty("Authorization", sourceBasicAuth);
		sourceConnection.connect();

		URL targetUrl = new URL("http://pso004test.botronlabs.com/browse/CSI-6777");
		HttpURLConnection targetConnection = (HttpURLConnection) targetUrl.openConnection();
		targetConnection.setRequestMethod("GET");
		targetConnection.setRequestProperty("Authorization", targetBasicAuth);
		targetConnection.connect();

		int sourceResponseCode = sourceConnection.getResponseCode();
		int targetResponseCode = targetConnection.getResponseCode();

		assertThat("Response codes are different", targetResponseCode, is(sourceResponseCode));
	}
}
