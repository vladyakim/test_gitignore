package com.botronsoft.pso.migrations.tests.bitbucket;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.migration.framework.environment.BitbucketEnvironment;
import com.botronsoft.pso.migration.framework.environment.DefaultBitbucketEnvironment;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
import com.botronsoft.pso.rest.client.BitBucketClient;
import com.botronsoft.pso.rest.client.model.SshKey;
import com.google.common.collect.Lists;
import com.googlecode.junittoolbox.ParallelParameterized;

//import com.atlassian.stash.rest.client.api.entity.SshKey;

@RunWith(ParallelParameterized.class)
public class TestUsers {

	public static final int MAX_PAGE_LIMIT = 1048576;

	private static BitBucketClient sourceClient = Env.SOURCE_BB_ENV.getBitBucketClient();
	private static BitBucketClient targetClient = Env.TARGET_BB_ENV.getBitBucketClient();
	private String sourceUser;
	private String targetUser;
	private static BitbucketEnvironment env = new DefaultBitbucketEnvironment(Env.SOURCE);
	private static MappingFactory mappingFactory = new FileSystemMappingFactory("src/main/resources/" + env.getKey());
	private static MapperReader mappingReader = mappingFactory.createReader();

	public TestUsers(String sourceUser, String targetUser) {
		this.sourceUser = sourceUser;
		this.targetUser = targetUser;
	}

	@Parameters(name = "user: {0} -> {1}")
	public static Collection<String[]> data() throws Exception {
		ArrayList<String[]> result = new ArrayList<>();

		List<String> users = Env.SOURCE_BB_ENV.getBitBucketClient().getAllUsers();

		// TODO: mapping currently is not loaded must copy mapping files from bitbucket resource folder

		Map<String, String> usersMapping = mappingReader.map(ObjectTypeKeys.BB_USER_NAME).entrySet().stream()
				.collect(toMap(entry -> entry.getKey(), entry -> entry.getValue().stream().collect(toList()).get(0)));

		for (String user : users) {
			String targetUser = usersMapping.containsKey(user) ? usersMapping.get(user) : user;
			result.add(new String[] { user, targetUser });
		}

		return result;
	}

	@Test
	public void sshKeysMatch() {

		List<SshKey> sKeys = sourceClient.getUserSSHKeys(sourceUser);
		assertTrue("User does not have SSH keys on SOURCE", !sKeys.isEmpty());

		List<SshKey> tKeys = targetClient.getUserSSHKeys(targetUser);
		assertTrue("No matching TARGET SSH Key", !tKeys.isEmpty());

		for (Pair<SshKey, SshKey> p : pair(sKeys, tKeys)) {
			SshKey sRepo = p.getLeft();
			SshKey tRepo = p.getRight();
			assertThat("Label is different", (tRepo.getLabel()), equalTo(sRepo.getLabel()));
			assertThat("Key is different", tRepo.getText(), equalTo(sRepo.getText()));
		}
	}

	public static <T> List<Pair<T, T>> pair(Iterable<T> left, Iterable<T> right) {
		Iterator<T> rightIterator = right.iterator();
		return Lists.newArrayList(left).stream().map(l -> Pair.of(l, rightIterator.next())).collect(Collectors.toList());
	}

}
