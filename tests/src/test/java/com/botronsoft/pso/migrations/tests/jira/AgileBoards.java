package com.botronsoft.pso.migrations.tests.jira;

import static com.google.common.truth.Truth.assertThat;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.botronsoft.pso.commons.csv.MappingFileReader;
import com.botronsoft.pso.jmt.reporting.model.customfield.BoardInfo;
import com.botronsoft.pso.jmt.rest.client.reporting.ReportingClient;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.rest.client.JiraClient;
import com.botronsoft.pso.rest.client.exception.AtlassianRestException;

import groovy.util.logging.Slf4j;
import io.restassured.path.json.JsonPath;

@RunWith(Parameterized.class)
@Slf4j
public class AgileBoards {

	private static final ReportingClient sourceJmtReportingClient = Environments.jira(Env.SOURCE).getJmtReportingClient();
	private static final ReportingClient targetJmtReportingClient = Environments.jira(Env.TARGET).getJmtReportingClient();
	private static final int viewInLastNDays = 30;
	private JiraClient sourceClient;
	private JiraClient targetClient;
	private static Map<String, String> userMap; // TODO: Do we need user mapping at all? compare owners, admins?
	private static Map<String, String> projectMap;
	private static Map<String, String> agileBoardMap;
	private static Map<String, String> inTestScopeAgileBoardMap;
	private static Map<String, String> statusMap;

	private static String sourceId;
	private static String targetId;
	private static String sourceOwnerUsername;
	private static String targetOwnerUsername;

	@Rule
	public ErrorCollector errorCollector = new ErrorCollector();

	// There could be cases when board owner might be inactive user but agile board itself is shared and used by other users. If this is the
	// case then impersonation would not work and test will throw NPE (eg. boardSwimlanes(), boardColumns()). If real life scenario we might
	// consider how to deal with it. See
	// https://jira.botronsoft.com/browse/PJM-3103
	@Parameters(name = "AgileBoardDetails [{0}]:[{1}]:[{2}]:[{3}]")
	public static Collection<Object[]> data() throws Exception {

		MappingFileReader mappingReader = new MappingFileReader("mappings");
		Set<String> load = mappingReader.loadSet("boardids.csv").load();
		agileBoardMap = new HashMap<>();
		inTestScopeAgileBoardMap = new HashMap<>();

		load.stream().forEach(l -> {
			List<String> list = Arrays.asList(l.split(";"));
			String source = list.get(0);
			String target = list.get(1);
			agileBoardMap.put(source, target);
		});

		List<String> lastViewedAgileBoards = sourceJmtReportingClient.collectLastViewedItemsOfType("RapidView", viewInLastNDays);

		inTestScopeAgileBoardMap = agileBoardMap.entrySet().stream().filter(map -> lastViewedAgileBoards.contains(map.getKey()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		// get owner for each inscope source and target board
		Map<Long, String> sourceParameters = getTestParamenters(sourceJmtReportingClient);
		Map<Long, String> targetParameters = getTestParamenters(targetJmtReportingClient);

		return inTestScopeAgileBoardMap.entrySet().stream().map(e -> Arrays.asList(e.getKey(), e.getValue(),
				sourceParameters.get(Long.valueOf(e.getKey())), targetParameters.get(Long.valueOf(e.getValue())))).map(p -> p.toArray())
				.collect(Collectors.toList());
	}

	private static Map<Long, String> getTestParamenters(ReportingClient client) {
		return client.collectAgileBoards().stream()
				.filter(board -> inTestScopeAgileBoardMap.values().contains(String.valueOf(board.getId())))
				.collect(Collectors.toMap(BoardInfo::getId, BoardInfo::getOwnerUsername));
	}

	private static HashMap<String, String> loadToMap(Set<String> input, HashMap<String, String> output) {
		input.stream().forEach(l -> {
			List<String> list = Arrays.asList(l.split(";"));
			String source = list.get(0);
			String target = list.get(1);
			output.put(source, target);
		});

		return output;
	}

	private String getMappedProjectIssueKey(Object obj, String name) {
		return projectMap.get(getValueForObject(obj, name).toString().split("-")[0]) == null ? getValueForObject(obj, name).toString()
				: projectMap.get(getValueForObject(obj, name).toString().split("-")[0]) + "-"
						+ getValueForObject(obj, name).toString().split("-")[1];
	}

	@BeforeClass
	public static void prepare() throws Exception {

		MappingFileReader mappingReader = new MappingFileReader("user_management");
		Set<String> load = mappingReader.loadSet("user_mapping.csv").load();

		// Load user mapping
		userMap = dataToMap(load, new HashMap<String, String>());

		mappingReader = new MappingFileReader("mappings");
		load = mappingReader.loadSet("projectMap.csv").load();
		projectMap = loadToMap(load, new HashMap<>());

		load = mappingReader.loadSet("statusMap.csv").load();
		statusMap = loadToMap(load, new HashMap<>());

	}

	@Before
	public void impersonateAsOwner() {
		sourceClient = Environments.jira(Env.SOURCE).getJiraClient();
		sourceClient.setImpersonatedUser(sourceOwnerUsername);
		targetClient = Environments.jira(Env.TARGET).getJiraClient();
		targetClient.setImpersonatedUser(targetOwnerUsername);
	}

	private static Map<String, String> dataToMap(Set<String> load, Map<String, String> map) {
		load.stream().forEach(l -> {
			List<String> list = Arrays.asList(l.split(";"));
			String source = list.get(0);
			String target = list.get(1);
			map.put(source, target);
		});

		return map;
	}

	public AgileBoards(String sourceKey, String targetKey, String sourceOwner, String targetOwner) {
		sourceId = sourceKey;
		targetId = targetKey;
		sourceOwnerUsername = sourceOwner;
		targetOwnerUsername = targetOwner;
	}

	private boolean isSubtask(Object obj) {
		Object object = toMap(obj).get("fields");
		Object issueTypeDetails = toMap(object).get("issuetype");
		return Boolean.parseBoolean(toMap(issueTypeDetails).get("subtask").toString());
	}

	private HashMap<?, ?> toMap(Object obj) {
		return new ObjectMapper().convertValue(obj, HashMap.class);
	}

	// TODO: one method to get values instead of specific method as for issue key and isSubtask?
	// E.g. subtast -> Boolean.parseBoolean(getValue(getValue(getValue(obj, "fields"), "issuetype"), "subtask").toString())

	private Object getValueForObject(Object obj, String key) {
		return toMap(obj).get(key);
	}

	/**
	 *
	 */

	@Test
	public void boardOwner() {

		String mappedSourceOwnerUsername = userMap.get(sourceOwnerUsername) == null ? sourceOwnerUsername
				: userMap.get(sourceOwnerUsername);

		assertThat("Backlog issue count mismatch", targetOwnerUsername, is(mappedSourceOwnerUsername));
	}

	@Test
	public void boardColumns() {

		// Source statuses are checked against mapping if update needed
		Map<String, String> sourceStatusesMapped = sourceClient.getAllStatuses().entrySet().stream().collect(
				Collectors.toMap(e -> e.getKey(), e -> statusMap.get(e.getValue()) == null ? e.getValue() : statusMap.get(e.getValue())));
		Map<String, String> targetStatuses = targetClient.getAllStatuses();

		JsonPath sourceBoardConfig = sourceClient.getBoardColumnConfig(sourceId);
		JsonPath targetBoardConfig = targetClient.getBoardColumnConfig(targetId);

		List<String> sourceBoardColumns = sourceClient.getBoardColumns(sourceId);
		List<String> targetBoardColumns = targetClient.getBoardColumns(targetId);

		assertThat(sourceBoardColumns).containsExactlyElementsIn(targetBoardColumns).inOrder();

		List<List<String>> allUsedSourceStatuses = sourceBoardConfig.getList("currentViewConfig.columns.statusIds");
		List<List<String>> allUsedTargetStatuses = targetBoardConfig.getList("currentViewConfig.columns.statusIds");

		List<List<String>> sourceFinalColumnData = new ArrayList<List<String>>();
		List<List<String>> targetFinalColumnData = new ArrayList<List<String>>();

		// Add separator after each column content to make sure not only order is the same once result is flatten but also that statuses
		// comes from same column
		allUsedSourceStatuses.forEach((l) -> {
			l.forEach(id -> sourceStatusesMapped.get(id));
			l.add("|");
			sourceFinalColumnData.add(l);
		});

		allUsedTargetStatuses.forEach((l) -> {
			l.forEach(id -> targetStatuses.get(id));
			l.add("|");
			targetFinalColumnData.add(l);
		});

		List<String> sourceColumnStatuses = sourceFinalColumnData.stream().flatMap(List::stream)
				.map(e -> e.equalsIgnoreCase("|") ? e : targetStatuses.get(e)).collect(toList());
		List<String> targetColumnStatuses = allUsedTargetStatuses.stream().flatMap(List::stream)
				.map(e -> e.equalsIgnoreCase("|") ? e : targetStatuses.get(e)).collect(toList());

		assertThat(sourceColumnStatuses).containsExactlyElementsIn(targetColumnStatuses).inOrder();
	}

	@Test
	public void boardSwimlanes() {

		JsonPath sourceBoardConfig = sourceClient.getBoardColumnConfig(sourceId);
		JsonPath targetBoardConfig = targetClient.getBoardColumnConfig(targetId);

		List<Object> sourceSwimlanes = sourceBoardConfig.getList("currentViewConfig.swimlanes");
		List<Object> targetSwimlanes = targetBoardConfig.getList("currentViewConfig.swimlanes");

		assertThat("Number of swimlanes do not match", targetSwimlanes.size(), is(sourceSwimlanes.size()));

		List<String> sourceSwimlaneNames = sourceBoardConfig.getList("currentViewConfig.swimlanes.name", String.class);
		List<String> targetSwimlaneNames = targetBoardConfig.getList("currentViewConfig.swimlanes.name", String.class);

		assertThat(sourceSwimlaneNames).containsExactlyElementsIn(targetSwimlaneNames).inOrder();
	}

	@Test
	public void boardQuickFilters() {

		JsonPath sourceBoardConfig = sourceClient.getBoardColumnConfig(sourceId);
		JsonPath targetBoardConfig = targetClient.getBoardColumnConfig(targetId);

		List<Object> sourceQuickFilters = sourceBoardConfig.getList("currentViewConfig.quickFilters");
		List<Object> targetQuickFilters = targetBoardConfig.getList("currentViewConfig.quickFilters");

		assertThat("Number of quick filters do not match", targetQuickFilters.size(), is(sourceQuickFilters.size()));

		List<String> sourceQuickFilterNames = sourceBoardConfig.getList("currentViewConfig.quickFilters.name", String.class);
		List<String> targetQuickFilterNames = targetBoardConfig.getList("currentViewConfig.quickFilters.name", String.class);

		assertThat(sourceQuickFilterNames).containsExactlyElementsIn(targetQuickFilterNames).inOrder();
	}

	@Test
	public void backlogSizeKanbanBoard() {

		String sourceBoardType = sourceClient.getBoardType(sourceId);

		assumeTrue("Board type is not Kanban", sourceBoardType.equalsIgnoreCase("kanban"));

		// Run test only if backlog is enabled for a kanban board
		try {
			sourceClient.getKanbanBoardBacklog(sourceId);
		} catch (AtlassianRestException e) {
			if (e.getMessage().equalsIgnoreCase("[Kanban backlog is not enabled for this board.]")) {
				assumeTrue("Backlog not enabled for board!", false);
			}
		}

		int sizeSourceBacklog = sourceClient.getKanbanBoardBacklog(sourceId).getList("issues").size();
		int sizeTargetBacklog = targetClient.getKanbanBoardBacklog(targetId).getList("issues").size();

		assertThat("Backlog issue count mismatch", sizeTargetBacklog, is(sizeSourceBacklog));

	}

	@Test
	public void backlogSizeScrumBoard() {

		String sourceBoardType = sourceClient.getBoardType(sourceId);

		assumeTrue("Board type is not Scrum", sourceBoardType.equalsIgnoreCase("scrum"));

		long sizeSourceBacklog = sourceClient.getAgileBoardBacklog(sourceId).getList("issues").stream().filter(obj -> !isSubtask(obj))
				.count();
		long sizeTargetBacklog = targetClient.getAgileBoardBacklog(targetId).getList("issues").stream().filter(obj -> !isSubtask(obj))
				.count();

		assertThat("Backlog issue count mismatch", sizeTargetBacklog, is(sizeSourceBacklog));

	}

	@Test
	public void backlogOrderKanbanBoard() {

		String sourceBoardType = sourceClient.getBoardType(sourceId);

		assumeTrue("Board type is not Kanban", sourceBoardType.equalsIgnoreCase("kanban"));

		int sizeSourceBacklog = sourceClient.getKanbanBoardBacklog(sourceId).getList("issues").size();

		assumeTrue("No backlog in source board. Skipping test!", sizeSourceBacklog > 0);

		List<String> sourceBacklogStatuses = sourceClient.getKanbanBoardBacklog(sourceId).getList("backlogColumn.statusIds");
		List<String> targetBacklogStatuses = targetClient.getKanbanBoardBacklog(targetId).getList("backlogColumn.statusIds");

		// Apply project key mapping on source issue key
		List<String> sourceBacklog = sourceClient.getKanbanBoardBacklog(sourceId).getList("issues").stream()
				.filter(issue -> sourceBacklogStatuses.contains(getValueForObject(issue, "statusId")))
				.map(entry -> getMappedProjectIssueKey(entry, "key")).collect(toList());
		List<String> sourceToDo = sourceClient.getKanbanBoardBacklog(sourceId).getList("issues").stream()
				.filter(issue -> !sourceBacklogStatuses.contains(getValueForObject(issue, "statusId")))
				.map(entry -> getMappedProjectIssueKey(entry, "key")).collect(toList());

		List<String> targetBacklog = targetClient.getKanbanBoardBacklog(targetId).getList("issues").stream()
				.filter(issue -> targetBacklogStatuses.contains(getValueForObject(issue, "statusId")))
				.map(entry -> getValueForObject(entry, "key").toString()).collect(toList());
		List<String> targetToDo = targetClient.getKanbanBoardBacklog(targetId).getList("issues").stream()
				.filter(issue -> !targetBacklogStatuses.contains(getValueForObject(issue, "statusId")))
				.map(entry -> getValueForObject(entry, "key").toString()).collect(toList());

		// TODO: Do we need separate checks?! Might be useful when elements are the same but order is diff, then it's only ranking issue
		try {
			assertThat(sourceBacklog).containsExactlyElementsIn(targetBacklog);
		} catch (Throwable e) {
			errorCollector.addError(new Throwable("Backlog entries do not match"));
		}

		try {
			assertThat(sourceBacklog).containsExactlyElementsIn(targetBacklog).inOrder();
		} catch (Throwable e) {
			errorCollector.addError(new Throwable("Issue order in backlog do not match"));
		}

		try {
			assertThat(sourceToDo).containsExactlyElementsIn(targetToDo);
		} catch (Throwable e) {
			errorCollector.addError(new Throwable("To Do entries do not match"));
		}

		try {
			assertThat(sourceToDo).containsExactlyElementsIn(targetToDo).inOrder();
		} catch (Throwable e) {
			errorCollector.addError(new Throwable("Issue order in Backlog To Do do not match"));
		}

	}

	@Test
	public void backlogOrderScrumBoard() {

		String sourceBoardType = sourceClient.getBoardType(sourceId);

		assumeTrue("Board type is not Scrum", sourceBoardType.equalsIgnoreCase("scrum"));

		long sizeSourceBacklog = sourceClient.getAgileBoardBacklog(sourceId).getList("issues").stream().filter(obj -> !isSubtask(obj))
				.count();

		assumeTrue("No backlog in source board. Skipping test!", sizeSourceBacklog > 0);

		List<String> sourceBacklog = sourceClient.getAgileBoardBacklog(sourceId).getList("issues").stream().filter(obj -> !isSubtask(obj))
				.map(entry -> getMappedProjectIssueKey(entry, "key")).collect(toList());

		// Apply project key mapping on source issue key
		List<String> targetBacklog = targetClient.getAgileBoardBacklog(targetId).getList("issues").stream().filter(obj -> !isSubtask(obj))
				.map(entry -> getValueForObject(entry, "key").toString()).collect(toList());

		try {
			assertThat(sourceBacklog).containsExactlyElementsIn(targetBacklog);
		} catch (Throwable e) {
			errorCollector.addError(new Throwable("Backlog entries do not match"));
		}

		try {
			assertThat(sourceBacklog).containsExactlyElementsIn(targetBacklog).inOrder();
		} catch (Throwable e) {
			errorCollector.addError(new Throwable("Issue order in backlog do not match"));
		}

	}

	@Test
	public void backlogIssuesFlaggedKanbanBoard() {

		String sourceBoardType = sourceClient.getBoardType(sourceId);

		assumeTrue("Board type is not Kanban", sourceBoardType.equalsIgnoreCase("kanban"));

		List<Object> sourceIssues = sourceClient.getKanbanBoardBacklog(sourceId).getList("issues");

		assumeTrue("No backlog in source board. Skipping test!", sourceIssues.size() > 0);

		List<String> sourceFlagged = sourceIssues.stream()
				.filter(issue -> Boolean.parseBoolean(
						getValueForObject(issue, "flagged") == null ? "false" : getValueForObject(issue, "flagged").toString()))
				.map(flagged -> getMappedProjectIssueKey(flagged, "key")).collect(toList());

		assumeTrue("No flagged issues in backlog in source board. Skipping test!", sourceFlagged.size() > 0);

		List<Object> targetIssues = targetClient.getKanbanBoardBacklog(targetId).getList("issues");

		List<String> targetFlagged = targetIssues.stream()
				.filter(issue -> Boolean.parseBoolean(
						getValueForObject(issue, "flagged") == null ? "false" : getValueForObject(issue, "flagged").toString()))
				.map(flagged -> getValueForObject(flagged, "key").toString()).collect(toList());

		// without .inOrder() this is already covered by the backlog order test
		try {
			assertThat(sourceFlagged).containsExactlyElementsIn(targetFlagged);
		} catch (Throwable e) {
			errorCollector.addError(new Throwable("Flagged issues do not match"));
		}

	}

	@Test
	public void backlogIssuesFlaggedScrumBoard() {

		String sourceBoardType = sourceClient.getBoardType(sourceId);

		assumeTrue("Board type is not Scrum", sourceBoardType.equalsIgnoreCase("scrum"));

		List<Object> sourceIssues = sourceClient.getAgileBoardBacklog(sourceId).getList("issues").stream().filter(obj -> !isSubtask(obj))
				.collect(toList());

		assumeTrue("No backlog in source board. Skipping test!", sourceIssues.size() > 0);

		List<String> sourceFlagged = sourceIssues.stream()
				.filter(issue -> Boolean.parseBoolean(getValueForObject(getValueForObject(issue, "fields"), "flagged") == null ? "false"
						: getValueForObject(getValueForObject(issue, "fields"), "flagged").toString()))
				.map(flagged -> getMappedProjectIssueKey(flagged, "key")).collect(toList());

		assumeTrue("No flagged issues in backlog in source board. Skipping test!", sourceFlagged.size() > 0);

		List<Object> targetIssues = targetClient.getAgileBoardBacklog(targetId).getList("issues").stream().filter(obj -> !isSubtask(obj))
				.collect(toList());
		List<String> targetFlagged = targetIssues.stream()
				.filter(issue -> Boolean.parseBoolean(getValueForObject(getValueForObject(issue, "fields"), "flagged") == null ? "false"
						: getValueForObject(getValueForObject(issue, "fields"), "flagged").toString()))
				.map(flagged -> getMappedProjectIssueKey(flagged, "key")).collect(toList());

		// without .inOrder() this is already covered by the backlog order test
		try {
			assertThat(sourceFlagged).containsExactlyElementsIn(targetFlagged);
		} catch (Throwable e) {
			errorCollector.addError(new Throwable("Flagged issues do not match"));
		}

	}

	@Test
	public void activeSprintSummary() {
		assumeTrue("Skipping test as current board type is Kanban", sourceClient.getBoardType(sourceId).equalsIgnoreCase("scrum"));

		Optional<Object> sourceActiveSprint = sourceClient.getSprints(sourceId).getList("values").stream()
				.filter(sprint -> getValueForObject(sprint, "state").toString().equalsIgnoreCase("active")).findFirst();
		assumeTrue("No active sprint for source board", sourceActiveSprint.isPresent());

		Optional<Object> targetActiveSprint = targetClient.getSprints(targetId).getList("values").stream()
				.filter(sprint -> getValueForObject(sprint, "state").toString().equalsIgnoreCase("active")).findFirst();

		assertTrue("No active sprint on target", targetActiveSprint.isPresent());

		String sourceSprintName = getValueForObject(sourceActiveSprint.get(), "name").toString();
		String sourceSprintGoal = getValueForObject(sourceActiveSprint.get(), "goal") == null ? ""
				: getValueForObject(sourceActiveSprint.get(), "goal").toString();
		String sourceSprintStart = getValueForObject(sourceActiveSprint.get(), "startDate").toString();
		String sourceSprintEnd = getValueForObject(sourceActiveSprint.get(), "endDate").toString();

		String targetSprintName = getValueForObject(targetActiveSprint.get(), "name").toString();
		String targetSprintGoal = getValueForObject(targetActiveSprint.get(), "goal") == null ? ""
				: getValueForObject(targetActiveSprint.get(), "goal").toString();
		String targetSprintStart = getValueForObject(targetActiveSprint.get(), "startDate").toString();
		String targetSprintEnd = getValueForObject(targetActiveSprint.get(), "endDate").toString();

		errorCollector.checkThat("Sprint name mismatch", targetSprintName, is(sourceSprintName));
		errorCollector.checkThat("Sprint goal mismatch", targetSprintGoal, is(sourceSprintGoal));
		errorCollector.checkThat("Sprint start date mismatch", targetSprintStart, is(sourceSprintStart));
		errorCollector.checkThat("Sprint end date mismatch", targetSprintEnd, is(sourceSprintEnd));
	}

	@Test
	public void activeSprintIssues() {

		assumeTrue("Skipping test as current board type is Kanban", sourceClient.getBoardType(sourceId).equalsIgnoreCase("scrum"));

		Optional<Object> sourceActiveSprint = sourceClient.getSprints(sourceId).getList("values").stream()
				.filter(sprint -> getValueForObject(sprint, "state").toString().equalsIgnoreCase("active")).findFirst();

		assumeTrue("No active sprint for source board", sourceActiveSprint.isPresent());

		String sourceSprintId = getValueForObject(sourceActiveSprint.get(), "id").toString();

		Optional<Object> targetActiveSprint = targetClient.getSprints(targetId).getList("values").stream()
				.filter(sprint -> getValueForObject(sprint, "state").toString().equalsIgnoreCase("active")).findFirst();

		assertTrue("No active sprint on target", targetActiveSprint.isPresent());

		String targetSprintId = getValueForObject(targetActiveSprint.get(), "id").toString();

		Map<String, List<String>> sourceIssuesPerColumn = new LinkedHashMap<String, List<String>>();
		Map<String, List<String>> targetIssuesPerColumn = new LinkedHashMap<String, List<String>>();

		List<Object> sourceColumns = sourceClient.getBoardColumnConfig(sourceId).getList("currentViewConfig.columns");
		List<Object> targetColumns = targetClient.getBoardColumnConfig(targetId).getList("currentViewConfig.columns");

		Map<String, String> sourceIssueStatus = new LinkedHashMap<String, String>();

		sourceClient.getSprintsDetails(sourceId, sourceSprintId).getList("issues").forEach(entry -> {

			sourceIssueStatus.put(getMappedProjectIssueKey(entry, "key"),
					getValueForObject(getValueForObject(getValueForObject(entry, "fields"), "status"), "id").toString());

		});

		Map<String, String> targetIssueStatus = new LinkedHashMap<String, String>();

		sourceClient.getSprintsDetails(targetId, targetSprintId).getList("issues").forEach(entry -> {

			targetIssueStatus.put(getValueForObject(entry, "key").toString(),
					getValueForObject(getValueForObject(getValueForObject(entry, "fields"), "status"), "id").toString());

		});

		sourceColumns.forEach(entry -> {

			List<String> statuses = (List<String>) getValueForObject(entry, "statusIds");

			List<String> issues = sourceIssueStatus.entrySet().stream().filter(x -> statuses.contains(x.getValue()))
					.map(issue -> issue.getKey()).collect(toList());

			sourceIssuesPerColumn.put(getValueForObject(entry, "name").toString(), issues);
		});

		targetColumns.forEach(entry -> {

			List<String> statuses = (List<String>) getValueForObject(entry, "statusIds");

			List<String> issues = targetIssueStatus.entrySet().stream().filter(x -> statuses.contains(x.getValue()))
					.map(issue -> issue.getKey()).collect(toList());

			targetIssuesPerColumn.put(getValueForObject(entry, "name").toString(), issues);
		});

		assertThat(sourceIssuesPerColumn).containsExactlyEntriesIn(targetIssuesPerColumn).inOrder();
	}

	@Test
	public void activeSprintFlaggedIssues() {

		assumeTrue("Skipping test as current board type is Kanban", sourceClient.getBoardType(sourceId).equalsIgnoreCase("scrum"));

		Optional<Object> sourceActiveSprint = sourceClient.getSprints(sourceId).getList("values").stream()
				.filter(sprint -> getValueForObject(sprint, "state").toString().equalsIgnoreCase("active")).findFirst();

		assumeTrue("No active sprint for source board", sourceActiveSprint.isPresent());

		String sourceSprintId = getValueForObject(sourceActiveSprint.get(), "id").toString();

		Optional<Object> targetActiveSprint = targetClient.getSprints(targetId).getList("values").stream()
				.filter(sprint -> getValueForObject(sprint, "state").toString().equalsIgnoreCase("active")).findFirst();

		assertTrue("No active sprint on target", targetActiveSprint.isPresent());

		String targetSprintId = getValueForObject(targetActiveSprint.get(), "id").toString();

		List<String> sourceFlaggedIssues = sourceClient.getSprintsDetails(sourceId, sourceSprintId).getList("issues").stream()
				.filter(entry -> Boolean.parseBoolean(getValueForObject(getValueForObject(entry, "fields"), "flagged").toString()))
				.map(e -> getMappedProjectIssueKey(e, "key")).collect(toList());

		List<String> targetFlaggedIssues = targetClient.getSprintsDetails(targetId, targetSprintId).getList("issues").stream()
				.filter(entry -> Boolean.parseBoolean(getValueForObject(getValueForObject(entry, "fields"), "flagged").toString()))
				.map(e -> getValueForObject(e, "key").toString()).collect(toList());

		assertThat(sourceFlaggedIssues).containsExactlyElementsIn(targetFlaggedIssues);

	}
}
