package com.botronsoft.pso.migrations.tests.confluence;

import org.junit.BeforeClass;
import org.junit.ClassRule
import org.junit.Test
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import com.botronsoft.pso.commons.mapping.api.MapperReader
import com.botronsoft.pso.commons.mapping.api.MappingFactory
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory
import com.botronsoft.pso.migration.framework.environment.Environments
import com.botronsoft.pso.migrations.Env
import com.botronsoft.pso.migrations.ObjectTypeKeys
import com.botronsoft.pso.migrations.tests.framework.CacheRule
import com.googlecode.junittoolbox.ParallelParameterized;

import groovy.util.logging.Slf4j;

@RunWith(ParallelParameterized.class)
@Slf4j
public class TestSpacePermissions {
	private String key;


	static MappingFactory factory = new FileSystemMappingFactory(Environments.confluence(Env.SOURCE).getKey());
	static MapperReader reader = factory.createReader();


	@Parameters(name = "space {0}")
	public static Collection<Object[]> data() throws Exception {
		return new ArrayList<String>(reader.map(ObjectTypeKeys.MIGRATED_SPACE_KEY).keySet());
	}

	@BeforeClass
	public static void prepare() throws Exception {

		String.metaClass.fix = {
			delegate.replace("\u2019", "'").normalize().trim()
		}
	}


	public TestSpacePermissions(String key) {
		this.key = key
	}


	@ClassRule
	public static CacheRule sourcePermissions = new CacheRule({ String key->
		Environments.confluence(Env.SOURCE).getConfluenceClient().getGSpacePermissionSets(key);
	})
	@ClassRule
	public static CacheRule targetPermissions = new CacheRule({ String key->
		Environments.confluence(Env.TARGET).getConfluenceClient().getGSpacePermissionSets(reader.map(key));
	})

	@ClassRule
	public static CacheRule sourceMembership = new CacheRule(100,{ String groupName->
		Environments.confluence(Env.SOURCE).getCMCClient().getAllMembersOfGroup(groupName)
	})
	@ClassRule
	public static CacheRule targetMembership = new CacheRule(100,{ String groupName->
		Environments.confluence(Env.TARGET).getCMCClient().getAllMembersOfGroup(groupName);
	})

	static def localAccounts=["admin", "confluence-admin"]

	private def List<String> getFullUserPerms(perms,CacheRule membership) {
		def uperms = perms.collectMany{
			it.spacePermissions
		}.findAll{
			it.userName !=null
		}.collect{
			"${it.userName.toLowerCase()}:${it.type}"
		}.sort()

		def gperms = perms.collectMany{
			it.spacePermissions
		}.findAll{
			it.groupName !=null
		}.collectMany{ perm->
			def users = membership.get(perm.groupName.toLowerCase())
			return users.findAll{
				!(it.toLowerCase() in localAccounts)
			}.collect{ username->
				"${username.toLowerCase()}:${perm.type}"
			}
		}.unique().sort()

		def res =  (uperms+gperms).unique().sort()
		//		println res
		return res
	}


	private def List<String> getUserPerms(perms) {
		perms?.collectMany{
			it.spacePermissions
		}.findAll{
			it.userName !=null && !(it.userName in localAccounts)
		}.collect{
			"${it.userName.toLowerCase()}:${it.type}"
		}.sort()
	}

	private def List<String> getAnonymousPerms(perms) {
		perms?.collectMany{
			it.spacePermissions
		}.findAll{
			it.userName ==null && it.groupName==null
		}.collect{
			"${it.type}"
		}.sort()
	}
	private def List<String> getGroupPerms(perms) {
		perms?.collectMany{
			it.spacePermissions
		}.findAll{
			it.groupName !=null
		}.collect{
			def gname = it.groupName
			"${gname.toLowerCase()}:${it.type}"
		}.sort()
	}

	/**
	 * Verifies that all anonymous space permissions are removed as per the Garmin AMS
	 */
	@Test
	public void anonymousAccessRemoved() {
		Iterable tAnonymousPerm = getAnonymousPerms(targetPermissions.get(reader.map(key)))
		assertThat("Space has anonymous access on target", tAnonymousPerm,
				notNullValue());
	}

	/**
	 * Verifies that all space permissions were migrated to the target instance
	 */
	@Test
	public void userPermissions() {
		def  sUsersPerm = getUserPerms(sourcePermissions.get(key))
		def  tUsersPerm =  getUserPerms(targetPermissions.get(reader.map(key)))

		assumeTrue(tUsersPerm!=null)

		def  sNewUsersPerm = []
		sUsersPerm.each {
			String oldName = it.split(":")[0];
			def newU = transformationMapping.getNewUserName(oldName)
			sNewUsersPerm.asList().add(it.toString().replace(oldName, newU));
		}
		assertThat("User permissions are  different",tUsersPerm.collect{
			it
		}.sort(),
		containsInAnyOrder(sNewUsersPerm.collect{
			it
		}.sort().toArray()));
	}

	@Test
	public void groupPermissions() {
		def Iterable sPerms = getGroupPerms(sourcePermissions.get(key))
		def Iterable tPerms =  getGroupPerms(targetPermissions.get(reader.map(key)))

		assertThat("Group permissions are  different",tPerms,
				containsInAnyOrder(sPerms.collect{
					it
				}.sort().toArray()));
	}

	/**
	 * Verifies that the permissions are the same for all users either by direct permission or through group permission.
	 * <p>
	 * E.g. user "John" has EDIT permission through group confluence-user and ADMIN permission assigned directly.
	 * This test will verify that  John has the same permissions in target
	 * <p>
	 * Will fail if a user on target has a permission which is not avialble on source e.g. the members of group "conf-admins" are different between soruce and target
	 */

	@Test
	public void usersInGroupPermissions() {
		def Iterable sPerms = getFullUserPerms(sourcePermissions.get(key),sourceMembership)
		def Iterable tPerms =  getFullUserPerms(targetPermissions.get(reader.map(spaceKey)),targetMembership)

		assertThat("Group permissions are  different",tPerms,
				containsInAnyOrder(sPerms.collect{
					it
				}.sort().toArray()));
	}
}
