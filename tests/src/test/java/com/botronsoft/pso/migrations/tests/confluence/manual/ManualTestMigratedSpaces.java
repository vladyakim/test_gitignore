package com.botronsoft.pso.migrations.tests.confluence.manual;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.utils.URIBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.botronsoft.pso.migrations.tests.framework.AbstractBrowserTool;

/**
 * Webdriver code that opens the source and target boards side by side, performing SU to the owner user This code needs CSV data from the
 * following query: <br/>
 * <h3>Agile Boards</h3>
 *
 * <pre>
{@code
SELECT rv.id, old_id, name, lower_user_name
FROM mjira.AO_60DB71_RAPIDVIEW rv
LEFT JOIN mjira.app_user au ON rv.owner_user_name = au.user_key
WHERE old_id IS NOT NULL
ORDER BY owner_user_name;
}
 * </pre>
 *
 * <h3>Dashboards</h3>
 *
 * <pre>
{@code
SELECT pp.id, old_id, pagename, lower_user_name
FROM mjira.portalpage pp
LEFT JOIN mjira.app_user au ON pp.username = au.user_key
WHERE old_id IS NOT NULL
ORDER BY username;
}
 * </pre>
 *
 * <h3>Filters</h3>
 *
 * <pre>
{@code
SELECT s.id,old_id,filtername, au.lower_user_name FROM mjira.searchrequest s
LEFT JOIN mjira.app_user au ON s.authorname = au.user_key
WHERE old_id IS NOT NULL
ORDER BY authorname;
}
 * </pre>
 *
 */
public class ManualTestMigratedSpaces extends AbstractBrowserTool {

	private String source;
	private String target;
	private String srcBaseUrl;
	private WebDriverWait wait;
	private WebDriverWait scrwait;

	private static final String QUICK_LOGIN_URL = "/rest/auth/1/session";
	//
	private final String baseUrl;
	private final String password;
	private final String user;
	private final String scrpassword;
	private final String scruser;
	private final WebDriver driver;

	public ManualTestMigratedSpaces(String baseUrl, String user, String password, String srcBaseUrl, String scruser, String scrpassword)
			throws Exception {
		super(new Properties());
		this.baseUrl = baseUrl;
		this.user = user;
		this.password = password;
		this.scruser = scruser;
		this.scrpassword = scrpassword;
		this.srcBaseUrl = srcBaseUrl;

		wait = new WebDriverWait(getDriver(), 30);
		quickLoginAdmin(baseUrl);
		driver = new ChromeDriver();
		scrwait = new WebDriverWait(driver, 30);
		sourceLogin(driver);
	}

	private static enum TestType {
		SPACE
	}

	private String getUrl(String relative) {
		return getUrl(baseUrl, relative);
	}

	private String getscrUrl(String relative) {
		return getUrl(srcBaseUrl, relative);
	}

	private String getUrl(String base, String relative) {
		return base + "/" + relative;
	}

	protected void login() throws Exception {
		URI uri = new URIBuilder(getUrl(QUICK_LOGIN_URL)).addParameter("os_username", user).addParameter("os_password", password)
				.addParameter("os_destination", QUICK_LOGIN_URL).build();
		get(uri.toString());
		Thread.sleep(10000);
	}

	public void run(String tagetKey, String sourceKey) throws Exception {

		target = getDriver().getWindowHandle();
		navigateTo(getUrl("spaces/viewspacesummary.action?key=" + tagetKey + "&showAllAdmins=true"));
		waitForTarget(wait);
		// navigateTo(getUrl("spaces/viewspacesummary.action?key=DEMO&showAllAdmins=true"));
		// Thread.sleep(5000);
		driver.get(srcBaseUrl + "/spaces/viewspacesummary.action?key=" + sourceKey + "&showAllAdmins=true");
		waitFor(scrwait);
	}

	private void navigateTo(String url) {
		getDriver().get(url);
	}

	private void sourceLogin(WebDriver driver) throws Exception {
		URI uri = new URIBuilder(getscrUrl(QUICK_LOGIN_URL)).addParameter("os_username", scruser).addParameter("os_password", scrpassword)
				.addParameter("os_destination", QUICK_LOGIN_URL).build();
		driver.get(uri.toString());
		Thread.sleep(10000);
	}

	private void quickLoginAdmin(String url) throws URISyntaxException, InterruptedException {
		URI uri = new URIBuilder(url).addParameter("os_username", user).addParameter("os_password", password)
				.addParameter("os_destination", QUICK_LOGIN_URL).build();
		getDriver().get(uri.toString());
		Thread.sleep(10000);
	}

	private String getSourceUrl(String relative) {
		return srcBaseUrl + "/" + relative;
	}

	private String openTab() {
		((RemoteWebDriver) getDriver()).executeScript("window.open('');");
		String newTabInstance = getDriver().getWindowHandles().toArray(new String[0])[1];
		return newTabInstance;
	}

	private void waitForScriptsPage() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Switch to a different user")));
		getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

	}

	private void waitForSwitchLink() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("here")));
	}

	private void waitForTarget(WebDriverWait wait) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Space Details")));
	}

	private void waitFor(WebDriverWait wait) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Space Details")));
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception, IOException {

		String targetBaseUrl = "https://targetconfluence.com";
		String targetUsername = "targetUser";
		String targetPassword = "XXXXXX";
		String sourceBaseUrl = "http://sourceconfluence.com";
		String sourceUsername = "sourceUser";
		String sourcePassword = "XXXXXXX";
		ManualTestMigratedSpaces view = new ManualTestMigratedSpaces(targetBaseUrl, targetUsername, targetPassword, sourceBaseUrl,
				sourceUsername, sourcePassword);
		List<String> inKeys = Arrays.asList("KEY1", "KEY2");

		inKeys.forEach(key -> {

			try {
				view.run(key, key);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		});
		view.run("DIGITALHELP", "HELP");

	}
}
