package com.botronsoft.pso.migrations.tests.bitbucket;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeThat;
import static org.junit.Assume.assumeTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.rest.client.BitBucketClient;
import com.google.common.collect.Lists;
import com.googlecode.junittoolbox.ParallelParameterized;

import io.restassured.path.json.JsonPath;

@RunWith(ParallelParameterized.class)
public class TestProjects {

	private static final Logger log = LoggerFactory.getLogger(TestProjects.class);
	public static final int MAX_PAGE_LIMIT = 1048576;

	private static BitBucketClient sourceClient = Env.SOURCE_BB_ENV.getBitBucketClient();
	private static BitBucketClient targetClient = Env.TARGET_BB_ENV.getBitBucketClient();
	private String projectKey;

	public TestProjects(String projectKey) {
		this.projectKey = projectKey;
	}

	@Parameters(name = "project with key {0}")
	public static Collection<String[]> data() throws Exception {
		ArrayList<String[]> result = new ArrayList<>();

		List<String> projects = Env.SOURCE_BB_ENV.getBitBucketClient().getAllProjectsKeys();

		for (String key : projects) {
			result.add(new String[] { key });
		}

		return result;
	}

	@Test
	public void projectDetailsMatch() throws Exception {
		assumeThat("Not applicable for personal projects", projectKey, not(startsWith("~")));
		JsonPath sJson = sourceClient.getProject(projectKey);
		JsonPath tJson = targetClient.getProject(projectKey);

		assertTrue("Project is missing on target", tJson != null);

		assertThat("Project name not matching", tJson.getString("name"), equalTo(sJson.getString("name")));
		assertThat("Project description not matching", tJson.getString("description"), equalTo(sJson.getString("description")));
		assertThat("Project type not matching", tJson.getString("type"), equalTo(sJson.getString("type")));
		assertThat("Project visibillity not matching", tJson.getString("public"), equalTo(sJson.getString("public")));

	}

	@Test
	public void projectUserPermissionsMatch() throws Exception {
		assumeThat("Not applicable for personal projects", projectKey, not(startsWith("~")));
		JsonPath sJson = sourceClient.getProjectUserPermissions(projectKey, 0, MAX_PAGE_LIMIT);
		JsonPath tJson = targetClient.getProjectUserPermissions(projectKey, 0, MAX_PAGE_LIMIT);

		assumeTrue("Project is missing on target", tJson != null);

		String deploymentUser = "root";

		List<Object> tUsers = tJson.getList("values.findAll{it.user.name.toLowerCase() != '" + deploymentUser
				+ "'}.collect{/${it.permission};${it.user.name.toLowerCase()};${it.user.type}/}");

		List<Object> sUsers = sJson.getList("values.collect{/${it.permission};${it.user.name.toLowerCase()};${it.user.type}/}");

		assertThat("Permissions mismatch", Arrays.asList(tUsers), containsInAnyOrder(sUsers));

	}

	@Test
	@Ignore
	public void projectGroupPermissionsMatch() throws Exception {
		assumeThat("Not applicable for personal projects", projectKey, not(startsWith("~")));
		JsonPath sJson = sourceClient.getProjectGroupPermissions(projectKey, 0, MAX_PAGE_LIMIT);
		JsonPath tJson = targetClient.getProjectGroupPermissions(projectKey, 0, MAX_PAGE_LIMIT);

		List<Object> tGroups = tJson.getList("values.collect{/${it.permission};${it.group.name.toLowerCase()}/}");
		List<Object> sGroups = sJson.getList("values.collect{/${it.permission};${it.group.name.toLowerCase()}/}");
		assertThat("Group permissions mismatch", Arrays.asList(sGroups), containsInAnyOrder(tGroups));

	}

	@Test
	public void reposMatch() throws Exception {
		List<JSONObject> sReposPage = sourceClient.getProjectRepositories(projectKey, 0, MAX_PAGE_LIMIT);
		List<JSONObject> tReposPage = targetClient.getProjectRepositories(projectKey, 0, MAX_PAGE_LIMIT);

		assumeTrue("Project is missing on target", tReposPage != null);

		List<String> tRepos = collectRepos(tReposPage);
		List<String> sRepos = collectRepos(sReposPage);

		assertThat("Repositories mismatch", Arrays.asList(tRepos), containsInAnyOrder(sRepos));

		for (Pair<JSONObject, JSONObject> p : pair(sReposPage, tReposPage)) {
			JSONObject sRepo = p.getLeft();
			JSONObject tRepo = p.getRight();
			assertThat("Name is different of repo", (tRepo.get("name")), equalTo(sRepo.get("name")));
			assertThat("Public setting is different of repo", tRepo.get("public"), equalTo(sRepo.get("public")));

		}

	}

	public static <T> List<Pair<T, T>> pair(Iterable<T> left, Iterable<T> right) {
		Iterator<T> rightIterator = right.iterator();
		return Lists.newArrayList(left).stream().map(l -> Pair.of(l, rightIterator.next())).collect(Collectors.toList());
	}

	private static List<String> collectRepos(List<JSONObject> reposPage) {
		return reposPage.stream().map(s -> {
			try {
				return String.valueOf(s.get("slug"));
			} catch (JSONException e) {

				// not sure whether this is the correct way to cause test failure or if I should return empty list instead?!
				log.error("Unable to retrieve repo list missing json element. See exception for futher details\n{}", e.getMessage());
				return null;
			}
		}).collect(Collectors.toList());
	}

}
