package com.botronsoft.pso.migrations.tests.confluence

import static com.google.common.truth.Truth.*

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import com.botronsoft.pso.commons.mapping.api.MapperReader
import com.botronsoft.pso.commons.mapping.api.MappingFactory
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory
import com.botronsoft.pso.migration.framework.environment.Environments
import com.botronsoft.pso.migrations.Env
import com.botronsoft.pso.migrations.ObjectTypeKeys
import com.botronsoft.pso.rest.client.ConfluenceClient
import com.googlecode.junittoolbox.ParallelParameterized

import groovy.util.logging.Slf4j;

@RunWith(ParallelParameterized.class)
@Slf4j
public class TestUserSettings {
	private static sourceUrl
	private static sourceUser
	private static souurcePassword

	private static targetUrl
	private static targetUser
	private static targetPassword


	private static ConfluenceClient sourceClient;
	private static ConfluenceClient targetClient;

	static MappingFactory factory = new FileSystemMappingFactory(Environments.confluence(Env.SOURCE).getKey());
	static MapperReader reader = factory.createReader();

	public static final String PROPERTY_USER_SUBSCRIBE_TO_DIGEST = "confluence.prefs.email.notify";
	public static final String PROPERTY_USER_NOTIFY_FOR_MY_OWN_ACTIONS = "confluence.prefs.notify.for.my.own.actions";
	public static final String PROPERTY_USER_WATCH_MY_OWN_CONTENT = "confluence.prefs.watch.my.own.content";
	public static final String PROPERTY_USER_EMAIL_SHOW_DIFF = "confluence.prefs.email.show.diff";
	public static final String PROPERTY_USER_NOTIFY_ON_NEW_FOLLOWERS = "confluence.prefs.notify.on.new.followers";
	public static final String PROPERTY_USER_SUBSCRIBE_TO_RECOMMENDED_UPDATES = "confluence.prefs.daily.summary.receive.updates";
	public static final String PROPERTY_USER_SUBSCRIBE_TO_RECOMMENDED_UPDATES_SET = "confluence.prefs.daily.summary.receive.updates.set";
	public static final String PROPERTY_USER_RECOMMENDED_UPDATES_SCHEDULE = "confluence.prefs.daily.summary.schedule";



	public static final String PROPERTY_USER_LOCALE = "confluence.user.locale";
	public static final String PROPERTY_USER_TIME_ZONE = "confluence.user.time.zone";
	public static final String PROPERTY_USER_SITE_HOMEPAGE = "confluence.user.site.homepage";

	@BeforeClass
	public static void prepare() throws Exception {
		targetClient = Environments.confluence(Env.TARGET).getConfluenceClient()
		sourceClient = Environments.confluence(Env.SOURCE).getConfluenceClient()
	}

	@Parameters(name = "User: {0}:{1}")
	public static Collection<Object[]> data() throws Exception {
		new ArrayList<String>(reader.map(ObjectTypeKeys.USER_NAME))
				.collect{
					[it.key, it.value].toArray()
				}
	}


	private String oldUsername;
	private String newUsername;

	public TestUserSettings(String oldUsername, String newUsername) {
		this.oldUsername = oldUsername
		this.newUsername = newUsername
	}


	@Test
	public void userPreferences() {
		def source = sourceClient.getUserPreferencesBoolean(oldUsername, PROPERTY_USER_SUBSCRIBE_TO_DIGEST)
		def target = targetClient.getUserPreferencesBoolean(newUsername, PROPERTY_USER_SUBSCRIBE_TO_DIGEST)

		assertWithMessage("User Preferences are different(PROPERTY_USER_SUBSCRIBE_TO_DIGEST)").that(source).isEqualTo(target)

		source = sourceClient.getUserPreferencesBoolean(oldUsername, PROPERTY_USER_NOTIFY_FOR_MY_OWN_ACTIONS)
		target = targetClient.getUserPreferencesBoolean(newUsername, PROPERTY_USER_NOTIFY_FOR_MY_OWN_ACTIONS)

		assertWithMessage("User Preferences are different(PROPERTY_USER_NOTIFY_FOR_MY_OWN_ACTIONS)").that(source).isEqualTo(target)

		source = sourceClient.getUserPreferencesBoolean(oldUsername, PROPERTY_USER_WATCH_MY_OWN_CONTENT)
		target = targetClient.getUserPreferencesBoolean(newUsername, PROPERTY_USER_WATCH_MY_OWN_CONTENT)

		assertWithMessage("User Preferences are different(PROPERTY_USER_WATCH_MY_OWN_CONTENT)").that(source).isEqualTo(target)

		source = sourceClient.getUserPreferencesBoolean(oldUsername, PROPERTY_USER_EMAIL_SHOW_DIFF)
		target = targetClient.getUserPreferencesBoolean(newUsername, PROPERTY_USER_EMAIL_SHOW_DIFF)

		assertWithMessage("User Preferences are different(PROPERTY_USER_EMAIL_SHOW_DIFF)").that(source).isEqualTo(target)

		source = sourceClient.getUserPreferencesBoolean(oldUsername, PROPERTY_USER_NOTIFY_ON_NEW_FOLLOWERS)
		target = targetClient.getUserPreferencesBoolean(newUsername, PROPERTY_USER_NOTIFY_ON_NEW_FOLLOWERS)

		assertWithMessage("User Preferences are different(PROPERTY_USER_EMAIL_SHOW_DIFF)").that(source).isEqualTo(target)

		source = sourceClient.getUserPreferencesBoolean(oldUsername, PROPERTY_USER_SUBSCRIBE_TO_RECOMMENDED_UPDATES)
		target = targetClient.getUserPreferencesBoolean(newUsername, PROPERTY_USER_SUBSCRIBE_TO_RECOMMENDED_UPDATES)

		assertWithMessage("User Preferences are different(PROPERTY_USER_SUBSCRIBE_TO_RECOMMENDED_UPDATES)").that(source).isEqualTo(target)

		source = sourceClient.getUserPreferencesBoolean(oldUsername, PROPERTY_USER_SUBSCRIBE_TO_RECOMMENDED_UPDATES_SET)
		target = targetClient.getUserPreferencesBoolean(newUsername, PROPERTY_USER_SUBSCRIBE_TO_RECOMMENDED_UPDATES_SET)

		assertWithMessage("User Preferences are different(PROPERTY_USER_SUBSCRIBE_TO_RECOMMENDED_UPDATES)").that(source).isEqualTo(target)

		source = sourceClient.getUserPreferencesBoolean(oldUsername, PROPERTY_USER_RECOMMENDED_UPDATES_SCHEDULE)
		target = targetClient.getUserPreferencesBoolean(newUsername, PROPERTY_USER_RECOMMENDED_UPDATES_SCHEDULE)

		assertWithMessage("User Preferences are different(PROPERTY_USER_RECOMMENDED_UPDATES_SCHEDULE)").that(source).isEqualTo(target)

		source = sourceClient.getUserPreferencesString(oldUsername, PROPERTY_USER_LOCALE)
		target = targetClient.getUserPreferencesString(newUsername, PROPERTY_USER_LOCALE)

		assertWithMessage("User Preferences are different(PROPERTY_USER_LOCALE)").that(source).isEqualTo(target)

		source = sourceClient.getUserPreferencesString(oldUsername, PROPERTY_USER_TIME_ZONE)
		target = targetClient.getUserPreferencesString(newUsername, PROPERTY_USER_TIME_ZONE)

		assertWithMessage("User Preferences are different(PROPERTY_USER_TIME_ZONE)").that(source).isEqualTo(target)

		source = sourceClient.getUserPreferencesString(oldUsername, PROPERTY_USER_SITE_HOMEPAGE)
		target = targetClient.getUserPreferencesString(newUsername, PROPERTY_USER_SITE_HOMEPAGE)

		assertWithMessage("User Preferences are different(PROPERTY_USER_SITE_HOMEPAGE)").that(source).isEqualTo(target)
	}


	//	@Test
	//	public void myFavorites() {
	//		def source = sourceClient.getUserFavoriteContent(username)?.getList("collect{it.title}")
	//		def target = targetClient.getUserFavoriteContent(username)?.getList("collect{it.title}")
	//
	//
	//		assertThat("Space watchers are different",target, containsInAnyOrder(source.toArray()));
	//	}
}
