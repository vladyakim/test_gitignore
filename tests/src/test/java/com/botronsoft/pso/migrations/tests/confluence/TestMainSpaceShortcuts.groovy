package com.botronsoft.pso.migrations.tests.confluence

import org.junit.BeforeClass;
import org.junit.ClassRule
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import com.botronsoft.pso.commons.mapping.api.MapperReader
import com.botronsoft.pso.commons.mapping.api.MappingFactory
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory
import com.botronsoft.pso.migration.framework.environment.Environments
import com.botronsoft.pso.migrations.Env
import com.botronsoft.pso.migrations.ObjectTypeKeys
import com.botronsoft.pso.migrations.tests.framework.CacheRule
import com.google.common.truth.Truth
import com.googlecode.junittoolbox.ParallelParameterized;

import groovy.util.logging.Slf4j;

@RunWith(ParallelParameterized.class)
@Slf4j
public class TestMainSpaceShortcuts {
	private String spaceKey

	static MappingFactory factory = new FileSystemMappingFactory(Environments.confluence(Env.SOURCE).getKey());
	static MapperReader reader = factory.createReader();

	def static spaceKeyMapping

	public TestMainSpaceShortcuts(String spaceKey) {
		this.spaceKey = spaceKey
	}

	@Parameters(name = "space {0}")
	public static Collection<Object[]> data() throws Exception {
		return new ArrayList<Object[]>(reader.map(ObjectTypeKeys.MIGRATED_SPACE_KEY).keySet());
	}


	@ClassRule
	public static CacheRule sourceSpaceShortcuts = new CacheRule({ String key->
		Environments.confluence(Env.SOURCE).getConfluenceClient().getMainSpaceShortcuts(key,true)
	})
	@ClassRule
	public static CacheRule targetSpaceShortcuts = new CacheRule({ String key->
		Environments.confluence(Env.TARGET).getConfluenceClient().getMainSpaceShortcuts(key,true);
	})

	@BeforeClass
	public static void prepare() throws Exception {
		String.metaClass.fix = {
			delegate.replace("\u2019", "'")?.replace('–','-') .normalize().trim()
		}
	}

	private def Iterable<String> getSpaceShortcutsTitle(shortcuts) {
		shortcuts.findAll{ it.title != null }.collect{
			it.title.fix()
		}.sort()
	}

	private def Iterable<String> getSpaceShortcutsUrl(shortcuts) {
		shortcuts.findAll{ it.title != null }.collect{ "${it.title}:$it.url" }.sort()
	}

	private def Iterable<String> getSpaceShortcutsHidden(shortcuts) {
		shortcuts.findAll{ it.title != null }.collect{ "${it.title}:$it.hidden" }.sort()
	}

	private def Iterable<String> getSpaceShortcutsStyle(shortcuts) {
		shortcuts.findAll{ it.title != null }.collect{ "${it.title}:$it.styleClass" }.sort()
	}

	private def Iterable<String> getSpaceShortcutsTooltip(shortcuts) {
		shortcuts.findAll{ it.title != null }.collect{ "${it.title}:$it.tooltip" }.sort()
	}


	@Test
	public void spaceShortcutsTitle() {
		def Iterable sShortcutsTitle = getSpaceShortcutsTitle(sourceSpaceShortcuts.get(spaceKey))
		def Iterable tShortcutsTitle = getSpaceShortcutsTitle(targetSpaceShortcuts.get(reader.map(spaceKey)))


		Truth.assertThat(tShortcutsTitle.size() >= sShortcutsTitle.size())
		Truth.assertThat(tShortcutsTitle).containsAnyIn(sShortcutsTitle)
	}

	@Test
	public void spaceShortcutsUrl() {
		def Iterable sShortcutsTitleUrl = sourceSpaceShortcuts.get(spaceKey).collect{
			"${it.title}:$it.url"
		}.collect{it.toLowerCase()}.sort()
		def Iterable tShortcutsTitleUrl = getSpaceShortcutsUrl(targetSpaceShortcuts.get(reader.map(spaceKey))).collect{it.toLowerCase()}

		Truth.assertThat(tShortcutsTitleUrl).containsAnyIn(sShortcutsTitleUrl)
	}

	@Test
	public void spaceShortcutsHidden() {
		def Iterable sShortcutsTitleHidden = getSpaceShortcutsHidden(sourceSpaceShortcuts.get(spaceKey))
		def Iterable tShortcutsTitleHidden = getSpaceShortcutsHidden(targetSpaceShortcuts.get(reader.map(spaceKey)))

		Truth.assertThat(tShortcutsTitleHidden).containsAnyIn(sShortcutsTitleHidden)
	}



	@Test
	public void spaceShortcutsTooltip() {
		def Iterable sShortcutsTitleTooltip = getSpaceShortcutsTooltip(sourceSpaceShortcuts.get(spaceKey))
		def Iterable tShortcutsTitleTooltip = getSpaceShortcutsTooltip(targetSpaceShortcuts.get(reader.map(spaceKey)))

		Truth.assertThat(tShortcutsTitleTooltip).containsAnyIn(sShortcutsTitleTooltip)
	}
}
