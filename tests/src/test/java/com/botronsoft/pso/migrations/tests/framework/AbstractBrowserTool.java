package com.botronsoft.pso.migrations.tests.framework;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public abstract class AbstractBrowserTool {
	private static final org.slf4j.Logger log = LoggerFactory.getLogger(AbstractBrowserTool.class);
	private static final String SETTING_DOWNLOAD_PATH = "downloadPath";
	//
	private Properties settings;
	private String downloadFilepath;
	private ChromeDriver driver;
	private String sessionId;

	public AbstractBrowserTool(Properties p) {
		this(p, null);
	}

	public AbstractBrowserTool(Properties p, String sessionName) {
		settings = p;
		downloadFilepath = p.getProperty(SETTING_DOWNLOAD_PATH);
		sessionId = String.valueOf(System.currentTimeMillis());
		if (sessionName != null) {
			sessionId = sessionName + "_" + sessionId;
		}
		configureLogging();
	}

	private void configureLogging() {
		FileAppender fa = new FileAppender();
		fa.setName("FileLogger");
		fa.setFile(new File(getWorkingDir(), "session.log").getAbsolutePath());
		fa.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n"));
		fa.setThreshold(Level.INFO);
		fa.setAppend(true);
		fa.activateOptions();

		// add appender to any Logger (here is root)
		Logger.getRootLogger().addAppender(fa);
	}

	protected String getSessionId() {
		return sessionId;
	}

	protected File getWorkingDir() {
		File workdir = new File(System.getProperty("user.dir") + "/target/runs/" + sessionId);
		workdir.mkdirs();
		return workdir;
	}

	private ChromeDriver openBrowser() {
		String driverPath = settings.getProperty("webdriver.path", null);
		if (driverPath == null) {
			String os = "osx";
			if (SystemUtils.IS_OS_MAC) {
				os = "osx/chromedriver";
			}

			if (SystemUtils.IS_OS_WINDOWS) {
				os = "windows/googlechrome/64bit/chromedriver.exe";
			}
			driverPath = new File(System.getProperty("user.dir"), "target/driver/binaries/" + os)
					.getAbsolutePath();
			new File(driverPath).setExecutable(true);
		}
		System.setProperty("webdriver.chrome.driver", driverPath);
		// Ensure the downloads path exists
		if (downloadFilepath != null) {
			new File(downloadFilepath).mkdirs();
		}

		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.default_directory", downloadFilepath);
		ChromeOptions options = new ChromeOptions();
		HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
		options.setExperimentalOption("prefs", chromePrefs);
		options.addArguments("--test-type");
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new ChromeDriver(cap);
		return driver;
	}

	protected void sleep(long seconds) {
		driver().manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
	}

	protected void waitUntil(long timeout, Function<WebDriver, ?> condition) {
		new WebDriverWait(driver(), timeout).until(condition);
	}

	protected void waitUntil(long timeout, long sleepMills, Function<WebDriver, ?> condition) {
		new WebDriverWait(driver(), timeout, sleepMills).until(condition);
	}

	protected WebElement findElement(By by) {
		return driver().findElement(by);
	}

	protected List<WebElement> findElements(By by) {
		return driver().findElements(by);
	}

	protected void get(String url) {
		driver().get(url);
	}

	protected String getSetting(String key) {
		return settings.getProperty(key);
	}

	private WebDriver driver() {
		if (driver == null) {
			driver = openBrowser();
		}
		return driver;
	}

	protected WebDriver getDriver() {
		return driver();
	}

	protected void takeScreenshot(String name) {
		try {
			AShot aShot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(50));
			ImageIO.write(aShot.takeScreenshot(getDriver()).getImage(), "png", getImageFile(name));

			// File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			// File destFile = getImageFile(name);
			// FileUtils.moveFile(scrFile, destFile);
		} catch (Throwable e) {
			log.error("Failed to taked screenshot", e);
			// return null;
		}
	}

	protected void close() {
		WebDriver webDriver = getDriver();
		if (driver != null) {
			webDriver.close();
			driver = null;
		}
	}

	protected File getImageFile(String name) {
		return new File(getWorkingDir(), getFileName(name, ".png"));
	}

	protected File getWorkDirFile(String name, String suffix) {
		return new File(getWorkingDir(), getFileName(name, suffix));
	}

	protected String getFileName(String name, String suffix) {
		return name.replaceAll("[^a-zA-Z0-9]", "_").trim() + suffix;
	}

	protected void writeFile(String name, String suffix, String contents) throws IOException {
		FileUtils.write(getWorkDirFile(name, suffix), contents, "UTF-8");
	}

	protected void appendFile(String name, String suffix, String contents) throws IOException {
		FileUtils.write(getWorkDirFile(name, suffix), contents, "UTF-8", true);
	}

	protected void appendFile(String name, String suffix, Collection<String> contents)
			throws IOException {
		FileUtils.writeLines(getWorkDirFile(name, suffix), contents, "UTF-8", true);
	}

	protected void writeFile(String name, String suffix, Collection<String> contents)
			throws IOException {
		FileUtils.writeLines(getWorkDirFile(name, suffix), contents, "UTF-8");
	}
}
