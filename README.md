# Engagement Template Repository #

Engagement repository provide one with the tooling necessary to migrate configuration and data from one Atlassian application to another.

Currently covered applications:

* JIRA
* Confluence

along with migration tests for each.

# Repository How-to #
## How is this repository organized? ##

 * **psoeng-parent** - Manages POM dependencies, organize repositories modules listed below
 * **psoeng-common** - describes Client applications, instances and app/instance specific properties
 * **psoeng-jira** - contains tooling necessary to execute JIRA migration
 * **psoeng-confluence** - contains tooling necessary to execute Confluence migration
 * **psoeng-tests** - contains application specific test suites used to verify successful migration.

## How do I get set up? ##

* **migration.properties** - file containing application specific properties such as URLs, usernames and passwords, paths to file/folders. File entries should follow below guidelines:

```
#format env.{appTypeKey}.{envKey}.{propName}
#appTypeKey - jira, confluence, bitbucket
#envKey - lower case alphanumeric string
#propName - lower case alphanumeric string
```

	
_Do not use the file to store and commit any user specific credentials_. Use **local.migration.properties** instead

* **local.migration.properties** - file is not included within the repository on purpose. Furthermore, it's part of the **.gitignore** content. The idea behind is not to commit any user specific properties such as local paths, credentials etc.
    * in order to create the file you can create a blank file or simply copy/paste migration.properties and save it as **local.migration.properties** or use **sample.local.migration.properties**
    * Any entry in **local.migration.properties** overwrites same one available in **migration.properties**


* **Env.java** - interface describing Client specific Atlassian application setup see javadoc within source for further details.

## What JIRA migration general flow is? ##

*  **Get snapshots**
*  **Generate AMS**
*  **User management**
*  **Apply transformations** 
*  **Migrate SOURCE** 	  
*  **Run tests** 
*  **Provide Customer with migration reports** 

_For further details regarding JIRA migration specifics please refer to_ **README.md** _within_ *[psoeng-jira](jira)*

## What Confluence migration general flow is? ##

_For further details regarding CONFLUENCE migration specifics please refer to_ **README.md** _within_ **psoeng-confluence**

## Have any questions? ##

Drop us an email **PSOTeam@botronsoft.com**
