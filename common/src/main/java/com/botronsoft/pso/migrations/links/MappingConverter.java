package com.botronsoft.pso.migrations.links;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MapperWriter;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
import com.botronsoft.pso.urlreplacer.context.ObjectTypeKeysConfluence;
import com.botronsoft.pso.urlreplacer.context.ObjectTypeKeysJira;

public final class MappingConverter {

	static final Logger log = LoggerFactory.getLogger(MappingConverter.class);

	public static Map<String, String> objectTypeKeysJira;
	static {
		objectTypeKeysJira = new HashMap<>();
		objectTypeKeysJira.put(ObjectTypeKeys.PROJECTKEY, ObjectTypeKeysJira.PROJECTKEY);
		objectTypeKeysJira.put(ObjectTypeKeys.FILTERID, ObjectTypeKeysJira.FILTERID);
		objectTypeKeysJira.put(ObjectTypeKeys.BOARDID, ObjectTypeKeysJira.BOARDID);
		objectTypeKeysJira.put(ObjectTypeKeys.DASHBOARDID, ObjectTypeKeysJira.DASHBOARDID);
		objectTypeKeysJira.put(ObjectTypeKeys.PROJECTID, ObjectTypeKeysJira.PROJECTID);
		objectTypeKeysJira.put(ObjectTypeKeys.CUSTOMFIELD, ObjectTypeKeysJira.CUSTOMFIELD);
		objectTypeKeysJira.put(ObjectTypeKeys.ISSUETYPE, ObjectTypeKeysJira.ISSUETYPE);
		objectTypeKeysJira.put(ObjectTypeKeys.STATUS, ObjectTypeKeysJira.STATUS);
		objectTypeKeysJira.put(ObjectTypeKeys.SPRINT, ObjectTypeKeysJira.SPRINT);
		objectTypeKeysJira.put(ObjectTypeKeys.PROJECTFILTERNAME, ObjectTypeKeysJira.PROJECTFILTERNAME);
		objectTypeKeysJira.put(ObjectTypeKeys.JIRAATTACHMENTID, ObjectTypeKeysJira.JIRAATTACHMENTID);
		objectTypeKeysJira.put(ObjectTypeKeys.JIRAUSERNAME, ObjectTypeKeysJira.JIRAUSERNAME);
		objectTypeKeysJira.put(ObjectTypeKeys.PRIORITY, ObjectTypeKeysJira.PRIORITY);
		objectTypeKeysJira.put(ObjectTypeKeys.RESOLUTION, ObjectTypeKeysJira.RESOLUTION);
		objectTypeKeysJira.put(ObjectTypeKeys.COMPONENT, ObjectTypeKeysJira.COMPONENT);
		objectTypeKeysJira.put(ObjectTypeKeys.COMMENTID, ObjectTypeKeysJira.COMMENTID);
		objectTypeKeysJira.put(ObjectTypeKeys.SERVICEDESKPORTAL, ObjectTypeKeysJira.SERVICEDESKPORTAL);
		objectTypeKeysJira.put(ObjectTypeKeys.ISSUEID, ObjectTypeKeysJira.ISSUEID);
		objectTypeKeysJira.put(ObjectTypeKeys.VERSIONID, ObjectTypeKeysJira.VERSIONID);
	}

	public static Map<String, String> objectTypeKeysConfluence;
	static {
		objectTypeKeysConfluence = new HashMap<>();
		objectTypeKeysConfluence.put(ObjectTypeKeys.SPACE_KEY, ObjectTypeKeysConfluence.SPACE_KEY);
		objectTypeKeysConfluence.put(ObjectTypeKeys.PAGE_ID, ObjectTypeKeysConfluence.PAGE_ID);
		objectTypeKeysConfluence.put(ObjectTypeKeys.BLOGPOST_ID, ObjectTypeKeysConfluence.BLOGPOST_ID);
		objectTypeKeysConfluence.put(ObjectTypeKeys.COMMENT_ID, ObjectTypeKeysConfluence.COMMENT_ID);
		objectTypeKeysConfluence.put(ObjectTypeKeys.ATTACHMENT_ID, ObjectTypeKeysConfluence.ATTACHMENT_ID);
		objectTypeKeysConfluence.put(ObjectTypeKeys.TEMPLATE_ID, ObjectTypeKeys.TEMPLATE_ID);
		objectTypeKeysConfluence.put(ObjectTypeKeys.QUESTION_ID, ObjectTypeKeysConfluence.QUESTION_ID);
		objectTypeKeysConfluence.put(ObjectTypeKeys.ANSWER_ID, ObjectTypeKeysConfluence.ANSWER_ID);
		objectTypeKeysConfluence.put(ObjectTypeKeys.QUESTION_COMMENT_ID, ObjectTypeKeysConfluence.QUESTION_COMMENT_ID);
	}

	public static void convert(String inFolder, String outFolder) {
		MappingFactory mappingFactoryReader = new FileSystemMappingFactory(inFolder);
		MapperReader mappingReader = mappingFactoryReader.createReader();

		MappingFactory mappingFactoryWriter = new FileSystemMappingFactory(outFolder);
		MapperWriter mappingWriter = mappingFactoryWriter.createWriter();

		objectTypeKeysJira.forEach((k, v) -> {
			commonConvert(k, v, mappingReader, mappingWriter);
		});

		objectTypeKeysConfluence.forEach((k, v) -> {
			commonConvert(k, v, mappingReader, mappingWriter);
		});
	}

	private static void commonConvert(String key, String v, MapperReader mappingReader, MapperWriter mappingWriter) {
		Map<String, List<String>> readMap = mappingReader.map(key);
		if (!readMap.isEmpty()) {
			readMap.forEach((n, o) -> {
				if (o.size() > 1) {
					o.forEach(oldvalue -> {
						mappingWriter.writeMap(v, oldvalue, n);
					});
				} else {
					mappingWriter.writeMap(v, n, o.get(0).toString());
				}
			});
		} else {
			log.warn("Creating empty file for " + v);
			mappingWriter.writeMap(v, null, null);
		}
	}
}
