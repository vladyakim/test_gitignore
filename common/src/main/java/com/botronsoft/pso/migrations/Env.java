package com.botronsoft.pso.migrations;

import java.util.List;

import com.botronsoft.pso.migration.framework.environment.BitbucketEnvironment;
import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migration.framework.environment.JiraEnvironment;
import com.botronsoft.pso.migration.framework.environment.LocalEnvironment;
import com.google.common.collect.Lists;

/**
 * Interface describes Client specific Atlassian applications setup.
 * <p>
 * <b>Must be updated accordingly for every migration depending on:</b>
 * <p>
 * <ul>
 * <li>what are the applications being migrated;
 * <li>number of source applications being migrated/merged. See {@link #SOURCE_INSTANCES}. For easier iteration over numerous source
 * instances.
 * </ul>
 * <p>
 * Other <b>Interface fields</b> can be set as needed. E.g. such for paths to file/folders. See
 * <a href="https://bitbucket.org/botronsoft/psoeng-template/src/master/README.md">README.md</a> under <b>psoeng-parent</b>.
 */
public interface Env {
	String SOURCE = "source";
	String TARGET = "target";
	List<String> SOURCE_INSTANCES = Lists.newArrayList(SOURCE);

	String LOCAL = "local";
	String WORKDIR = "work";
	String JMTJAR = "jmtjar";
	String CMJJAR = "cmjjar";

	LocalEnvironment LOCAL_ENV = Environments.local();

	JiraEnvironment SOURCE_JIRA_ENV = Environments.jira(SOURCE);
	JiraEnvironment TARGET_JIRA_ENV = Environments.jira(TARGET);

	ConfluenceEnvironment SOURCE_CONF_ENV = Environments.confluence(SOURCE);
	ConfluenceEnvironment TARGET_CONF_ENV = Environments.confluence(TARGET);

	BitbucketEnvironment SOURCE_BB_ENV = Environments.bitbucket(SOURCE);
	BitbucketEnvironment TARGET_BB_ENV = Environments.bitbucket(TARGET);

}
