package com.botronsoft.pso.migrations;

import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;

import io.github.swagger2markup.markup.builder.MarkupDocBuilder;
import io.github.swagger2markup.markup.builder.MarkupDocBuilders;
import io.github.swagger2markup.markup.builder.MarkupLanguage;
import io.github.swagger2markup.markup.builder.MarkupTableColumn;

/**
 * Abstract AMS generator
 */
public abstract class AbstractAMSGenerator {

	protected static final MarkupDocBuilder MARKUP_BUILDER = MarkupDocBuilders.documentBuilder(MarkupLanguage.CONFLUENCE_MARKUP);

	protected abstract String prefix(String name);

	protected List<String> row(String... values) {
		return Lists.newArrayList(values);
	}

	protected static void macroToc() {
		MARKUP_BUILDER.textLine("{toc}", true);
	}

	protected List<MarkupTableColumn> columns(Collection<String> names) {
		return Lists.newArrayList(names).stream().map(name -> column(name)).collect(toList());
	}

	protected static List<MarkupTableColumn> columns(String... names) {
		return Lists.newArrayList(names).stream().map(name -> column(name)).collect(toList());
	}

	protected static MarkupTableColumn column(String name) {
		return new MarkupTableColumn(name);
	}
}
