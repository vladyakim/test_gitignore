package com.botronsoft.pso.migrations.confluence.samples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;

class ReindexTaskList {

	private static final Logger logger = LoggerFactory.getLogger(ReindexTaskList.class);

	/**
	 * Triggers task list reindex in "target" instance
	 * hibernate.c3p0.max_size=60
	 * tasklist.storagetoaoupgradetask.numthreads = 40 
	 * threads:19
	 * 
	 */
	private static void triggerReindex() {
		logger.info("Trigerring tasklist reindex...");
		Environments.confluence(Env.TARGET).getCMCClient().triggerTaskReindex();
		logger.info("DONE: Check the Confluence logs for more details on the progress...");

	}
	
	public static void main(String[] args) {
		triggerReindex();
	}
}
