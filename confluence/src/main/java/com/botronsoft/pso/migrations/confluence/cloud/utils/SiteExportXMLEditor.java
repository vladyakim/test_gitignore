package com.botronsoft.pso.migrations.confluence.cloud.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.SequenceInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.ranges.RangeException;

import com.botronsoft.pso.migrations.Env;

/**
 * Utility class with single purpose of editing Confluence cloud site export XML
 * to have its content reordered so user favorites are assigned correctly. See
 * <a href=
 * "https://confluence.atlassian.com/confkb/how-to-restore-missing-favorites-after-import-from-xml-744719060.html">Atlassian
 * KB article</a> and <a href=
 * "https://jira.atlassian.com/browse/CONFCLOUD-36348">CONFCLOUD-36348</a> for
 * further details.
 * 
 * @author vladimir
 *
 */
public class SiteExportXMLEditor {

	private static File siteExport;
	private static File unzippedFile;

	private static final String USER_OBJECT_OPENING_TAG = "<object class=\"ConfluenceUserImpl\" package=\"com.atlassian.confluence.user\">";
	private static final String OBJECT_CLOSING_TAG = "</object>";

	private static final String ENTITIES_XML = "entities.xml";
	private final static byte[] BUFFER = new byte[1024];

	public static void main(String[] args) throws Exception {

		// Set path to zip file in local.migration.properties for example:
		// #Confluence Cloud work dir
		// env.local.confluence.cloud.work=/Users/vladimir/Documents/Confluence/Cloud/Export

		// name of site export zip file
		String siteExportFileName = "Confluence-export.zip";

		siteExport = new File(Env.LOCAL_ENV.getProperty("confluence.cloud.work") + "/" + siteExportFileName);

		// unzip entity.xml into temp folder
		unzippedFile = unzipSiteExportEntityFile(siteExport);

		// reorder into new xml and zip into new archive prepended with execution timestamp, leaving initial archive untouched
		reorderInNewFileAndUpdateArchive(unzippedFile);

	}

	private static void reorderInNewFileAndUpdateArchive(File input) {

		File outFile = new File(input.getParent(), ENTITIES_XML);
		File tempFile = new File(input.getParent(), "temp.xml");

		if (outFile.exists()) {
			outFile.delete();
		}

		if (tempFile.exists()) {
			tempFile.delete();
		}

		long entityXMLSize = input.length();
		long bytesProcessed = 0;

		try (BufferedReader br = new BufferedReader(new FileReader(input));
				PrintWriter bWriter = new PrintWriter(new BufferedWriter(new FileWriter(outFile, true)));
				PrintWriter bWriterTempFile = new PrintWriter(new BufferedWriter(new FileWriter(tempFile, true)));) {

			System.out.println("Reordering entity.xml started...");

			int progressIncrement = 10;
			int percentIncrement = Math.round(entityXMLSize / progressIncrement);
			int sizeIncrement = percentIncrement;

			// write first 2 lines of the entity.xml into the new one
			int headerLinesRead = 1;
			String headerLine = null;
			while (headerLinesRead < 3) {
				headerLine = br.readLine();
				bWriter.println(headerLine);
				bytesProcessed += headerLine.getBytes().length;
				headerLinesRead++;
			}

			String line = null;
			while ((line = br.readLine()) != null) {
				if (line.equalsIgnoreCase(USER_OBJECT_OPENING_TAG)) {
					bWriter.println(line);
					bytesProcessed += line.getBytes().length;

					String userTag = br.readLine();
					while (userTag != null) {
						bWriter.println(userTag);
						bytesProcessed += userTag.getBytes().length;

						userTag = br.readLine();
						if (userTag.equalsIgnoreCase(OBJECT_CLOSING_TAG)) {
							bWriter.println(userTag);
							bytesProcessed += userTag.getBytes().length;
							break;
						}
					}
				} else {
					bWriterTempFile.println(line);
					bytesProcessed += line.getBytes().length;
				}

				if (bytesProcessed > percentIncrement) {
					System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " - "
							+ progressIncrement + "% of entity.xml file processed...");
					progressIncrement += 10;
					percentIncrement += sizeIncrement;
				}

			}
			System.out.println("Entire entity.xml file processed!");
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Preparing entity.xml for archiving...");

		
		try(FileInputStream is = new FileInputStream(tempFile);BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(outFile,true)) ){
			IOUtils.copyLarge(is, os);
		} catch (Exception e) {
			throw new RuntimeException(e);
		};

		System.out.println("Preparing new site export zip with revised entity.xml ...");
		addOrReplaceEntry(ENTITIES_XML, getInputStreamFromFile(outFile));

	}

	private static FileInputStream getInputStreamFromFile(File file) {
		try {
			return new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static File unzipSiteExportEntityFile(File siteExport) {

		File unzippedEntityFile = null;

		File destDir = new File(siteExport.getParent() + "/Temp");

		if (!destDir.exists()) {
			destDir.mkdirs();
		}

		System.out.println("Extracting entity.xml file into temp folder: " + destDir + " started ...");

		ZipInputStream zis = null;
		try {
			BufferedInputStream bis = new BufferedInputStream(getInputStreamFromFile(siteExport));
			zis = new ZipInputStream(bis);
			ZipEntry zipEntry = zis.getNextEntry();
			while (zipEntry != null) {
				if (zipEntry.getName().equalsIgnoreCase(ENTITIES_XML)) {
					unzippedEntityFile = new File(destDir, "initial_" + zipEntry.getName());
					FileOutputStream fos = new FileOutputStream(unzippedEntityFile);
					int len;
					while ((len = zis.read(BUFFER)) > 0) {
						fos.write(BUFFER, 0, len);
					}
					fos.close();
				}
				zipEntry = zis.getNextEntry();
			}
			zis.closeEntry();
			zis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Extracting entity.xml file into temp folder complete!");

		return unzippedEntityFile;
	}

	private static void addOrReplaceEntry(String entry, InputStream entryData) {
		try {
			File transformedFile = new File(siteExport.getParent(),
					new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + "-" + siteExport.getName());


			try(ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(transformedFile));){
				addOrReplaceEntry(siteExport, entry, entryData, zipOutputStream);
				System.out.println("Revised site zip available: " + transformedFile.getAbsolutePath());
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static void addOrReplaceEntry(File source, String entry, InputStream entryData,
			ZipOutputStream zipOutputStream) {
		try (ZipFile zipSourceFile = new ZipFile(source)) {

			writeZipEntry(zipOutputStream, entry, entryData);

			Enumeration<? extends ZipEntry> zipEntries = zipSourceFile.entries();
			while (zipEntries.hasMoreElements()) {
				ZipEntry zipEntry = zipEntries.nextElement();
				String entryName = zipEntry.getName();
				if (!entry.equalsIgnoreCase(entryName)) {
					try {
						writeZipEntry(zipOutputStream, entryName, zipSourceFile.getInputStream(zipEntry));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static void writeZipEntry(ZipOutputStream zipOutputStream, String entryName, InputStream entryData) {
		ZipEntry entry = new ZipEntry(entryName);
		try {
			zipOutputStream.putNextEntry(entry);
			int len;
			while ((len = entryData.read(BUFFER)) > 0) {
				zipOutputStream.write(BUFFER, 0, len);
			}
			zipOutputStream.closeEntry();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
