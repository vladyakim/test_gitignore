package com.botronsoft.pso.migrations.confluence.samples;

import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.confluence.cmc.rest.model.bulk.BulkMacroScanInput;
import com.botronsoft.pso.confluence.cmc.rest.model.bulk.BulkMacroUpdateInput;
import com.botronsoft.pso.confluence.cmc.utils.MacroController;
import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
import com.botronsoft.pso.migrations.links.MappingConverter;
import com.botronsoft.pso.urlreplacer.context.ObjectTypeKeysJira;

import groovy.json.JsonOutput;


public class UpdateMacros {

	/**
	 * Updates macro in target instance
	 * 
	 * @param env         confluence "source" instance
	 * @param macroName   list of macros to be updated
	 * @param mappingFile path to file containing map of old and new values
	 * @param controller  controls which parameter that should be updated
	 * 
	 */
	public void updateMacros(ConfluenceEnvironment env, List<String> spaces, List<String> macroName, String mappingFile,
			MacroController controller, Map<String, String> appLinkHost, String mainMappingFile,
			String baseMappingDir) {
		BulkMacroUpdateInput input = new BulkMacroUpdateInput();
		input.setMacroName(macroName);
		input.setMappingFile(mappingFile);
		input.setSpaceKeys(spaces);
		input.setController(controller);
		input.setAppLinkHost(appLinkHost);

		Environments.confluence(Env.TARGET).getCMCClient().bulkUpdatePageIdsInMacros(input, mainMappingFile, baseMappingDir);

	}

	/**
	 * Updates server and serverId parameters for jira and jirachart macro in target
	 * instance
	 * 
	 * @param env confluence "source" instance
	 * 
	 */
	void updateAppLink(ConfluenceEnvironment env, List<String> spaces) {
		List<String> macros = asList("jira", "jirachart");
		Env.TARGET_CONF_ENV.getFileManagerClient().upload(
				Env.TARGET_CONF_ENV.getProperty("import") + "/mapping",
				new File(env.getKey(), ObjectTypeKeys.APPLICATION_LINK + ".csv").getAbsoluteFile());
		String mappingFile = Env.TARGET_CONF_ENV.getProperty("import") + "/mapping";
		updateMacros(env, spaces, macros, mappingFile, MacroController.SERVER, null, "", "");
	}

	/**
	 * Updates customfield parameters for jira and jirachart macro in target
	 * instance
	 * 
	 * @param env confluence "source" instance
	 * 
	 */
	void updateCustomFields(ConfluenceEnvironment env, List<String> spaces) {
		List<String> macros = asList("jira", "jirachart");
		Env.TARGET_CONF_ENV.getFileManagerClient().upload(
				Env.TARGET_CONF_ENV.getProperty("import") + "/mapping",
				new File(env.getKey(), ObjectTypeKeys.JIRA_SERVER + ".csv").getAbsoluteFile());
		String mappingFile = Env.TARGET_CONF_ENV.getProperty("import") + "/mapping";
		Map<String,String> appHost = new HashMap();
		appHost.put("0f08accd-2ea8-370c-8bb5-3a0601c372f6","https://jira.botronsoft.com");
		//appHost.put("2ea480ae-39e9-398a-89b2-bc50244ec928", "http://pso005test.botronlabs.com");
		updateMacros(env, spaces, macros, mappingFile, MacroController.CF, appHost, "", "");
	}

	/**
	 * Updates jql and jqlQuery parameters for jira and jirachart macro in target
	 * instance
	 * 
	 * @param env confluence "source" instance
	 * 
	 */
	void updateJQLs(ConfluenceEnvironment env, List<String> spaces) {
		List<String> macros = asList("jira", "jirachart");
		BulkMacroUpdateInput input = new BulkMacroUpdateInput();
		input.setMacroName(macros);
		input.setSpaceKeys(spaces);
		input.setController(MacroController.JQL);
		Map<String,String> appHost = new HashMap();
		appHost.put("0f08accd-2ea8-370c-8bb5-3a0601c372f6","https://jira.botronsoft.com");
	//	appHost.put("2ea480ae-39e9-398a-89b2-bc50244ec928", "http://pso005test.botronlabs.com");
		input.setAppLinkHost(appHost);
//		Environments.confluence(Env.TARGET).getCMCClient().bulkUpdatePageIdsInMacros(input, "C:\\Users\\User\\git\\psoeng-template\\confluence\\src\\main\\resources/mappingFile.csv",
//				"C:\\Users\\User\\git\\psoeng-template\\confluence\\src\\main\\resources\\gen", appHost);
		String mappingFolder = Env.TARGET_CONF_ENV.getProperty("import") + "/mapping";
		System.out.println(mappingFolder);
		Environments.confluence(Env.TARGET).getCMCClient().bulkUpdatePageIdsInMacros(input, 
				mappingFolder + "/mappingFile.csv",
				Env.TARGET_CONF_ENV.getProperty("import"));
				
		
	}

	/**
	 * Updates jira filter or project id parameters for gadget macro in target
	 * instance
	 * 
	 * @param env confluence "source" instance
	 * 
	 */
	void updateGadget(ConfluenceEnvironment env, List<String> spaces) {
		List<String> macros = asList("gadget");
		BulkMacroUpdateInput input = new BulkMacroUpdateInput();
		input.setMacroName(macros);
		input.setSpaceKeys(spaces);
		input.setController(MacroController.JIRAFILTERPROJECT);
		Map<String,String> appHost = new HashMap();
		appHost.put("0f08accd-2ea8-370c-8bb5-3a0601c372f6","https://jira.botronsoft.com");
		//appHost.put("2ea480ae-39e9-398a-89b2-bc50244ec928", "http://pso005test.botronlabs.com");
		input.setAppLinkHost(appHost);
		String mappingFolder = Env.TARGET_CONF_ENV.getProperty("import") + "/mapping";
		input.setMappingFile(mappingFolder);
		Env.TARGET_CONF_ENV.getFileManagerClient().upload(
				Env.TARGET_CONF_ENV.getProperty("import") + "/mapping",
				new File(env.getKey(), ObjectTypeKeys.JIRA_SERVER + ".csv").getAbsoluteFile());
		
		Environments.confluence(Env.TARGET).getCMCClient().bulkUpdatePageIdsInMacros(input, 
				"","");
				
		
	}
	
	/**
	 * Updates templateid parameters for create-from-template macro in target
	 * instance
	 * 
	 * @param env confluence "source" instance
	 * 
	 */
	void updateTemplateIds(ConfluenceEnvironment env, List<String> spaces) {
		List<String> macros = asList("create-from-template");
		String mappingFile = Env.TARGET_CONF_ENV.getProperty("import") + "/mapping";
		updateMacros(env, spaces, macros, mappingFile, MacroController.TEMPLATE, null, "", "");
	}

	
	/**
	 * Updates space key and name parameters for gadget macro in target
	 * instance
	 * 
	 * @param env confluence "source" instance
	 * 
	 */
	void updateSpace(ConfluenceEnvironment env, List<String> spaces) {
		List<String> macros = asList("gadget");
		Env.TARGET_CONF_ENV.getFileManagerClient().upload(
				Env.TARGET_CONF_ENV.getProperty("import") + "/mapping",
				new File(env.getKey(), ObjectTypeKeys.SPACE_NAME + ".csv").getAbsoluteFile());
		Env.TARGET_CONF_ENV.getFileManagerClient().upload(
				Env.TARGET_CONF_ENV.getProperty("import") + "/mapping",
				new File(env.getKey(), ObjectTypeKeys.SPACE_KEY + ".csv").getAbsoluteFile());
		String mappingFile = Env.TARGET_CONF_ENV.getProperty("import") + "/mapping";
		updateMacros(env, spaces, macros, mappingFile, MacroController.SPACEKEY, null, "", "");
	}
	
	/**
	 * Updates page ids parameters for jira, jirachart and gadget macro in target
	 * instance
	 * 
	 * @param env confluence "source" instance
	 * 
	 */
	void updatePageIds(ConfluenceEnvironment env, List<String> spaces) {
		List<String> macros = asList("jira", "jirachart", "gadget");
		Env.TARGET_CONF_ENV.getFileManagerClient().upload(
				Env.TARGET_CONF_ENV.getProperty("import") + "/mapping",
				new File(env.getKey(), ObjectTypeKeys.PAGE_ID + ".csv").getAbsoluteFile());
		String mappingFile = Env.TARGET_CONF_ENV.getProperty("import") + "/mapping";
		updateMacros(env, spaces, macros, mappingFile, MacroController.PAGE, null, "", "");
	}
	
	/**
	 * Scans target instance content for macro usage
	 * 
	 * @param env         confluence "source" instance
	 * @param macroName   list of macros to be searched for
	 * @param controller  controls which parameter values should be extracted
	 * @param resultWrite path to file in which result should be written 
	 * 
	 */	
	private void scanMacros(ConfluenceEnvironment env, List<String> spaces, String macroName, List<String> controllers) {
		
		BulkMacroScanInput input = new BulkMacroScanInput();
		input.setMacroName(macroName);
		input.setParameters(controllers);
		input.setSpaceKeys(spaces);
		 Environments
				.confluence(Env.TARGET).getCMCClient()
				.bulkScanMacros(input);
			
	}

	/**
	 * Scans target instance content for jira macro usage and extracts jql and jqlQuery parameters values 
	 * 
	 * @param env confluence "source" instance
	 * 
	 */	
	private void scanForJiraMacro(ConfluenceEnvironment env, List<String> spaces) {
		List<String> params = asList("jql", "jqlQuery");
		scanMacros(env, spaces, "jira", params);
	}

	/**
	 * Scans target instance content for jirachart macro usage and extracts jql and jqlQuery parameters values 
	 * 
	 * @param env confluence "source" instance
	 * 
	 */	
	private void scanForJiraChartMacro(ConfluenceEnvironment env, List<String> spaces) {
		List<String> params = asList("jql", "jqlQuery");
		scanMacros(env, spaces, "jirachart", params);
	}

	/**
	 * Scans target instance content for cqlsearch macro usage and extracts cql parameters values 
	 * 
	 * @param env confluence "source" instance
	 * 
	 */	
	private void scanForCQLMacro(ConfluenceEnvironment env, List<String> spaces) {
		List<String> params = asList("cql");
		scanMacros(env, spaces, "cqlsearch", params);
	}

	public static void main(String[] args) {
		UpdateMacros updateMacro = new UpdateMacros();
		
		String mappingFolder = Env.TARGET_CONF_ENV.getProperty("import") + "/mapping";
        ConfluenceEnvironment targetEnv = Environments.confluence(Env.TARGET);
     
        targetEnv.getFileManagerClient().upload(mappingFolder, new File(ClassLoader.getSystemResource("mappingFile.csv").getFile()));
//1.
//		Environments.confluence(Env.SOURCE_INSTANCES).stream().forEach(env -> {
//
//			MappingFactory factory = new FileSystemMappingFactory(env.getKey());
//			MapperReader reader = factory.createReader();
//			List<String> spaces = new ArrayList<String>(reader.map(ObjectTypeKeys.MIGRATED_SPACE_KEY).keySet());
//			updateMacro.scanForJiraMacro(env, spaces);
//			updateMacro.scanForJiraChartMacro(env, spaces);
//			updateMacro.scanForCQLMacro(env, spaces);
//		});

//2.
		Environments.confluence(Env.SOURCE_INSTANCES).stream().forEach(env -> {
			MappingFactory factory = new FileSystemMappingFactory(env.getKey());
			MapperReader reader = factory.createReader();
			List<String> spaces = new ArrayList<String>(reader.map(ObjectTypeKeys.MIGRATED_SPACE_KEY).keySet());
		
	    	updateMacro.updateTemplateIds(env, spaces);
			updateMacro.updateJQLs(env, spaces);
//			updateMacro.updateCustomFields(env, spaces);
//			updateMacro.updateGadget(env, spaces);
//			updateMacro.updatePageIds(env, spaces);
//			updateMacro.updateSpace(env, spaces);
//			//It is important to be executed last, otherwise we are going to lose source serverId
//			updateMacro.updateAppLink(env, spaces);
		});
	}
}
