package com.botronsoft.pso.migrations.confluence.samples;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.confluence.cmc.rest.client.CMCRestClient;
import com.botronsoft.pso.confluence.cmc.rest.model.BrowseShortcutBean;
import com.botronsoft.pso.confluence.cmc.rest.model.TemplateBean;
import com.botronsoft.pso.confluence.cmc.rest.model.WhitelistRuleBean;
import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;

public class GlobalSettingsMigration {

	/**
	 * Migrates user global templates from each source environment into target
	 * 
	 * @param excludedTemplates template names that will be excluded from migration
	 * 
	 */

	public void migrateGlobalTemplates(List<String> excludedTemplates) {
		CMCRestClient targetClient = Env.TARGET_CONF_ENV.getCMCClient();
		Environments.confluence(Env.SOURCE_INSTANCES).parallelStream().forEach(env -> {
			CMCRestClient client = env.getCMCClient();
			List<TemplateBean> exportedTemplates = client.getAllGlobalTemplates().stream()
					.filter(gt -> gt.getPluginkey() == null && !excludedTemplates.contains(gt.getName()))
					.map(temp-> transformTemplate(temp, env)).collect(Collectors.toList());
			targetClient.importGlobalTemplates(exportedTemplates);
		});
	}

	/**
	 * Transform user global templates creator and modifier
	 * 
	 * @param template TemplateBean which creator and modifier will be transformed
	 * @param env source environment
	 * 
	 */
	public TemplateBean transformTemplate(TemplateBean template, ConfluenceEnvironment env) {
		MappingFactory factory = new FileSystemMappingFactory(env.getKey());
		MapperReader reader = factory.createReader();
		Optional<String> newUserName = reader.map(ObjectTypeKeys.USER_NAME, template.getCreator());
		template.setCreator(newUserName.map(username -> Optional.of(username).get())
				.orElse(Optional.of(template.getCreator()).get()));
		newUserName = reader.map(ObjectTypeKeys.USER_NAME, template.getModifier());
		template.setModifier(newUserName.map(username -> Optional.of(username).get())
				.orElse(Optional.of(template.getModifier()).get()));
		return template;
	}

	/**
	 * Migrates browse shortcuts from each source environment into target
	 * 
	 * @param excludedShortcuts shortcut names that will be excluded from migration
	 * 
	 */
	public void migrateBrowseShortcuts(List<String> excludedShortcuts) {
		CMCRestClient targetClient = Env.TARGET_CONF_ENV.getCMCClient();
		Environments.confluence(Env.SOURCE_INSTANCES).parallelStream().forEach(env -> {
			CMCRestClient client = env.getCMCClient();
			List<BrowseShortcutBean> exportedShortcuts = client.getAllBrowseShortcuts().stream()
					.filter(gt->!excludedShortcuts.contains(gt.getName()))
					.collect(Collectors.toList());
			targetClient.importBrowseShortcuts(exportedShortcuts);
		});
	}
	
	/**
	 * Migrates whitelists from each source environment into target
	 * 
	 * @param excludedWhitelist whitelist expressions that will be excluded from migration
	 * 
	 */
	public void migrateWhitelist(List<String> excludedWhitelist) {
		CMCRestClient targetClient = Env.TARGET_CONF_ENV.getCMCClient();
		Environments.confluence(Env.SOURCE_INSTANCES).parallelStream().forEach(env -> {
			CMCRestClient client = env.getCMCClient();
			List<WhitelistRuleBean> exportedWhiteList = client.getAllWhitelist().stream()
					.filter(gt->!excludedWhitelist.contains(gt.getExpression()))
					.collect(Collectors.toList());
			targetClient.importWhitelistRules(exportedWhiteList);
		});
	}
	

	/**
	 * Migrates external gadget specifications from each source environment into target
	 * 
	 * @param excludedGadgetSpecs external gadget specification uris that will be excluded from migration
	 * 
	 */
	public void migrateExternalGadgetSpecs(List<String> excludedGadgetSpecs) {
		CMCRestClient targetClient = Env.TARGET_CONF_ENV.getCMCClient();
		Environments.confluence(Env.SOURCE_INSTANCES).parallelStream().forEach(env -> {
			CMCRestClient client = env.getCMCClient();
			List<String> exportedGadgetSpecs = client.getAllGadgetSpecs().stream()
					.filter(gt->!excludedGadgetSpecs.contains(gt))
					.collect(Collectors.toList());
			targetClient.importGadgetSpecs(exportedGadgetSpecs);
		});
	}
	
	/**
	 * Migrates external gadget feeds from each source environment into target
	 * 
	 * @param excludedGadgetSpecs external gadget feed uris that will be excluded from migration
	 * 
	 */
	public void migrateExternalGadgetFeeds(List<String> excludedGadgetFeed) {
		CMCRestClient targetClient = Env.TARGET_CONF_ENV.getCMCClient();
		Environments.confluence(Env.SOURCE_INSTANCES).parallelStream().forEach(env -> {
			CMCRestClient client = env.getCMCClient();
			List<String> exportedGadgetFeeds = client.getAllGadgetFeeds().stream()
					.filter(gt->!excludedGadgetFeed.contains(gt))
					.collect(Collectors.toList());
			targetClient.importGadgetFeeds(exportedGadgetFeeds);
		});
	}
	
	public static void main(String[] args) {
		GlobalSettingsMigration gsm = new GlobalSettingsMigration();
		List<String> excludedTemplates = Arrays.asList("");
		gsm.migrateGlobalTemplates(excludedTemplates);
		
		List<String> excludedShortcuts = Arrays.asList("");
		gsm.migrateBrowseShortcuts(excludedShortcuts);
		
		List<String> excludedWhiteList = Arrays.asList("");
		gsm.migrateWhitelist(excludedWhiteList);
		
		List<String> excludedSpecs = Arrays.asList("");
		gsm.migrateExternalGadgetSpecs(excludedSpecs);
		
		List<String> excludedFeeds = Arrays.asList("");
		gsm.migrateExternalGadgetFeeds(excludedFeeds);
	}
}
