package com.botronsoft.pso.migrations.confluence.samples;

import java.io.File;
import java.util.Optional;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.confluence.cmc.teamcal.model.ModelStore;
import com.botronsoft.pso.confluence.cmc.teamcal.model.teamcalendars.CalendarRoot;
import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;

/**
 * Exports, transforms and imports Team Calendras
 * 
 */
public class TeamCalendarsTransform {

	/**
	 * Exports Team Calendars
	 * 
	 * @param env confluence "source" instance
	 * 
	 */
	public static void exportCalendars(ConfluenceEnvironment env) {
		String outfile = env.getProperty("export") + "/teamcalendars/teamcalendars_export.xml";
		System.out.println("Exporting all calendars from " + env.getName() + " to" + outfile);
		env.getCMCCalendarsClient().exportCalendars(outfile);
		env.getFileManagerClient().downloadFile(outfile,
				Environments.local().workfile("teamcalendars_export.xml"));
	}

	/**
	 * Imports Team Calendars
	 * 
	 * @param env confluence "target" instance
	 */
	public static void importCalendars(ConfluenceEnvironment env) {
		String outfile = env.getProperty("import") + "/teamcalendars";
		env.getFileManagerClient().upload(outfile, Environments.local().workfile("teamcalendars_import.xml"));
		env.getCMCCalendarsClient().importCalendars(outfile + "/teamcalendars_import.xml");
	}

	/**
	 * Transforms Team Calendars
	 * 
	 * @param env confluence "source" instance
	 */
	public static void transformCalendars(ConfluenceEnvironment env) {
		/**
		 * Transform calendars
		 */
		File inFile = Environments.local().workfile("teamcalendars_export.xml");
		CalendarRoot root = ModelStore.load(inFile);

		MappingFactory factory = new FileSystemMappingFactory(env.getKey());
		MapperReader reader = factory.createReader();

		root.getCalendars().forEach(cal -> {
			// Update calendar space keys
			if (cal.getSpaceKey() != null) {
				Optional<String> newKey = reader.map(ObjectTypeKeys.SPACE_KEY, cal.getSpaceKey());
				if (newKey.isPresent() && !newKey.equals(cal.getSpaceKey())) {
					System.out.println(cal.getId() + ":" + cal.getSpaceKey() + " -> " + newKey);
					cal.setSpaceKey(newKey.get());
				}
			} else {
				System.out.println("Space is not migrated " + cal.getSpaceKey() + ", calendar:" + cal.getId());
				cal.setSpaceKey(null);
			}
			// jira appLink and project update
			if (cal.getExtraProperties() != null) {
				cal.getExtraProperties().stream().filter(it -> it.getKey().equals("applicationId")).forEach(appLink -> {
					Optional<String> newAppLink = reader.map(ObjectTypeKeys.APPLICATION_LINK, appLink.getValue());
					if (newAppLink.isPresent() && !(newAppLink.equals(appLink.getValue()))) {
						System.out.println(cal.getId() + ":" + appLink.getValue() + " -> " + newAppLink);
						appLink.setValue(newAppLink.get());
					}
				});
				cal.getExtraProperties().stream().filter(it -> it.getKey().equals("searchFilterId"))
						.forEach(searchFilterId -> {
							// TODO change JIRA mapping
							Optional<String> newFilterId = reader.map(ObjectTypeKeys.FILTERID,
									searchFilterId.getValue().toString());
							if (newFilterId.isPresent() && !newFilterId.equals(searchFilterId.getValue().toString())) {
								System.out
										.println(cal.getId() + ":" + searchFilterId.getValue() + " -> " + newFilterId);
								searchFilterId.setValue(newFilterId.get());
							}
						});
				cal.getExtraProperties().stream().filter(it -> it.getKey().equals("customfield_")).forEach(cf -> {
					String id = cf.getValue().replace("customfield_", "");
					Optional<String> newCFId = reader.map(ObjectTypeKeys.CUSTOMFIELD, id);
					if (newCFId.isPresent() && !newCFId.equals(id)) {
						System.out.println(cal.getId() + ":" + cf.getValue() + " -> " + newCFId);
						cf.setValue("customfield_" + newCFId.get());
					}
				});
			}
		});

		root.getUsers().forEach(user -> {
			// Update users
			if (user.getName() != null) {
				Optional<String> newName = reader.map(ObjectTypeKeys.USER_NAME, user.getName().toLowerCase());
				if (newName.isPresent() && !(newName.equals(user.getName().toLowerCase()))) {
					System.out.println("User:" + user.getName() + " -> " + newName);
					user.setName(newName.get());
				}
			}
			if (user.getKey() != null) {
				Optional<String> newKey = reader.map(ObjectTypeKeys.USER_KEY, user.getKey());
				if (newKey.isPresent() && !newKey.equals(user.getKey())) {
					System.out.println("User:" + user.getKey() + " -> " + newKey);
					user.setKey(newKey.get());
				}
			}
		});
		File outFile = new File(inFile.getParentFile(), "teamcalendars_import.xml");
		System.out.println("Saving transformed model to " + outFile);
		ModelStore.store(root, outFile.getAbsolutePath());
	}

	public static void main(String[] args) {

		Environments.confluence(Env.SOURCE_INSTANCES).parallelStream().forEach(env -> {
			TeamCalendarsTransform.exportCalendars(env);
		});

		Environments.confluence(Env.SOURCE_INSTANCES).parallelStream().forEach(env -> {
			TeamCalendarsTransform.transformCalendars(env);
		});

		TeamCalendarsTransform.importCalendars(Environments.confluence(Env.TARGET));
	}
}