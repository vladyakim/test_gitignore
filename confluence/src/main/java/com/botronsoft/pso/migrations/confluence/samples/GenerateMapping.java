package com.botronsoft.pso.migrations.confluence.samples;

import com.botronsoft.pso.commons.mapping.api.MapperWriter;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;


public class GenerateMapping {
	
	MappingFactory factory;
	MapperWriter writer;
	
	public GenerateMapping(ConfluenceEnvironment env) {
		this.factory = new FileSystemMappingFactory(env.getKey());
		this.writer =  factory.createWriter();
	}

	private void writeSpaceKeys() {
		writer.writeMap(ObjectTypeKeys.SPACE_KEY, "ds", "dssource");
	}
	
	private void writeSpaceNames() {
		writer.writeMap(ObjectTypeKeys.SPACE_NAME, "Demonstration Space", "Source Demonstration Space");
	}

	private void writeUserNames() {
		writer.writeMap(ObjectTypeKeys.USER_NAME, "vera.paskaleva@botronsoft.com", "admin");
	}
	
	private void writeMigratedSpaces() {
		writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"BPSO", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"CMJ", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"CMJ11", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"CMJCS", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"CMJKB", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"CMJS", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"CYCJ", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"DUMMYIC", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"ICJ", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"ILV", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"IMC", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"MAR", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"PA", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"PCT", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"PE", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"PSOMM", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"WCMFJ", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"alldoc", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"ds", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"~648367378", null);
				writer.writeMap(ObjectTypeKeys.MIGRATED_SPACE_KEY,"~bor.georgiev", null);
	}
	
	private void writeApplicationLinks() {
		writer.writeMap(ObjectTypeKeys.APPLICATION_LINK, "0f08accd-2ea8-370c-8bb5-3a0601c372f6", "2ea480ae-39e9-398a-89b2-bc50244ec928");
		writer.writeMap(ObjectTypeKeys.APPLICATION_LINK, "Botron Software Solutions", "JIRA");
		
	}
	
	private void writeJiraServer() {
		writer.writeMap(ObjectTypeKeys.JIRA_SERVER, "0f08accd-2ea8-370c-8bb5-3a0601c372f6", "/var/atlassian/application-data/confluence/cmc/import/mapping");
	}
	
	public static void main(String args[]) {
		Environments.confluence(Env.SOURCE_INSTANCES).parallelStream().forEach(env -> {
		GenerateMapping mapping = new GenerateMapping(env);
		mapping.writeMigratedSpaces();
		mapping.writeSpaceKeys();
		mapping.writeSpaceNames();
		mapping.writeUserNames();
		mapping.writeApplicationLinks();
		mapping.writeJiraServer();
		});
	}
}
