package com.botronsoft.pso.migrations.confluence.samples;

import static java.util.Arrays.asList;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.confluence.cmc.rest.model.bulk.BulkMacroScanInput;
import com.botronsoft.pso.confluence.cmc.utils.MacroController;
import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
import com.google.gson.Gson;

public class OpenAPIMacroHandler extends UpdateMacros{


	private void scanForOpenAPIMacroInTarget(ConfluenceEnvironment env, MapperReader reader) {
		List<String> params = asList("plain-text-body");
		List<String> lines = new ArrayList<String>();
		BulkMacroScanInput input = new BulkMacroScanInput();
		input.setMacroName("open-api");
		input.setParameters(params);
		input.setSpaceKeys(new ArrayList<String>(reader.map(ObjectTypeKeys.MIGRATED_SPACE_KEY).keySet()));
		Environments
				.confluence(Env.TARGET).getCMCClient()
				.bulkScanMacros(input);
		
		try {
			Files.write(Paths.get(Env.LOCAL_ENV.workfile("openAPI_macro_scan_target.txt").toURI()), lines, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
					StandardOpenOption.APPEND);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void scanForOpenAPIMacro(ConfluenceEnvironment env, MapperReader reader) {
		List<String> params = asList("plain-text-body");
		List<String> lines = new ArrayList<String>();
		BulkMacroScanInput input = new BulkMacroScanInput();
		input.setMacroName("open-api");
		input.setParameters(params);
		input.setSpaceKeys(new ArrayList<String>(reader.map(ObjectTypeKeys.MIGRATED_SPACE_KEY).keySet()));
		Environments
				.confluence(Env.TARGET).getCMCClient()
				.bulkScanMacros(input);
		try {
			Files.write(Paths.get(Env.LOCAL_ENV.workfile("openAPI_macro_scan"+ env + ".txt").toURI()), lines, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
					StandardOpenOption.APPEND);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

private void generateOpenAPiMapping(MapperReader reader) {
	scanForOpenAPIMacroInTarget(Environments.confluence(Env.TARGET), reader);
	List<String> targetResult = new ArrayList<String>();
	Map<String,OpenAPIInfo> targetOpenApi = new HashMap<String, OpenAPIMacroHandler.OpenAPIInfo>();
	try (Stream<String> lines = Files.lines(Paths.get(Env.LOCAL_ENV.workfile("openAPI_macro_scan_target.txt").toURI()))) {
		targetResult = lines.collect(Collectors.toList());
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	Gson g = new Gson();
	targetResult.forEach( t -> {
		OpenAPIInfo info = g.fromJson(t, OpenAPIInfo.class);
		targetOpenApi.put(info.getPageId()+info.getMacoId(), info);
	});
	
	
	Environments.confluence(Env.SOURCE_INSTANCES).stream().forEach(env -> {
	
	List<String> result = new ArrayList<String>();
	Map<String,OpenAPIInfo> sourceOpenApi = new HashMap<String, OpenAPIMacroHandler.OpenAPIInfo>();
	try (Stream<String> lines = Files.lines(Paths.get(Env.LOCAL_ENV.workfile("openAPI_macro_scan"+ env + ".txt").toURI()))) {
		result = lines.collect(Collectors.toList());
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	result.forEach( t -> {
		OpenAPIInfo info = g.fromJson(t, OpenAPIInfo.class);
		sourceOpenApi.put(reader.map(ObjectTypeKeys.PAGE_ID, info.getPageId()).get()+info.getMacoId(), info);
	});
	});
}
	/**
	 * Updates open-api-body parameter for OPENAPI macro in target
	 * instance
	 * 
	 * @param env confluence "source" instance
	 * 
	 */
	void updateOpenApiMacros(ConfluenceEnvironment env, List<String> spaces) {
		List<String> macros = asList("open-api");
		String mappingFile = "";
		updateMacros(env,spaces, macros, mappingFile,MacroController.OPENAPI, null, "", "");
	}
	
	class OpenAPIInfo {

		private String pageId;
		public String getPageId() {
			return pageId;
		}
		public void setPageId(String pageId) {
			this.pageId = pageId;
		}
		public String getMacoId() {
			return macoId;
		}
		public void setMacoId(String macoId) {
			this.macoId = macoId;
		}
		public String getBody() {
			return body;
		}
		public void setBody(String body) {
			this.body = body;
		}
		private String macoId;
		private String body;

	}


public static void main(String[] args) {
	OpenAPIMacroHandler updateMacro = new OpenAPIMacroHandler();
Environments.confluence(Env.SOURCE_INSTANCES).stream().forEach(env -> {
	MappingFactory factory = new FileSystemMappingFactory(env.getKey());
	MapperReader reader = factory.createReader();
	List<String> spaces = new ArrayList<String>(reader.map(ObjectTypeKeys.MIGRATED_SPACE_KEY).keySet());
	updateMacro.scanForOpenAPIMacro(env, reader);
	updateMacro.scanForOpenAPIMacroInTarget(env, reader);
	updateMacro.generateOpenAPiMapping(reader);
	updateMacro.updateOpenApiMacros(env, spaces);
});

}

}

 
