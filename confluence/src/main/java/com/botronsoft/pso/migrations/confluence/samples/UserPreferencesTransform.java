package com.botronsoft.pso.migrations.confluence.samples;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.confluence.cmc.usersettings.model.UserBean;
import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class UserPreferencesTransform {

	AtomicInteger counter = new AtomicInteger(1);

	private String replaceSpecialChars(String input) {
		String newTitle = input;
		if (input != null) {
			newTitle = input.replace("\u2019", "'").replace("–", "-").replace("\u2013", "-").replace("\u00a0", " ");
		}
		return newTitle;
	}

	/**
	 * Copy preferences for LICENSED users ONLY
	 * 
	 * @param env confluence "source" instance
	 * 
	 */
	public static String export(ConfluenceEnvironment env, String outFile) {
		return env.getCMCRestUserSettingsClient().exportAllLicensedUsersToFile(outFile);
	}

	public void transform(ConfluenceEnvironment env) throws IOException {
		
		File inFile = Environments.local().workfile("export_usersettings.txt");
		File outFile = Environments.local().workfile("import_usersettings.txt");
		FileOutputStream fos = new FileOutputStream(outFile);
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

		MappingFactory factory = new FileSystemMappingFactory(env.getKey());
		MapperReader reader = factory.createReader();
		
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();

		try (Stream<String> lines = Files.lines(inFile.toPath())) {
			lines.forEach(line -> {
				UserBean bean = new GsonBuilder().create().fromJson(line, new TypeToken<UserBean>() {
				}.getType());
				
				if (reader.map(ObjectTypeKeys.USER_NAME, bean.getUserName().toLowerCase()).isPresent()) {
					bean.setUserName(reader.map(ObjectTypeKeys.USER_NAME, bean.getUserName().toLowerCase()).get());
				}

				bean.getUserFollowings().getData().forEach(following -> {
					if (reader.map(ObjectTypeKeys.USER_NAME, following.getUsername().toLowerCase()).isPresent()) {
						following.setUsername(reader.map(ObjectTypeKeys.USER_NAME, following.getUsername().toLowerCase()).get());
					}
				});

				// Replace special chars in titles as this might lead to failure in finding the
				// page/blog in target
				bean.getFavorites().getData().forEach(fav -> {
					if (fav.getTitle() != null) {
						fav.setTitle(replaceSpecialChars(fav.getTitle()));

						if (fav.getSpaceKey() != null && reader.map(ObjectTypeKeys.SPACE_NAME,fav.getSpaceKey()).isPresent()) {
							fav.setSpaceKey(
									reader.map(ObjectTypeKeys.SPACE_NAME,fav.getSpaceKey()).get());
						}
					}
				});
				bean.getWatches().getData().forEach(w -> {
					if (w.getTitle() != null) {
						if (w.getTitle().contains("Unpair a meter in the Android settings (Mobile 3.0.x ")) {
							System.out.println(w.getTitle());
							if (w.getTitle().contains("–")) {
								System.out.println(replaceSpecialChars(w.getTitle()));
							}
						}
						w.setTitle(replaceSpecialChars(w.getTitle()));
						// w.setTitle(replaceSpecialChars(w.getTitle()));
						if (w.getSpaceKey() != null && reader.map(ObjectTypeKeys.SPACE_KEY, w.getSpaceKey()).isPresent()) {
							w.setSpaceKey(reader.map(ObjectTypeKeys.SPACE_KEY, w.getSpaceKey()).get());
						}
					}
				});

				try {
					bw.write(gson.toJson(bean));
					bw.newLine();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		} catch (IOException e) {

			e.printStackTrace();
		}
		bw.close();
	}

	/**
	 * Import preferences for LICENSED users ONLY
	 * 
	 * @param env confluence "target" instance
	 * 
	 */
	public static String importPreferences(ConfluenceEnvironment env, String inFile) {
		env.getFileManagerClient().upload(inFile,
				Environments.local().workfile("import_usersettings.txt"));
		return env.getCMCRestUserSettingsClient().importSettingsFromFile(inFile + "/import_usersettings.txt");
	}

	public static void main(String[] args) {

		Environments.confluence(Env.SOURCE_INSTANCES).parallelStream().forEach(env -> {
			String outfile = env.getProperty("export") + "/userpreferences/export_usersettings.txt";
			UserPreferencesTransform.export(env, outfile);
			env.getFileManagerClient().downloadFile(outfile,
					Environments.local().workfile("export_usersettings.txt"));
		});

		Environments.confluence(Env.SOURCE_INSTANCES).parallelStream().forEach(env -> {
			try {
				new UserPreferencesTransform().transform(env);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

		UserPreferencesTransform.importPreferences(Environments.confluence(Env.TARGET),
				Environments.confluence(Env.TARGET).getProperty("import") + "/userpreferences");
	}
}