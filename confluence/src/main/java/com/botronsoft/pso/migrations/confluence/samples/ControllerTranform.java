package com.botronsoft.pso.migrations.confluence.samples;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MapperWriter;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.confluence.cmc.rest.model.SpaceInfo;
import com.botronsoft.pso.confluence.cmc.rest.model.SpaceKeyMapping;
import com.botronsoft.pso.confluence.cmc.rest.model.TransformerInputData;
import com.botronsoft.pso.confluence.cmc.rest.model.UserMappingBean;
import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class ControllerTranform {

	private static final Logger logger = LoggerFactory.getLogger(ControllerTranform.class);

	private MapperReader mappingReader;
	private MappingFactory mappingFactory;
	private ConfluenceEnvironment env;
	private MapperWriter mappingWriter;
	

	public ControllerTranform(ConfluenceEnvironment env) {
			mappingFactory = new FileSystemMappingFactory(env.getKey());
			mappingReader = mappingFactory.createReader();
			mappingWriter = mappingFactory.createWriter();
			this.env = env;
		}

	/**
	 * Applies mapping transformations on exported space files in "source" instance
	 * and copy space files to "target" instance
	 * 
	 * @param env          confluence "source" instance
	 * @param threads      number of threads used for running transformation
	 * @param skipExisting whether to skip already transformed spaces
	 * @throws IOException 
	 * 
	 */
	public void startTransform(int threads, boolean skipExisting) throws IOException {
			
		TransformerInputData input = new TransformerInputData();
		input.setInputDir(env.getProperty("export"));
		input.setOutputDir(env.getProperty("import"));

		input.setNumberOfThreads(threads);
		input.setSkipExisting(skipExisting);

		input.setSpaceKeysMapping(this.generateTransformerInput());
		input.setUsersMapping(mappingReader.map(ObjectTypeKeys.USER_KEY).entrySet()
				.stream().collect(Collectors.toMap(Map.Entry::getKey, 
	                      e -> e.getValue().get(0))));
		input.setUsersRenameMapping(mappingReader.map(ObjectTypeKeys.USER_NAME).entrySet()
				.stream().collect(Collectors.toMap(Map.Entry::getKey, 
	                      e -> e.getValue().get(0))));

		env.getCMCClient().transformServiceStart(input);
	}

	/**
	 * Dump list of all spaces with key information BEFORE any changes are made on
	 * the source instance spaces
	 * 
	 * @param env confluence instance
	 * 
	 */
	// TODO Probably it should be in another class
	public  List<SpaceInfo>  generateSpaceFiles() throws IOException {
		logger.info("Generating space file for " + env.getName());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		List<SpaceInfo> spaceInfoList = new ArrayList<SpaceInfo>(env.getCMCClient().getSpaceInformation());
		File file = Env.LOCAL_ENV.workfile(env.getSpacesFileName());
		logger.info("Writing " + spaceInfoList.size() + " records to " + file.getAbsolutePath() + "...");
		Files.write(file.toPath(), gson.toJson(spaceInfoList).getBytes());
		return spaceInfoList;
	}

	/**
	 * This method ensures all source users have a userKey in Target Confluence and
	 * produces a mapping file for each instance
	 * 
	 * @param env confluence instance
	 * 
	 */
	// TODO Probably it should be in another class
	public void generateUserKeyMap() throws IOException {
		List<UserMappingBean> keyMap = new ArrayList<UserMappingBean>();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		Map<Object, Object> inputUserMappingSource = env.getCMCClient().getAllUserKeys().entrySet().stream()
				.collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue()));

		inputUserMappingSource.forEach((key, value) -> {
			if(mappingReader.map(ObjectTypeKeys.USER_NAME, value.toString().toLowerCase()).isPresent()) {
				inputUserMappingSource.put(key, mappingReader.map(ObjectTypeKeys.USER_NAME, value.toString().toLowerCase()).get());
			} else {
				inputUserMappingSource.put(key,  value.toString().toLowerCase());
			}
		});
		logger.info("Generating user mapping for " + inputUserMappingSource);
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("userMapping", inputUserMappingSource);
		logger.info("Generating user mapping for " + m);
		keyMap = Environments.confluence(Env.TARGET).getCMCClient().mapUserKeys(gson.toJson(m));
		File outFile = Env.LOCAL_ENV.workfile("user_keymap_" + env.getKey() + ".txt");
		logger.info("Writing " + keyMap.size() + " records to ..." + outFile);
		Files.write(outFile.toPath(), gson.toJson(keyMap).getBytes());
	}

	/**
	 * Produces a mapping file for user keys for each instance
	 * 
	 * @param env confluence instance
	 * 
	 */
	// TODO Probably it should be in another class
	public void transformToUserKeyMapping() throws IOException {
		
		File inFile = Env.LOCAL_ENV.workfile("user_keymap_" + env.getKey() + ".txt");
		Collection<UserMappingBean> loaded = null;
		try (FileReader jsonReader = new FileReader(inFile)) {
			loaded = new GsonBuilder().create().fromJson(jsonReader, new TypeToken<Collection<UserMappingBean>>() {
			}.getType());
			loaded.forEach(l -> {
				mappingWriter.writeMap(ObjectTypeKeys.USER_KEY, l.getOldKey(), l.getNewKey());
			});
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}
		private Collection<SpaceKeyMapping> generateTransformerInput() throws IOException {

			List<SpaceKeyMapping> list = new ArrayList<SpaceKeyMapping>();
			List<SpaceInfo> sps = generateSpaceFiles();
					sps.forEach(p-> {
				SpaceKeyMapping info = new SpaceKeyMapping();
				info.setSpaceInfo(p);
				if(mappingReader.map(ObjectTypeKeys.SPACE_KEY, p.getKey()).isPresent()) {
				String newKey = mappingReader.map(ObjectTypeKeys.SPACE_KEY, p.getKey()).get();
				info.setNewKey(newKey);
				list.add(info);
				}
					});

			return Collections.unmodifiableList(list);
	}

	public static void main(String[] args) throws IOException {

		Collection<ConfluenceEnvironment> source_environments = Environments.confluence(Env.SOURCE_INSTANCES);
		Collection<ConfluenceEnvironment> all_environments = Environments.confluence(Env.SOURCE_INSTANCES);
		all_environments.add(Environments.confluence(Env.TARGET));
		// 1.
		source_environments.forEach(env -> {
			ControllerTranform controllerTranform = new ControllerTranform(env);
			try {
				controllerTranform.generateUserKeyMap();
				controllerTranform.transformToUserKeyMapping();
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

		// 2.
		all_environments.parallelStream().forEach(env -> {
			ControllerTranform controllerTranform = new ControllerTranform(env);
			try {
				controllerTranform.generateSpaceFiles();
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

		// 3.
		source_environments.parallelStream().forEach(env -> {
			ControllerTranform controllerTranform = new ControllerTranform(env);
				try {
					controllerTranform.startTransform(2, true);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		});

	}
}
