package com.botronsoft.pso.migrations.confluence.samples.ams;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.confluence.cmc.rest.client.CMCRestClient;
import com.botronsoft.pso.confluence.cmc.rest.model.BrowseShortcutBean;
import com.botronsoft.pso.confluence.cmc.rest.model.SpaceInfo;
import com.botronsoft.pso.confluence.cmc.rest.model.TemplateBean;
import com.botronsoft.pso.confluence.cmc.rest.model.WhitelistRuleBean;
import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.DefaultConfluenceEnvironment;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.rest.client.BitBucketClient;
import com.botronsoft.pso.rest.client.ConfluenceClient;
import com.botronsoft.pso.rest.client.UPMClient;
import com.botronsoft.pso.rest.client.model.AddonInfo;

public class ConfluenceAMSGenarator extends ConfluenceAbstractAMSGenerator {

	private static CMCRestClient sourceCMCClient = Env.SOURCE_CONF_ENV.getCMCClient();
	private static CMCRestClient targetCMCClient = Env.TARGET_CONF_ENV.getCMCClient();
	private static UPMClient sourceUPMClient = Env.SOURCE_CONF_ENV.getUpmClient();
	private static UPMClient targetUPMClient = Env.TARGET_CONF_ENV.getUpmClient();
	@Override
	protected String prefix(String name) {
		return null;
	}

	void generateSpaces() {

		Map<String, String> sourceSpaces = sourceCMCClient.getSpaceInformation().stream().filter(sp -> sp.getGlobal())
				.collect(toMap(SpaceInfo::getKey, SpaceInfo::getName));
		Map<String, String> targetSpaces = targetCMCClient.getSpaceInformation().stream().filter(sp -> sp.getGlobal())
				.collect(toMap(SpaceInfo::getKey, SpaceInfo::getName));

		Set<String> spaceWithConflictingKeys = sourceSpaces.keySet().stream().collect(toSet());
		spaceWithConflictingKeys.retainAll(targetSpaces.keySet());
		Set<String> spacesWIthConflictingNames = sourceSpaces.entrySet().stream().filter(entry -> {
			List<String> lowerProjectNames = targetSpaces.values().stream().map(e -> e.toLowerCase()).collect(toList());
			return lowerProjectNames.contains(entry.getValue().toLowerCase());
		}).map(e -> e.getKey()).collect(toSet());

		Set<String> additionsSpaceKeys = sourceSpaces.entrySet().stream()
				.filter(space -> !spaceWithConflictingKeys.contains(space.getKey())
						&& !spacesWIthConflictingNames.contains(space.getKey()))
				.map(p -> p.getKey()).collect(toSet());

		MARKUP_BUILDER.sectionTitleLevel1("Spaces");

		Map<String, String> newSpacess = sourceSpaces.entrySet().stream()
				.filter(entry -> additionsSpaceKeys.contains(entry.getKey()))
				.collect(toMap(Map.Entry::getKey, Map.Entry::getValue));

		generateAdditionsTable(newSpacess, columns("Space Key", "Space Name"));

		MARKUP_BUILDER.sectionTitleLevel2("Conflicting Space keys");

		generateMatchesKeysTable(
				sourceSpaces.entrySet().stream().filter(entry -> spaceWithConflictingKeys.contains(entry.getKey()))
						.collect(toMap(Map.Entry::getKey, Map.Entry::getValue)),
				columns("Source Space Key", "Source Space Name", "Conflicting Reason", "Conflict Resolution"),
				"Space Key", "N/A");

		MARKUP_BUILDER.sectionTitleLevel2("Conflicting Space names");

		generateMatchesNamesTable(
				sourceSpaces.entrySet().stream().filter(entry -> spacesWIthConflictingNames.contains(entry.getKey()))
						.collect(toMap(Map.Entry::getKey, Map.Entry::getValue)),
				columns("Source Space Key", "Source Space Name", "Conflicting Reason", "Conflict Resolution"),
				targetSpaces, "N/A");

	}

	void generatePersonalSpaces() {

		Map<String, String> sourceSpaces = sourceCMCClient.getSpaceInformation().stream().filter(sp -> !sp.getGlobal())
				.collect(toMap(SpaceInfo::getKey, SpaceInfo::getName));
		Map<String, String> targetSpaces = targetCMCClient.getSpaceInformation().stream().filter(sp -> !sp.getGlobal())
				.collect(toMap(SpaceInfo::getKey, SpaceInfo::getName));

		Set<String> spaceWithConflictingKeys = sourceSpaces.keySet().stream().collect(toSet());
		spaceWithConflictingKeys.retainAll(targetSpaces.keySet());
		
		Set<String> spacesWIthConflictingNames = sourceSpaces.values().stream().collect(toSet());
		spaceWithConflictingKeys.retainAll(targetSpaces.values());
		
		Set<String> additionsSpaceKeys = sourceSpaces.entrySet().stream()
				.filter(space -> !spaceWithConflictingKeys.contains(space.getKey())
						&& !spacesWIthConflictingNames.contains(space.getKey()))
				.map(p -> p.getKey()).collect(toSet());
		
		MARKUP_BUILDER.sectionTitleLevel1("Personal Spaces");

		Map<String, String> newSpacess = sourceSpaces.entrySet().stream()
				.filter(entry -> additionsSpaceKeys.contains(entry.getKey()))
				.collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		generateAdditionsTable(newSpacess, columns("Space Key", "Space Name"));

		MARKUP_BUILDER.sectionTitleLevel2("Conflicting Personal Space keys");

		generateMatchesKeysTable(
				sourceSpaces.entrySet().stream().filter(entry -> spaceWithConflictingKeys.contains(entry.getKey()))
						.collect(toMap(Map.Entry::getKey, Map.Entry::getValue)),
				columns("Source Space Key", "Source Space Name", "Conflicting Reason", "Conflict Resolution"),
				"Space Key", "N/A");

	}

	void generateGlobalTemplates() {
		List<String> sourceGlobalTemplates = sourceCMCClient.getAllGlobalTemplates().stream().map(TemplateBean::getName)
				.collect(toList());
		List<String> targetGlobalTemplates = targetCMCClient.getAllGlobalTemplates().stream().map(TemplateBean::getName)
				.collect(toList());

		
		MARKUP_BUILDER.sectionTitleLevel1("Global Settings");
		
		MARKUP_BUILDER.sectionTitleLevel2("Global Templates and Blueprints");

		MARKUP_BUILDER.sectionTitleLevel3("Conflicting Global Page Templates");

		generateConflictsTable(
				new ArrayList<>(sourceGlobalTemplates.stream().filter(entry -> targetGlobalTemplates.contains(entry))
				.collect(Collectors.toSet())),
				columns("Source Global Template Name", "Conflict Resolution"),
				"N/A");
		
		
		MARKUP_BUILDER.sectionTitleLevel3("New Global Page Templates");
		
		Set<String> additionsGlobalTemplates = sourceGlobalTemplates.stream()
				.filter(gt -> !targetGlobalTemplates.contains(gt))
				.collect(toSet());

		generateAdditionsTable(new ArrayList<>(additionsGlobalTemplates), columns("Source global page template"));
	}

	void generateBrowseShortcuts() {
		Map<String,String> sourceShortCuts = sourceCMCClient.getAllBrowseShortcuts().stream()
				.collect(toMap(BrowseShortcutBean::getName, BrowseShortcutBean::getExpandedValue));
		Map<String,String> targetShortcuts = targetCMCClient.getAllBrowseShortcuts().stream()
				.collect(toMap(BrowseShortcutBean::getName, BrowseShortcutBean::getExpandedValue));

		Map<String, String> lowerSourceShortCuts = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
		lowerSourceShortCuts.putAll(sourceShortCuts);
		
		Map<String, String> lowerTargetShortcuts = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
		lowerTargetShortcuts.putAll(targetShortcuts);
		
		Set<String> shortcutsWIthConflictingNames = sourceShortCuts.entrySet().stream().filter(entry -> {
			return (lowerTargetShortcuts.containsKey(entry.getKey())) && 
					!(lowerTargetShortcuts.get(entry.getKey()).equals(entry.getValue()));
		}).map(e -> e.getKey()).collect(toSet());
		
		Set<String> shortcutsWIthConflictingValues = sourceShortCuts.entrySet().stream().filter(entry -> {
			return (lowerTargetShortcuts.containsValue(entry.getValue())) && 
					!(lowerTargetShortcuts.entrySet().parallelStream()
							.filter(p -> p.getValue().equals(entry.getValue())).findAny().get().getKey().equals(entry.getKey()));
		}).map(e -> e.getKey()).collect(toSet());

		Set<String> additionsShortcuts = sourceShortCuts.entrySet().stream()
				.filter(sh -> !shortcutsWIthConflictingNames.contains(sh.getKey())
						&& !shortcutsWIthConflictingValues.contains(sh.getKey()) 
						&& !targetShortcuts.containsKey(sh.getKey()))
				.map(p -> p.getKey()).collect(toSet());

		MARKUP_BUILDER.sectionTitleLevel2("Browse Shortcuts");

		Map<String, String> newShortcuts = sourceShortCuts.entrySet().stream()
				.filter(entry -> additionsShortcuts.contains(entry.getKey()))
				.collect(toMap(Map.Entry::getKey, Map.Entry::getValue));

		MARKUP_BUILDER.sectionTitleLevel3("New Browse Shortcuts");
		generateAdditionsTable(newShortcuts, columns("Key", "Expanded Value"));
		
		MARKUP_BUILDER.sectionTitleLevel3("Conflicting Browse Shortcut Keys");

		generateMatchesKeysTable(
				sourceShortCuts.entrySet().stream().filter(entry -> shortcutsWIthConflictingNames.contains(entry.getKey()))
						.collect(toMap(Map.Entry::getKey, Map.Entry::getValue)),
				columns("Source Key", "Source Expanded Value", "Conflicting Reason", "Conflict Resolution"),
				"Shortcut Key", "N/A");

		MARKUP_BUILDER.sectionTitleLevel3("Conflicting Browse Shortcut Expanded Values");

		generateMatchesKeysTable(
				sourceShortCuts.entrySet().stream().filter(entry -> shortcutsWIthConflictingValues.contains(entry.getKey()))
						.collect(toMap(Map.Entry::getKey, Map.Entry::getValue)),
				columns("Source Key", "Source Expanded Value", "Conflicting Reason", "Conflict Resolution"),
				"Expanded Value", "N/A");
	}
	
	void generateUserMacro() {
		List<String> sourceUserMacro = (List<String>) sourceCMCClient.getAllUserMacroNames();
		List<String> targetUserMacro = (List<String>) targetCMCClient.getAllUserMacroNames();

		Set<String> userMacrosWIthConflictingNames = sourceUserMacro.stream().filter(entry -> {
			List<String> lowermacroNames = targetUserMacro.stream().map(e -> e.toLowerCase()).collect(toList());
			return lowermacroNames.contains(entry.toLowerCase());
		}).collect(toSet());

		
		
		MARKUP_BUILDER.sectionTitleLevel2("User Macros");

		MARKUP_BUILDER.sectionTitleLevel3("Conflicting User Macros");
		
		generateConflictsTable(
				sourceUserMacro.stream().filter(entry -> userMacrosWIthConflictingNames.contains(entry))
				.collect(Collectors.toList()),
				columns("Source User Macro Names", "Conflict Resolution"),
				"N/A");
		
		MARKUP_BUILDER.sectionTitleLevel3("New User Macros");
		
		Set<String> additionsUserMacro = sourceUserMacro.stream()
				.filter(gt -> !userMacrosWIthConflictingNames.contains(gt))
				.collect(toSet());

		generateAdditionsTable(new ArrayList<>(additionsUserMacro), columns("Source user macros"));
	}
	
	void generateWhitelist() {
		Map<String,String> sourceWhitelist = sourceCMCClient.getAllWhitelist().stream().filter(p -> !p.getType().equals("APPLICATION_LINK"))
				.collect(toMap(WhitelistRuleBean::getExpression, WhitelistRuleBean::getType));
		Map<String,String> targetWhitelist= targetCMCClient.getAllWhitelist().stream()
				.collect(toMap(WhitelistRuleBean::getExpression, WhitelistRuleBean::getType));
	
		Set<String> shortcutsWIthConflictingExp = sourceWhitelist.entrySet().stream().filter(entry -> {
			List<String> lowerShortcutsNames = targetWhitelist.keySet().stream().map(e -> e.toLowerCase()).collect(toList());
			List<String> lowerValues = targetWhitelist.values().stream().map(e -> e.toLowerCase()).collect(toList());
			return (lowerShortcutsNames.contains(entry.getKey().toLowerCase())) && 
					!(lowerValues.contains(entry.getValue().toLowerCase()));
		}).map(e -> e.getKey()).collect(toSet());
		
		
		MARKUP_BUILDER.sectionTitleLevel2("Whitelist");
		
		
		MARKUP_BUILDER.sectionTitleLevel3("New Whitelist");
		
		Set<String> additionsWhitelist = sourceWhitelist.entrySet().stream()
				.filter(gt -> !shortcutsWIthConflictingExp.contains(gt.getKey()) 
						&& !targetWhitelist.keySet().contains(gt.getKey()))
				.map(p -> p.getKey()).collect(toSet());

		Map<String, String> newWhitelist = sourceWhitelist.entrySet().stream()
				.filter(entry -> additionsWhitelist.contains(entry.getKey()))
				.collect(toMap(Map.Entry::getKey, Map.Entry::getValue));

		generateAdditionsTable(newWhitelist, columns("Whitelist Expression", "Whitelist Type"));
		
		MARKUP_BUILDER.sectionTitleLevel3("Conflicting Whitelist");

		generateMatchesKeysTable(
				sourceWhitelist.entrySet().stream().filter(entry -> shortcutsWIthConflictingExp.contains(entry.getKey()))
						.collect(toMap(Map.Entry::getKey, Map.Entry::getValue)),
				columns("Source Whitelist Expression", "Source Whitelist Type", "Conflicting Reason", "Conflict Resolution"),
				"Whitelist Type", "N/A");
		
	}
	
	
	void generateConfluenceSettings() {

		Map<String, String> sourceSettings = sourceCMCClient.getAllConfluenceSettings();
		Map<String, String> targetSettings = targetCMCClient.getAllConfluenceSettings();


		MARKUP_BUILDER.sectionTitleLevel3("Conflicting Settings");

			
		Set<String> settingsWIthConflictingValues = sourceSettings.entrySet().stream().filter(entry -> {
			return !targetSettings.get(entry.getKey()).equals(entry.getValue());
		}).map(e -> e.getKey()).collect(toSet());
		
		
		Map<String, Map<String,String>> result = new HashMap<String, Map<String,String>>();
		Map<String,String> c = sourceSettings.entrySet().stream().filter(entry -> settingsWIthConflictingValues.contains(entry.getKey()))
		.collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
		c.forEach((k,v) -> {
			Map<String,String> m = new HashMap<String,String>();
			m.put(v, targetSettings.entrySet().stream().filter(p -> p.getKey().equals(k)).findAny().get().getValue());
			result.put(k, m);
		});
		
		
		generateConflictsTable(result,
				columns("Setting Name", "Source Setting Value", "Target Setting Value", "Conflict Resolution"),  "N/A");

	}
	
	void generateExternalGadgetFeeds() {

		List<String> sourceGadgetFeeds = sourceCMCClient.getAllGadgetFeeds().stream().map(f -> f.toString()).collect(toList());
		List<String> targetGadgetFeeds = targetCMCClient.getAllGadgetFeeds().stream().map(f -> f.toString()).collect(toList());

		Set<String> wIthConflictingNames = sourceGadgetFeeds.stream().filter(entry -> {
			List<String> lowermacroNames = targetGadgetFeeds.stream().map(e -> e.toLowerCase()).collect(toList());
			return lowermacroNames.contains(entry.toLowerCase());
		}).collect(toSet());

		
		
		MARKUP_BUILDER.sectionTitleLevel2("External Gadgets");
		
		MARKUP_BUILDER.sectionTitleLevel3("New External Gadgets Feeds");
		
		Set<String> additionsUserMacro = sourceGadgetFeeds.stream()
				.filter(gt -> !wIthConflictingNames.contains(gt))
				.collect(toSet());
		
		Map<String, String> newUserMAcro = sourceGadgetFeeds.stream()
				.filter(entry -> additionsUserMacro.contains(entry))
				.collect(Collectors.toMap(item -> item, item -> ""));
		
		generateAdditionsTable(newUserMAcro, columns("Source External Gadgets Feeds"));
	}
	
	void generateExternalGadgetSpecs() {

		List<String> sourceGadgetFeeds = sourceCMCClient.getAllGadgetSpecs().stream().map(f -> f.toString()).collect(toList());;
		List<String> targetGadgetFeeds = targetCMCClient.getAllGadgetSpecs().stream().map(f -> f.toString()).collect(toList());;

		Set<String> wIthConflictingNames = sourceGadgetFeeds.stream().filter(entry -> {
			List<String> lowermacroNames = targetGadgetFeeds.stream().map(e -> e.toLowerCase()).collect(toList());
			return lowermacroNames.contains(entry.toLowerCase());
		}).collect(toSet());
		
		MARKUP_BUILDER.sectionTitleLevel3("New External Gadgets");
		
		Set<String> additionsExt = sourceGadgetFeeds.stream()
				.filter(gt -> !wIthConflictingNames.contains(gt))
				.collect(toSet());
		

		generateAdditionsTable(new ArrayList<>(additionsExt), columns("Source External Gadgets"));
		
	}
	
	void generateAddons() {

		List<AddonInfo> sourceAddons =  sourceUPMClient.getAddons().stream().filter(a -> a.isUserInstalled()).collect(toList());
		List<AddonInfo> targetAddons = targetUPMClient.getAddons().stream().filter(a -> a.isUserInstalled()).collect(toList());

		Set<AddonInfo> wIthConflictingNames = sourceAddons.stream().filter(entry -> {
			List<String> lowerNames = targetAddons.stream().map(e -> e.getKey().toLowerCase()).collect(toList());
			return lowerNames.contains(entry.getKey().toLowerCase());
		}).collect(toSet());
		
		MARKUP_BUILDER.sectionTitleLevel1("Add-ons");
		
		List<List<String>> addons = new ArrayList<List<String>>();
		sourceAddons.parallelStream().forEach(a -> {
			List<String> addon =  new ArrayList<String>();
			addon.add(a.getName());
			addon.add(a.getVersion());
			addon.add(String.valueOf(wIthConflictingNames.contains(a)));
			addon.add(wIthConflictingNames.contains(a)?targetAddons.parallelStream().filter(ta -> ta.getKey().equals(a.getKey())).findAny().get().getVersion():"N/A");
			addon.add(a.isUsesLicensing()?"PAID APP":"FREE APP");
			addon.add("");
			addons.add(addon);
		});
		
		generateAddonTable(addons, columns("Source Add-ons", "Source Version", "Available in Target", "Target Version", "Licence details", "Install in Target?"));
	}
	
	void generateAMSData() {
		macroToc();
		generateSpaces();
		generatePersonalSpaces();
		generateGlobalTemplates();
		generateExternalGadgetFeeds();
		generateExternalGadgetSpecs();
		generateBrowseShortcuts();
		generateUserMacro();
		generateWhitelist();
		generateConfluenceSettings();
		generateAddons();
		System.out.println(MARKUP_BUILDER.toString());
	}

	public static void main(String[] args) {
		new ConfluenceAMSGenarator().generateAMSData();

	}
	
}
