package com.botronsoft.pso.migrations.confluence.samples;



import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;

/**
 * Updates URLs pointing to SOURCE CONFLUENCE data to point to migrated data on TARGET
 * 
 */
class UpdateLinks {

	static final Logger log = LoggerFactory.getLogger(UpdateLinks.class);

	public static void main(String[] args) {
		String mappingFolder = Env.TARGET_CONF_ENV.getProperty("import") + "/mapping";
        ConfluenceEnvironment targetEnv = Environments.confluence(Env.TARGET);
        targetEnv.getFileManagerClient().upload(mappingFolder, new File(new UpdateLinks().
						getClass().getClassLoader().getResource("mappingFile.csv").getFile()));
        
//        targetEnv.getCMCClient().reportLinks("C:\\Users\\User\\git\\psoeng-template\\confluence\\src\\main\\resources\\mappingFile.csv",
//        		"C:\\Users\\User\\git\\psoeng-template\\confluence\\src\\main\\resources\\gen");
        
        System.out.println(mappingFolder + "/mappingFile.csv");
        targetEnv.getCMCClient().reportLinks(mappingFolder + "/mappingFile.csv",
        		Env.TARGET_CONF_ENV.getProperty("import"));
        
//        targetEnv.getCMCClient().bulkUpdateLinks(mappingFolder + "/mappingFile.csv",
//        		Env.TARGET_CONF_ENV.getProperty("import"));
	}

}

