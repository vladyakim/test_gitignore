package com.botronsoft.pso.migrations.confluence.samples;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.confluence.cmc.rest.model.SpaceInfo;
import com.botronsoft.pso.confluence.cmc.rest.model.bulk.BulkSpaceRenameInput;
import com.botronsoft.pso.confluence.cmc.rest.model.bulk.BulkSpaceRenameInput.SpaceNameBean;
import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * RUN ONLY AFTER ALL SPACES WERE IMPORTED INTO TARGET Renames spaces, new names
 * are taken from resource file renameSpaces.txt
 */

class RenameSpaces {

	static final Logger log = LoggerFactory.getLogger(RenameSpaces.class);

	/**
	 * This method builds input for rename spaces
	 */

	public static BulkSpaceRenameInput buildRenameInput(ConfluenceEnvironment sourceEnv, MapperReader reader) {
		BulkSpaceRenameInput input = new BulkSpaceRenameInput();
		input.setNumberOfThreads(1);
		input.setSpaceNamesMapping(getSpaceNameMap(sourceEnv, ObjectTypeKeys.SPACE_NAME, Env.LOCAL_ENV.workdir(), reader));
		log.info(input.toString());
		return input;
	}

	private static Map<String, SpaceNameBean> getSpaceNameMap(ConfluenceEnvironment sourceEnv, String type, File workDir, MapperReader reader) {
		Map<String, SpaceNameBean> bean = new HashMap<String, SpaceNameBean>();
		System.out.println(getAllSpaceInfo(sourceEnv, workDir));
		getAllSpaceInfo(sourceEnv, workDir).stream().filter(sp -> reader.map(type, sp.getName()).isPresent() && !reader.map(type, sp.getName()).get().equals(sp.getName()))
				.forEach(space -> {
					SpaceNameBean spaceBean = new SpaceNameBean();
					spaceBean.setName(space.getName());
					spaceBean.setNewName(reader.map(type, space.getName()).get());
					Optional<String> newKey = reader.map(ObjectTypeKeys.SPACE_KEY, space.getKey());
					if (newKey.isPresent())
						bean.put(newKey.get(), spaceBean);
					else {
						bean.put(space.getKey(), spaceBean);
					}
				});
		return bean;
	}

	private static Collection<SpaceInfo> getAllSpaceInfo(ConfluenceEnvironment sourceEnv, File workDir) {
		File file = new File(workDir, "all-spaces-" + sourceEnv.getKey().toUpperCase() + ".txt");
		Collection<SpaceInfo> loaded = null;
		try (FileReader jsonReader = new FileReader(file)) {
			loaded = new GsonBuilder().create().fromJson(jsonReader, new TypeToken<Collection<SpaceInfo>>() {
			}.getType());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return loaded;

	}

// EXECUTE THE RENAME OPERATION
	public static void main(String[] args) throws IOException {
		Environments.confluence(Env.SOURCE_INSTANCES).stream().forEach(env -> {
			MappingFactory factory = new FileSystemMappingFactory(env.getKey());
			MapperReader reader = factory.createReader();
		Environments.confluence(Env.TARGET).getCMCClient().bulkRenameSpaces(buildRenameInput(env,reader));
		});
	}
}
