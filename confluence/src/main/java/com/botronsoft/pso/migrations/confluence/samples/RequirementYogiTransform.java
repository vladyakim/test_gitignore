package com.botronsoft.pso.migrations.confluence.samples;

import java.io.File;
import java.util.Optional;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.confluence.cmc.model.requirements.RequirementRoot;
import com.botronsoft.pso.confluence.cmc.requirementyogi.RequirementsModelServiceImpl;
import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;

public class RequirementYogiTransform {

	public static void exportRequirements(ConfluenceEnvironment env) {
		String outfile = env.getProperty("export") + "/yogi/requirements_yogi.xml";
		System.out.println("Exporting requirements from " + env.getName() + " to " + outfile);
		env.getCMCRequirementYogiClient().exportRequirements(outfile);
		env.getFileManagerClient().downloadFile(outfile,
				Environments.local().workfile("requirements_yogi.xml"));
	}

	public static void importRequirements(ConfluenceEnvironment env) {
		String outfile = env.getProperty("export") + "/yogi/requirements_yogi.xml";
		env.getFileManagerClient().upload(outfile, Environments.local().workfile("requirements_yogi.xml"));
		env.getCMCRequirementYogiClient().importRequirements(outfile);
	}

	public static void transformRequirements(ConfluenceEnvironment env) {
		/**
		 * Transform requirements
		 */
		File inFile = Environments.local().workfile("requirements_yogi.xml");
		RequirementsModelServiceImpl modelService = new RequirementsModelServiceImpl();
		RequirementRoot root = modelService.loadModel(inFile);

		MappingFactory factory = new FileSystemMappingFactory(env.getKey());
		MapperReader reader = factory.createReader();

		root.getSpaces().forEach(space -> {
			// Update space keys
			if (space.getKey() != null) {
				Optional<String> newKey = reader.map(ObjectTypeKeys.SPACE_KEY, space.getKey());
				if (newKey.isPresent() && !newKey.equals(space.getKey())) {
					System.out.println(space.getNativeId() + ":" + space.getKey() + " -> " + newKey);
					space.setKey(newKey.get());
				}
			} else {
				System.out.println("Space is not migrated " + space.getKey());
				space.setKey(null);
			}
		});
		// jira appLink and project update
		root.getRequirements().forEach(requirement -> {
			requirement.getLink().forEach(link -> {
				if (link.getApplink() != null) {
					Optional<String> newAppLink = reader.map(ObjectTypeKeys.APPLICATION_LINK, link.getApplink());
					if (newAppLink.isPresent() && !(newAppLink.equals(link.getApplink()))) {
						System.out.println(link.getNativeId() + ":" + link.getApplink() + " -> " + newAppLink);
						link.setApplink(newAppLink.get());
					}
				} else {
					System.out.println("Application is not migrated " + link.getApplink());
				}
			});
			if ((requirement.getHtmlException() != null)
					&& requirement.getHtmlException().contains("https://support.share-XXX.com")) {
				String htmlException = requirement.getHtmlException();
				requirement.setHtmlException(
						htmlException.replaceAll("https://support.share-XXX.com", "https://qa.target.com"));
			}
			requirement.getProperty().forEach(property -> {
				if ((property.getValueu() != null) && property.getValueu().contains("https://support.share-XXXX.com")) {
					String valueu = property.getValueu();
					property.setValueu(valueu.replaceAll("https://support.share-XXX.com", "https://qa.target.com"));
				}
			});
		});

		root.getUsers().forEach(user -> {
			// Update users
			if (user.getName() != null) {
				Optional<String> newName = reader.map(ObjectTypeKeys.USER_NAME, user.getName().toLowerCase());
				if (newName.isPresent() && !(newName.equals(user.getName().toLowerCase()))) {
					System.out.println("User:" + user.getName() + " -> " + newName);
					user.setName(newName.get());
				}
			}

			if (user.getKey() != null) {
				Optional<String> newKey = reader.map(ObjectTypeKeys.USER_KEY, user.getKey());
				if (newKey.isPresent() && !newKey.equals(user.getKey())) {
					System.out.println("User:" + user.getKey() + " -> " + newKey);
					user.setKey(newKey.get());
				}
			}
		});
		File outFile = new File(inFile.getParentFile(), "requirements_yogi_transformed.xml");
		System.out.println("Saving transformed model to " + outFile);
		modelService.storeModel(root, outFile);
	}

	public static void main(String[] args) {

		// Environments.confluence(Env.SOURCE).parallelStream().forEach(env -> {
		// RequirementYogiTransform.exportRequirements(env);
		// });

		// Environments.confluence(Env.SOURCE).parallelStream().forEach(env -> {
		// RequirementYogiTransform.transformRequirements(env);
		// });

		RequirementYogiTransform.importRequirements(Environments.confluence(Env.TARGET));
	}
}