package com.botronsoft.pso.migrations.confluence.samples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.confluence.cmc.rest.client.CMCRestClient;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;

/**
 * EXPORT SPACES
 */
class ExportSource {
	
	static final Logger log = LoggerFactory.getLogger(ExportSource.class);

	/**
	 * Exports the spaces from the source instances
	 *
	 * @param inKeys space keys that will be exported. If null all source spaces will be exported.
	 * @param excluded space keys that will be excluded from exported spaces
	 * @param numberthreads number of threads used for exporting spaces 
	 * @param tempFolder confluence downloadPath
	 * @param parentCMCFolder cmc folder created in the "source instance"
	 * 
	 */
	
	public static void startExportINITIAL(List<String> inKeys, List excluded, int numberthreads, String tempFolder,
			String parentCMCFolder) {

		Environments.confluence(Env.SOURCE_INSTANCES).parallelStream().forEach(env -> {
			CMCRestClient client = env.getCMCClient();
			client.exportSpaces(inKeys, excluded, numberthreads, tempFolder, parentCMCFolder);
			
		});
	}

	public static void main(String[] args) {
		ExportSource.startExportINITIAL(new ArrayList(), new ArrayList(), 10,  "/var/atlassian/application-data/confluence/temp", "/var/atlassian/application-data/confluence");
	}
}
