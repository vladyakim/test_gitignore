package com.botronsoft.pso.migrations.confluence.samples;

import java.io.IOException;

import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;

public class ControllerImport {

	/**
	 * Start importing spaces 
	 * 
	 * @param env confluence instance
	 * 
	 */
	public static void startImport(ConfluenceEnvironment env) {
		env.getCMCClient().startImportService(env.getProperty("import"));
	}

	
	public static void main(String[] args) throws IOException {

		
		
		
		ControllerImport.startImport(Environments.confluence(Env.TARGET));
	}
}
