package com.botronsoft.pso.migrations.confluence.samples;

import java.util.List;

import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.rest.client.ConfluenceClient;
import com.botronsoft.pso.rest.client.refined.ConfluenceRefinedClient;
import com.google.gson.Gson;

import io.restassured.mapper.ObjectMapper;
import io.restassured.path.json.JsonPath;

class UpdateRefinedAddon {

	private static void updateCategories() {

		ConfluenceRefinedClient client = Environments.confluence(Env.TARGET).getConfluenceRefinedClient();
		Environments.confluence(Env.SOURCE_INSTANCES).stream().forEach(env -> {
			List<String> categoryKeys = env.getConfluenceRefinedClient().getCategoriesBuSiteId(0);
			categoryKeys.stream().forEach(key -> {
				JsonPath json = env.getConfluenceRefinedClient().getRefinedCategory(key);
				client.setRefinedCategory(key,
						JsonPath.from(json.toString().replaceAll("\"siteId\":0", "\"siteId\":1")).get());
			});
		});

	}

	public static void main(String[] args) {
			updateCategories();
	}

}
