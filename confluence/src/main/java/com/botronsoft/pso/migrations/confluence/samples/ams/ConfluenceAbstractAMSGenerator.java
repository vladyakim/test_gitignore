package com.botronsoft.pso.migrations.confluence.samples.ams;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.botronsoft.pso.migrations.AbstractAMSGenerator;

import io.github.swagger2markup.markup.builder.MarkupTableColumn;

/**
 * Sample confluence AMS generator
 *
 */
public abstract class ConfluenceAbstractAMSGenerator extends AbstractAMSGenerator {

	protected void generateAdditionsTable(Map<String, String> additions, List<MarkupTableColumn> tableHeaders) {
		if (additions.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}

		List<List<String>> cells = new TreeMap<>(additions).entrySet().stream().map(entry -> {
			if (!entry.getValue().equalsIgnoreCase("")) {
				return Arrays.asList(entry.getKey(), entry.getValue());
			}
			return Arrays.asList(entry.getKey());
		}).collect(toList());
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}
	
	protected void generateAdditionsTable(List<String> additions, List<MarkupTableColumn> tableHeaders) {
		if (additions.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}
		List<List<String>> cells = additions.stream().map(entry -> {
			return Arrays.asList(entry);
		}).collect(toList());
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}

	protected void generateMatchesKeysTable(Map<String, String> conflictingKeys, List<MarkupTableColumn> tableHeaders, String reason,
			String resolution) {
		if (conflictingKeys.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}
		List<List<String>> cells = new TreeMap<>(conflictingKeys).entrySet().stream()
				.map(entry -> Arrays.asList(entry.getKey(), entry.getValue(), reason, resolution)).collect(toList());
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}

	protected void generateMatchesNamesTable(Map<String, String> conflictingKeys, List<MarkupTableColumn> tableHeaders,
			Map<String, String> targetProjects, String resolution) {
		if (conflictingKeys.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}
		List<List<String>> cells = new TreeMap<>(conflictingKeys).entrySet().stream().map(entry -> {
			return Arrays.asList(entry.getKey(), entry.getValue(),
					String.format("Target Space with key *%s* with same name *%s*",
							targetProjects.entrySet().stream().filter(project -> project.getValue().equalsIgnoreCase(entry.getValue()))
									.findFirst().get().getKey(),
							targetProjects.entrySet().stream().filter(project -> project.getValue().equalsIgnoreCase(entry.getValue()))
									.findFirst().get().getValue()),
					resolution);
		}).collect(toList());
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}

	protected void generatePersonalSpacesConflictsTable(Map<String, List<String>> conflictingKeys, List<MarkupTableColumn> tableHeaders,
			String resolution) {
		if (conflictingKeys.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}
		List<List<String>> cells = new TreeMap<>(conflictingKeys).entrySet().stream().map(entry -> {
			return Arrays.asList(entry.getKey(), String.join("\\\\", entry.getValue()), resolution);
		}).collect(toList());
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}
	
	protected void generateConflictsTable(Map<String, Map<String,String>> conflicts, List<MarkupTableColumn> tableHeaders,
			String resolution) {
		if (conflicts.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}
		List<List<String>> cells = new ArrayList<List<String>>();
		conflicts.forEach((k,v)->{
			List<String> values = new ArrayList<String>();
			values.add(k);
			v.forEach((t,p) -> {
				values.add(t);
				values.add(p);
				values.add(resolution);
			});
			cells.add(values);
		});
				
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}
	
	protected void generateConflictsTable(List<String> conflicts, List<MarkupTableColumn> tableHeaders,
			String resolution) {
		if (conflicts.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}
		List<List<String>> cells = new ArrayList<List<String>>();
		conflicts.forEach( k ->{
			List<String> values = new ArrayList<String>();
			values.add(k);
			values.add(resolution);
			cells.add(values);
		});
				
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}
	
	protected void generateAddonTable(List<List<String>> conflicts, List<MarkupTableColumn> tableHeaders) {
		if (conflicts.isEmpty()) {
			MARKUP_BUILDER.boldTextLine("N/A");
			return;
		}
		List<List<String>> cells = new ArrayList<List<String>>();
		conflicts.forEach( k ->{
			List<String> values = new ArrayList<String>();
			values.addAll(k);
			cells.add(values);
		});
				
		MARKUP_BUILDER.tableWithColumnSpecs(tableHeaders, cells);
	}
}
