package com.botronsoft.pso.migrations.confluence.samples;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MapperWriter;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.confluence.cmc.rest.model.SpaceShortcuts;
import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
import com.botronsoft.pso.confluence.cmc.rest.model.SpaceSidebarLinkBean;

import java.util.ArrayList;
import java.util.Collection;

class CopySpaceShortcuts {

	static final Logger log = LoggerFactory.getLogger(CopySpaceShortcuts.class);

	/**
	 * Exports, transforms and imports space shortcuts
	 * 
	 * @param env confluence "source" instance
	 * 
	 */
	static void copyShortcuts(ConfluenceEnvironment env) {
		MappingFactory factory = new FileSystemMappingFactory(env.getKey());
		MapperReader reader = factory.createReader();
		List<String> spaces =  new ArrayList<String>( reader.map(ObjectTypeKeys.MIGRATED_SPACE_KEY).keySet());

		SpaceShortcuts shortcuts = env.getCMCClient().exportShortcuts(spaces);
		// process data
		// 1. update space keys - spaceKey and destSpaceKey
		// 2. update hardcodedUrl for type=EXTERNAL_LINK
		Collection<SpaceSidebarLinkBean> datas = (Collection<SpaceSidebarLinkBean>) shortcuts.getData();
		for (Object o : datas) {
			SpaceSidebarLinkBean shortcut = (SpaceSidebarLinkBean) o;
			if (spaces.contains(shortcut.getSpaceKey().toLowerCase())) {
				Optional<String> newSpaceKey = reader.map(ObjectTypeKeys.SPACE_KEY, shortcut.getSpaceKey());
				if (newSpaceKey.isPresent() && newSpaceKey.get() != shortcut.getSpaceKey()) {
					log.info("Replacing 'spaceKey' " + shortcut.getSpaceKey() + " with " + newSpaceKey.get());
					shortcut.setSpaceKey(newSpaceKey.get());
				}
			}
			if (shortcut.getDestSpaceKey() != null && spaces.contains(shortcut.getDestSpaceKey().toLowerCase())) {
				Optional<String> newDestSpaceKey = reader.map(ObjectTypeKeys.SPACE_KEY, shortcut.getDestSpaceKey());
				if (newDestSpaceKey.isPresent() && newDestSpaceKey.get() != shortcut.getDestSpaceKey()) {
					log.info("Replacing 'destSpaceKey' " + shortcut.getDestSpaceKey() + " with " + newDestSpaceKey.get());
					shortcut.setDestSpaceKey(newDestSpaceKey.get());
				}
			}
			if (shortcut.getDestPageTitle() != null) {
				shortcut.setDestPageTitle(
						shortcut.getDestPageTitle().replaceAll("\"", "").replace("\u201c", "").replace("\u201d", "")
								.replace("\u2019", "'").replace("\u2013", "-").replace('–', '-').replace("\\\\", "\\"));
			}
		}
		shortcuts.getDataMainLinks().stream().forEach(mainLink -> {
			if (spaces.contains(mainLink.getSpaceKey())) {
				Optional<String> newSpaceKey = reader.map(ObjectTypeKeys.SPACE_KEY, mainLink.getSpaceKey());
				if (newSpaceKey.isPresent() && newSpaceKey.get() != mainLink.getSpaceKey()) {
					log.info("Replacing 'spaceKey' " + mainLink.getSpaceKey() + " with " + newSpaceKey.get());
					mainLink.setSpaceKey(newSpaceKey.get());
				}
			}
		});

		Environments.confluence(Env.TARGET).getCMCClient().importShortcuts(shortcuts);
	}

//Migrate ONE by ONE
	public static void main(String[] args) {
		Environments.confluence(Env.SOURCE_INSTANCES).stream().forEach(env -> {
			
			copyShortcuts(env);
		});
	}
}
