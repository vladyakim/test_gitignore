package com.botronsoft.pso.migrations.confluence.samples;

import static java.util.stream.Collectors.toMap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MapperWriter;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.confluence.cmc.reporting.model.AttachmentInfo;
import com.botronsoft.pso.confluence.cmc.reporting.model.CommentInfo;
import com.botronsoft.pso.confluence.cmc.reporting.model.PageInfo;
import com.botronsoft.pso.confluence.cmc.reporting.model.TemplateInfo;
import com.botronsoft.pso.confluence.cmc.utils.CsvSerializer;
import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
import com.botronsoft.pso.migrations.links.MappingConverter;

public class LinkUpdaterMappingGenerator {
	private static final Logger log = LoggerFactory.getLogger(LinkUpdaterMappingGenerator.class);

	class MappedInfo {
		String scrId;
		String dstId;

		public String getScrId() {
			return scrId;
		}

		public void setScrId(String scrId) {
			this.scrId = scrId;
		}

		public String getDstId() {
			return dstId;
		}

		public void setDstId(String dstId) {
			this.dstId = dstId;
		}
	}

	static void dumpIntoLocalFiles() throws Exception {

		File outputDir = Environment.LOCAL.getFile("linksupdater");
		outputDir.mkdir();
		File inputFolder = new File(outputDir, "input");
		if (!inputFolder.exists()) {
			inputFolder.getAbsoluteFile().mkdirs();
		}
		Environments.confluence(Env.SOURCE_INSTANCES).parallelStream().forEach(env -> {
			try {
				log.info("Dumping pages info from {}", env.getName());
				env.getCMCReportingClient()
						.collectPageInfo(new File(outputDir, "/input/" + env.getName() + "_Pages.csv.gz"));
				log.info("Dumping attachments info from {}", env.getName());
				env.getCMCReportingClient()
						.collectAttachmentInfo(new File(outputDir, "/input/" + env.getName() + "_Attachments.csv.gz"));
				log.info("Dumping blogposts info from {}", env.getName());
				env.getCMCReportingClient()
						.collectBlogpostInfo(new File(outputDir, "/input/" + env.getName() + "_Blogposts.csv.gz"));
				log.info("Dumping comments info from {}", env.getName());
				env.getCMCReportingClient()
						.collectCommentInfo(new File(outputDir, "/input/" + env.getName() + "_Comments.csv.gz"));
				log.info("Dumping templates info from {}", env.getName());
				env.getCMCReportingClient()
						.collectTemplateInfo(new File(outputDir, "/input/" + env.getName() + "_Templates.csv.gz"));
			} catch (Throwable t) {
				log.error("Error in dump from " + env.getName(), t);
			}
		});

	}

	static void dumpIntoTargetLocalFiles() throws Exception {
		File outputDir = Environment.LOCAL.getFile("linksupdater");
		outputDir.mkdir();
		ConfluenceEnvironment env = Environments.confluence(Env.TARGET);
		try {
			log.info("Dumping pages info from {}", env.getName());
			env.getCMCReportingClient()
					.collectPageInfo(new File(outputDir, "/input/" + env.getName() + "_Pages.csv.gz"));
			log.info("Dumping attachments info from {}", env.getName());
			env.getCMCReportingClient()
					.collectAttachmentInfo(new File(outputDir, "/input/" + env.getName() + "_Attachments.csv.gz"));
			log.info("Dumping blogposts info from {}", env.getName());
			env.getCMCReportingClient()
					.collectBlogpostInfo(new File(outputDir, "/input/" + env.getName() + "_Blogposts.csv.gz"));
			log.info("Dumping comments info from {}", env.getName());
			env.getCMCReportingClient()
					.collectCommentInfo(new File(outputDir, "/input/" + env.getName() + "_Comments.csv.gz"));
			log.info("Dumping templates info from {}", env.getName());
			env.getCMCReportingClient()
					.collectTemplateInfo(new File(outputDir, "/input/" + env.getName() + "_Templates.csv.gz"));
		} catch (Throwable t) {
			log.error("Error in dump from " + env.getName(), t);
		}

	}

	public static void generateMappingCsvFiles() throws IOException {
		File input = Environment.LOCAL.getFile("linksupdater");
		File inputFolder = new File(input, "input");
		if (!inputFolder.exists()) {
			inputFolder.getAbsoluteFile().mkdirs();
		}
		File mappingErrorsReport = new File(input + "/missing_mappings.csv");
		Map<String, String> mappingErrors = new HashMap<String, String>();

		Collection<ConfluenceEnvironment> list = Environments.confluence(Env.SOURCE_INSTANCES);
		ConfluenceEnvironment envT = Environments.confluence(Env.TARGET);

		List<PageInfo> targetPages = CsvSerializer.listFromCsv(PageInfo.class,
				new File(input, "input/" + envT.getName() + "_Pages.csv.gz"));

		List<AttachmentInfo> targetAttachments = CsvSerializer.listFromCsv(AttachmentInfo.class,
				new File(input, "input/" + envT.getName() + "_Attachments.csv.gz"));

		List<CommentInfo> targetComments = CsvSerializer.listFromCsv(CommentInfo.class,
				new File(input, "input/" + envT.getName() + "_Comments.csv.gz"));

		List<PageInfo> targetBlogposts = CsvSerializer.listFromCsv(PageInfo.class,
				new File(input, "input/" + envT.getName() + "_Blogposts.csv.gz"));

		List<TemplateInfo> targetTemplates = CsvSerializer.listFromCsv(TemplateInfo.class,
				new File(input, "input/" + envT.getName() + "_Templates.csv.gz"));

		Map<String, PageInfo> targetPageMap = targetPages.parallelStream()
				.collect(toMap(PageInfo::getKey, Function.identity(), (value1, value2) -> {
					return value1;
				}));
		targetPages = null;

		Map<String, AttachmentInfo> targetAttachmentsMap = targetAttachments.parallelStream()
				.collect(toMap(AttachmentInfo::getKey, Function.identity(), (value1, value2) -> {
					return value1;
				}));
		targetAttachments = null;

		Map<String, CommentInfo> targetCommentsMap = targetComments.parallelStream()
				.collect(toMap(CommentInfo::getKey, Function.identity(), (value1, value2) -> {
					return value1;
				}));
		targetComments = null;

		Map<String, PageInfo> targetBlogpostsMap = targetBlogposts.parallelStream()
				.collect(toMap(PageInfo::getKey, Function.identity(), (value1, value2) -> {
					return value1;
				}));
		targetBlogposts = null;

		Map<String, TemplateInfo> targetTemplatesMap = targetTemplates.parallelStream()
				.collect(toMap(TemplateInfo::getKey, Function.identity(), (value1, value2) -> {
					return value1;
				}));
		targetTemplates = null;

		list.stream().forEach(env -> {
			MappingFactory factory = new FileSystemMappingFactory(env.getKey());
			MapperWriter writer = factory.createWriter();
			MapperReader reader = factory.createReader();

			List<PageInfo> srcPages = CsvSerializer.listFromCsv(PageInfo.class,
					new File(input, "input/" + env.getName() + "_Pages.csv.gz"));

			log.info("Mapping generation PAGE for instance {} started!", env.getName());

			Map<Long, Long> pagesMap = new HashMap<Long, Long>();
			srcPages.forEach(i -> {
				// return getVersion() + getSpaceKey() + getTitle() + getCreator() +
				// getCreationDate();
				String newSpaceKey = i.getSpaceKey();
				Optional<String> spaceMap = reader.map(ObjectTypeKeys.SPACE_KEY, i.getSpaceKey());
				if (spaceMap.isPresent()) {
					newSpaceKey = spaceMap.get();
				}
				String newUserName = i.getCreator();
				Optional<String> userNameMap = reader.map(ObjectTypeKeys.USER_NAME, i.getCreator());
				if (userNameMap.isPresent()) {
					newUserName = spaceMap.get();
				}
				PageInfo pageInfo = targetPageMap.get(i.getVersion() + newSpaceKey
						+ i.getTitle().trim().replace("Can?t", "Can't").replace(" ? ", " - ") + newUserName
						+ i.getCreationDate());
				if (pageInfo == null) {
					pageInfo = targetPageMap.get((i.getVersion() - 1) + newSpaceKey
							+ i.getTitle().trim().replace("Can?t", "Can't").replace(" ? ", " - ") + newUserName
							+ i.getCreationDate());
				}
				if (pageInfo == null) {
					String errorMsg = String.format("Mapping not found for page: %s", i.getKey());
					mappingErrors.put(errorMsg, "page");
				} else {
					pagesMap.put(i.getId(), pageInfo.getId());
					writer.writeMap(ObjectTypeKeys.PAGE_ID, i.getId().toString(), pageInfo.getId().toString());
				}
			});

			log.info("Mapping generation PAGE for instance {} DONE!", env.getName());

			List<PageInfo> srcBlogs = CsvSerializer.listFromCsv(PageInfo.class,
					new File(input, "input/" + env.getName() + "_Blogposts.csv.gz"));

			log.info("Mapping generation BLOGPOSTS for instance {} started!", env.getName());

			Map<Long, Long> blogsMap = new HashMap<Long, Long>();

			srcBlogs.forEach(i -> {
				// return getVersion() + getSpaceKey() + getTitle() + getCreator() +
				// getCreationDate();
				String newSpaceKey = i.getSpaceKey();
				Optional<String> spaceMap = reader.map(ObjectTypeKeys.SPACE_KEY, i.getSpaceKey());
				if (spaceMap.isPresent()) {
					newSpaceKey = spaceMap.get();
				}
				String newUserName = i.getCreator();
				Optional<String> userNameMap = reader.map(ObjectTypeKeys.USER_NAME, i.getCreator());
				if (userNameMap.isPresent()) {
					newUserName = spaceMap.get();
				}
				PageInfo pageInfo = targetBlogpostsMap
						.get(i.getVersion() + newSpaceKey + i.getTitle().trim() + newUserName + i.getCreationDate());
				if (pageInfo == null) {
					String errorMsg = String.format("Mapping not found for blog: %s", i.getKey());
					mappingErrors.put(errorMsg, "blogpost");
				} else {
					blogsMap.put(i.getId(), pageInfo.getId());
					writer.writeMap(ObjectTypeKeys.BLOGPOST_ID, i.getId().toString(), pageInfo.getId().toString());
				}
			});

			log.info("Mapping generation BLOGPOSTS for instance {} DONE!", env.getName());

			log.info("Mapping generation ATTACHMENTS for instance {} started!", env.getName());

			Map<Long, Long> attachmentsMap = new HashMap<Long, Long>();

			List<AttachmentInfo> srcAttachments = CsvSerializer.listFromCsv(AttachmentInfo.class,
					new File(input, "input/" + env.getName() + "_Attachments.csv.gz"));

			srcAttachments.forEach(a -> {
				AttachmentInfo attachmentInfo = null;
				PageInfo info = srcPages.stream().filter(p -> p.getId().equals(a.getPageId())).findFirst().orElse(null);
				if (info == null) {
					info = srcBlogs.stream().filter(p -> p.getId().equals(a.getPageId())).findFirst().orElse(null);
				}
				if ((info != null) && !info.getSpaceKey().equals("rwlayoutkey")) {
					Long newPageId = pagesMap.get(a.getPageId());
					if (newPageId == null) {
						newPageId = blogsMap.get(a.getPageId());
					}
					if (newPageId == null) {
						String errorMsg = String.format("Cannot find migrated page: %s ", a.getPageId());
						mappingErrors.put(errorMsg, "attachment");
					} else {
						// getPageId() + getTitle() + getCreationDate();
						String scrAttachmentMappingKey = newPageId + a.getTitle() + a.getCreationDate();
						attachmentInfo = targetAttachmentsMap.get(scrAttachmentMappingKey);
					}

					if (attachmentInfo != null) {
						attachmentsMap.put(a.getId(), attachmentInfo.getId());
						writer.writeMap(ObjectTypeKeys.ATTACHMENT_ID, a.getId().toString(),
								attachmentInfo.getId().toString());
					} else {
						String errorMsg = String.format(
								"No attachment found on TARGET for source page : %s with title : %s", a.getPageId(),
								a.getTitle());
						mappingErrors.put(errorMsg, "attachment");
						// log.warn(errorMsg);

					}
				}
			});

			log.info("Mapping generation ATTACHMENTS for instance {} DONE!", env.getName());

			log.info("Mapping generation COMMENTS for instance {} started!", env.getName());

			Map<Long, Long> commentsMap = new HashMap<Long, Long>();

			List<CommentInfo> srcComments = CsvSerializer.listFromCsv(CommentInfo.class,
					new File(input, "input/" + env.getName() + "_Comments.csv.gz"));
			// return getPageId() + getCreator() + getCreationDate() + getVersion();
			srcComments.forEach(c -> {
				CommentInfo commentInfo = null;
				String newUserName = c.getCreator();
				Optional<String> userName = reader.map(ObjectTypeKeys.USER_NAME, c.getCreator());
				if (userName.isPresent()) {
					newUserName = userName.get();
				}

				Long newPageId = pagesMap.get(c.getPageId());
				if (newPageId == null) {
					newPageId = blogsMap.get(c.getPageId());
				}
				if (newPageId == null) {
					newPageId = attachmentsMap.get(c.getPageId());
				}
				if (newPageId == null) {
					String errorMsg = String.format("Cannot find migrated page: %s", c.getPageId());
					mappingErrors.put(errorMsg, "comment");
				} else {
					String scrCommentMappingKey = newPageId + newUserName + c.getCreationDate() + c.getVersion();
					if (scrCommentMappingKey.contains("16 Jan 2019 20:53:12 GMT")) {
						System.out.println(scrCommentMappingKey);
					}
					commentInfo = targetCommentsMap.get(scrCommentMappingKey);
				}
				if (commentInfo != null) {
					commentsMap.put(c.getId(), commentInfo.getId());
					writer.writeMap(ObjectTypeKeys.COMMENT_ID, c.getId().toString(), commentInfo.getId().toString());
				} else {
					String errorMsg = String.format("Missing comment on TARGET with id: %s", c.getId());
					// log.warn(errorMsg);
					mappingErrors.put(errorMsg, "comment");
				}
			});

			log.info("Mapping generation COMMENTS for instance {} DONE!", env.getName());

			log.info("Mapping generation TEMPLATES for instance {} started!", env.getName());

			Map<Long, Long> templatesMap = new HashMap<Long, Long>();

			List<TemplateInfo> srcTemplates = CsvSerializer.listFromCsv(TemplateInfo.class,
					new File(input, "input/" + env.getName() + "_Templates.csv.gz"));
			// getVersion() + getSpaceKey() + getPluginKey()
			srcTemplates.forEach(t -> {
				TemplateInfo templateInfo = null;
				String newSpaceKey = t.getSpaceKey();
				Optional<String> spaceMap = reader.map(ObjectTypeKeys.SPACE_KEY, t.getSpaceKey());
				if (spaceMap.isPresent()) {
					newSpaceKey = spaceMap.get();
				}
				String scrTemplateMappingKey = t.getTemplateName() + t.getVersion() + newSpaceKey + t.getPluginKey();
				templateInfo = targetTemplatesMap.get(scrTemplateMappingKey);

				if (templateInfo != null) {
					templatesMap.put(t.getId(), templateInfo.getId());
					writer.writeMap(ObjectTypeKeys.TEMPLATE_ID, t.getId().toString(), templateInfo.getId().toString());
				} else {
					String errorMsg = String.format("Missing template on TARGET with id: %s", t.getId());
					// log.warn(errorMsg);
					mappingErrors.put(errorMsg, "template");
				}
			});

			log.info("Mapping generation TEMPLATES for instance {} DONE!", env.getName());

			pagesMap.clear();
			blogsMap.clear();
			attachmentsMap.clear();
			commentsMap.clear();
			templatesMap.clear();

			log.info("Mapping generation for instance {} complete!", env.getName());
		});

		writeMapToFile(mappingErrors, mappingErrorsReport);
		log.info("All mapping generation DONE!");
	}

	static void writeMapToFile(Map<?, ?> map, File output) {
		try (PrintWriter w = new PrintWriter(output)) {
			map.forEach((k, v) -> {
				w.println(k + ";" + v);
			});
		} catch (FileNotFoundException e) {
			log.error(e.getMessage());
		}
	}

	public static void main(String[] args) throws Exception {
		// dumps input data from all instances
		dumpIntoLocalFiles();
		dumpIntoTargetLocalFiles();
		generateMappingCsvFiles();

		MappingConverter converter = new MappingConverter();
		converter.convert(Env.SOURCE_CONF_ENV.getKey(), Environments.local().workdir() + "/link_mapping");
		Env.TARGET_CONF_ENV.getFileManagerClient().upload(
				Env.TARGET_CONF_ENV.getProperty("import") + "/mapping",
				new File(Environments.local().workdir(), "link_mapping"));
	
	}

}
