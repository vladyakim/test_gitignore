package com.botronsoft.pso.migrations.confluence.samples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.botronsoft.pso.commons.mapping.api.MapperReader;
import com.botronsoft.pso.commons.mapping.api.MappingFactory;
import com.botronsoft.pso.commons.mapping.impl.FileSystemMappingFactory;
import com.botronsoft.pso.migration.framework.environment.ConfluenceEnvironment;
import com.botronsoft.pso.migration.framework.environment.Environments;
import com.botronsoft.pso.migrations.Env;
import com.botronsoft.pso.migrations.ObjectTypeKeys;
/**
 * Exports, transforms and imports Comala Workflows
 * Must be executed after {@link LinkUpdaterMappingGenerator}
 * 
 */
public class ComalaWorkflowsTransform {

	public static void exportWorkflows(ConfluenceEnvironment env) {
		String outfile = env.getProperty("export") + "/comala/comala_workflows.json";
		System.out.println("Exporting global comala workflows from Digital to " + outfile);
		env.getCMCClient().exportComalaWorkflows(outfile);
		env.getFileManagerClient().downloadFile(outfile, Environments.local().workfile("comala_workflows.json"));
	}

	public static void importWorkflows(ConfluenceEnvironment env) {
		String outfile = env.getProperty("import") + "/comala/comala_workflows.json";
		env.getFileManagerClient().upload(env.getProperty("import") + "/comala",
				Environments.local().workfile("comala_workflows.json"));
		env.getCMCClient().importComalaWorkflows(outfile);
	}

	public static void updateWorkflowsProperties(MapperReader reader, ConfluenceEnvironment env) {
		Map<String, String> userKeys = reader.map(ObjectTypeKeys.USER_KEY).entrySet().stream()
				.collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().get(0)));
		List<String> spaces = new ArrayList<String>(reader.map(ObjectTypeKeys.MIGRATED_SPACE_KEY).keySet());
		Environments.confluence(Env.TARGET).getCMCClient().updateUserWorkflowContentProperty(userKeys, spaces,
				 env.getProperty("import") + "/mapping/conf_"+env.getKey()+"attachments.csv");

	}

	public static void main(String[] args) {

		Environments.confluence(Env.SOURCE_INSTANCES).parallelStream().forEach(env -> {
			ComalaWorkflowsTransform.exportWorkflows(env);
		});

		ComalaWorkflowsTransform.importWorkflows(Environments.confluence(Env.TARGET));

		Environments.confluence(Env.SOURCE_INSTANCES).parallelStream().forEach(env -> {
			MappingFactory factory = new FileSystemMappingFactory(env.getKey());
			MapperReader reader = factory.createReader();
			ComalaWorkflowsTransform.updateWorkflowsProperties(reader, env);
		});
	}
}