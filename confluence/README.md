# Engagement Template Repository #

Engagement repository provide one with the tooling necessary to migrate configuration and data from one Atlassian application to another.

Currently covered applications:

* Confluence

along with migration tests for each.

# Repository How-to #
## How is this repository organized? ##

 * **psoeng-parent** - Manages POM dependencies, organize repositories modules listed below
 * **psoeng-common** - describes Client applications, instances and app/instance specific properties
 * **psoeng-jira** - contains tooling necessary to execute JIRA migration
 * **psoeng-confluence** - contains tooling necessary to execute Confluence migration
 * **psoeng-tests** - contains application specific test suites used to verify successful migration.

## How do I get set up? ##

* **migration.properties** - file containing application specific properties such as URLs, usernames and passwords, paths to file/folders. File entries should follow below guidelines:

```
#format env.{appTypeKey}.{envKey}.{propName}
#appTypeKey - jira, confluence, bitbucket
#envKey - lower case alphanumeric string
#propName - lower case alphanumeric string
```

	
_Do not use the file to store and commit any user specific credentials_. Use **local.migration.properties** instead

* **local.migration.properties** - file is not included within the repository on purpose. Furthermore, it's part of the **.gitignore** content. The idea behind is not to commit any user specific properties such as local paths, credentials etc.
    * in order to create the file you can create a blank file or simply copy/paste migration.properties and save it as **local.migration.properties** or use **sample.local.migration.properties**
    * Any entry in **local.migration.properties** overwrites same one available in **migration.properties**


* **Env.java** - interface describing Client specific Atlassian application setup see javadoc within source for further details.

* **mappingFile.csv** - file containing information needed for URL replace. For more information is available **[How to use URL Replacer]("https://confluence.botronsoft.com/display/SER/How+to+use+URL+Replacer")**.

## What CONFLUENCE migration general flow is? ##
	    
######  Prerequisites 
Before you execute the next steps, ensure you have met the following requirements:
* You need to check source and target timezone as fix source timezone accordingly
* You have CMC plugin installed on all instances
* In case of restoring Confluence Cloud backup you might need to run `com.botronsoft.pso.migrations.confluence.cloud.utils.SiteExportXMLEditor.java` code reorders site export xml for user favorites (stored for later pages) to work properly. See [CONFCLOUD-36348](https://jira.atlassian.com/browse/CONFCLOUD-36348) for further details and if modification needed based on Confluence versions. 
    
    
*  **Generate AMS**
AMS document layout and content might defer from engagement to engagement due to instance and/or Customer specifics. Still, any previous CONFLUENCE AMS can be used as a starting point for preparing current engagement AMS and modify it according to the needs. Common addendum to the AMS is the Appendix section that list in details all conflicting and non-conflicting item, how conflict are handled and in general provide overview what the impact on TARGET instance will be.
   `com.botronsoft.pso.migrations.confluence.samples.ams.ConfluenceAMSGenarator` creates AMS appendix details with all new and conflicting items. Refer class java doc for further details
    
*  **User management**
Important step going further with actual migration execution is to agree how missing and/or conflicting users will be handled. Following must be done prior migration:
    * Export all SOURCE users to get list of those owning data, activity or setting within CONFLUENCE 
    * Export all TARGET users to get list of those owning data, activity or setting within CONFLUENCE 
    * Go through matching usernames whether those are same people or not (display name, email from export might be of a help doing so)
    * In case of different SOURCE/TARGET usernames but actually same people mapping must be confirmed
    * Agree on group membership whether source membership is preserved and should be applied on target
  
Goal is to come up with mapping of SOURCE to TARGET user and whether SOURCE users are to be created active or inactive. 

######  Prerequisites 
Before you execute the next steps, ensure you have met the following requirements:
* You have executed `com.botronsoft.pso.migrations.confluence.samples.GenerateMapping` with filled mapping, agreed with the Customer after AMS analysis.


*  **Manage users and prepare transformations** 
	`com.botronsoft.pso.migrations.confluence.samples.ControllerTranform` any source user should be available on target, including missing users. This step ensures all source users have a userKey in Target Confluence and produces a mapping file for each instance. This class also starts transformation session.
	
*  **Check multiple spaces**
   ```sql
   SELECT COUNT(*) FROM CONTENT
   JOIN CONTENT xpage ON CONTENT.PAGEID = xpage.CONTENTID
   WHERE CONTENT.SPACEID != xpage.SPACEID
   UNION ALL
   SELECT COUNT(*) FROM CONTENT
   JOIN CONTENT xpage ON CONTENT.PARENTID = xpage.CONTENTID
   WHERE CONTENT.SPACEID != xpage.SPACEID
   UNION ALL
   SELECT COUNT(*) FROM CONTENT JOIN CONTENT xpage ON CONTENT.PREVVER = xpage.CONTENTID
   WHERE CONTENT.SPACEID != xpage.SPACEID;
   ```
   If the query returns non-zero values, refer https://confluence.atlassian.com/confkb/importing-a-space-fails-with-could-not-execute-statement-error-in-confluence-server-949242294.html
   
*  **Export Spaces** 
	`com.botronsoft.pso.migrations.confluence.samples.ExportSource` exports spaces from source instances.
    
    
* **Import Spaces** 
	`com.botronsoft.pso.migrations.confluence.samples.ControllerImport` import spaces into target instances.


* **Rename Spaces** 
	`com.botronsoft.pso.migrations.confluence.samples.RenameSpaces` renames spaces, if any.


* **Import Space Shortcuts** 
	`com.botronsoft.pso.migrations.confluence.samples.CopySpaceShortcuts` exports, transform and import space shortcuts.

* **Import Global Settings** 
	`com.botronsoft.pso.migrations.confluence.samples.GlobalSettingsMigration` exports, transform and import global templates, browse shortcuts, whitelists, external gadgets and feeds.

* **Generate SOURCE object to TARGET object Mapping** 
	If JIRA instance is migrated in parallel with CONFLUENCE, JIRA deploy must be done before this step. If source and target confluence instances have different timezones we beed to adjust the below scipt to match dates. This class will upload mapping files on target instance.
	`com.botronsoft.pso.migrations.confluence.samples.LinkUpdaterMappingGenerator` 


* **Migrate Team Calendars Addon** 
	`com.botronsoft.pso.migrations.confluence.samples.TeamCalendarsTransform` exports, transforms and imports Team Calendars Addon.
	
######  Prerequisites 
Before you execute the next steps, ensure you have met the following requirements:
* You need to update `psoeng-confluence/src/main/resources/mappingFile.csv`

* **Update Macros** 
	`com.botronsoft.pso.migrations.confluence.samples.UpdateMacros` updates available macros.
	

* **Update User Preferences** 
	`com.botronsoft.pso.migrations.confluence.samples.UserPreferencesTransform` exports, transforms and imports user preferences.
	
	
*  **Update Links** `com.botronsoft.pso.migrations.confluence.samples.UpdateLinks` Once data is migrated over any URLs pointing to SOURCE CONFLUENCE data must be updated to point to migrated data on TARGET. Mapping files must be generated. 

	 
*  **Run tests** 
   Upon successful deploy of content **psoeng-tests** module under package `com.botronsoft.pso.migrations.tests.confluence` holds test to verify successful migration.
    * _TestSpaces.java_  compares space content details such as page titles whether matching on SOURCE and TARGET
    * _TestSpacePermissions.java_  compares space permissions whether matching on SOURCE and TARGET
    * _TestUserSettings.java_  compares user setting whether matching on SOURCE and TARGET
    * _TestMainSpaceShortcuts.java_  compares space shortcuts whether matching on SOURCE and TARGET


## Have any questions? ##

Drop us an email **PSOTeam@botronsoft.com**